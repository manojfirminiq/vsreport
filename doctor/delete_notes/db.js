const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_CONSULTATION = CONSTANTS.DOCTOR_CONSULTATION;
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;


let verifyHospital = async (hospitalId) =>{
    let paramsTable = {
        TableName : HOSPITAL_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalId
        }
    };
    const {Count, Items} = await dbClient.query(paramsTable).promise();
    if(Count <= 0){
        return false;
    }else{
         return process.env.STAGE + '_' + CONSTANTS.RPM + '_' + Items[0].dbIdentifier + `_`+ CONSTANTS.NOTES_TABLE;
    }
};

let getNotesStartDate = async (hospitalTable, doctorId, userID, start) =>{
     let paramsTable = {
        TableName : hospitalTable,
        KeyConditionExpression: "#doctor = :id AND #userID_start = :userID_start"  ,
        ExpressionAttributeNames:{
        "#doctor": "doctorID",
        "#userID_start": "userID_startDate"
        
        },
        ExpressionAttributeValues: {
        ":id": doctorId,
        ":userID_start": `${userID}_${start}`
        }
    };
    const {Count, Items} = await dbClient.query(paramsTable).promise();
    
    if(Count <= 0){
        console.log( "Count: ",Count);
        return false;
    }else{
        console.log( "Count: ",Items);
         return Items;
    }
};


const flagNotesForDelete = async (notes, hospitalTable) => {
    notes.modifiedDate = new Date().getTime();
    let params = {
        TableName: hospitalTable,
        Item: notes
    };
    return dbClient.put(params).promise();
};

module.exports.flagNotesForDelete = flagNotesForDelete;
module.exports.getNotesStartDate = getNotesStartDate;
module.exports.verifyHospital = verifyHospital;