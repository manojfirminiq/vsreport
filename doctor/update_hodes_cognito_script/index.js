const AWS = require("aws-sdk");
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

const getDoctors = async (nextPaginationKey) => {
    let params = {
        TableName : process.env.HODES_DOCTOR_TABLE
    };
    
    if(nextPaginationKey) {
        params.ExclusiveStartKey = nextPaginationKey;
    }
    
    return dbClient.scan(params).promise();
};

const updateDynamoDB = async(doctor) => {
    doctor.subscriptionFlag = doctor.subscriptionFlag ? doctor.subscriptionFlag : true;
    let params = {
        TableName: process.env.HODES_DOCTOR_TABLE,
        Item: doctor
    };
    return dbClient.put(params).promise();
}

const updateCognito = async(emailAddress, hospitalID) => {
    let params = {
        UserAttributes: [
            {
              Name: 'custom:hospitalID',
              Value: hospitalID
            }
        ],
        UserPoolId: process.env.COGNITO_POOL_ID,
        Username: emailAddress
    };
    try{
        let updateCognito = await cognitoidentityserviceprovider.adminUpdateUserAttributes(params).promise();
        console.log(updateCognito);
        return Promise.resolve();
    } catch(error) {
        return Promise.reject(error);
    }
};

exports.handler = async (event) => {
    let nextPaginationKey = null;
    
    do {
        let {Items, LastEvaluatedKey} = await getDoctors(nextPaginationKey);
        nextPaginationKey = LastEvaluatedKey;
        for(let doctor of Items) {
            await updateCognito(doctor.emailAddress, doctor.hospitalID);            
            await updateDynamoDB(doctor);
        }
    } while (nextPaginationKey);
    
    const response = {
        statusCode: 200,
        body: JSON.stringify({success: true}),
    };
    return response;
};
