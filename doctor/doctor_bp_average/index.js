const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');

const getBpAverage = async ({doctorId, userId, hospitalID}) => {
    try{
        if(!userId){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_ID_REQ.CODE});
        }
        if(!hospitalID){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE});
        }
        let hospitalTable = await DB.verifyHospital(hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }
        let userDetails = await DB.fetchDoctorPatient(userId, hospitalTable.DOCTOR_PATIENT_TABLE);
        if(!userDetails){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_NOT_FOUND.CODE});
        }
        // if hospital is hodes, get readings using hubId, else use userId
        if(hospitalTable.DOCTOR_PATIENT_TABLE.includes(CONSTANTS.HODES)) {
            userId = userDetails.hubID;
        }
        let {Items} = await DB.getBpAverage(userId, hospitalTable.BP_AVERAGE_TABLE);
        if(Items.length < 0) {
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_NOT_FOUND.CODE});
        } else {
            Items = Items.map(
                ({createdDate, modifiedDate, attributes:{...attributesRest}, ...rest}) =>
                ({...attributesRest})
            );
        }
        
        let response = {
            success: true,
            data: Items[0]
        };
        
        return Promise.resolve(response);
    } catch(error){
        return Promise.resolve({
            success: false,
            message: error,
            errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
        });
    }
};

exports.handler = async (event, context, callback) => {
    let response = await getBpAverage(event);
    callback(null, response);
};
