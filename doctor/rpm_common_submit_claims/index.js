const CONSTANT = require('./CONSTANTS.js')
const AWS = require("aws-sdk");
AWS.config.update({ region:process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const scanHospitals = async () => {
  const params = {
    TableName: CONSTANT.HOSPITAL_MASTER,
  };
  const { Count, Items } = await dbClient.scan(params).promise();
  return Count == 0 ? false : Items;
};

const verifyHospital = (hospitals, orderID)=>{
    for(let hospital of hospitals ){
        let hospitalExists = orderID.toLowerCase().indexOf(hospital.dbIdentifier);
        let orderIdentifierMatch = orderID.indexOf(hospital.orderIdentifier);
        if(hospitalExists != -1 || orderIdentifierMatch != -1){
          var obj = {
           RPM_CLAIM_TABLE: process.env.STAGE + '_' + CONSTANT.RPM + '_' + hospital.dbIdentifier + `_`+ CONSTANT.RPM_CLAIM_TABLE,
           RPM_EHR_MAPPING_TABLE: process.env.STAGE + '_' + CONSTANT.RPM + '_' + hospital.dbIdentifier + `_`+ CONSTANT.RPM_EHR_MAPPING_TABLE,
           RPM_ORDER_TABLE: process.env.STAGE + '_' + CONSTANT.RPM + '_' + hospital.dbIdentifier + `_`+ CONSTANT.RPM_ORDER_TABLE,
           RPM_USER_TABLE: process.env.STAGE + '_' + CONSTANT.RPM + '_' + hospital.dbIdentifier + `_`+ CONSTANT.RPM_USER_TABLE
        };
        return obj;
        }
    }
    return false;
};

const objectEquals = (x, y) => {
    // if both are function
    if (x instanceof Function) {
        if (y instanceof Function) {
            return x.toString() === y.toString();
        }
        return false;
    }
    if (x === null || x === undefined || y === null || y === undefined) { return x === y; }
    if (x === y || x.valueOf() === y.valueOf()) { return true; }

    // if one of them is date, they must had equal valueOf
    if (x instanceof Date) { return false; }
    if (y instanceof Date) { return false; }

    // if they are not function or strictly equal, they both need to be Objects
    if (!(x instanceof Object)) { return false; }
    if (!(y instanceof Object)) { return false; }

    var p = Object.keys(x);
    return Object.keys(y).every(function (i) { return p.indexOf(i) !== -1; }) ?
            p.every(function (i) { return objectEquals(x[i], y[i]); }) : false;
};

const getEhrUserDetails = async (userID, RPM_USER_TABLE) => {
    const params = {
        TableName : RPM_USER_TABLE,
        KeyConditionExpression : "#userID = :userID",
        ExpressionAttributeValues: {
            ':userID': userID
        },
        ExpressionAttributeNames: {
            '#userID': 'userID'
        }
    };
    const { Count, Items } = await dbClient.query(params).promise();
    return Count == 0 ? false : Items;
};

const getYearMonth = (timestamp) => {
    const date = new Date(timestamp);
    let month = date.getMonth() + 1;
    month = month < 10 ? '0' + month : month; 
    const year = (date.getFullYear()).toString();
    return year+month;
};

const insertOrUpdateClaim = async (yearMonth,ehrID,userID,claimDetails, RPM_CLAIM_TABLE) => {
    try{
      const serverTime = Date.now();
      if(claimDetails) {
        return updateClaims(yearMonth,ehrID,userID,serverTime,RPM_CLAIM_TABLE);
      } else {
        const item = {
          yearMonth,
          createdDate: serverTime,
          modifiedDate: serverTime,
          ehrID,
          attributes: {
            countOfUsers: 1,
            userIDBilled: [userID]
          }
        };
        await saveClaimItem(item, RPM_CLAIM_TABLE);
        return Promise.resolve(true);
      }
    }catch(e){
      throw (e);
    }
};

const saveClaimItem = async (Item, table) => {
    try{
        let params = {
            TableName: table,
            Item
        };
        return dbClient.put(params).promise();
    } catch(e) {
        throw e;
    }
}

const updateClaims = async (yearMonth,ehrID,userID,serverTime,RPM_CLAIM_TABLE) => {
    try{
        let queryParams = {
            TableName : RPM_CLAIM_TABLE,
            Key: {yearMonth, ehrID },
            UpdateExpression: "set #attributes.#userIDBilled = list_append (#attributes.#userIDBilled, :userID),modifiedDate = :modifiedDate, #attributes.#countOfUsers = #attributes.#countOfUsers + :inc",
            ExpressionAttributeValues: {
                ":userID" : [userID],
                ":modifiedDate": serverTime,
                ":inc": 1
            },
            ExpressionAttributeNames: {
                "#userIDBilled": 'userIDBilled',
                "#attributes": 'attributes',
                "#countOfUsers": 'countOfUsers'
            }
        }
        await dbClient.update(queryParams).promise();
        return Promise.resolve(true);
    }catch (e) {
        return Promise.resolve(false);
    }
}

const getClaimDetails = async (yearMonth,ehrID,RPM_CLAIM_TABLE) => {
    try{
        let queryParams = {
            TableName: RPM_CLAIM_TABLE,
            KeyConditionExpression:"yearMonth = :yearMonth AND ehrID = :ehrID",
            ExpressionAttributeValues:{
                ":ehrID": ehrID,
                ":yearMonth": yearMonth,
            }
        };
        const {Count } = await dbClient.query(queryParams).promise();
        return Count ? true : false;
    } catch(e) {
        throw e;
    }
}

const processClaims = async (userID, ehrID, billingEndDate, RPM_CLAIM_TABLE) => {
    try{  
      let yearMonth = getYearMonth(billingEndDate);
      yearMonth = Number(yearMonth)
      let claimDetails = await getClaimDetails(yearMonth,ehrID,RPM_CLAIM_TABLE);
      let response = await insertOrUpdateClaim(yearMonth,ehrID,userID,claimDetails,RPM_CLAIM_TABLE);
      return Promise.resolve(response);
    } catch(e) {
      console.log(e);
      return Promise.resolve(false);
    }
  };
  
exports.handler = async (event) => {
    try {
        if(event && !event.app) { event.app = CONSTANT.RPM }
        for (let record of event.Records) { 
            if (record.eventName == 'INSERT' || record.eventName == 'MODIFY') {
            const newImage = (AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage));
            const oldImage = (AWS.DynamoDB.Converter.unmarshall(record.dynamodb.OldImage));
            console.log("oldImage =>", oldImage);
            console.log("newImage =>", newImage);
            if(newImage && newImage.attributes) {
                let isChangeImage = oldImage && oldImage.attributes ? !objectEquals(oldImage.attributes,newImage.attributes) : true;
                if(isChangeImage) {
                    let ehrUserDetails;
                    let hospitals= await scanHospitals();
                    let hospitalTable= verifyHospital(hospitals, newImage.attributes.orderID);
                    ehrUserDetails = await getEhrUserDetails(newImage.userID, hospitalTable.RPM_USER_TABLE);
                    let ehrID = ehrUserDetails && ehrUserDetails[0].ehrID ? ehrUserDetails[0].ehrID : null;
                    if(ehrUserDetails[0].attributes.status && ehrUserDetails[0].attributes.status == 1) {
                         await processClaims(ehrUserDetails[0].userID, ehrID, newImage.billingEndDate, hospitalTable.RPM_CLAIM_TABLE );
                    }
                }
            }
            }
        }

        return Promise.resolve({"success" : true});
    } catch (error) {
        return Promise.reject(error);
    }
    
 };