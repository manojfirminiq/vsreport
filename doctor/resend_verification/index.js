'use strict';
const CLIENT_ID = process.env.CLIENTID;
const USER_POOL_ID = process.env.USERPOOLID;
const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const CONSTANTS = require('./CONSTANTS.js');

const resendVerificationMail = async (emailAddress) => {
    if(!emailAddress) {
        return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.EMAIL_ADD_REQ, "errorCode": CONSTANTS.ERROR_CODES.REQ_PARAM});
    }
    return new Promise(function(resolve, reject) {
        try {
            let params = {
                  ClientId: CLIENT_ID,
                  Username: emailAddress.toLowerCase().trim(),
            };
            cognitoidentityserviceprovider.resendConfirmationCode(params, function(err, data) {
              if (err) {
                  console.log(err)
                  resolve({"success": false, "message": err.message, "errorCode": CONSTANTS.ERROR_CODES.VERIFICATION_MAIL_FAILED}); // an error occurred
              } else {
                  resolve({"success": true});           // successful response
              }
            });
        } catch (error) {
            console.log(error);
            resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.INTERNAL_SERVER_ERROR, "errorCode": CONSTANTS.ERROR_CODES.INTERNAL_SERVER_ERROR});
        }
    });
}; 

exports.handler = async (event, context, callback) => {
    const response = await resendVerificationMail(event.emailAddress);
    callback(null, response);
};
