const AWS=require('aws-sdk');
const moment=require('moment');
const {RPM,WEIGHT_MANUAL,RPM_USER_TABLE,DEVICE_TYPE,BP_READINGS_ALERTS_TABLE,DOCTOR_PATIENT_TABLE,NON_BP_READINGS_ALERTS_TABLE,DOCTOR_PROFILE_TABLE,HOURS_24,NON_BP_READINGS_TABLE,HR_24,HR_72,ERRORS, HODES}=require("./CONSTANTS.js")

AWS.config.update({region: process.env.REGION});
const dbClient=new AWS.DynamoDB.DocumentClient({convertEmptyValues: true});
const BP_READING_TABLE=process.env.GLOBAL_RPM_BP_READINGS;
const NON_BP_READING_TABLE=process.env.GLOBAL_RPM_NON_BP_READINGS;
const DOCTOR_HOSPITAL_TABLE = process.env.DOCTOR_HOSPITAL_TABLE;
const dateTimeNow=Number(moment().format('x'));
const convert = require('convert-units');
const { sendEmailToDoctor } = require("./sendEmail");

let fetchDoctorProfile = async (userID, dbIdentifier) => {
    let tableName =  process.env.STAGE + RPM + dbIdentifier + `_`+ DOCTOR_PROFILE_TABLE;
    let params = {
        TableName: tableName,
        KeyConditionExpression:"userID = :id",
        ExpressionAttributeValues:{
            ":id": userID
        },
        ScanIndexForward: false
    };
    let {Items, Count} = await dbClient.query(params).promise();
    return Count?Items[0]:false;
};

let getImeiDetails = async (imei, dbIdentifier) => { 
    let tableName =  process.env.STAGE + RPM + dbIdentifier + `_`+ RPM_USER_TABLE;
    try {
        let queryParams = {
            TableName: tableName,
            IndexName:"imei-index",
            KeyConditionExpression:"imei = :imeikey",  
            ExpressionAttributeValues:{
                ":imeikey": imei
            }
        };
        return dbClient.query(queryParams).promise();
    } catch(e) {
        throw e;
    }
};

let fetchDoctorPatient = async (userID, dbIdentifier) => {
    let tableName =  process.env.STAGE + RPM + dbIdentifier + `_`+ DOCTOR_PATIENT_TABLE;
    let params = {
        TableName: tableName,
        IndexName: 'userID-doctorID-index',
        KeyConditionExpression:"userID = :id",
        ExpressionAttributeValues:{
            ":id": userID
        },
        ScanIndexForward: false
    };
    let {Items, Count} = await dbClient.query(params).promise();
    return Count?Items[0]:false;
};

const getHospital = async () => {
    let params = {
        TableName : DOCTOR_HOSPITAL_TABLE
    };
    return dbClient.scan(params).promise();
};

const identifyTableIdentifier = (hospitals, eventSource) => {
    let dbIdentifier = null;
    for(let i=0; i<hospitals.length; i++) {
        let identifierName = hospitals[i].dbIdentifier;
        if(eventSource.includes(identifierName)) {
            dbIdentifier = identifierName;
        }
    }
    return dbIdentifier;
 };


exports.saveReading=async (eventSourceARN, readings) => {
    try {
        const hospitals = await getHospital();
        const dbIdentifier = await identifyTableIdentifier(hospitals.Items, eventSourceARN)
        if(!dbIdentifier)   return Promise.resolve({success: false,message: ERRORS.HOSPITAL_ID_NOT_FOUND.MESSAGE,errorCode: ERRORS.HOSPITAL_ID_NOT_FOUND.CODE});
        let docProfile;
        let base64data;
        let promiseArr=[];
            let newData;
            let userID;
            if(readings.userId_type_deviceLocalName){
                userID = readings.userId_type_deviceLocalName.split('_')[0]
            }else{
                userID = readings.userID
            }
            if(userID) {
                let patientID;
                if(dbIdentifier === HODES){
                    let imeiDetails = await getImeiDetails(Number(userID), dbIdentifier);
                    patientID = imeiDetails.Items[0].userID;
                } else {
                    patientID = userID;
                }
                
                let docPatient = await fetchDoctorPatient(patientID, dbIdentifier);
                docProfile = await fetchDoctorProfile(docPatient.doctorID, dbIdentifier);
                let patientUserID = docPatient.userID;
                base64data = Buffer.from(patientUserID).toString('base64').replace(/=/gi,'');

                if(docPatient.attributes.threshold) {
                    newData={};
                    newData.diaLow=docPatient.attributes.threshold.diaLow? Number(docPatient.attributes.threshold.diaLow):null;
                    newData.diaHigh=docPatient.attributes.threshold.diaHigh? Number(docPatient.attributes.threshold.diaHigh):null;
                    newData.sysLow=docPatient.attributes.threshold.sysLow? Number(docPatient.attributes.threshold.sysLow):null;
                    newData.sysHigh=docPatient.attributes.threshold.sysHigh? Number(docPatient.attributes.threshold.sysHigh):null;
                    newData.weight24hr=docPatient.attributes.threshold.weight24hr? Number(docPatient.attributes.threshold.weight24hr):null;
                    newData.weight72hr=docPatient.attributes.threshold.weight72hr? Number(docPatient.attributes.threshold.weight72hr):null;
                }
            }
            if(!readings.userId_type_deviceLocalName) {
                if(newData && newData != null && newData != "undefined"){
                    if(newData.diaLow != null && newData.diaHigh != null && newData.sysLow != null && newData.sysHigh != null){
                        if((readings.attributes.diastolic < newData.diaLow || readings.attributes.diastolic > newData.diaHigh) || (readings.attributes.systolic < newData.sysLow || readings.attributes.systolic > newData.sysHigh)) {
                            const bpData=await getBPData(userID, dbIdentifier, readings.attributes.measurementLocalDate);
                            if(bpData&&bpData.length===0) {
                                let item=mapBPAlertsData(readings,newData);
                                promiseArr.push(saveItem(item, dbIdentifier, BP_READINGS_ALERTS_TABLE));
                            }else {
                                let item=mapBPAlertsData(readings ,newData);
                                promiseArr.push(updateBPAlerts(item, dbIdentifier, BP_READINGS_ALERTS_TABLE));
                            }

                            if(docProfile.subscriptionFlag) {
                               await sendEmailToDoctor(docProfile.emailAddress, base64data, true);
                            }
                        }
                    }
                    
                }
            }

            if(readings.userId_type_deviceLocalName) {
                if(newData && newData != null && newData != "undefined") {
                    if(newData.weight24hr != null && newData.weight72hr != null){
                        let fromDate = Number(readings.measurementDate) - (3* HOURS_24) -1;
                        let toDate = Number(readings.measurementDate)-1;  //this is to ignore current reading from set of reading we are fetching
                        let weightReadings = await getWeightReadings(readings.userId_type_deviceLocalName, dbIdentifier, fromDate,toDate);
                        weightReadings = weightReadings.sort(sortweight);
                        let weight72hrreading = [];
                        let weight24hrreading = [];
                        weight72hrreading = weightReadings
                        const filterweightReading = async ()=>{
                            weightReadings.some(reading => {
                                if(readings.measurementDate - reading.measurementDate <= HOURS_24) {
                                    weight24hrreading.push(reading)
                                }
                            });
                        }                          
                        await filterweightReading();
                        const weightData=await getWeightData(userID+WEIGHT_MANUAL, dbIdentifier, readings.attributes.measurementLocalDate);
                        let item= mapNonBPAlertsData(readings,newData);
                        let alertFlag =  false;
                        if(weight24hrreading.length > 0){
                            if(Number(newData.weight24hr) && Number(newData.weight24hr) > 0 && (Number(weightintolb(readings.attributes.weight)) - Number(weightintolb(weight24hrreading[0].attributes.weight))) > Number(newData.weight24hr)){
                                item.alerts.weight.weightDiff24hr= Number(weightintolb(readings.attributes.weight)) - Number(weightintolb(weight24hrreading[0].attributes.weight))
                                item.alerts.weight.thresholdType24hr = HR_24; 
                                alertFlag = true;
                            }
                        }
                        if(weight72hrreading.length > 0){
                            if(Number(newData.weight72hr) && Number(newData.weight72hr) > 0 && (Number(weightintolb(readings.attributes.weight)) - Number(weightintolb(weight72hrreading[0].attributes.weight)))  > Number(newData.weight72hr)){
                                item.alerts.weight.weightDiff72hr = Number(weightintolb(readings.attributes.weight)) - Number(weightintolb(weight72hrreading[0].attributes.weight))
                                item.alerts.weight.thresholdType72hr = HR_72;
                                alertFlag = true;
                            }
                        }                         
                        if(weightData&&weightData.length===0) {
                           if(alertFlag) promiseArr.push(saveItem(item, dbIdentifier, NON_BP_READINGS_ALERTS_TABLE));
                        }else {
                            if(alertFlag) promiseArr.push(updateNonBPAlerts(item, dbIdentifier, NON_BP_READINGS_ALERTS_TABLE));
                        } 
                         if(docProfile.subscriptionFlag && alertFlag) {
                            await sendEmailToDoctor(docProfile.emailAddress, base64data, false);
                        }                       
                    } 
                }
            }
            return await Promise.all(promiseArr).then(() => {
            return new Promise((resolve,reject) => {
                resolve({"success": true});
            });
        })
    }catch(e) {
        console.log(e)
    }
};

const saveItem= async(item, dbIdentifier, table_name) => {
    let tableName =  process.env.STAGE + RPM + dbIdentifier + `_`+ table_name;
    try{
        let params={
        TableName: tableName,
        Item: item
    };
    return await dbClient.put(params).promise();
    }catch(e){
        console.log(e)
    }
};

const weightintolb = ( weight ) =>{
    let weight_in_lbs= convert(Number(weight)).from('kg').to('lb').toFixed(1).toString();
    if (Number(weight_in_lbs) % 1 === 0) {
        weight_in_lbs = Math.floor(Number(weight_in_lbs)).toString();
    }
    return weight_in_lbs
}

const getBPData=async (userID, dbIdentifier, measurementDate) => {
    let tableName =  process.env.STAGE + RPM + dbIdentifier + `_`+ BP_READINGS_ALERTS_TABLE;
    const query={
        "TableName": tableName,
        KeyConditionExpression: "#uid = :id AND measurementDate = :measurementDate",
        ExpressionAttributeValues: {
            ':id': userID,
            ':measurementDate': measurementDate
        },
        ExpressionAttributeNames: {
            '#uid': 'userID'
        },
        ScanIndexForward: false
    };
    const {Items}=await queryDb(query);
    return Items;
};

const getWeightReadings = async(userId_type_deviceLocalName, dbIdentifier, fromDate, toDate) => {
    let tableName =  process.env.STAGE + RPM + dbIdentifier + `_`+ NON_BP_READINGS_TABLE;
        let params = {
            TableName: tableName,
            KeyConditionExpression: "#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate",
            ExpressionAttributeValues: {
                ':id': userId_type_deviceLocalName,
                ':minDate': Number(fromDate),
                ':maxDate': Number(toDate)
            },
            ExpressionAttributeNames: {
                '#uid': 'userId_type_deviceLocalName'
            }
        };
        const { Items, Count } = await dbClient.query(params).promise();
        return Count ? Items : [];
};
const  sortweight = (a, b) =>{
  const weightA = a.attributes.weight
  const weightB = b.attributes.weight

  let comparison = 0;
  if (weightA > weightB) {
    comparison = 1;
  } else if (weightA < weightB) {
    comparison = -1;
  }
  return comparison;
};

const getWeightData=async (userId_type_deviceLocalName, dbIdentifier, measurementDate) => {
    let tableName =  process.env.STAGE + RPM + dbIdentifier + `_`+ NON_BP_READINGS_ALERTS_TABLE;
    const query={
        "TableName": tableName,
        KeyConditionExpression: "#uid = :id AND measurementDate = :measurementDate",
        ExpressionAttributeValues: {
            ':id': userId_type_deviceLocalName,
            ':measurementDate': measurementDate
        },
        ExpressionAttributeNames: {
            '#uid': 'userId_type_deviceLocalName'
        },
        ScanIndexForward: false
    };
    const {Items}=await queryDb(query);
    return Items;
};

let queryDb=(params) => {
    return dbClient.query(params).promise();
};

const mapBPAlertsData=(bpReading,newData) => {
    let reading=bpReading.attributes;
    let obj={
        measurementDate: bpReading.measurementDate,
        userID: bpReading.userID,
        modifiedDate: dateTimeNow,
        createdDate: dateTimeNow,
        attributes: {},
        alerts: {},
        app: 'RPM'
    };

    obj.attributes={
        "diastolic": reading.diastolic,
        "diastolicUnit": reading.diastolicUnit,
        "isManualEntry": 0,
        "measurementLocalDate": reading.measurementLocalDate,
        "pulse": reading.pulse,
        "pulseUnit": reading.pulseUnit,
        "systolic": reading.systolic,
        "systolicUnit": reading.systolicUnit,
        "timeZone": "0",
        "transferDate": reading.transferDate,
        "userNumberInDevice": reading.userNumberInDevice
    };
    obj.alerts={
        "bp": {
            "Notes": "",
            "resolveStatus": "0",
        },
        "threshold": {
            "diaHigh": newData.diaHigh,
            "diaLow": newData.diaLow,
            "sysHigh": newData.sysHigh,
            "sysLow": newData.sysLow
        },
        "thresholdBpUnit": reading.systolicUnit
    };

    if(obj.attributes.diastolic>obj.alerts.threshold.diaHigh) {
        obj.alerts.bp.diaDiff=(obj.attributes.diastolic-obj.alerts.threshold.diaHigh);
    }
    else if(obj.attributes.diastolic<obj.alerts.threshold.diaLow) {
        obj.alerts.bp.diaDiff=(obj.attributes.diastolic-obj.alerts.threshold.diaLow);
    }

    if(obj.attributes.systolic>obj.alerts.threshold.sysHigh) {
        obj.alerts.bp.sysDiff=(obj.attributes.systolic-obj.alerts.threshold.sysHigh);
    }
    else if(obj.attributes.systolic<obj.alerts.threshold.sysLow) {
        obj.alerts.bp.sysDiff=(obj.attributes.systolic-obj.alerts.threshold.sysLow);
    }

    if(bpReading.isDoctorLinked) {
        obj.isDoctorLinked=1
    }
    return obj;
};

const mapNonBPAlertsData = (weightReading,newData) => {
    let reading=weightReading.attributes;
    let obj={
        app: "RPM",
        measurementDate: weightReading.measurementDate,
        attributes: {},
        alerts: {},
        userId_type_deviceLocalName: weightReading.userId_type_deviceLocalName,
        createdDate: dateTimeNow,
        modifiedDate: dateTimeNow
    };

    obj.attributes={
        "bmiValue": reading.bmiValue,
        "bodyFatPercentage": reading.bodyFatPercentage,
        "deleteFlag": "0",
        "isManualEntry": "",
        "measurementLocalDate": reading.measurementLocalDate,
        "type": "weight",
        "weight": reading.weight,
        "weightLBS": Number(reading.weightLBS).toFixed(1),
    };

    if(weightReading.isDoctorLinked) {
        obj.attributes.isDoctorLinked=1
    }
    obj.alerts={
                "thresholdWeightUnit": "lbs",
                "threshold": {
                    "weight72hr": newData.weight72hr,
                    "weight24hr": newData.weight24hr
                },
                "weight": {
                    "resolveStatus": "0",
                    "Notes": ""
                }
            };
    return obj;
};

const updateBPAlerts=({userID,attributes,modifiedDate,measurementDate,alerts}, dbIdentifier, bpAlertsTable) => {
    let tableName =  process.env.STAGE + RPM + dbIdentifier + `_`+ bpAlertsTable;
    var params={
        TableName: tableName,
        Key: {
            "userID": userID,
            "measurementDate": measurementDate
        },
        UpdateExpression: "set attributes = :attributes, modifiedDate = :modifiedDate, alerts = :alerts",
        ExpressionAttributeValues: {
            ":attributes": attributes,
            ":modifiedDate": modifiedDate,
            ":alerts": alerts
        }
    };
    return dbClient.update(params).promise();
};

const updateNonBPAlerts=({userId_type_deviceLocalName,attributes,modifiedDate,measurementDate,alerts}, dbIdentifier, nonBpAlertsTable) => {
    let tableName =  process.env.STAGE + RPM + dbIdentifier + `_`+ nonBpAlertsTable;
    var params={
        TableName: tableName,
        Key: {
            "userId_type_deviceLocalName": userId_type_deviceLocalName,
            "measurementDate": measurementDate
        },
        UpdateExpression: "set attributes = :attributes, modifiedDate = :modifiedDate, alerts= :alerts",
        ExpressionAttributeValues: {
            ":attributes": attributes,
            ":modifiedDate": modifiedDate,
            ":alerts": alerts
        }
    };
    return dbClient.update(params).promise();
};
