exports.constants = {
  tenMinutes: 10 * 60 * 1000,
  insert: "INSERT",
  modify: "MODIFY",
  remove: "REMOVE"
};
exports.HOURS_24 = 86400000; // miliseconds in a day
exports.BP_READINGS_TABLE = process.env.BP_READINGS_TABLE;
exports.BP_READINGS_ALERTS_TABLE = process.env.BP_READINGS_ALERTS_TABLE;
exports.NON_BP_READINGS_ALERTS_TABLE = process.env.NON_BP_READINGS_ALERTS_TABLE;
exports.DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
exports.NON_BP_READINGS_TABLE = process.env.NON_BP_READINGS_TABLE;
exports.RPM_USER_TABLE = process.env.RPM_USER_TABLE;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.HR_24 = '24hr';
exports.HR_72 = '72hr';
exports.WEIGHT_MANUAL = "_weight_Manual";
exports.RPM = "_rpm_";
exports.ERRORS = {
  HOSPITAL_ID_NOT_FOUND: {
      CODE: "HOSPITAL_ID_NOT_FOUND",
      MESSAGE: "Hospital Id not found"
  }
};
exports.HODES = 'hodes';