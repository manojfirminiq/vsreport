var aws = require('aws-sdk');
aws.config.update({region: process.env.REGION});
var ses = new aws.SES({region: process.env.REGION});

exports.sendEmailToDoctor = async (docEmail,base64data,value)=> {
   try{
      let params = {
              Destination: {
                ToAddresses: [docEmail]
              },
              Message: {
                Body: {
                  Text: {
                    Data: `Please click here to view your patients alert on the VitalSight program. ${process.env.FE_URL}#/patient-summary/${base64data}/${value}`
                  }
                },
                Subject: {
                  Data: "VitalSight Alert Notification"
                }
              },
              Source: `${process.env.SENDER_EMAIL}`
            };
      ses.sendEmail(params, function (err, data) {
         if (err) {
           console.log(err)
           return err;
       } else {
           return data
       }
   });
   }catch(err){
       console.log(err)
   }
};