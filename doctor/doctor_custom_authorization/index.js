const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const USER_POOL_ID = process.env.USERPOOLID;
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

exports.handler =  function(event, context, callback) {
    var token = event.authorizationToken;
    if(!token) {
        callback("Unauthorized");
    } else {
        var params = {
          AccessToken: token /* required */
        };
        cognitoidentityserviceprovider.getUser(params, function(err, data) {
          if (err) { console.log("error", err, err.stack); // an error occurred
            callback("Unauthorized");
          } else {
              let email = null, userId = null, hospitalID = null;
              for(let attribute of data.UserAttributes){
                  console.log(attribute.Name, "custom:userId");
                  if(attribute.Name === "email") {
                      email = attribute.Value;
                  }
                 if(attribute.Name == "custom:userId"){
                     userId = attribute.Value;
                 } 
                 if(attribute.Name === "custom:hospitalID") {
                      hospitalID = attribute.Value;
                  }
              }
              callback(null, generatePolicy('user', 'Allow', event.methodArn, email, userId, hospitalID));
          }
        });
        
    }
};

// Help function to generate an IAM policy
var generatePolicy = function(principalId, effect, resource, email, userId, hospitalID) {
    var authResponse = {};
    
    authResponse.principalId = principalId;
    if (effect && resource) {
        var policyDocument = {};
        policyDocument.Version = '2012-10-17'; 
        policyDocument.Statement = [];
        var statementOne = {};
        statementOne.Action = 'execute-api:Invoke'; 
        statementOne.Effect = effect;
        statementOne.Resource = resource;
        policyDocument.Statement[0] = statementOne;
        authResponse.policyDocument = policyDocument;
    }
    console.log(userId);
    // Optional output with custom properties of the String, Number or Boolean type.
    authResponse.context = {
        "email": email,
        "userId": userId,
        "hospitalID": hospitalID
    };
    return authResponse;
};