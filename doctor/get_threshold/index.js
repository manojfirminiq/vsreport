const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');

const getThreshold = async ({doctorId, userId, hospitalID}) => {
    try{
        if(!userId){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_ID_REQ.CODE});
        }
        if(!hospitalID) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE});
        let hospitalTable = await DB.verifyHospital(hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }
      
        let Items = await DB.getThreshold(doctorId, userId, hospitalTable );

        if(!Items) {
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_NOT_FOUND.CODE});
        } 
        let newData = {};
        newData.diaLow = Items[0].attributes.threshold.diaLow;
        newData.diaHigh = Items[0].attributes.threshold.diaHigh;
        newData.sysLow = Items[0].attributes.threshold.sysLow;
        newData.sysHigh = Items[0].attributes.threshold.sysHigh;
       //newData.weightLowerLimit = Items[0].attributes.threshold.weightLowerLimit;
        //newData.weightUpperLimit = Items[0].attributes.threshold.weightUpperLimit;
        newData.weight24hr = Items[0].attributes.threshold.weight24hr?Items[0].attributes.threshold.weight24hr: "";
        newData.weight72hr = Items[0].attributes.threshold.weight72hr?Items[0].attributes.threshold.weight72hr: "";
        
        let response = {
            success: true,
            data: newData
        };
        
        return Promise.resolve(response);
    } catch(error){
        return Promise.resolve({
            "success": false,
            "message": CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
            "errorCode": CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
        });
    }
};

exports.handler = async (event, context, callback) => {
    console.log("event =====>", event);
    let response = await getThreshold(event);
    callback(null, response);
};


