exports.ERRORS = {
    INTERNAL_SERVER_ERROR: {
        CODE: "INTERNAL_SERVER_ERROR",
        MESSAGE: "Internal Server Error"
    },
    USER_ID_REQ: {
        CODE: "USER_ID_REQ",
        MESSAGE: "User id required"
    },
    HOSPITAL_ID_REQ: {
        CODE: "HOSPITAL_ID_REQ",
        MESSAGE: "Hospital id required"
    },
    INVALID_STATUS: {
        CODE: "STATUS_REQ",
        MESSAGE: "Correct Status required"
    },
    BILLING_START_DATE_REQ: {
        CODE: "BILLING_START_DATE_REQ",
        MESSAGE: "Billing Start Date required"
    },
    USER_NOT_FOUND: {
        CODE: "USER_NOT_FOUND",
        MESSAGE: "User not found"
    },
    USER_NOT_FOUND_TRANSACTION_99453: {
        CODE: "USER_NOT_FOUND_TRANSACTION_99453_TABLE",
        MESSAGE: "User not found in Financial Transaction_99453 Table"
    },
    USER_NOT_FOUND_TRANSACTION: {
        CODE: "USER_NOT_FOUND_FIN_TRANSACTION_TABLE",
        MESSAGE: "User not found in Financial Transaction Table"
    },
    HOSPITAL_ID_INVALID: {
        CODE: "HOSPITAL_ID_INVALID",
        MESSAGE: "Hospital Id is invalid"
    },
    INVALID_CODE: {
        CODE: "CODE_REQ",
        MESSAGE: "Code required"
    }
};
exports.STATUS_UPDATE = {
    CODE: "STATUS_UPDATE",
    MESSAGE: "Status Updated Successfully"
}
exports.ACCEPTED_CODE = ['99453', '99454'];
exports.CODE = {
    PATIENT_CODE_99453 : '99453',
    PATIENT_CODE_99454 : '99454'
}
exports.SUBMITTED = 'SUBMITTED';
