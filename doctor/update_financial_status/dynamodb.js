const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
const FINANCIAL_TRANSACTION_TABLE = process.env.FINANCIAL_TRANSACTION_TABLE;
const FINANCIAL_TRANSACTION_99453_TABLE = process.env.FINANCIAL_TRANSACTION_99453_TABLE;
const DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;


let verifyHospital = async (hospitalId) =>{
    let params = {
        TableName : HOSPITAL_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalId
        }
    };

    const {Count, Items} = await dbClient.query(params).promise();
    if(Items.length == 0){
        return false;
    } else{
        var obj = {
            DOCTOR_PATIENT:  process.env.STAGE +`_rpm_` + Items[0].dbIdentifier + `_`+ DOCTOR_PATIENT_TABLE,
            DOCTOR_PROFILE:  process.env.STAGE +`_rpm_` + Items[0].dbIdentifier + `_`+ DOCTOR_PROFILE_TABLE,
            FINANCIAL_TRANSACTION_TABLE:  process.env.STAGE +`_rpm_` + Items[0].dbIdentifier + `_`+ FINANCIAL_TRANSACTION_TABLE,
            FINANCIAL_TRANSACTION_99453_TABLE:  process.env.STAGE +`_rpm_` + Items[0].dbIdentifier + `_`+ FINANCIAL_TRANSACTION_99453_TABLE,
        };
        return obj;
    }
};

let getFinTransaction99453Details = async (userID, FINANCIAL_TRANSACTION_99453_TABLE) => { 
    try {
        let params = {
            TableName: FINANCIAL_TRANSACTION_99453_TABLE,
            KeyConditionExpression : "#userID = :userID",
            ExpressionAttributeValues: {
                ':userID': userID
            },
            ExpressionAttributeNames: {
                '#userID': 'userID'
            }
        };
        const { Count, Items } = await dbClient.query(params).promise();
        return Count > 0 ? Items: [];
    } catch(e) {
        throw e;
    }
};

const updateFinTransaction99453Details = async (user_ID,FINANCIAL_TRANSACTION_99453_TABLE, attributes) => {
    var params = {
        TableName: FINANCIAL_TRANSACTION_99453_TABLE,
        Key: {
            "userID": user_ID
        },
        UpdateExpression: "SET attributes = :attributes, modifiedDate = :modifiedDate",
        ExpressionAttributeValues: {
            ":attributes": attributes,
            ":modifiedDate": new Date().getTime()
        },
        ReturnValues: "UPDATED_NEW"
    };
    return dbClient.update(params).promise();
};

const getEhrFinTransactionDetails = async (userID,FINANCIAL_TRANSACTION_TABLE, billingStartDate) => {
    try {
    let params = {
        TableName : FINANCIAL_TRANSACTION_TABLE,
        KeyConditionExpression : "#userID = :userID AND #billingStartDate = :billingStartDate",
        ExpressionAttributeValues: {
            ':userID': userID,
            ':billingStartDate': billingStartDate
        },
        ExpressionAttributeNames: {
            '#billingStartDate': 'billingStartDate',
            '#userID': 'userID'
        }
    };
    const { Count, Items } = await dbClient.query(params).promise();
    return Count > 0 ? Items: [];  
    } catch(e) {
        throw e;
    }    
};


const updateEhrFinTransactionTable = async(userID,FINANCIAL_TRANSACTION_TABLE, attributes, billingStartDate) => {
    var params = {
        TableName: FINANCIAL_TRANSACTION_TABLE,
        Key: {
            "userID": userID,
            "billingStartDate": billingStartDate
        },
        UpdateExpression: "SET attributes = :attributes, modifiedDate = :modifiedDate",
        ExpressionAttributeValues: {
            ":attributes": attributes,
            ":modifiedDate": new Date().getTime()
        },
        ReturnValues: "UPDATED_NEW"
    };
    return dbClient.update(params).promise();
};

const getPatient = async (doctorId, userId, hospitalTable) => {
    let params = {
        TableName : hospitalTable,
        KeyConditionExpression : "#doctorId = :doctorId AND #userId = :userId",
        ExpressionAttributeValues: {
            ':doctorId': doctorId,
            ':userId': userId
        },
        ExpressionAttributeNames: {
            '#doctorId': 'doctorID',
            '#userId': 'userID'
        }
    };   
    return dbClient.query(params).promise();
};

module.exports.getPatient = getPatient;
module.exports.verifyHospital = verifyHospital;
module.exports.getFinTransaction99453Details = getFinTransaction99453Details;
module.exports.getEhrFinTransactionDetails = getEhrFinTransactionDetails;
module.exports.updateFinTransaction99453Details = updateFinTransaction99453Details;
module.exports.updateEhrFinTransactionTable = updateEhrFinTransactionTable;