const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');

const updateFinancialStatus = async(hospitalID, doctorId, userId, status, code, billingStartDate) => {
    let userID;
    try{
        let hospitalTableDetails = await DB.verifyHospital(hospitalID);
        if(!hospitalTableDetails){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }
        let {Items} = await DB.getPatient(doctorId, userId, hospitalTableDetails.DOCTOR_PATIENT);      
        if(Items.length <= 0) {
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_NOT_FOUND.CODE});
        } else {
            userID = Items[0].userID;
            if(code == CONSTANTS.CODE.PATIENT_CODE_99453) {
                let ehrFin99453Details = await DB.getFinTransaction99453Details(userID, hospitalTableDetails.FINANCIAL_TRANSACTION_99453_TABLE);
                if(ehrFin99453Details.length > 0) {
                    let attributes = ehrFin99453Details[0].attributes ? ehrFin99453Details[0].attributes : {};
                    attributes.status = status.toUpperCase();
                    attributes.claimTimestamp = new Date().getTime();
                    await DB.updateFinTransaction99453Details(userID,hospitalTableDetails.FINANCIAL_TRANSACTION_99453_TABLE, attributes);
                } else {
                    return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION_99453.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION_99453.CODE});
                }
            } else if (code == CONSTANTS.CODE.PATIENT_CODE_99454) {
                let ehrFinDetails = await DB.getEhrFinTransactionDetails(userID, hospitalTableDetails.FINANCIAL_TRANSACTION_TABLE,parseInt(billingStartDate));
                if(ehrFinDetails.length > 0) {
                    let attributes = ehrFinDetails[0].attributes  ? ehrFinDetails[0].attributes : {};
                    attributes.status = status.toUpperCase();
                    attributes.claimTimestamp = new Date().getTime();
                    await DB.updateEhrFinTransactionTable(userID,hospitalTableDetails.FINANCIAL_TRANSACTION_TABLE, attributes, parseInt(billingStartDate));
                } else {
                    return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION.CODE});
                }
            }
        }
        let response = {
            success: true,
            msg : CONSTANTS.STATUS_UPDATE.MESSAGE
        };
        return Promise.resolve(response);
    } catch (error) {
        return Promise.resolve({
            success: false,
            message: error,
            errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
        });
    }
}

const applyValidation = async ({hospitalID, doctorId, userId, status, code, billingStartDate, }) => {
  
    if(!hospitalID){
        return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE});
    }
    if(!userId){
        return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_ID_REQ.CODE});
    }
    if (!code || !CONSTANTS.ACCEPTED_CODE.includes(code)) {
        return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.INVALID_CODE.MESSAGE, "errorCode": CONSTANTS.ERRORS.INVALID_CODE.CODE});
    }
    if(!status || status.toUpperCase() != CONSTANTS.SUBMITTED){
        return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.INVALID_STATUS.MESSAGE, "errorCode": CONSTANTS.ERRORS.INVALID_STATUS.CODE});
    }
    if(CONSTANTS.CODE.PATIENT_CODE_99454 == code && !billingStartDate){
        return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.BILLING_START_DATE_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.BILLING_START_DATE_REQ.CODE});
    }
    return await updateFinancialStatus(hospitalID, doctorId, userId, status, code, billingStartDate);
};

exports.handler = async (event, context, callback) => {
    console.log(event);
    if (event.body) {
        event.body.emailAddress = event.emailAddress;
        event.body.doctorId = event.doctorId;
        event.body.hospitalID = event.hospitalID;
        event = event.body;
    }
    let response = await applyValidation(event);
    callback(null, response);
};