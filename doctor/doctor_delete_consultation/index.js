const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_CONSULTATION = CONSTANTS.DOCTOR_CONSULTATION;
const DB = require('./db');

const applyValidation = async (params) => {
    try {
        if( !params.userId ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE});
        if( !params.timestamp ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.TIMESTAMP_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.TIMESTAMP_NOT_FOUND.CODE});
        let hospitalTable = await DB.verifyHospital(params.hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }
        let consultation = await DB.deletePatientConsultation(params, hospitalTable.DOCTOR_CONSULTATION);

        return Promise.resolve({
            success: true
        });

    } catch (error) {
        console.log(error);
        return Promise.resolve({
            success: false,
            errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
            message: error
        });
    }
};

exports.handler = async (event, context, callback) => {
    const response = await applyValidation(event);
    callback(null, response);
};
