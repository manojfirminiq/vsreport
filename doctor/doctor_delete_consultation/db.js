const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_CONSULTATION = CONSTANTS.DOCTOR_CONSULTATION;
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;
const DOCTOR_TABLE = CONSTANTS.DOCTOR_TABLE;

let verifyHospital = async (hospitalID) =>{
    let paramsTable = {
        TableName : DOCTOR_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalID
        }
    };
    console.log("parmsTableeeeeeee", paramsTable);
    const {Count, Items} = await dbClient.query(paramsTable).promise();
    if(Items.length == 0){
        return false;
    }else{
        return {
            DOCTOR_CONSULTATION:process.env.STAGE + '_' + CONSTANTS.RPM + '_' + Items[0].dbIdentifier + `_`+DOCTOR_CONSULTATION
        }
    }
};

const deletePatientConsultation = async (params,DOCTOR_CONSULTATION) => {
    var params={
        TableName: DOCTOR_CONSULTATION,
        Key: {
            "doctorID": params.doctorId,
            "userID_startTime": `${params.userId}_${params.timestamp}`
        },
        UpdateExpression: "set isdeleted = :isdeleted, modifiedDate = :modifiedDate",
        ExpressionAttributeValues: {
            ":isdeleted": true,
            ":modifiedDate": Date.now()
        }
    };
    console.log(params)
    return dbClient.update(params).promise();
};

module.exports.deletePatientConsultation = deletePatientConsultation;
module.exports.verifyHospital = verifyHospital;