const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const DOCTOR_TABLE = process.env.DOCTOR_TABLE;
const CONSTANTS = require('./CONSTANTS.js');

let verifyHospital = async (hospitalId) =>{
    let paramsTable = {
        TableName : DOCTOR_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalId
        }
    };
    const {Count, Items} = await dbClient.query(paramsTable).promise();
    if(Items.length == 0){
        return false;
    }else{
         return process.env.STAGE +`_rpm_` + Items[0].dbIdentifier + `_`+ DOCTOR_PATIENT_TABLE;
    }
};

const getPatient = async (doctorId, patientId, hospitalTable) => {
    let params = {
        TableName : hospitalTable,
        KeyConditionExpression : "#doctorId = :doctorId AND #userId = :userId",
        ExpressionAttributeValues: {
            ':doctorId': doctorId,
            ':userId': patientId
        },
        ExpressionAttributeNames: {
            '#doctorId': 'doctorID',
            '#userId': 'userID'
        }
    };
    
    return dbClient.query(params).promise();
};

const updatePatientCode = async (patientDetails, hospitalTable) => {
    patientDetails.modifiedDate = new Date().getTime();
    let params = {
        TableName: hospitalTable,
        Item: patientDetails
    };
    return dbClient.put(params).promise();
};

module.exports.getPatient = getPatient;
module.exports.updatePatientCode = updatePatientCode;
module.exports.verifyHospital = verifyHospital;