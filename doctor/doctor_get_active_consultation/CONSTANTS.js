exports.ERRORS = {
    INTERNAL_SERVER_ERROR: {
        CODE: "INTERNAL_SERVER_ERROR",
        MESSAGE: "Internal Server Error"
    },
    USER_NOT_FOUND: {
        CODE: "USER_NOT_FOUND",
        MESSAGE: "User not found"
    },
    USER_ID_REQUIRED: {
        CODE: "REQUIRED_PARAMETER",
        MESSAGE: "User Id is required"
    },
    HOSPITAL_ID_REQ: {
        CODE: "HOSPITAL_ID_REQ",
        MESSAGE: "Hospital id required"
    },
    HOSPITAL_ID_INVALID: {
        CODE: "HOSPITAL_ID_INVALID",
        MESSAGE: "Hospital Id is invalid"
    },
    TYPE_REQUIRED: {
        CODE: "TYPE_REQUIRED",
        MESSAGE: "Type is required"
    },
    NO_ACTIVE_CONSULTATION: {
        CODE: "NO_ACTIVE_CONSULTATION",
        MESSAGE: "No active consultation available"
    },
    START_TIME_REQUIRED: {
        CODE: "REQUIRED_PARAMETER",
        MESSAGE: "consStartTime is required"
    },
    END_TIME_REQUIRED: {
        CODE: "REQUIRED_PARAMETER",
        MESSAGE: "consEndTime is required"
    },
    START_TIME_INAVLID: {
        CODE: "START_TIME_INVALID",
        MESSAGE: "consStartTime should be type number"
    },
    END_TIME_INAVLID: {
        CODE: "END_TIME_INVALID",
        MESSAGE: "consEndTime should be type number"
    },
    START_END_TIME_INAVLID: {
        CODE: "START_END_TIME_INVALID",
        MESSAGE: "consEndTime should be greater than consStartTime"
    },
    
};
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.RPM = 'rpm';
exports.ACTIVE = 'active';
