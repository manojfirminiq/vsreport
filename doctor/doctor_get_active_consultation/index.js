const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');

const checkPatientConsulation = (consultations, userId, consStartTime, consEndTime) => {
    return consultations.filter(consultation => {
        delete consultation.createdDate;
        delete consultation.modifiedDate;
        delete consultation.userID_startTime;
        delete consultation.isdeleted;
        if(consStartTime && consEndTime)
        return ( consultation.startTime >= consStartTime && 
                 consultation.startTime <= consEndTime &&
                 userId==consultation.userID);
        else return userId==consultation.userID;              
    }).sort(function(a, b){return b.startTime - a.startTime});
} 

const applyValidation = async ({doctorId, userId, hospitalID, type, consStartTime, consEndTime }) => {
    try {

        if( !userId ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE});
        if( !hospitalID ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE});
        let hospitalTable = await DB.verifyHospital(hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }        
        if(consStartTime && !consEndTime) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.END_TIME_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.END_TIME_REQUIRED.CODE});;
        if(!consStartTime && consEndTime) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.START_TIME_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.START_TIME_REQUIRED.CODE});
        
        if(consStartTime && consEndTime) {
            consStartTime = Number(consStartTime); 
            if(isNaN(consStartTime)) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.START_TIME_INAVLID.MESSAGE, "errorCode": CONSTANTS.ERRORS.START_TIME_INAVLID.CODE});
            consEndTime = Number(consEndTime);
            if(isNaN(consEndTime)) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.END_TIME_INAVLID.MESSAGE, "errorCode": CONSTANTS.ERRORS.END_TIME_INAVLID.CODE});
            
            if(consStartTime>= consEndTime) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.START_END_TIME_INAVLID.MESSAGE, "errorCode": CONSTANTS.ERRORS.START_END_TIME_INAVLID.CODE});
        }        
        let {Count, Items} = await DB.getConsultation(doctorId, hospitalTable, type);
        if(!Count) 
            return Promise.resolve({
                success:false,
                message: CONSTANTS.ERRORS.NO_ACTIVE_CONSULTATION.MESSAGE, 
                errorCode: CONSTANTS.ERRORS.NO_ACTIVE_CONSULTATION.CODE
            })        
        let userConsultation = checkPatientConsulation(Items, userId, consStartTime, consEndTime)
        if(!userConsultation.length)
            return Promise.resolve({
                success:false,
                message: CONSTANTS.ERRORS.NO_ACTIVE_CONSULTATION.MESSAGE, 
                errorCode: CONSTANTS.ERRORS.NO_ACTIVE_CONSULTATION.CODE
            })
        
        if(type == CONSTANTS.ACTIVE){
            if(userConsultation[0].endTime)
                return Promise.resolve({
                    success:false,
                    message: CONSTANTS.ERRORS.NO_ACTIVE_CONSULTATION.MESSAGE, 
                    errorCode: CONSTANTS.ERRORS.NO_ACTIVE_CONSULTATION.CODE
                })
            else
                return Promise.resolve({
                    success:true,
                    consultation: userConsultation[0]
                })
        } else{
            return Promise.resolve({
                success: true,
                consultation: userConsultation
            });
        }
        

    } catch (error) {
        console.log(error);
        return Promise.resolve({
            success: false,
            errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
            message: error
        });
    }
};

exports.handler = async (event, context, callback) => {
    const response = await applyValidation(event);
    callback(null, response);
};