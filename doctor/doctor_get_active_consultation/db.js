const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();


const CONSTANTS = require('./CONSTANTS.js');

let verifyHospital = async (hospitalId) =>{
    let paramsTable = {
        TableName : CONSTANTS.HOSPITAL_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalId
        }
    };
    const {Count, Items} = await dbClient.query(paramsTable).promise();
    if(!Count){
        return false;
    }else{
         return process.env.STAGE + `_` + CONSTANTS.RPM + `_` + Items[0].dbIdentifier + `_`+ CONSTANTS.DOCTOR_CONSULTATION;
    }
};

const getConsultation = async (doctorId, hospitalTable, type) => {
    let params = {
        TableName : hospitalTable,
        KeyConditionExpression : "#doctorId = :doctorId",
        ExpressionAttributeValues: {
            ':doctorId': doctorId,
            ':isdeletedValue': true
        },
        ExpressionAttributeNames: {
            '#doctorId': 'doctorID',
            '#isdeleted': 'isdeleted'
        },
        FilterExpression : `#isdeleted <> :isdeletedValue`,
        ScanForwardIndex:false
    };
    return dbClient.query(params).promise();
};

module.exports.getConsultation = getConsultation;
module.exports.verifyHospital = verifyHospital;