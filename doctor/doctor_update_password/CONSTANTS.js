exports.ERRORS = {
    INVALID_OLD_PASSWORD: {
        CODE: "INVALID_OLD_PASSWORD",
        MESSAGE: "Invalid or wrong password"
    },
    INTERNAL_SERVER_ERRROR: {
        CODE:"INTERNAL_SERVER_ERRROR",
        MESSAGE: "Internal server error."
    }
};

exports.COGNITO_ERROR = {
    INVALID_OLD_PASSWORD: "NotAuthorizedException"
};
