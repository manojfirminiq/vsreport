const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const CONSTANTS = require('./CONSTANTS');
const lambda = new AWS.Lambda({
    region: process.env.REGION 
});

const cognitoUpdatePassword = async (password, oldPassword, accessToken) => {
    try{
        let params = {
            AccessToken: accessToken,
            PreviousPassword: oldPassword,
            ProposedPassword: password
        };
        await new AWS.CognitoIdentityServiceProvider().changePassword(params).promise();
        return {
            success: true,
            message: "Password updated succesfully."
        };
    } catch(error){
        console.log(error);
        let response = {
            success: false,
            // message: error.message
        };
        if(error.code && error.code === CONSTANTS.COGNITO_ERROR.INVALID_OLD_PASSWORD ){
            response.errorCode = CONSTANTS.ERRORS.INVALID_OLD_PASSWORD.CODE;
            response.message = CONSTANTS.ERRORS.INVALID_OLD_PASSWORD.MESSAGE;
        } else {
            response.errorCode = CONSTANTS.ERRORS.INTERNAL_SERVER_ERRROR.CODE;
            response.message = CONSTANTS.ERRORS.INTERNAL_SERVER_ERRROR.MESSAGE;
        }
        
        return response;
    }
    
};

const invalidateTokens = async ( emailAddress ) => {
    const payLoad = {
        "emailAddress" : emailAddress
    };
    return new Promise((resolve, reject) => {
            lambda.invoke({
            FunctionName: process.env.CLIENT_LAMBDA,
            Payload: JSON.stringify(payLoad) // pass params
        }, function(error, data) {
            if(error){
                reject(error);
            } else{
                resolve(data);
            }
        });
});
};

const applyValidation = async ({accessToken, emailAddress, body:{ password, oldPassword }}) => {
    let response = await cognitoUpdatePassword(password, oldPassword, accessToken);
    if(response.success){
        invalidateTokens(emailAddress)
    }
   return response;
};

exports.handler = async (event, context, callback) => {
    let accessToken = event.headers["Authorization"] ? event.headers["Authorization"] : event.headers["authorization"];
    event.accessToken = accessToken;
    const response = await applyValidation(event);
    callback(null, response);
};
