const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const INVALID_TOKEN_MESSAGE = `Invalid or expired token`;

const generateHtmlTemplate = (body) => {
    return `<!DOCTYPE html><html lang = "en-US">
    <head>
    	<meta charset = "UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<title>Verify Email</title>
    	<!--link rel="stylesheet" href="https://s3-us-west-2.amazonaws.com/ofs-static-files/css/customSelectBox.css"-->
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jScrollPane/2.2.1/style/jquery.jscrollpane.min.css">
    	<link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    	<script src="https://s3-us-west-2.amazonaws.com/ofs-static-files/js/SelectBox.js"></script>
    	<!--script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.selectbox/1.2.0/jquery.selectBox.min.js"></script-->
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jScrollPane/2.2.1/script/jquery.jscrollpane.min.js"></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    	
    	<style>
    	    html, body {
                padding: 0;
                margin: 0;
                height:100%;
                width:100%;
                background: #0268b5;
                color: #edf0f7;
                font-family: 'lato light';
            }
            
            body {
                display:flex;
            }
    	    * {
            	box-sizing: border-box;
            	font-family: 'Lato', sans-serif;
            }
            h1,p {
            	margin: 0;
            }
            .subscription-page {
                max-width: 640px;
                width: 100%;
                margin: 0 auto;
                padding: 20px;
                
            }
            .subscription-page p{
                padding: 20px;
                border: 1px solid green;
                color: green;
                background-color: white;
            }
            .subscription-page p.error{
                border: 1px solid red;
                color: red;
            }
            .center-align {
                with: 100%;
                text-align:center;
                padding: 10px;
            }
    
            .line-break{
                clear:both;
            }
    	</style>
    </head>
    <body class="hasJS">
        ${body}
    </body>
    
    <html>`;
    
};


const confrimSignup = async (code, userName) => {
    const params = {
        ClientId: process.env.CLIENT_ID, /* required */
        ConfirmationCode: code, /* required */
        Username: userName, /* required */
    };
    return cognitoidentityserviceprovider.confirmSignUp(params).promise();
};

exports.handler = async function(event, context) {
    let body = '';
    try{
        await confrimSignup(event.confirmationCode, event.userName);
        body = `<div class="subscription-page container center-align"><p class="success">Email Address verified successfully.</p></div>`;
    } catch(error){
        console.log(error);
        if(error.code === "NotAuthorizedException"){
            body = `<div class="subscription-page container center-align"><p class="error">Email Adddress is already verified.</p></div>`;
        } else if(error.code === "UserNotFoundException"){
            body = `<div class="subscription-page container center-align"><p class="error">User not found.</p></div>`;
        }else {
            body = `<div class="subscription-page container center-align"><p class="error">${error.message}</p></div>`;
        }
    }

    let content = generateHtmlTemplate(body);
    context.succeed(content);
};