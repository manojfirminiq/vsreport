exports.handler = function(event, context) {

    var symbolCollectionString = '!“#$%&‘()*+,-./ :;<=>?@[\\]^_`{}|~';
    var html = `<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width">
    <title>Omron</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style>
        @font-face {
    font-family: 'lato light';
    src: url(./../fonts/lato_light.ttf);
}

html, body {
    padding: 0;
    margin: 0;
    height:100%;
    width:100%;
    background: #0268b5;
    color: #edf0f7;
    font-family: 'lato light';
}

body {
    display:flex;
}

#updatePassword {
    margin: auto;
    width: 90%;
    display: flex;
    flex-direction: column;
    max-width: 500px;
}

#updatePassword input, button {
    margin: auto;
    width: 100%;
    -webkit-appearance: none;
    -moz-appearance: none;
    outline: none;
    color: #edf0f7;
    font-weight: 100;
}

.inputWrapper {
    max-width: 450px;
}

::placeholder { 
    color: #edf0f7;
    opacity: 1; 
}

#updatePassword .eye-icon {
    float: right;
    height: 20px;
    width: 20px;
    margin-top: -25px;
    position: relative;
    cursor: pointer;
}

#updatePassword input[type="password"], #updatePassword input[type="text"] {
    font-size: 14px;
    height: 30px;
    background: #0268b5;
    border: 0;
    border-bottom: 1px solid #edf0f7; 
}

#updatePassword input[type="submit"], button {
    font-size: 14px;
    height: 50px;
    border-radius: 4px;
    background: #009e6a;
    width: 70%;
    border: 0;
    margin-bottom: 20px;
}

#updatePassword .error {
    margin-bottom: 20px;
    color: red;
    text-align: center;
    margin-top: 2px;
    font-size: 14px;
}

#updatePassword .success {
    margin-bottom: 20px;
    color: white;
    text-align: center;
    margin-top: 2px;
    font-size: 14px;
}

.passwordCriteria{
    margin-left: 20px;
    font-weight: 100;
    font-size: 13px;
}

.passwordCriteria li {
    line-height: 20px;
}

.header {
    font-size: 24px;
    text-align: center;
    font-weight: 100;
    margin-bottom: 50px;
}
    </style>
</head>
<body>
<!-- <div id="wrapper"> -->
        <form id="updatePassword">
            <p class="header">Password Reset</p>
            <!-- <input type="password" id="resetCode" name="resetCode" style="display: none;" />
            <input type="text" id="emailAddress" name="emailAddress" style="display: none;" /> -->
            <div class="inputWrapper">
                <input type="password" id="password" name="password" placeholder="New password"  minlength="8" autocomplete="new-password" />
                <span toggle="#password" class="fa fa-fw fa-eye  fa-eye-slash eye-icon"></span>
            </div>
            <div class="inputWrapper">
                <input type="password" id="confirmPassword" name="confirmPassword" placeholder="Confirm password"  minlength="8" autocomplete="new-password" style="margin-top: 25px"/>
                <span toggle="#confirmPassword" class="fa fa-fw fa-eye  fa-eye-slash eye-icon"></span>
            </div>
            <span class="error"></span>
            <span class="success"></span>
            <button type="submit" value="Update password">Update password <i class="loader fa fa-circle-o-notch fa-spin" style="font-size:20px"></i></button>
            <ul class="passwordCriteria">
                <li style="list-style-type:none;">Minimum password length must be 8 characters. It can contain:</li>
                <li>Uppercase and Lowercase letters</li>
                <li>Numbers</li>
                <li>Symbols ( ${symbolCollectionString} )</li>
            </ul>
        </form>
    <!-- </div> -->
    <script>
        function getUrlVars(){
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        $('.loader').hide();

        $(".eye-icon").click(function(e) {

            $(this).toggleClass("fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

        $( document ).ready(function() {
            $('#resetCode').val(getUrlVars()["token"]);
            $('#emailAddress').val(getUrlVars()["emailAddress"]);
            document.title = "Omron VitalSight";
        });

        $('#updatePassword').submit(e => {
            e.preventDefault();
            if($('#updatePassword').hasClass("loading")) {
                return;
            }
            $('.error').empty();
            $('.success').empty();
            if( $('#password').val().length === 0 || $('#confirmPassword').val().length === 0 ) {
                $('.error').text("Fields cannot be empty");
                return;
            }
            if(!$('#password').val().match(/^[\\u0021-\\u007E]{8,20}$/g)){
                $('.error').text("The password you entered does not meet the requirements. Please try another password.");
                return;
            }
            if( $('#password').val() !== $('#confirmPassword').val() ) {
                $('.error').text("Password mismatch");
                return;
            }
            $('#updatePassword').addClass("loading");
            $('.loader').show();
            $.ajax({
                url: "${process.env.CHANGE_PASSWORD_URL}",
                dataType: 'json',
                data: JSON.stringify({ 
                    resetCode: getUrlVars()["token"],
                    emailAddress: getUrlVars()["emailAddress"],
                    password: $('#password').val()
                }),
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type': 'application/json' 
                },
                method: "POST",
                success: (data) => {
                    $('.loader').hide();
                    $('#updatePassword').removeClass("loading");
                    if(data.success != true){
                        $('.error').text(data.errorMessage ? data.errorMessage : data.message);
                    } else {
                         $('.success').text(data.message);
                    }
                    // outputs "SUCESSSSS"
                },
                error: (data) => {
                    $('.loader').hide();
                    $('#updatePassword').removeClass("loading");
                    $('.error').text(data.errorMessage);
                }
            });
        });
    </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"></link>
    </body>
</html>`;

    context.succeed(html);
};