const crypto = require('crypto');
const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();
var lambda = new AWS.Lambda({
    region: process.env.REGION //change to your region
});
const CONSTANTS = require('./CONSTANTS.js');

const saveItem = (item, TABLE_NAME) => {
    const params = {
      TableName: TABLE_NAME,
      Item: item
    };
    return dbClient.put(params).promise();
};

const removeEmptyString = obj => {
    for (let o in obj) {
      if (typeof obj[o] === "object" && obj[o] !== null) {
        // Recursively remove empty string
        obj[o] = removeEmptyString(obj[o]);
      }
      obj[o] =
        typeof obj[o] === "boolean" ||
        Number.isInteger(obj[o]) ||
        (obj[o] && obj[o].length !== 0)
          ? obj[o]
          : null;
    }
    return obj;
  };


const generateUserId = (mrn) => {
    let userID = crypto.createHash('sha256').update(mrn).digest("hex");
    userID = crypto.createHash('sha256').update(userID).digest("hex");
    return userID;
};

const getNpIDFromDoctorProfile = async (doctorID, table) => {
    console.log("DOCTOR_PROFILE_TABLE")
    const params = {
        TableName : table,
        KeyConditionExpression: "#userID = :userID",
        ExpressionAttributeValues: {
            ':userID': doctorID
        },
        ExpressionAttributeNames: {
            '#userID': 'userID'
        }
    };
    const { Count, Items } = await dbClient.query(params).promise();
    console.log("Items==>", Items)
    return Count > 0 ? Items : false;
};

const getDbIdenitifierFromHospital = async (hospitalID) => {
    let params = {
        "TableName": CONSTANTS.HOSPITAL_MASTER_TABLE,
        KeyConditionExpression: "#hospitalID = :hospitalID",
        ExpressionAttributeValues: {
            ':hospitalID': hospitalID
        },
        ExpressionAttributeNames: {
            '#hospitalID': 'hospitalID'
        }
    };
    const { Count, Items } = await dbClient.query(params).promise();

    return Count > 0 ? Items : false;
};

const saveOrderProcessingDetail = async (patientName,patientAddress,patientContact,patientMRN,deviceType,hospitalID,doctorID, email, dob, threshold, gender) => {
    const currentDate = new Date().getTime();
    let userID = await generateUserId(patientMRN.toLowerCase());
    console.log("hospitalID==>", hospitalID)
    let hospitalDetail = await getDbIdenitifierFromHospital(hospitalID);
    console.log("getDbIdenitifierFromHospital==>", hospitalDetail)
    let docProfileTable = CONSTANTS.STAGE + '_' + CONSTANTS.RPM + '_' + hospitalDetail[0].dbIdentifier + '_' +CONSTANTS.DOCTOR_PROFILE_TABLE;
    let npIDDetails = await getNpIDFromDoctorProfile(doctorID, docProfileTable);
    if(npIDDetails && hospitalDetail) {
        let clinicalInfo=[];
        let bpModelNo='';
        let weightModelNo='';
        for(let device of deviceType){
            if(device.type == CONSTANTS.BP){
                clinicalInfo.push({ code : 1, description: CONSTANTS.OMRON+" "+device.size+" "+CONSTANTS.BP })
                if(device.size.toLowerCase()==CONSTANTS.LARGE){
                    bpModelNo=CONSTANTS.BP_MODELS[1];
                }else{
                    bpModelNo=CONSTANTS.BP_MODELS[0];
                }    
            } else {
                clinicalInfo.push({ code : 2, description: CONSTANTS.OMRON+" "+device.size+" "+CONSTANTS.SCALE })
                if(device.size.toLowerCase()==CONSTANTS.LARGE){
                    weightModelNo=CONSTANTS.WEIGHT_MODELS[1];
                }else{
                    weightModelNo=CONSTANTS.WEIGHT_MODELS[0];
                }
            }
        }
        clinicalInfo.push({ code : 3,  description: CONSTANTS.HUB })
        let item = {
            userID: userID,
            orderID_createdDate: hospitalDetail[0].orderIdentifier+currentDate + '_' + currentDate,
            modifiedDate: currentDate,
            createdDate: currentDate,
            attributes: {},
            processStatus:1,
            orderDate: currentDate,
            orderTZ:0 //local time, hence keeping it 0
        };
        let name = patientName.split(' ');
        let attributes = {
            patientIdentifier:'',
            bpModelNo:bpModelNo,
            scaleModelNo:weightModelNo,
            transactionDateTime:currentDate,
            applicationOrderID:item.orderID_createdDate,
            collectionDateTime:currentDate,
            eventDateTime:currentDate,
            status:'',
            clinicalInfo: clinicalInfo,
            mrNumberType:CONSTANTS.MRN,
            firstName: name[0],
            middleName:'',
            lastName: name && name.length>1 ? name[name.length-1]:'',
            name:patientName,
            dateOfBirth: dob,
            gender:gender?gender:'',
            homePhone: patientContact,
            officePhone:'',
            mobilePhone:'',
            mrNumber: patientMRN,
            ehrID: hospitalID,
            address: patientAddress,
            npID: npIDDetails[0].npID,
            ehrEmailAddress:[],
            notes:[],
            threshold:threshold,
            weightPrefUnit:CONSTANTS.WEIGHT_UNIT,
            goals:{
                sys: 0,
                dia: 0,
                pulse: 0,
                weight:0
            },
            language:'',
            race:'',
            maritalStatus:'',
            visit:{
                visitNumber:'',
                accountNumber:'',
                visitDateTime: ''
            },
            diagnosis:[],
            code:'',
            codeSet:'',
            procedure:{
                code:'',
                codeSet:''
            },
            provider:{
                ID:npIDDetails[0].npID,
                type:CONSTANTS.NPI,
                firstName:npIDDetails[0].attributes.name.split(' ')[0],
                lastName:npIDDetails[0].attributes.name.split(' ')[1]
            },
            facilityCode:''
        }
        attributes.ehrEmailAddress.push(email);

        item.attributes = attributes;
        item.attributes.isDoctorLinked = 1;
        let tableName = CONSTANTS.STAGE + '_' + CONSTANTS.RPM + '_' + hospitalDetail[0].dbIdentifier + '_' + CONSTANTS.ORDER_PROCESSING;
        await saveItem(item, tableName);
        return Promise.resolve({"success": true, "message": CONSTANTS.MESSAGES.SUCCESS });
    } else {
       if (!npIDDetails) return Promise.resolve({"success": false, errorCode: 'NP_ID_NOT_FOUND', "message": CONSTANTS.MESSAGES.NP_ID_NOT_FOUND });
       if (!hospitalDetail) return Promise.resolve({"success": false, errorCode: 'DB_IDENTIFIER_NOT_FOUND', "message": CONSTANTS.MESSAGES.DB_IDENTIFIER_NOT_FOUND });
    }
};

const applyValidation = async ({doctorID, hospitalID, body:{ patientName,patientAddress,patientContact,patientMRN, patientDOB, patientEmail, device, patientThreshold, gender}}) => {
    try {
        if( !patientName ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.NAME_KEY_REQ, errorCode: 'REQUIRED_PARAMETER'});
        if( !patientAddress ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.ADDRESS_KEY_REQ, errorCode: 'REQUIRED_PARAMETER'});
        if( !patientContact ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.CONTACT_KEY_REQ, errorCode: 'REQUIRED_PARAMETER'});
        if( !patientMRN ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.MRN_KEY_REQ, errorCode: 'REQUIRED_PARAMETER'});
        if( !device ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.DEVICE_KEY_REQ, errorCode: 'REQUIRED_PARAMETER'});
        if( !doctorID ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.DOCTOR_ID_KEY_REQ, errorCode: 'REQUIRED_PARAMETER'});
        if( !hospitalID ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.HOSPITAL_ID_KEY_REQ, errorCode: 'REQUIRED_PARAMETER'});
        return await saveOrderProcessingDetail(patientName,patientAddress,patientContact,patientMRN,device,hospitalID,doctorID, patientEmail, patientDOB, patientThreshold, gender)
    } catch (error) {
        console.log(error);
        return Promise.resolve({"success": false, "message": error.message, errorCode: 'INTERNAL_SERVER_ERROR'});
    }
};

exports.handler = async (event, context, callback) => {
    const response = await applyValidation(event);
    callback(null, response);
};