
exports.MESSAGES = {
    SUCCESS: "SUCCESSFULLY SUBMIT DETAILS",
    NP_ID_NOT_FOUND: "npID not found in Doctor Profile Table",
    DB_IDENTIFIER_NOT_FOUND: `dbIdentifier not found against hospital ID is required`
};
exports.ORDER_PROCESSING = process.env.ORDER_PROCESSING;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = 'rpm';
exports.ORDER_PROCESSING = 'order_processing';
exports.WEIGHT_UNIT = 'lbs';
exports.BP = 'BP';
exports.HUB = 'Hub';
exports.OMRON = "Omron";
exports.SCALE = "Scale"
exports.MRN = 'MRN';
exports.NPI = 'NPI',
exports.LARGE = 'large';
exports.BP_MODELS=[
    'BP7250',
    'HEM-9210T'
];
exports.WEIGHT_MODELS=[
    'BCM500',
    'HN-290T'
];
exports.VALIDATION_MESSAGES = {
    NAME_KEY_REQ: `Patient Name is required` ,
    ADDRESS_KEY_REQ: `Patient Address is required` ,
    CONTACT_KEY_REQ: `Patient Contact is required` ,
    MRN_KEY_REQ: 'MRN Number is required',
    DEVICE_KEY_REQ: `Device Type is required` ,
    HOSPITAL_ID_KEY_REQ: `Hospital ID is required`,
    DOCTOR_ID_KEY_REQ: `Doctor ID is required`
};