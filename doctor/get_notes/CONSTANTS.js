exports.ERRORS = {
    INTERNAL_SERVER_ERROR: {
        CODE: "INTERNAL_SERVER_ERROR",
        MESSAGE: "Internal Server Error"
    },
    USER_ID_REQ: {
        CODE: "USER_ID_REQ",
        MESSAGE: "User id required"
    },
    HOSPITAL_ID_REQ: {
        CODE: "HOSPITAL_ID_REQ",
        MESSAGE: "Hospital id required"
    },
    NOTES_NOT_FOUND: {
        CODE: "NOTES_NOT_FOUND",
        MESSAGE: "NOTES not found"
    },
    HOSPITAL_ID_INVALID: {
        CODE: "HOSPITAL_ID_INVALID",
        MESSAGE: "Hospital Id is invalid"
    }
};
exports.RPM = 'rpm';
exports.NOTES_TABLE = process.env.NOTES_TABLE;
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;