const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');

const getNotes = async ({doctorId, userId, hospitalID, start, end}) => {
     try{
        if(!userId){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_ID_REQ.CODE});
        }
        
        if(!hospitalID) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE});
        let hospitalTable = await DB.verifyHospital(hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }
        let Items;
        if(!start){
            Items = await DB.getNotes(hospitalTable, doctorId, userId);
            if(!Items) {
                return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.NOTES_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.NOTES_NOT_FOUND.CODE});
            }   
        }else{
            Items = await DB.getNotesStartDate(hospitalTable, doctorId, userId, start, end );
            if(!Items) {
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.NOTES_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.NOTES_NOT_FOUND.CODE});
            }   
        }
        
        let response = {
            success: true,
            data: Items
        };        
        return Promise.resolve(response);
    } catch(error){
        return Promise.resolve({
            "success": false,
            "message": CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
            "errorCode": CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
        });
    }
};

exports.handler = async (event, context, callback) => {
    let response = await getNotes(event);
    callback(null, response);
};

