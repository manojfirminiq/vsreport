const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_PATIENT = CONSTANTS.DOCTOR_PATIENT;
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;


let verifyHospital = async (hospitalId) => {
    let paramsTable = {
        TableName : HOSPITAL_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalId
        }
    };
    const {Items} = await dbClient.query(paramsTable).promise();
    if(Items.length == 0){
        return false;
    }else{
         return process.env.STAGE +`_rpm_` + Items[0].dbIdentifier + `_`+ DOCTOR_PATIENT;
    }
};

const getPatient = async (doctorId, patientId, hospitalTable) => {
    console.log("getPatient====>", doctorId, patientId, hospitalTable)
    let params = {
        TableName : hospitalTable,
        KeyConditionExpression : "#doctorId = :doctorId AND #userId = :userId",
        ExpressionAttributeValues: {
            ':doctorId': doctorId,
            ':userId': patientId
        },
        ExpressionAttributeNames: {
            '#doctorId': 'doctorID',
            '#userId': 'userID'
        }
    };
    
    return dbClient.query(params).promise();
};



const updateThreshold = async (threshold, hospitalTable) => {
    console.log("updateThreshold====>",threshold, hospitalTable )
    let params = {
        TableName: hospitalTable,
        Item: threshold
    };
    return dbClient.put(params).promise();
};

module.exports.verifyHospital = verifyHospital;
module.exports.getPatient = getPatient;
module.exports.updateThreshold = updateThreshold;
