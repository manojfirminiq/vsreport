const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');

const applyValidation = async ({doctorId, hospitalID, body: {userId, diaLow, diaHigh, sysLow, sysHigh, weight24hr, weight72hr} }) => {
    try {
        if( !userId ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE});
        if(!diaLow ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.DIA_LOW_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.DIA_LOW_REQUIRED.CODE});
        if(!diaHigh ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.DIA_HIGH_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.DIA_HIGH_REQUIRED.CODE});
        if(!sysLow ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.SYS_LOW_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.SYS_LOW_REQUIRED.CODE});
        if(!sysHigh ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.SYS_HIGH_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.SYS_HIGH_REQUIRED.CODE});
        if(!hospitalID) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE});
        let hospitalTable = await DB.verifyHospital(hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }
        let {Count, Items} = await DB.getPatient(doctorId, userId, hospitalTable);
        if(Count === 0) {
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_NOT_FOUND.CODE});
        }
        
        Items[0].attributes.threshold.diaLow = diaLow;
        Items[0].attributes.threshold.diaHigh = diaHigh;
        Items[0].attributes.threshold.sysLow = sysLow;
        Items[0].attributes.threshold.sysHigh = sysHigh;
        //Items[0].attributes.threshold.weightLowerLimit = weightLowerLimit;
        //Items[0].attributes.threshold.weightUpperLimit = weightUpperLimit;
        Items[0].attributes.threshold.weight24hr = weight24hr ? weight24hr : 0;
        Items[0].attributes.threshold.weight72hr = weight72hr ? weight72hr : 0;
        
        await DB.updateThreshold(Items[0], hospitalTable);
        
        return Promise.resolve({
            success: true
        });
    } catch (error) {
        console.log(error);
        return Promise.resolve({
            success: false,
            errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
            message: error
        });
    }
};

exports.handler = async (event, context, callback) => {
    console.log("event===>", event)
    
    const response = await applyValidation(event);
    callback(null, response);
};