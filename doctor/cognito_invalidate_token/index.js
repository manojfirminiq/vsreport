const crypto = require('crypto');
const AWS = require('aws-sdk');
const dbClient = new AWS.DynamoDB.DocumentClient();
AWS.config.update({region: process.env.REGION});
const USER_POOL_ID = process.env.USERPOOLID;
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const USER_NOT_FOUND = 'User not found';
const EMAIL_REQ = 'Please provide a valid emailAddress';

const cognitoInvalidateToken = async (emailAddress) =>{
    var params = {
        UserPoolId: USER_POOL_ID, /* required */
        Username: emailAddress /* required */
    };
    return new Promise((resolve, reject) =>{
        cognitoidentityserviceprovider.adminUserGlobalSignOut(params, (signout_err, data) => { //data ={}
            console.log(signout_err, 'signout_err');
            if (signout_err)
                resolve({ "success": "False", "error": signout_err.message });
            else
                resolve({ "success": "True", "message": "Inavalidated all tokens" });
        });
    });
};
const applyValidation = async ({ emailAddress }) => {
    try {
        emailAddress = emailAddress.toLowerCase();
        if( !emailAddress ) return Promise.resolve({"success": "False", "error": EMAIL_REQ});
        return await cognitoInvalidateToken(emailAddress);
    } catch(err) {
        return Promise.resolve({"success": "False", "error": err.message});
    }
};

exports.handler = async (event, context, callback) => {
    const response = await applyValidation(event);
    callback(null, response);
};