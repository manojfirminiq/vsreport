const { uuid } = require('uuidv4');
const CONSTANTS = require('./CONSTANTS.js');
const DB= require('./dynamodb');
const COGNITO = require('./cognito');

const generateUserId = () => {
    return uuid();
};


const signup = async(params) => {
    try {
        let hospitalTable = await DB.getHospital(params.hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.HOSPITAL_ID_INVALID, "errorCode": CONSTANTS.ERROR_CODES.HOSPITAL_ID_INVALID});
        }
        let npCounter = await DB.getNpIdCounter(hospitalTable, String(params.npID));
        if(npCounter && npCounter > 0){
            return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.NPID_ALREADY_EXIST, "errorCode": CONSTANTS.ERROR_CODES.NPID_ALREADY_EXIST});
        }
        params.app = CONSTANTS.APP;
        await COGNITO.addUserToPool(params);
        await DB.postUserData(params, hospitalTable);

        return Promise.resolve({"success": true, "message": CONSTANTS.MESSAGES.SUCCESS});
    }  catch (error){
        console.log("Err", error);
        return Promise.reject(error);
    }
};

const applyValidation = async ({ emailAddress, password, hospitalID, npID, ...rest }) => {
    try {

        if( !emailAddress ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.EMAIL_REQ, "errorCode": CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER});
        if( !password ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.PASSWORD_REQ, "errorCode": CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER});
        if( !hospitalID ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.HOSPITAL_REQ, "errorCode": CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER});
        if( !npID ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.NPID_REQ, "errorCode": CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER});
        
        if( password.trim().length < CONSTANTS.PASSWORD_LENGTH ){
            return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.PASSWORD_REQ, "errorCode": CONSTANTS.ERROR_CODES.INVALID_PASSWORD});
        }

        let {Count} = await DB.getHospital(hospitalID);
        if(Count === 0) {
            return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.HOSPITAL_REQ, "errorCode": "HOSPITAL_NOT_FOUND"});
        }

        emailAddress = emailAddress.toLowerCase();
        const userId = generateUserId();
        let response = await signup({ emailAddress, password, userId,hospitalID, npID, ...rest });
        return response;

    } catch (error) {
        console.log(error, "in catch");
        return Promise.resolve({"success": false, "message": error.message, errorCode: error.errorCode ? error.errorCode : 'INTERNAL_SERVER_ERROR'});
    }
};

exports.handler = async (event, context, callback) => {
    console.log("event==> ",event);
    if(event.body) {
        console.log(event.sourceIP);
        event.body.sourceIP = event.sourceIP;
        event = event.body;
    }
    const response = await applyValidation(event);
    callback(null, response);
};