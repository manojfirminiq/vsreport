const AWS = require('aws-sdk');
const AmazonCognitoIdentity =  new AWS.CognitoIdentityServiceProvider();
const CONSTANTS = require('./CONSTANTS')

const addUserToPool = async ({emailAddress, password, userId, app, hospitalID}) =>{
    let attributeList = [];
    
    attributeList.push({ 'Name' : 'custom:appName', 'Value': app });
    attributeList.push({ 'Name' : 'custom:userId', 'Value':  userId});
    attributeList.push({'Name' : 'custom:hospitalID', 'Value':  hospitalID});
    const params = {ClientId: process.env.CLIENT_ID,
        Username: emailAddress,
        Password: password,
        UserAttributes: attributeList
    };
    
    return new Promise((resolve, reject) => {
        AmazonCognitoIdentity.signUp(params, (err, result)=>{
            
            if(err){
                console.log(err.code, CONSTANTS.COGNITO_ERROR.USER_ALREADY_EXIST, 'errorsssss');
                const response = {
                    success: false,
                    errorCode: "",
                    message: ""
                };
                
                switch(err.code) {
                    case (CONSTANTS.COGNITO_ERROR.USER_ALREADY_EXISTS):
                        response.errorCode = CONSTANTS.ERROR_CODES.USER_ALREADY_EXISTS;
                        break;
                    case (CONSTANTS.COGNITO_ERROR.PASSWORD_CONSTRAINT):
                        response.errorCode = CONSTANTS.ERROR_CODES.INVALID_PASSWORD;
                        break;
                    default:
                        response.errorCode = CONSTANTS.ERROR_CODES.INTERNAL_SERVER_ERROR;
                        break;
                }
                response.message = err.message;
                console.log(response)
                reject(response);
            } else {
                resolve(result);
            }
        });
    });
};

module.exports.addUserToPool = addUserToPool;