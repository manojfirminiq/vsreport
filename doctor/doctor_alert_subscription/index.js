const DB = require("./dynamodb");
const CONSTANTS = require("./CONSTANTS");

const subscribeDoctor = async (Items, hospitalTable) => {
  if (Items.subscriptionFlag) {
    return Promise.resolve({
      success: false,
      message: CONSTANTS.ERRORS.DOCTOR_ALREADY_SUBSCRIBED.MESSAGE,
      errorCode: CONSTANTS.ERRORS.DOCTOR_ALREADY_SUBSCRIBED.CODE
    });
  } else {
    Items.subscriptionFlag = CONSTANTS.SUBSCRIPTION.SUBSCRIBEFLAG;
    await DB.updateDoctorData(Items, hospitalTable.DOCTOR_TABLE);
    return Promise.resolve({
      success: true,
      message: CONSTANTS.SUBSCRIBE_MSG
    });
  }
};

const unSubscribeDoctor = async (Items, hospitalTable) => {
  if (Items.subscriptionFlag) {
    Items.subscriptionFlag = CONSTANTS.SUBSCRIPTION.UNSUBSCRIBEFLAG;
    await DB.updateDoctorData(Items, hospitalTable.DOCTOR_TABLE);
    return Promise.resolve({
      success: true,
      message: CONSTANTS.UNSUBSCRIBE_MSG
    });
  } else {
    return Promise.resolve({
      success: false,
      message: CONSTANTS.ERRORS.DOCTOR_NOT_SUBSCRIBED.MESSAGE,
      errorCode: CONSTANTS.ERRORS.DOCTOR_NOT_SUBSCRIBED.CODE
    });
  }
};

const applyValidation = async ({ doctorId, hospitalID, status }) => {
  try {
    if (!status) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.STATUS_REQ.MESSAGE,
        errorCode: CONSTANTS.ERRORS.STATUS_REQ.CODE
      });
    }

    if (!hospitalID) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
        errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
      });
    }

    let hospitalTable = await DB.getHospital(hospitalID);
    if (!hospitalTable) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
        errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
      });
    }

    if (
      status == CONSTANTS.SUBSCRIPTION.SUBSCRIBE ||
      status == CONSTANTS.SUBSCRIPTION.UNSUBSCRIBE
    ) {
      let { Count, Items } = await DB.getDoctorData(
        doctorId,
        hospitalTable.DOCTOR_TABLE
      );

      if (Count > 0 && Items) {
        if (status == CONSTANTS.SUBSCRIPTION.SUBSCRIBE) {
          return await subscribeDoctor(Items[0], hospitalTable);
        } else if (status == CONSTANTS.SUBSCRIPTION.UNSUBSCRIBE) {
          return await unSubscribeDoctor(Items[0], hospitalTable);
        }
      } else {
        return Promise.resolve({
          success: false,
          message: CONSTANTS.ERRORS.DOCTOR_ID_NOT_FOUND.MESSAGE,
          errorCode: CONSTANTS.ERRORS.DOCTOR_ID_NOT_FOUND.CODE
        });
      }
    } else {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.STATUS_INVALID.MESSAGE,
        errorCode: CONSTANTS.ERRORS.STATUS_INVALID.CODE
      });
    }
  } catch (error) {
    return Promise.resolve({
      success: false,
      message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
      errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
    });
  }
};

exports.handler = async (event, context, callback) => {
  console.log(event);
  event = { ...event, ...event.body };
  delete event.body;
  let response = await applyValidation(event);
  callback(null, response);
};
