const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require("./CONSTANTS.js");
const DOCTOR_TABLE_NAME = CONSTANTS.DOCTOR_TABLE_NAME;
const HOSPITAL_TABLE_NAME = CONSTANTS.HOSPITAL_TABLE_NAME;
const STAGE = CONSTANTS.STAGE;
const RPM = CONSTANTS.RPM;

const getHospital = async hospitalId => {
  const params = {
    TableName: HOSPITAL_TABLE_NAME,
    KeyConditionExpression: "#uid = :id",
    ExpressionAttributeValues: {
      ":id": hospitalId
    },
    ExpressionAttributeNames: {
      "#uid": "hospitalID"
    }
  };

  let { Count, Items } = await dbClient.query(params).promise();
  if (Count == 0) {
    return false;
  } else {
    return {
      Items: Items[0],
      DOCTOR_TABLE:
        STAGE + RPM + Items[0].dbIdentifier + `_` + DOCTOR_TABLE_NAME
    };
  }
};

const getDoctorData = async (doctorID, DOCTOR_TABLE) => {
  const params = {
    TableName: DOCTOR_TABLE,
    KeyConditionExpression: "#uid = :id",
    ExpressionAttributeValues: {
      ":id": doctorID
    },
    ExpressionAttributeNames: {
      "#uid": "userID"
    }
  };
  return dbClient.query(params).promise();
};

const updateDoctorData = async (Items, DOCTOR_TABLE) => {
  let params = {
    TableName: DOCTOR_TABLE,
    Item: Items
  };
  return dbClient.put(params).promise();
};

exports.getDoctorData = getDoctorData;
exports.getHospital = getHospital;
exports.updateDoctorData = updateDoctorData;
