exports.constants = {
  tenMinutes: 10 * 60 * 1000,
  insert: "INSERT",
  modify: "MODIFY",
  remove: "REMOVE"
};

exports.BP_READINGS_TABLE = 'bp_readings';
exports.DOCTOR_BP_AVERAGE_TABLE = 'doctor_bp_average';
exports.DOCTOR_PATIENT_TABLE = 'doctor_patient';
exports.NON_BP_READINGS_TABLE = 'non_bp_readings';
exports.RPM_USER_TABLE = 'ehr_user';
exports.MASTER_HOSPITAL_TABLE = process.env.MASTER_HOSPITAL_TABLE;

exports.attributes = {
    "lastNinetyDays": {
      "diastolic": 0,
      "highest": {
        "diastolic": 0,
        "systolic": 0
      },
      "lowest": {
        "diastolic": 0,
        "systolic": 0
      },
      "numberOfReadings": 0,
      "pulse": 0,
      "systolic": 0,
      "readings":[]
    },
    "lastSevenDays": {
      "diastolic": 0,
      "highest": {
        "diastolic": 0,
        "systolic": 0
      },
      "lowest": {
        "diastolic": 0,
        "systolic": 0
      },
      "numberOfReadings": 0,
      "pulse": 0,
      "systolic": 0,
      "readings":[]
    },
    "lastThirtyDays": {
      "diastolic": 0,
      "highest": {
        "diastolic": 0,
        "systolic": 0
      },
      "lowest": {
        "diastolic": 0,
        "systolic": 0
      },
      "numberOfReadings": 0,
      "pulse": 0,
      "systolic": 0,
      "readings":[]
    },
    "prevNinetyDays": {
      "diastolic": 0,
      "highest": {
        "diastolic": 0,
        "systolic": 0
      },
      "lowest": {
        "diastolic": 0,
        "systolic": 0
      },
      "numberOfReadings": 0,
      "pulse": 0,
      "systolic": 0,
      "readings":[]
    },
    "prevSevenDays": {
      "diastolic": 0,
      "highest": {
        "diastolic": 0,
        "systolic": 0
      },
      "lowest": {
        "diastolic": 0,
        "systolic": 0
      },
      "numberOfReadings": 0,
      "pulse": 0,
      "systolic": 0,
      "readings":[]
    },
    "prevThirtyDays": {
      "diastolic": 0,
      "highest": {
        "diastolic": 0,
        "systolic": 0
      },
      "lowest": {
        "diastolic": 0,
        "systolic": 0
      },
      "numberOfReadings": 0,
      "pulse": 0,
      "systolic": 0,
      "readings":[]
    },
    "prevYtoD": {
      "diastolic": 0,
      "highest": {
        "diastolic": 0,
        "systolic": 0
      },
      "lowest": {
        "diastolic": 0,
        "systolic": 0
      },
      "numberOfReadings": 0,
      "pulse": 0,
      "systolic": 0,
      "readings":[]
    },
    "YtoD": {
      "diastolic": 0,
      "highest": {
        "diastolic": 0,
        "systolic": 0
      },
      "lowest": {
        "diastolic": 0,
        "systolic": 0
      },
      "numberOfReadings": 0,
      "pulse": 0,
      "systolic": 0,
      "readings":[]
    }
  };

  exports.RPM = "rpm";
  exports.HODES = "hodes";
