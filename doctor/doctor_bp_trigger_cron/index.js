const AWS = require("aws-sdk");
const CONSTANTS = require("CONSTANTS");
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

let fetchWeight = async (userID,dbIdentifier) => {
    let params = {
        TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.NON_BP_READINGS_TABLE}`,
        KeyConditionExpression:"userId_type_deviceLocalName = :id",
        ExpressionAttributeValues:{
            ":id": userID+'_weight_Manual'
        },
        Limit:10,
        ScanIndexForward: false
    };
    let {Items, Count} = await dbClient.query(params).promise();
    return Count?Items:false;
}

let getImeiDetails = async (imei, dbIdentifier) => { 
        try {
            let queryParams = {
                TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.RPM_USER_TABLE}`,
                IndexName:"imei-index",
                KeyConditionExpression:"imei = :imeikey",  
                ExpressionAttributeValues:{
                    ":imeikey": imei
                }
            };
            return dbClient.query(queryParams).promise();
        } catch(e) {
            throw e;
        }
    }

let fetchAllPatients = async (dbIdentifier) => { 
    let params = {
        TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.DOCTOR_BP_AVERAGE_TABLE}`,
        ScanIndexForward: false
    };
    
    
    let patients = [];
    let lastPageKey;
    
    while(1){
        if(lastPageKey){
            params.ExclusiveStartKey = lastPageKey;
        }
        let {Items, Count, LastEvaluatedKey} = await dbClient.scan(params).promise();
        if(Count){
            patients = patients.concat(Items);
        }
        if(LastEvaluatedKey){
            lastPageKey = LastEvaluatedKey
        } else{
            break;
        }
    }
    return patients.length?patients:false
}

let fetchDoctorPatient = async (userID, dbIdentifier) => {
    let params = {
        TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.DOCTOR_PATIENT_TABLE}`,
        IndexName: 'userID-doctorID-index',
        KeyConditionExpression:"userID = :id",
        ExpressionAttributeValues:{
            ":id": userID
        },
        ScanIndexForward: false
    };
    let {Items, Count} = await dbClient.query(params).promise();
    return Count?Items[0]:false;
}


let fetchHighLowAvgReading = (record) => {
    if(record.numberOfReadings>0){
        record.systolic = Math.round(record.systolic/record.numberOfReadings);
        record.diastolic = Math.round(record.diastolic/record.numberOfReadings);
        record.pulse = Math.round(record.pulse/record.numberOfReadings);
        
        record.readings.sort(function(a, b){return a.systolic - b.systolic})        
        record.highest.systolic = record.readings[record.readings.length-1].systolic;
        record.lowest.systolic = record.readings[0].systolic;
        
        record.readings.sort(function(a, b){return a.diastolic - b.diastolic})        
        record.highest.diastolic = record.readings[record.readings.length-1].diastolic;
        record.lowest.diastolic = record.readings[0].diastolic;
    }
    delete record.readings;
    return record;
}

let fetchLastTen = async (userID, dbIdentifier) => {
    let params = {
        TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.BP_READINGS_TABLE}`,
        KeyConditionExpression: "userID = :id",
        ExpressionAttributeValues: {
            ':id': userID
        },
        Limit:10,
        ScanIndexForward: false
    };
    
    let {Items, Count} = await dbClient.query(params).promise();
    if(Count){
        Items.sort(function(a,b){return a.measurementDate-b.measurementDate});
    }
    return Count?Items:false;
}

let lastTenAvg = (records, weight) => {
    let avg={
        systolic:0,
        diastolic:0,
        pulse:0,
        irregularHeartBeat:records[records.length-1].attributes.irregularHB,
        lastReportedDate:parseInt(records[records.length-1].measurementDate),
        weight:weight && weight.attributes?weight.attributes.weight:0
    }
    for(let record of records){
        avg.systolic+=parseInt(record.attributes.systolic)
        avg.diastolic+=parseInt(record.attributes.diastolic)
        avg.pulse+=parseInt(record.attributes.pulse)
    }
    avg.systolic = Math.round(avg.systolic/records.length) 
    avg.diastolic = Math.round(avg.diastolic/records.length) 
    avg.pulse = Math.round(avg.pulse/records.length)

    if(weight && weight.measurementDate && weight.measurementDate > avg.lastReportedDate) {
        avg.lastReportedDate = weight.measurementDate;
    }

    return avg;
}

let formatRecords = async (bpReadings, userID, dbIdentifier) => {
    let attributes = JSON.parse(JSON.stringify(CONSTANTS.attributes));
    
    for(let reading of bpReadings){
        let startOfTheDay = new Date(new Date().setHours(0,0,0,0));
        let readingDate = parseInt(reading.measurementDate)+(parseInt(reading.attributes.timeZone)*1000);
        
        let lastNinetyDays = new Date(new Date().setHours(0,0,0,0)).setDate(startOfTheDay.getDate()-89);
        let lastSevenDays = new Date(new Date().setHours(0,0,0,0)).setDate(startOfTheDay.getDate()-6);
        let lastThirtyDays = new Date(new Date().setHours(0,0,0,0)).setDate(startOfTheDay.getDate()-29);
        
        let prevNinetyDays = new Date(new Date().setHours(0,0,0,0)).setDate(startOfTheDay.getDate()-179);
        let prevSevenDays = new Date(new Date().setHours(0,0,0,0)).setDate(startOfTheDay.getDate()-13);
        let prevThirtyDays = new Date(new Date().setHours(0,0,0,0)).setDate(startOfTheDay.getDate()-59);
        
        let yToD = new Date(new Date().getFullYear(), 0, 1).getTime();
        let prevYToD = new Date(new Date().getFullYear()-1, 0, 1).getTime();
        let lastYearThisDate = new Date(new Date().setHours(0,0,0,0)).setDate(startOfTheDay.getDate()-365);
        
        if(readingDate>=lastNinetyDays){
            attributes.lastNinetyDays.numberOfReadings++;
            attributes.lastNinetyDays.diastolic+=parseInt(reading.attributes.diastolic)
            attributes.lastNinetyDays.systolic+=parseInt(reading.attributes.systolic)
            attributes.lastNinetyDays.pulse+=parseInt(reading.attributes.pulse)
            attributes.lastNinetyDays.readings.push({
                systolic:parseInt(reading.attributes.systolic), 
                diastolic: parseInt(reading.attributes.diastolic)
            })
        } else if(readingDate>=prevNinetyDays){
            attributes.prevNinetyDays.numberOfReadings++;
            attributes.prevNinetyDays.diastolic+=parseInt(reading.attributes.diastolic)
            attributes.prevNinetyDays.systolic+=parseInt(reading.attributes.systolic)
            attributes.prevNinetyDays.pulse+=parseInt(reading.attributes.pulse)
            attributes.prevNinetyDays.readings.push({
                systolic:parseInt(reading.attributes.systolic), 
                diastolic: parseInt(reading.attributes.diastolic)
            })
        }
        
        if(readingDate>=lastSevenDays){
            attributes.lastSevenDays.numberOfReadings++;
            attributes.lastSevenDays.diastolic+=parseInt(reading.attributes.diastolic)
            attributes.lastSevenDays.systolic+=parseInt(reading.attributes.systolic)
            attributes.lastSevenDays.pulse+=parseInt(reading.attributes.pulse)
            attributes.lastSevenDays.readings.push({
                systolic:parseInt(reading.attributes.systolic), 
                diastolic: parseInt(reading.attributes.diastolic)
            })
        } else if(readingDate>=prevSevenDays){
            attributes.prevSevenDays.numberOfReadings++;
            attributes.prevSevenDays.diastolic+=parseInt(reading.attributes.diastolic)
            attributes.prevSevenDays.systolic+=parseInt(reading.attributes.systolic)
            attributes.prevSevenDays.pulse+=parseInt(reading.attributes.pulse)
            attributes.prevSevenDays.readings.push({
                systolic:parseInt(reading.attributes.systolic), 
                diastolic: parseInt(reading.attributes.diastolic)
            })
        }
        
        if(readingDate>=lastThirtyDays){
            attributes.lastThirtyDays.numberOfReadings++;
            attributes.lastThirtyDays.diastolic+=parseInt(reading.attributes.diastolic)
            attributes.lastThirtyDays.systolic+=parseInt(reading.attributes.systolic)
            attributes.lastThirtyDays.pulse+=parseInt(reading.attributes.pulse)
            attributes.lastThirtyDays.readings.push({
                systolic:parseInt(reading.attributes.systolic), 
                diastolic: parseInt(reading.attributes.diastolic)
            })
        } else if(readingDate>=prevThirtyDays){
            attributes.prevThirtyDays.numberOfReadings++;
            attributes.prevThirtyDays.diastolic+=parseInt(reading.attributes.diastolic)
            attributes.prevThirtyDays.systolic+=parseInt(reading.attributes.systolic)
            attributes.prevThirtyDays.pulse+=parseInt(reading.attributes.pulse)
            attributes.prevThirtyDays.readings.push({
                systolic:parseInt(reading.attributes.systolic), 
                diastolic: parseInt(reading.attributes.diastolic)
            })
        }
        
        if(readingDate>=yToD){
            attributes.YtoD.numberOfReadings++;
            attributes.YtoD.diastolic+=parseInt(reading.attributes.diastolic)
            attributes.YtoD.systolic+=parseInt(reading.attributes.systolic)
            attributes.YtoD.pulse+=parseInt(reading.attributes.pulse)
            attributes.YtoD.readings.push({
                systolic:parseInt(reading.attributes.systolic), 
                diastolic: parseInt(reading.attributes.diastolic)
            })
        } else if(readingDate>=prevYToD && readingDate<lastYearThisDate){
            attributes.prevYtoD.numberOfReadings++;
            attributes.prevYtoD.diastolic+=parseInt(reading.attributes.diastolic)
            attributes.prevYtoD.systolic+=parseInt(reading.attributes.systolic)
            attributes.prevYtoD.pulse+=parseInt(reading.attributes.pulse)
            attributes.prevYtoD.readings.push({
                systolic:parseInt(reading.attributes.systolic), 
                diastolic: parseInt(reading.attributes.diastolic)
            })
        }
    }
    
    attributes.lastSevenDays = fetchHighLowAvgReading(attributes.lastSevenDays);
    attributes.prevSevenDays = fetchHighLowAvgReading(attributes.prevSevenDays);
    
    attributes.lastThirtyDays = fetchHighLowAvgReading(attributes.lastThirtyDays);
    attributes.prevThirtyDays = fetchHighLowAvgReading(attributes.prevThirtyDays);
    
    attributes.lastNinetyDays = fetchHighLowAvgReading(attributes.lastNinetyDays);
    attributes.prevNinetyDays = fetchHighLowAvgReading(attributes.prevNinetyDays);
    
    attributes.YtoD = fetchHighLowAvgReading(attributes.YtoD);
    attributes.prevYtoD = fetchHighLowAvgReading(attributes.prevYtoD);

    let avgReadingsArr;
    if(bpReadings.length>=10){
        avgReadingsArr = bpReadings.slice(bpReadings.length-10, bpReadings.length);
    } else {
        avgReadingsArr = await fetchLastTen(userID, dbIdentifier);
    }
    let weight = await fetchWeight(userID, dbIdentifier);
    let weightReading;
    if(weight){
        for(let i=0; i<weight.length; i++){
            if(weight[i].attributes.deleteFlag=='0'){
                weightReading = weight[i];
                break;
            }
        }
    }

    let avg = lastTenAvg(avgReadingsArr, weightReading);
    return {attributes:attributes, userID:userID, avgReadings:avg};
    
}

let fetchBpReadings = async (userID, dbIdentifier) => {
    let from = new Date(new Date().getFullYear()-2, 11, 31).getTime(); //1 day less then last year start Date
    let params = {
        TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.BP_READINGS_TABLE}`,
        KeyConditionExpression: "userID = :id AND measurementDate > :lastYear",
        ExpressionAttributeValues: {
            ':id': userID,
            ':lastYear': from
        },
        ScanIndexForward: true //fetch readings in ascending order
    };
    let bpReadings = [];
    let lastPageKey;
    while(1){
        if(lastPageKey){
            params.ExclusiveStartKey = lastPageKey;
        }
        let bpR = await dbClient.query(params).promise();
        
        if(bpR.Count){
            bpReadings = bpReadings.concat(bpR.Items);
        }
        if(bpR.LastEvaluatedKey){
            lastPageKey = bpR.LastEvaluatedKey
        } else{
            break;
        }
    }
    
    return await formatRecords(bpReadings, userID, dbIdentifier);
}


const saveDoctorBPAverage = async(record, dbIdentifier) => {
  const params = {
    TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.DOCTOR_BP_AVERAGE_TABLE}`,
    Item: record
  };
  return dbClient.put(params).promise();
};

const saveDoctorPatient = async (record, dbIdentifier) => {
  const params = {
    TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.DOCTOR_PATIENT_TABLE}`,
    Item: record
  };
  return dbClient.put(params).promise();
};

const getHospitals = async () => {
    let params = {
        TableName : CONSTANTS.MASTER_HOSPITAL_TABLE
    };    
    
    let {Items} =  await dbClient.scan(params).promise();
    let hospitals = {};
    for (let item of Items) {
        hospitals[item.dbIdentifier] = item;
    }
    
    return hospitals;
};

const identifyTable = (hospitals, eventSource) => {
    let dbIdentifier = null;
    Object.keys(hospitals).forEach(function(key) {
        if(eventSource.includes(key)) {
            dbIdentifier = key;
        }
    });

    return dbIdentifier;
};

exports.handler = async (event, context) => {
    let userIds = new Set();
    let hospitals = await getHospitals();
    let hospitalIdentifier = {};
    if(event.Records){ // trigger flow
        for (const record of event.Records) {
            let dbIdentifier = identifyTable(hospitals, record.eventSourceARN);
            let recordToCheck = record.eventName == CONSTANTS.constants.insert || record.eventName == CONSTANTS.constants.modify
            ? AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage)
            : AWS.DynamoDB.Converter.unmarshall(record.dynamodb.OldImage);
            if(recordToCheck.attributes.isDoctorLinked){
                let userId = null;
                if(recordToCheck.userId_type_deviceLocalName){
                    userId = recordToCheck.userId_type_deviceLocalName.split('_')[0];
                }  
                else{
                    userId = recordToCheck.userID;
                }
                userIds.add(userId);
                hospitalIdentifier[userId] = dbIdentifier;
            }
        }
    } else { //cron flow

        Object.keys(hospitals).forEach(async function(dbIdentifier) {
            let patients = await fetchAllPatients(dbIdentifier);
            if(patients){
                for(let patient of patients){
                    userIds.add(patient.userID);
                    hospitalIdentifier[patient.userId] = dbIdentifier;
                }
            }
        });
    }

    if(userIds.size){
        for (const userId of userIds.values()) {

          let patientID;
          if(hospitalIdentifier[userId] === CONSTANTS.HODES){
            let imeiDetails = await getImeiDetails(Number(userId), hospitalIdentifier[userId]);
            patientID = imeiDetails.Items[0].userID;
          } else {
            patientID = userId;
          }
          
          let docPatient = await fetchDoctorPatient(patientID, hospitalIdentifier[userId]);
          if(docPatient){
            let fetchUserId;
            if(hospitalIdentifier[userId] === CONSTANTS.HODES){
                fetchUserId = docPatient.hubID;
            } else {
                fetchUserId = docPatient.userID;
            }
            let Item = await fetchBpReadings(String(fetchUserId), hospitalIdentifier[userId]);
            
            let avg = Item.avgReadings;
            delete Item.avgReadings;
            
            await saveDoctorBPAverage(Item, hospitalIdentifier[userId]);
          
            docPatient.attributes.averageBp={};
            docPatient.attributes.averageBp.systolic = avg.systolic;
            docPatient.attributes.averageBp.diastolic = avg.diastolic;
            docPatient.attributes.averageBp.pulse = avg.pulse;
            
            docPatient.attributes.irregularHeartBeat = avg.irregularHeartBeat;
            docPatient.attributes.lastReportedDate = avg.lastReportedDate;
            if(avg.weight)
                docPatient.attributes.weight = avg.weight;
            await saveDoctorPatient(docPatient, hospitalIdentifier[userId]);
          }
        }
    }
    return `Successfully processed ${userIds.size} records.`;
};
