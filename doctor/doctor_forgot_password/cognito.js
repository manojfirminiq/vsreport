const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});


const CONSTANTS = require('./CONSTANTS');

const forgotPassword = async(emailAddress) => {
    return new Promise(async(resolve) => {
        await new AWS.CognitoIdentityServiceProvider().forgotPassword({
            Username: emailAddress,
            ClientId : process.env.CLIENT_ID,
        }, (err, res) => {
            console.log(err);
            if(err) {
                // Removing other error messages to avoid valid/ invalid email guess.
                //if(err.code =='InvalidParameterException') return resolve({ success: false, message: CONSTANTS.VALIDATION_MESSAGES.INVALID_PARAMETER_EXCEPTION, errorCode: 'USER_NOT_FOUND' });
                //else if (err.code == 'UserNotFoundException') return resolve({success: false, message: CONSTANTS.VALIDATION_MESSAGES.USER_NOT_FOUND_EXCEPTION, errorCode: 'USER_NOT_FOUND'});
                if(err.code == CONSTANTS.ERRORS.LIMIT_EXCEEDED_EXCEPTION) return resolve({success: false, message: CONSTANTS.VALIDATION_MESSAGES.LIMIT_EXCEEDED_EXCEPTION, errorCode: 'LIMIT_EXCEEDED_EXCEPTION'});
                //else return resolve({ success: false, message: err.message, errorCode: 'USER_NOT_FOUND' });
            }            
            return resolve({ success: true, message: CONSTANTS.RESET_PASSWORD_MESSAGES.SUCCESS });
        });
    });
};

module.exports.forgotPassword = forgotPassword;