const cognito = require('./cognito');
const CONSTANTS = require('./CONSTANTS');

const INVALID_INPUTS = 'Please provide a valid emailAddress';

const applyValidation = async ({ emailAddress }) => {
    try{
        if( !emailAddress ) return Promise.resolve({"success": false, "message": INVALID_INPUTS, "errorCode": "REQUIRED_PARAMETER"});
        emailAddress = emailAddress.toLowerCase();

        return await cognito.forgotPassword(emailAddress);

    } catch(err) {
        console.log(err);
        return Promise.resolve({"success": false, "message": err.message, "errorCode": "INTERNAL_SERVER_ERROR"});
    }

};

exports.handler = async (event, context, callback) => {
    const response = await applyValidation(event);
    console.log(response);
    callback(null, response);
};