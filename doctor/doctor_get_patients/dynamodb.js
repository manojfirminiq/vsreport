const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const DOCTOR_TABLE = process.env.DOCTOR_TABLE;
const ALERT_NON_BP_TABLE = process.env.ALERT_NON_BP_TABLE;
const ALERT_BP_TABLE = process.env.ALERT_BP_TABLE;

let verifyHospital = async (hospitalId) =>{
    let paramsTable = {
        TableName : DOCTOR_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalId
        }
    };
    const {Count, Items} = await dbClient.query(paramsTable).promise();
    if(Items.length == 0){
        return false;
    }else{
        var obj = {
            DOCTOR_PATIENT:  process.env.STAGE +`_rpm_` + Items[0].dbIdentifier + `_`+ DOCTOR_PATIENT_TABLE,
            ALERT_BP_TABLE:  process.env.STAGE + `_rpm_` + Items[0].dbIdentifier + `_`+ ALERT_BP_TABLE,
            ALERT_NON_BP_TABLE:  process.env.STAGE + `_rpm_` + Items[0].dbIdentifier + `_`+ ALERT_NON_BP_TABLE
        };
        return obj;
    }
};

const getPatients = async (doctorId, nextPaginationKey, hospitalTable) => {
    let params = {
        TableName : hospitalTable,
        KeyConditionExpression : "#uid = :id",
        ExpressionAttributeValues: {
            ':id': doctorId
        },
        ExpressionAttributeNames: {
            '#uid': 'doctorID'
        }
    };
    
    if(nextPaginationKey) {
        params.ExclusiveStartKey = nextPaginationKey;
    }
    
    
    return dbClient.query(params).promise();
};


const getBpAlerts = async (userId, ALERT_BP_TABLE) => {
    let params = {
        TableName: ALERT_BP_TABLE,
        KeyConditionExpression: "#uid = :id",
        ExpressionAttributeValues: {
            ':id': userId
        },
        ExpressionAttributeNames: {
            '#uid': 'userID'
        }
    };
    params.ScanIndexForward = false;
    console.log(params); 
    return dbClient.query(params).promise();
};

const getNonBpAlerts =  async (userId, ALERT_NON_BP_TABLE) => {
    let params = {
        "TableName": ALERT_NON_BP_TABLE,
        KeyConditionExpression: "#uid = :id",
        ExpressionAttributeValues: {
            ':id': userId + '_weight_Manual'
        },
        ExpressionAttributeNames: {
            '#uid': 'userId_type_deviceLocalName',
        },
    };
    params.ScanIndexForward = false;
    console.log(params); 
    return dbClient.query(params).promise();
};

module.exports.getPatients = getPatients;
module.exports.verifyHospital = verifyHospital;
module.exports.getBpAlerts = getBpAlerts;
module.exports.getNonBpAlerts = getNonBpAlerts;