exports.ERRORS = {
    INTERNAL_SERVER_ERROR: {
        CODE: "INTERNAL_SERVER_ERROR",
        MESSAGE: "Internal Server Error"
    },
    HOSPITAL_ID_REQ: {
        CODE: "HOSPITAL_ID_REQ",
        MESSAGE: "Hospital id required"
    },
    HOSPITAL_ID_INVALID: {
        CODE: "HOSPITAL_ID_INVALID",
        MESSAGE: "Hospital Id is invalid"
    }
};
exports.TYPE = {
    BP: "bp",
    WEIGHT : "weight"
};

exports.HODES = "hodes";