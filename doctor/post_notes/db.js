const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_CONSULTATION = CONSTANTS.DOCTOR_CONSULTATION;
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;


let verifyHospital = async (hospitalId) =>{
    let paramsTable = {
        TableName : HOSPITAL_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalId
        }
    };
    const {Count, Items} = await dbClient.query(paramsTable).promise();
    if(Count <= 0){
        return false;
    }else{
         return process.env.STAGE + '_' + CONSTANTS.RPM + '_' + Items[0].dbIdentifier + `_`+ CONSTANTS.NOTES_TABLE;
    }
};

const saveNotes = async (notes, hospitalTable) => {
    notes.modifiedDate = new Date().getTime();
    let params = {
        TableName: hospitalTable,
        Item: notes
    };
    return dbClient.put(params).promise();
};

module.exports.saveNotes = saveNotes;
module.exports.verifyHospital = verifyHospital;