const AWS = require("aws-sdk");
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require("./CONSTANTS.js");

const DOCTOR_TABLE = process.env.DOCTOR_TABLE;
const BP_ALERT_TABLE = CONSTANTS.BP_ALERT_TABLE;
const NON_BP_ALERT_TABLE = CONSTANTS.NON_BP_ALERT_TABLE;
const DOCTOR_PATIENT_TABLE = CONSTANTS.DOCTOR_PATIENT_TABLE;
const DOCTOR_PROFILE_TABLE = CONSTANTS.DOCTOR_PROFILE_TABLE;
const RPM = CONSTANTS.RPM;

let verifyHospital = async hospitalId => {
  let paramsTable = {
    TableName: DOCTOR_TABLE,
    KeyConditionExpression: "#hospital = :id",
    ExpressionAttributeNames: {
      "#hospital": "hospitalID"
    },
    ExpressionAttributeValues: {
      ":id": hospitalId
    }
  };
  const { Items } = await dbClient.query(paramsTable).promise();
  if (Items.length == 0) {
    return false;
  } else {
    let obj = {
      DOCTOR_PATIENT:
        process.env.STAGE +
        RPM +
        Items[0].dbIdentifier +
        `_` +
        DOCTOR_PATIENT_TABLE,
      DOCTOR_PROFILE:
        process.env.STAGE +
        RPM +
        Items[0].dbIdentifier +
        `_` +
        DOCTOR_PROFILE_TABLE,
      BP_ALERT_TABLE:
        process.env.STAGE + RPM + Items[0].dbIdentifier + `_` + BP_ALERT_TABLE,
      NON_BP_ALERT_TABLE:
        process.env.STAGE +
        RPM +
        Items[0].dbIdentifier +
        `_` +
        NON_BP_ALERT_TABLE
    };
    return obj;
  }
};

const getBpReadings = async (BP_ALERT_TABLE, userId, measurementDate) => {
  let params = {
    TableName: BP_ALERT_TABLE,
    KeyConditionExpression: "#uid = :id AND #mDate = :date",
    ExpressionAttributeValues: {
      ":id": userId,
      ":date": measurementDate
    },
    ExpressionAttributeNames: {
      "#uid": "userID",
      "#mDate": "measurementDate"
    }
  };
  return dbClient.query(params).promise();
};

const getNonBpReadings = async (
  NON_BP_ALERT_TABLE,
  userId,
  measurementDate
) => {
  let params = {
    TableName: NON_BP_ALERT_TABLE,
    KeyConditionExpression: "#uid = :id AND #mDate = :date",
    ExpressionAttributeValues: {
      ":id": userId + "_weight_Manual",
      ":date": measurementDate
    },
    ExpressionAttributeNames: {
      "#uid": "userId_type_deviceLocalName",
      "#mDate": "measurementDate"
    }
  };
  return dbClient.query(params).promise();
};

const updateAlerts = async (readings, alertTable) => {
  let params = {
    TableName: alertTable,
    Item: readings
  };
  return dbClient.put(params).promise();
};

const getPatient = async (DOCTOR_PATIENT_TABLE, doctorId, userId) => {
  let params = {
    TableName: DOCTOR_PATIENT_TABLE,
    KeyConditionExpression: "#doctorId = :doctorId AND #userId = :userId",
    ExpressionAttributeValues: {
      ":doctorId": doctorId,
      ":userId": userId
    },
    ExpressionAttributeNames: {
      "#doctorId": "doctorID",
      "#userId": "userID"
    }
  };
  return dbClient.query(params).promise();
};

module.exports.getBpReadings = getBpReadings;
module.exports.getPatient = getPatient;
module.exports.getNonBpReadings = getNonBpReadings;
module.exports.updateAlerts = updateAlerts;
module.exports.verifyHospital = verifyHospital;
