const DB = require("./dynamodb");
const CONSTANTS = require("./CONSTANTS");

const applyValidation = async ({
  doctorId,
  hospitalID,
  body: { userId, resolveStatus, measurementDate, notes, type }
}) => {
  try {
    if (!userId) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE,
        errorCode: CONSTANTS.ERRORS.USER_ID_REQ.CODE
      });
    } else if (!resolveStatus) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.RESOLVE_STATUS_REQ.MESSAGE,
        errorCode: CONSTANTS.ERRORS.RESOLVE_STATUS_REQ.CODE
      });
    } else if (
      resolveStatus != CONSTANTS.RESOLVE_STATUS.ZERO &&
      resolveStatus != CONSTANTS.RESOLVE_STATUS.ONE
    ) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.RESOLVE_STATUS_INVALID.MESSAGE,
        errorCode: CONSTANTS.ERRORS.RESOLVE_STATUS_INVALID.CODE
      });
    } else if (!measurementDate) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.MEASUREMENT_DATE_REQ.MESSAGE,
        errorCode: CONSTANTS.ERRORS.MEASUREMENT_DATE_REQ.CODE
      });
    } else if (isNaN(parseInt(measurementDate))) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.MEASUREMENT_DATE_INVALID.MESSAGE,
        errorCode: CONSTANTS.ERRORS.MEASUREMENT_DATE_INVALID.CODE
      });
    } else if (!type) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.TYPE_REQ.MESSAGE,
        errorCode: CONSTANTS.ERRORS.TYPE_REQ.CODE
      });
    }
    let hospitalTableDetails = await DB.verifyHospital(hospitalID);
    if (!hospitalTableDetails) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
        errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
      });
    }
    let { Count, Items } = await DB.getPatient(
      hospitalTableDetails.DOCTOR_PATIENT,
      doctorId,
      userId
    );
    if (Count == 0) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
        errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
      });
    } else {
      // if hospital is hodes, get readings using hubId, else use userId
      if (hospitalTableDetails.DOCTOR_PATIENT.includes(CONSTANTS.HODES)) {
        userId = Items[0].hubID;
      }
    }

    if (type === CONSTANTS.BP) {
      let { Count, Items } = await DB.getBpReadings(
        hospitalTableDetails.BP_ALERT_TABLE,
        userId,
        parseInt(measurementDate)
      );
      if (Count > 0 && Items) {
        Items[0].alerts.bp.resolveStatus = resolveStatus;
        Items[0].alerts.bp.Notes = notes ? notes : null;
        Items[0].alerts.bp.resolvedDate = new Date().getTime();
        await DB.updateAlerts(Items[0], hospitalTableDetails.BP_ALERT_TABLE);
      } else {
        return Promise.resolve({
          success: false,
          message: CONSTANTS.ERRORS.NO_RECORD_FOUND.MESSAGE,
          errorCode: CONSTANTS.ERRORS.NO_RECORD_FOUND.CODE
        });
      }
    } else if (type === CONSTANTS.WEIGHT) {
      let { Count, Items } = await DB.getNonBpReadings(
        hospitalTableDetails.NON_BP_ALERT_TABLE,
        userId,
        parseInt(measurementDate)
      );
      if (Count > 0 && Items) {
        Items[0].alerts.weight.resolveStatus = resolveStatus;
        Items[0].alerts.weight.Notes = notes ? notes : null;
        Items[0].alerts.weight.resolvedDate = new Date().getTime();
        await DB.updateAlerts(
          Items[0],
          hospitalTableDetails.NON_BP_ALERT_TABLE
        );
      } else {
        return Promise.resolve({
          success: false,
          message: CONSTANTS.ERRORS.NO_RECORD_FOUND.MESSAGE,
          errorCode: CONSTANTS.ERRORS.NO_RECORD_FOUND.CODE
        });
      }
    } else {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.TYPE_NOT_FOUND.MESSAGE,
        errorCode: CONSTANTS.ERRORS.TYPE_NOT_FOUND.CODE
      });
    }
    return Promise.resolve({
      success: true
    });
  } catch (error) {
    return Promise.resolve({
      success: false,
      message: error,
      errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
    });
  }
};

exports.handler = async (event, context, callback) => {
  console.log(event);
  let response = await applyValidation(event);
  callback(null, response);
};
