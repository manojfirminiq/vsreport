const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');

const applyValidation = async ({doctorId, userId, hospitalID}) => {
    try {
        if( !userId ) return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE});
        let hospitalTable = await DB.verifyHospital(hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }
        let doctorPatient = await DB.getDoctorPatient(doctorId, userId, hospitalTable.DOCTOR_PATIENT);
        await DB.deleteDoctorPatient(doctorId, userId, hospitalTable.DOCTOR_PATIENT);
        await DB.deleteHospitalDeviceMaster(doctorPatient.Items[0].hubID, hospitalTable.HOSPITAL_DEVICE_MASTER);
        return Promise.resolve({
            success: true
        });
    } catch (error) {
        console.log(error);
        return Promise.resolve({
            success: false,
            errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
            message: error
        });
    }
};

exports.handler = async (event, context, callback) => {
    const response = await applyValidation(event);
    callback(null, response);
};
