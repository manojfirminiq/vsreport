exports.DOCTOR_PATIENT=process.env.DOCTOR_PATIENT;
exports.HOSPITAL_TABLE= process.env.HOSPITAL_TABLE;
exports.HOSPITAL_DEVICE_MASTER = process.env.HOSPITAL_DEVICE_MASTER;

exports.ERRORS = {
    INTERNAL_SERVER_ERROR: {
        CODE: "INTERNAL_SERVER_ERROR",
        MESSAGE: "Internal Server Error"
    },
    USER_ID_REQ: {
        CODE: "USER_ID_REQ",
        MESSAGE: "User id required"
    },
    HOSPITAL_ID_INVALID: {
        CODE: "HOSPITAL_ID_INVALID",
        MESSAGE: "Hospital Id is invalid"
    }
};

exports.RPM = "rpm";
exports.DELETE_FLAG = true;