const AWS = require('aws-sdk');
const moment = require('moment');
const LOG_S3 = require('./logtos3');
const {HOSPITAL_DEVICE_MASTER_TABLE ,RPM_HOSPITAL_MASTER ,APP_TYPE ,DEVICE_TYPE, HODES} = require("./CONSTANTS.js")

AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true });
const BP_READING_TABLE = process.env.GLOBAL_RPM_BP_READINGS;
const NON_BP_READING_TABLE = process.env.GLOBAL_RPM_NON_BP_READINGS;
const dateTimeNow = Number(moment().format('x'));


const findHospitalIDfromHub = async  (HubID) => {
    try {
        let paramsTable = {
                        TableName : HOSPITAL_DEVICE_MASTER_TABLE,
                        KeyConditionExpression: "#HubID = :id",
                        ExpressionAttributeNames:{
                        "#HubID": "hubID"
                        },
                        ExpressionAttributeValues: {
                        ":id": String(HubID)
                        }
                    };
        const {count, Items} = await dbClient.query(paramsTable).promise();
        return Items;
    } catch (e) {
        console.log("error" , e)
    }
}

const findHospitalDetails = async  (hospitalID) => {
    try {
        let paramsTable = {
                            TableName : RPM_HOSPITAL_MASTER,
                            KeyConditionExpression: "#hospital = :id",
                            ExpressionAttributeNames:{
                            "#hospital": "hospitalID"
                            },
                            ExpressionAttributeValues: {
                            ":id": hospitalID
                            }
                    };
        const {count, Items} = await dbClient.query(paramsTable).promise();
        return Items;
    } catch (e) {
        console.log("error" , e)
    }
}


exports.saveReading = async(astuteReadingList) => {
    try {
        let promiseArr = [];
        console.log('saveReading called: ' + astuteReadingList.length);
        for (let readings of astuteReadingList) {
            let bpTable  = BP_READING_TABLE;
            let nonBpTable = NON_BP_READING_TABLE
            if(readings.HubId){
                    const masterRecords = await findHospitalIDfromHub(readings.HubId);
                    if(masterRecords && masterRecords.length>0 && masterRecords[0].hospitalID.toLowerCase() != APP_TYPE.toLowerCase()){
                        const hospitalrecords = await findHospitalDetails(masterRecords[0].hospitalID);
                        bpTable = process.env.STAGE +`_${APP_TYPE.toLocaleLowerCase()}_` + hospitalrecords[0].dbIdentifier + `_${process.env.BP_READING}`
                        nonBpTable = process.env.STAGE +`_${APP_TYPE.toLocaleLowerCase()}_` + hospitalrecords[0].dbIdentifier +`_${process.env.NON_BP_READING}`
                        readings.isDoctorLinked = 1;

                        if(hospitalrecords[0].dbIdentifier === HODES) {
                             readings.userID = masterRecords[0].hubID;
                        } else {
                            readings.userID = masterRecords[0].userID ? masterRecords[0].userID : masterRecords[0].hubID;
                        }
                    } else {
                        //Handle for MSH
                        readings.userID = readings.HubId;
                    }
            }
            console.log(bpTable , nonBpTable);
            
             if (readings.DeviceType.toLowerCase() === process.env.DEVICE_TYPE_BP) {

                const bpData = await getBPData(readings.userID, Number(moment(readings.ReadingTimestampUtc).format('x')), bpTable);
                if (bpData && bpData.length === 0) {
                    let item = mapBPData(readings);
                    promiseArr.push(saveItem(item, bpTable));
                }
                else {
                    let item = mapBPData(readings);
                    promiseArr.push(updateBP(item, bpTable));
                }

              }
                if (readings.DeviceType.toLowerCase() === process.env.DEVICE_TYPE_WEIGHT) {

                    const weightData = await getWeightData(readings.userID + "_weight_manual", Number(moment(readings.ReadingTimestampUtc).format('x')), nonBpTable);
                    readings.deviceType = DEVICE_TYPE
                    readings.isManualEntry = 0
                    if (weightData && weightData.length === 0) {
                        let item = mapNonBPData(readings);
                        promiseArr.push(saveItem(item, nonBpTable));
                    }
                    else {
                        let item = mapNonBPData(readings);
                        promiseArr.push(updateNonBP(item, nonBpTable));
                    }
                }
        }

        return await Promise.all(promiseArr).then(() => {
            return new Promise((resolve, reject) => {
                resolve({ "success": true });
            });
        }).catch((error)=>{
            console.log(error);
            LOG_S3.uploadDataToS3(astuteReadingList);
        });
    }
    catch (e) {
        console.log(e);
        LOG_S3.uploadDataToS3(astuteReadingList);
    }
};

const saveItem = (item, table_name) => {
    let params = {
        TableName: table_name,
        Item: item
    };

    return dbClient.put(params).promise();
};

const mapBPData = (astuteBP) => {
    let reading = astuteBP.Reading;
    let obj = {
        measurementDate: Number(moment(reading.ReadingTimestampUtc).format('x')),
        userID: astuteBP.userID,
        modifiedDate: dateTimeNow,
        createdDate: dateTimeNow,
        attributes: {},
        app: 'RPM'
    };

    obj.attributes = setBPAttributes(astuteBP);
    
    // new field
    obj.astuteRawData = astuteBP;
    return obj;
};

const setBPAttributes = (astuteBP) => {
    const reading = astuteBP.Reading;
    let obj = {
        "atrialFibrillationDetection": 0,
        "consecutiveMeasurement": 0,
        "countArtifactDetection": 0,
        "cuffFlag": reading.CuffTooLoose ? 0 : 1,
        "dateEnabled": null,
        "deleteFlag": 0,
        "cuffWrapDetect": reading.CuffTooLoose ? 0 : 1,
        "deviceModel": astuteBP.DeviceModel,
        "deviceSerialID": astuteBP.DeviceSerialNumber,
        "diastolic": reading.Diastolic.toString(),
        "diastolicUnit": reading.BPUOM,
        "errNumber": 0,
        "errorDetails": 0,
        "internalDeviceTemp": null,
        "irregularHB": 0,
        "irregularPulseDetection": reading.IrregularPulseDetected ? 1 : 0,
        "isManualEntry": 0,
        "meanArterialPressure": reading.ArterialPressure.toString(),
        "measurementLocalDate": Number(moment(reading.ReadingTimestampUtc).format('x')),
        "measurementStartingMethod": null,
        "mets": null,
        "movementDetect": reading.BodyMovementDetected ? 1 : 0,
        "movementError": 0,
        "positionDetect": 0,
        "positioningIndicator": 0,
        "pulse": reading.HeartRate.toString(),
        "pulseUnit": "bpm",
        "roomTemperature": 0,
        "systolic": reading.Systolic.toString(),
        "systolicUnit": reading.BPUOM,
        "timeZone": "0",
        "timeZoneDevice": null,
        "transferDate": Number(moment(reading.ReadingTimestamp).format('x')),
        "userNumberInDevice": reading.UserId
    };
    if(astuteBP.isDoctorLinked){
        obj.isDoctorLinked = 1
    }
    return obj;
};

const mapNonBPData = (astuteWeightScale) => {
    let reading = astuteWeightScale.Reading;
    let obj = {
        measurementDate: moment(reading.ReadingTimestampUtc).unix()*1000,
        attributes: {},
        userId_type_deviceLocalName: astuteWeightScale.userID + "_weight_Manual",
        createdDate: dateTimeNow,
        modifiedDate: dateTimeNow
    };

    obj.attributes = {
        "app": "RPM",
        "bmiValue": reading.BMI,
        "bodyFatPercentage": reading.BodyFat,
        "deleteFlag": "0",
        "deviceModel": astuteWeightScale.DeviceModel,
        "deviceSerialID": astuteWeightScale.DeviceSerialNumber,
        "deviceType": "",
        "isManualEntry": "",
        "latitude": "",
        "longitude": "",
        "measurementLocalDate": Number(moment(reading.ReadingTimestamp).format('x')),
        "restingMetabolism": "",
        "skeletalMusclePercentage": "",
        "timeZone": "0",
        "type": "weight",
        "visceralFatLevel": "",
        "weight": reading.BodyWeightKg,
        "weightLBS": reading.BodyWeightLb,
        "zipCode": ""
    };
    if(astuteWeightScale.isDoctorLinked){
        obj.attributes.isDoctorLinked = 1
    }

    // new field
    obj.astuteRawData = astuteWeightScale;
    return obj;
};

const updateBP = ({ userID, attributes, modifiedDate, astuteRawData, measurementDate }, table) => {
    var params = {
        TableName: table,
        Key: {
            "userID": String(userID),
            "measurementDate": Number(measurementDate)
        },
        UpdateExpression: "set attributes = :attributes, modifiedDate = :modifiedDate, astuteRawData = :astuteRawData",
        ExpressionAttributeValues: {
            ":attributes": attributes,
            ":modifiedDate": modifiedDate,
            ":astuteRawData": astuteRawData
        }
    };
    return dbClient.update(params).promise();
};

const updateNonBP = ({ userId_type_deviceLocalName, attributes, modifiedDate, astuteRawData, measurementDate }, table) => {
    var params = {
        TableName: updateNonBP,
        Key: {
            "userId_type_deviceLocalName": String(userId_type_deviceLocalName),
            "measurementDate": Number(measurementDate)
        },
        UpdateExpression: "set attributes = :attributes, modifiedDate = :modifiedDate, astuteRawData = :astuteRawData",
        ExpressionAttributeValues: {
            ":attributes": attributes,
            ":modifiedDate": modifiedDate,
            ":astuteRawData": astuteRawData
        }
    };
    return dbClient.update(params).promise();
};

const getBPData = async(userID, measurementDate, table) => {
    const query = {
        "TableName": table,
        KeyConditionExpression: "#uid = :id AND measurementDate = :measurementDate",
        ExpressionAttributeValues: {
            ':id': String(userID),
            ':measurementDate': Number(measurementDate)
        },
        ExpressionAttributeNames: {
            '#uid': 'userID'
        },
        ScanIndexForward: false
    };
    const { Items } = await queryDb(query);
    console.log(Items);
    return Items;
};

const getWeightData = async(userId_type_deviceLocalName, measurementDate, table) => {
    const query = {
        "TableName": table,
        KeyConditionExpression: "#uid = :id AND measurementDate = :measurementDate",
        ExpressionAttributeValues: {
            ':id': String(userId_type_deviceLocalName),
            ':measurementDate': Number(measurementDate)
        },
        ExpressionAttributeNames: {
            '#uid': 'userId_type_deviceLocalName'
        },
        ScanIndexForward: false
    };
    const { Items } = await queryDb(query);
    console.log(Items);
    return Items;
};

let queryDb = (params) => {
    return dbClient.query(params).promise();
};
