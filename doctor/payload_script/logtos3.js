const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION })
const s3Client = new AWS.S3();

exports.uploadDataToS3 = async(responseData) => {
    let keyName = Date.now();
    let params = {
        Bucket: process.env.S3_ASTUTE,
        Key: process.env.S3_ASTUTE_FAIL_LOG + keyName + '.txt',
        Body: JSON.stringify(responseData),
        ContentType: 'text/plain',
    };

    let response = null;
    try {
        await s3Client.upload(params).promise();
    }
    catch (error) {
        throw error;
    }

    return Promise.resolve(response);
};
