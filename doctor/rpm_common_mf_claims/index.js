const CONSTANT = require('./CONSTANTS.js')
const axios = require('axios')
const AWS = require("aws-sdk");
AWS.config.update({ region:process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const callApi =async (payload)=>{
    let options = {
        method: 'POST',
        headers: {
            "Content-Type": CONSTANT.MF_CONTENT_TYPE,
            "source-host":CONSTANT.MF_SOURCE_HOST,
            "application-name":CONSTANT.MF_APPLICATION_NAME,
            "verification-token":CONSTANT.MF_VERIFICATION_TOKEN
        },
        data:payload,
        url: CONSTANT.MF_END_POINT,
    }
    return await axios(options)
}

const updateClaimsStatus = async (yearMonth, ehrID,tablename)=>{
   try {
            var queryParams  = {
                TableName : tablename,
                Key: {yearMonth, ehrID },
                UpdateExpression:  "set #attributes.#status = :submitstatus,modifiedDate = :modifiedDate",
                ExpressionAttributeNames:  {
                  "#attributes": 'attributes',
                  "#status":'status'
                },
                ExpressionAttributeValues: {
                    ':submitstatus': CONSTANT.SENT,
                    ":modifiedDate": Date.now()
                }
            }
            await dbClient.update(queryParams).promise();
            return  {success:true}
        } catch(e) {
            return  {success:false}
        }
}


const findHospitalID = async ()=>{
      let paramsTable = {
        TableName : CONSTANT.HOSPITAL_MASTER_TABLE,
    };
    const {Count, Items} = await dbClient.scan(paramsTable).promise();
    return Items;
}

const getPreviousClaimDetails = async (table,ehrID) => {
    try{
        const date = new Date();
        let month = date.getMonth();
        month = month < 10 ? '0' + month : month; 
        const year = (date.getFullYear()).toString();
        let yearMonth = year+month;
        
        let queryParams = {
            TableName: table,
            KeyConditionExpression:"yearMonth = :yearMonth AND ehrID = :ehrID",
            ExpressionAttributeValues:{
                ":ehrID": ehrID,
                ":yearMonth": Number(yearMonth),
            }
        };
        const {Items} = await dbClient.query(queryParams).promise();
        return Items ? Items : []
    } catch(e) {
        console.log(e);
    }
}

exports.handler = async (event) => {
    try {
         const hospital = await findHospitalID();
         let successcount = [];
         let failurecount = []
         if(hospital.length>0){
            for (var i = 0; i < hospital.length; i++) {
                try {
                let tablename = `${CONSTANT.STAGE}_${CONSTANT.RPM.toLowerCase()}_${hospital[i].dbIdentifier}_${CONSTANT.CLAIM}`;
                let claims = await getPreviousClaimDetails(tablename,hospital[i].hospitalID);
                if(claims && claims.length > 0){
                    for (var j = 0; j < claims.length; j++) {
                        try {
                            let mfparams = {...CONSTANT.MF_JSON}
                            if(hospital[i].claims.SKU.toLowerCase() != CONSTANT.TEST){
                                mfparams.Items[0].Quantity = claims[j].attributes.countOfUsers;
                                mfparams.Meta.FacilityCode = hospital[i].claims.facilityCode;
                                mfparams.Items[0].Identifiers[0].ID = hospital[i].claims.SKU;
                                if(claims[j].attributes && !claims[j].attributes.status){
                                    await callApi(mfparams).then(async response=>{
                                        if(response && response.data && response.data.status){
                                            let yearMonth = claims[j].yearMonth;
                                            let ehrID = claims[j].ehrID;
                                            let updatestatus = await updateClaimsStatus(yearMonth,ehrID,tablename);
                                            if(updatestatus && updatestatus.success){
                                                successcount.push(claims)
                                            }else{
                                                failurecount.push(claims)  
                                            }
                                         }
                                     }).catch(err=>{
                                         console.log(err)
                                     }); 
                                }   
                            }                            
                      } catch (errors) {
                          console.log(errors)
                      }
                    }
                }
                } catch (e) {
                    console.log(e)
                }
                if(i+1 == hospital.length){
                    console.log(`success count`,successcount.length)
                    console.log(`failurecount count`,failurecount.length)
                }
            }
         }
        
        
         
    } catch (error) {
        return Promise.reject(error);
    }

    return Promise.resolve({"success" : true});
 };
