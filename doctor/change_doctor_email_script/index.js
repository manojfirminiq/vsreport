const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');

const applyValidation = async ({doctorId, hospitalID, newEmailAddress }) => {
    try {
        if(!doctorId){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.DOCTOR_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.DOCTOR_ID_REQ.CODE});
        }
        if(!hospitalID){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE});
        }
        let hospitalTable = await DB.getHospital(hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }
        let doctorData = await DB.getDoctorData(doctorId, hospitalTable.DOCTOR_TABLE);
        if(doctorData) {
            await DB.updateDoctorEmail(doctorId, newEmailAddress, hospitalTable.DOCTOR_TABLE);
            return Promise.resolve({
                success: true
            });
        }else {
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_NOT_FOUND.CODE});
        }
    } catch (error) {
        console.log(error);
        return Promise.resolve({
            success: false,
            errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
            message: error
        });
    }
};

exports.handler = async (event, context, callback) => {
    const response = await applyValidation(event);
    callback(null, response);
};
