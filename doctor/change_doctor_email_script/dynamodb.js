const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_TABLE_NAME = process.env.DOCTOR_TABLE_NAME;
const HOSPITAL_TABLE_NAME = process.env.HOSPITAL_TABLE_NAME;

const getHospital = async(hospitalId) => {
    const params = {
        TableName : HOSPITAL_TABLE_NAME,
        KeyConditionExpression : "#uid = :id",
        ExpressionAttributeValues: {
            ':id': hospitalId
        },
        ExpressionAttributeNames: {
            '#uid': 'hospitalID'
        }
    };

    let {Count, Items} = await dbClient.query(params).promise();
    if(Items.length == 0){
        return false;
    }else{
        var obj = {
            Name: (Count > 0) ? Items[0].name : null,
            DOCTOR_TABLE:  process.env.STAGE + CONSTANTS.RPM + Items[0].dbIdentifier + `_`+ DOCTOR_TABLE_NAME
        };
        return obj;
    }
};

const getDoctorData = async (doctorID, doctorTable) => {
    const params = {
        TableName : doctorTable,
        KeyConditionExpression : "#uid = :id",
        ExpressionAttributeValues: {
            ':id': doctorID
        },
        ExpressionAttributeNames: {
            '#uid': 'userID'
        }
    };
    let {Count, Items} = await dbClient.query(params).promise();
    if(Items.length == 0){
        return false;
    }else{
        return Items;
    }
};

const updateDoctorEmail = async(doctorId, newEmailAddress, doctorTable) => {
    var params = {
        TableName: doctorTable,
        Key: {
            "userID": doctorId,
        },
        UpdateExpression: "SET emailAddress = :newEmailAddress" ,
        ExpressionAttributeValues: {
            ":newEmailAddress": newEmailAddress,
        },
        ReturnValues: "UPDATED_NEW"
    };
    return dbClient.update(params).promise();
};


module.exports.updateDoctorEmail = updateDoctorEmail;
module.exports.getHospital = getHospital;
module.exports.getDoctorData= getDoctorData;