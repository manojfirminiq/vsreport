const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const TABLE_NAME = process.env.DOCTOR_HOSPITAL_TABLE;

const getHospital = async (nextPaginationKey) => {
    let params = {
        TableName : TABLE_NAME
    };
    
    if(nextPaginationKey) {
        params.ExclusiveStartKey = nextPaginationKey;
    }    
    
    return dbClient.scan(params).promise();
};

module.exports.getHospital = getHospital;