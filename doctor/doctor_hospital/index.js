const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');

const getHospital = async () => {
    try {

        let {Items} = await DB.getHospital();        
        Items = Items.map(({hospitalID, name, ...rest}) => ({hospitalId: hospitalID, name}));        
        return Promise.resolve({
            success: true,
            data: Items
        });

    } catch (error) {
        console.log(error);
        return Promise.resolve({
            success: false,
            errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
            message: error
        });
    }
};

exports.handler = async (event, context, callback) => {
    
    const response = await getHospital();
    callback(null, response);
};
