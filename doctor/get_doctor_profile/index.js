const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./dynamodb.js');

const applyValidation = async ({ doctorId, hospitalID }) => {
    try {
        if(!hospitalID){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE});
        }
        let hospitalTable = await DB.getHospital(hospitalID);
        if(!hospitalTable){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }
        let response = {
            success: true
        };

        let doctorData = await DB.getDoctorData(doctorId, hospitalTable.DOCTOR_TABLE);
        if(!doctorData) {
            response.success = false;
            response.errorCode = CONSTANTS.ERRORS.USER_NOT_FOUND.CODE;
            response.message = CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE;
            return Promise.resolve(response);
        } else {
            doctorData.subscriptionFlag = (doctorData.subscriptionFlag) ? CONSTANTS.SUBSCRIBEFLAG : CONSTANTS.UNSUBSCRIBEFLAG;
            response.data = doctorData;
        }
        
        return response;

    } catch (error) {
        console.log(error, "in catch");
        return Promise.resolve({"success": false, "message": error.message, errorCode: error.errorCode ? error.errorCode : 'INTERNAL_SERVER_ERROR'});
    }
};

exports.handler = async (event, context, callback) => {
    console.log(JSON.stringify(event));
    const response = await applyValidation(event);
    callback(null, response);
};