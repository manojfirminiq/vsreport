const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_TABLE_NAME = process.env.DOCTOR_TABLE_NAME;
const HOSPITAL_TABLE_NAME = process.env.HOSPITAL_TABLE_NAME;

const getHospital = async(hospitalId) => {
    const params = {
        TableName : HOSPITAL_TABLE_NAME,
        KeyConditionExpression : "#uid = :id",
        ExpressionAttributeValues: {
            ':id': hospitalId
        },
        ExpressionAttributeNames: {
            '#uid': 'hospitalID'
        }
    };

    let {Count, Items} = await dbClient.query(params).promise();
    if(Items.length == 0){
        return false;
    }else{
        var obj = {
            Name: (Count > 0) ? Items[0].name : null,
            DOCTOR_TABLE:  process.env.STAGE +`_rpm_` + Items[0].dbIdentifier + `_`+ DOCTOR_TABLE_NAME
        };
        return obj;
    }
};

const getDoctorData = async (doctorID, hospitalTable) => {
    const params = {
        TableName : hospitalTable,
        KeyConditionExpression : "#uid = :id",
        ExpressionAttributeValues: {
            ':id': doctorID
        },
        ExpressionAttributeNames: {
            '#uid': 'userID'
        }
    };
    let {Count, Items} = await dbClient.query(params).promise();
    if(Count > 0) {
        let Item = Items.map(({
                createdDate,
                modifiedDate,
                attributes: { ...attributesRest },
                ...rest
        }) => ( { ...rest, ...attributesRest } ))[0];
            
        if(Item.hospitalID){
            Item.hospital = await getHospital(Item.hospitalID);
            Item.hospital = Item.hospital.Name
            delete Item.hospitalID;
        }
        
        return Item;
    } else {
        return null;
    }
};

module.exports.getDoctorData = getDoctorData;
module.exports.getHospital = getHospital;