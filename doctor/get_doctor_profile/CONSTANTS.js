exports.ERRORS = {
    USER_NOT_FOUND: {
        CODE: "USER_NOT_FOUND",
        MESSAGE: "User not found"
    },
    HOSPITAL_ID_REQ: {
        CODE: "HOSPITAL_ID_REQ",
        MESSAGE: "Hospital id required"
    },
    INTERNAL_SERVER_ERROR:{
        CODE: "INTERNAL_SERVER_ERROR",
        MESSAGE: "Internal Server Error"
    },
    HOSPITAL_ID_INVALID: {
        CODE: "HOSPITAL_ID_INVALID",
        MESSAGE: "Hospital Id is invalid"
    }
};

exports.SUBSCRIBEFLAG = true;
exports.UNSUBSCRIBEFLAG = false;