const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const BP_TABLE = process.env.BP_TABLE;
const NON_BP_TABLE = process.env.NON_BP_TABLE;
const DOCTOR_TABLE = process.env.DOCTOR_TABLE;
const ALERT_NON_BP_TABLE = process.env.ALERT_NON_BP_TABLE;
const ALERT_BP_TABLE = process.env.ALERT_BP_TABLE;

let verifyHospital = async (hospitalId) =>{
    let paramsTable = {
        TableName : DOCTOR_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalId
        }
    };
    const {Count, Items} = await dbClient.query(paramsTable).promise();
    if(Items.length == 0){
        return false;
    }else{
        var obj = {
                DOCTOR_PATIENT:  process.env.STAGE +`_rpm_` + Items[0].dbIdentifier + `_`+ DOCTOR_PATIENT_TABLE,
                BP_TABLE: process.env.STAGE + `_rpm_` + Items[0].dbIdentifier + `_`+ BP_TABLE,
                NON_BP_TABLE: process.env.STAGE + `_rpm_` + Items[0].dbIdentifier + `_`+ NON_BP_TABLE,
                ALERT_BP_TABLE:  process.env.STAGE + `_rpm_` + Items[0].dbIdentifier + `_`+ ALERT_BP_TABLE,
                ALERT_NON_BP_TABLE:  process.env.STAGE + `_rpm_` + Items[0].dbIdentifier + `_`+ ALERT_NON_BP_TABLE
            };
        return obj;
    }
};

const getPatient = async (doctorId, userId, hospitalTable) => {
    let params = {
        TableName : hospitalTable,
        KeyConditionExpression : "#doctorId = :doctorId AND #userId = :userId",
        ExpressionAttributeValues: {
            ':doctorId': doctorId,
            ':userId': userId
        },
        ExpressionAttributeNames: {
            '#doctorId': 'doctorID',
            '#userId': 'userID'
        }
    };
    
    return dbClient.query(params).promise();
};

const getBpReadings = async (userId, BP_TABLE, from, to, nextPaginationKey) => {
    console.log(to, from);
    let fromDate = new Date(Number(from));
    fromDate.setDate(fromDate.getDate() - 1);

    let toDate = new Date(Number(to));
    toDate.setDate(toDate.getDate() + 1);

    let params = {
        "TableName": BP_TABLE,
        KeyConditionExpression: "#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate",
        ExpressionAttributeValues: {
            ':id': userId,
            ':minDate': fromDate.getTime(),
            ':maxDate': toDate.getTime()
        },
        ExpressionAttributeNames: {
            '#uid': 'userID'
        }
    };
   
    if( nextPaginationKey ) {
        params.ExclusiveStartKey = {"userID": userId, "measurementDate": Number(nextPaginationKey)};
    }
    params.ScanIndexForward = false;
    console.log(params);
 
    return dbClient.query(params).promise();
};

const getBpAlerts = async (userId, ALERT_BP_TABLE) => {

    let params = {
        TableName: ALERT_BP_TABLE,
        KeyConditionExpression: "#uid = :id",
        ExpressionAttributeValues: {
            ':id': userId
        },
        ExpressionAttributeNames: {
            '#uid': 'userID'
        }
    };

    params.ScanIndexForward = false; 
    return dbClient.query(params).promise();
};

const getNonBpAlerts =  async (userId, ALERT_NON_BP_TABLE) => {

    let params = {
        "TableName": ALERT_NON_BP_TABLE,
        KeyConditionExpression: "#uid = :id",
        ExpressionAttributeValues: {
            ':id': userId+'_weight_Manual'
        },
        ExpressionAttributeNames: {
            '#uid': 'userId_type_deviceLocalName'
        },
    };

    params.ScanIndexForward = false;
    return dbClient.query(params).promise();
};

const getNonBpReadings = async (userId, NON_BP_TABLE, from, to, nextPaginationKey) => {
    console.log(to, from);
    let fromDate = new Date(Number(from));
    fromDate.setDate(fromDate.getDate() - 1);

    let toDate = new Date(Number(to));
    toDate.setDate(toDate.getDate() + 1);

    let params = {
        "TableName": NON_BP_TABLE,
        KeyConditionExpression: "#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate",
        ExpressionAttributeValues: {
            ':id': userId+'_weight_Manual',
            ':minDate': fromDate.getTime(),
            ':maxDate': toDate.getTime()
        },
        ExpressionAttributeNames: {
            '#uid': 'userId_type_deviceLocalName'
        }
    };
   
    if( nextPaginationKey ) {
        params.ExclusiveStartKey = {"userID": userId, "measurementDate": Number(nextPaginationKey)};
    }
    params.ScanIndexForward = false;
    console.log(params);
 
    return dbClient.query(params).promise();
};

module.exports.getPatient = getPatient;
module.exports.getNonBpReadings = getNonBpReadings;
module.exports.getBpReadings = getBpReadings;
module.exports.verifyHospital = verifyHospital;
module.exports.getBpAlerts = getBpAlerts;
module.exports.getNonBpAlerts = getNonBpAlerts;