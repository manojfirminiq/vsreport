exports.ERRORS = {
    INTERNAL_SERVER_ERROR: {
        CODE: "INTERNAL_SERVER_ERROR",
        MESSAGE: "Internal Server Error"
    },
    USER_ID_REQ: {
        CODE: "USER_ID_REQ",
        MESSAGE: "User id required"
    },
    HOSPITAL_ID_REQ: {
        CODE: "HOSPITAL_ID_REQ",
        MESSAGE: "Hospital id required"
    },
    USER_NOT_FOUND: {
        CODE: "USER_NOT_FOUND",
        MESSAGE: "User not found"
    },
    HOSPITAL_ID_INVALID: {
        CODE: "HOSPITAL_ID_INVALID",
        MESSAGE: "Hospital Id is invalid"
    },
    FROM_DATE: {
        CODE: "INVALID_FROM_DATE",
        MESSAGE: "Invalid from date"
    },
    TO_DATE: {
        CODE: "INVALID_TO_DATE",
        MESSAGE: "Invalid to date"
    }
};

exports.TYPE = {
    BP: "bp",
    WEIGHT : "weight"
};

exports.HODES = "hodes";