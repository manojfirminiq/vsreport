const moment = require('moment');
const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS.js');

const filterBpReadings = (readings, from, to, timeZone) => {
    let deviceStartTime = moment(Number(from)).utcOffset(Number(timeZone) / 60).format("YYYYMMDDHHmmssSSS");
    let deviceEndTime = moment(Number(to)).utcOffset(Number(timeZone) / 60).format("YYYYMMDDHHmmssSSS");
    let validReadings = [];
    for(let reading of readings) {
        let localTime = moment(reading.measurementDate);
        let readingTimeZone = timeZone;
        if (reading.timeZone) {
            readingTimeZone = reading.timeZone;
        }
        localTime = localTime.utcOffset(Number(readingTimeZone) / 60);
        localTime = localTime.format("YYYYMMDDHHmmssSSS");
        if (Number(localTime) >= Number(deviceStartTime) && Number(localTime) <= Number(deviceEndTime)) {
            validReadings.push(reading);
        }
    }

    return validReadings;

};

const applyValidation = async ({doctorId, userId, measurementDateFrom, measurementDateTo, nextPaginationKey, timeZone, hospitalID}) => {
    if(!userId){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_ID_REQ.CODE});
    }
    if(!hospitalID){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE});
    }
    if(!timeZone) {
        timeZone = 0;
    }
    if( !measurementDateFrom ){
        return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.FROM_DATE.MESSAGE, "errorCode": CONSTANTS.ERRORS.FROM_DATE.CODE});
    }
    if( !measurementDateTo ) {
        return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.TO_DATE.MESSAGE, "errorCode": CONSTANTS.ERRORS.TO_DATE.CODE});
    }
    let hubID;
    try{
        let hospitalTableDetails = await DB.verifyHospital(hospitalID);

        if(!hospitalTableDetails){
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, "errorCode": CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE});
        }

        let {Items} = await DB.getPatient(doctorId, userId, hospitalTableDetails.DOCTOR_PATIENT);
        
        if(Items.length < 0) {
            return Promise.resolve({"success": false, "message": CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE, "errorCode": CONSTANTS.ERRORS.USER_NOT_FOUND.CODE});
        } else {
            // if hospital is hodes, get readings using hubId, else use userId
            if(hospitalTableDetails.DOCTOR_PATIENT.includes(CONSTANTS.HODES)) {
                userId = Items[0].hubID;
            }

            Items = Items.map(
                ({createdDate, modifiedDate,patientCode, attributes:{name, gender, dob, ...attributesRest}, ...rest}) =>
                ({name, gender, dob, patientCode})
            );            
        }
        
        let response = {
            success: true,
            data: {
                user: Items[0],
                bp: []
            }
        };
        response.data.weight=[];
        response.data.alerts=[];
        let alertObject;
        let finalAlertArray=[], finalBpAlertArray= [], finalNonBpAlertArray=[];
     
        let weight = await DB.getNonBpReadings(userId, hospitalTableDetails.NON_BP_TABLE, measurementDateFrom, measurementDateTo, nextPaginationKey)
        if(weight.Count > 0) {
            response.data.weight = weight.Items.filter(({ attributes }) => Number(attributes.deleteFlag) !== 1)
            .map(({
                    measurementDate,
                    attributes: {bmiValue, weight, bodyFatPercentage, timeZone, ...rest }
            }) => ( {
                measurementDate, timeZone,
                bmiValue: Number(bmiValue),
                weight: Number(weight), 
                bodyFatPercentage: Number(bodyFatPercentage)
            } ));
            
            if(weight.LastEvaluatedKey) {
                response.weight.nextPaginationKey = weight.LastEvaluatedKey;
            }
            response.data.weight = filterBpReadings(response.data.weight, measurementDateFrom, measurementDateTo, timeZone);
        }
        let nonBpAlerts = await DB.getNonBpAlerts(userId, hospitalTableDetails.ALERT_NON_BP_TABLE);
        if(nonBpAlerts.Count > 0) {
            nonBpAlerts = nonBpAlerts.Items;
            // nonBpAlerts = nonBpAlerts.Items.filter(({ alerts }) => (alerts.weight.resolveStatus) !== '1')
            for(let i=0; i<nonBpAlerts.length; i++) {
                alertObject = {
                    "resolveStatus" : nonBpAlerts[i].alerts.weight.resolveStatus,
                    "notes" : nonBpAlerts[i].alerts.weight.Notes,
                    "measurementDate" : nonBpAlerts[i].measurementDate,
                    "resolvedDate": nonBpAlerts[i].alerts.weight.resolveStatus == '1' ? nonBpAlerts[i].alerts.weight.resolvedDate : null,
                    "thresholdType24hr" : nonBpAlerts[i].alerts.weight.thresholdType24hr ? nonBpAlerts[i].alerts.weight.thresholdType24hr : null,
                    "weightDiff24hr" : nonBpAlerts[i].alerts.weight.weightDiff24hr ? nonBpAlerts[i].alerts.weight.weightDiff24hr : null,
                    "thresholdType72hr" : nonBpAlerts[i].alerts.weight.thresholdType72hr ? nonBpAlerts[i].alerts.weight.thresholdType72hr : null,
                    "weightDiff72hr" : nonBpAlerts[i].alerts.weight.weightDiff72hr ? nonBpAlerts[i].alerts.weight.weightDiff72hr : null,
                    "thresholdWeightUnit" : nonBpAlerts[i].alerts.thresholdWeightUnit ? nonBpAlerts[i].alerts.thresholdWeightUnit : null,
                    "threshold" : nonBpAlerts[i].alerts.threshold ? nonBpAlerts[i].alerts.threshold : null,
                    "type" : CONSTANTS.TYPE.WEIGHT
                }
                finalNonBpAlertArray.push(alertObject);
            }
        }
        
        let bpReadings = await DB.getBpReadings(userId, hospitalTableDetails.BP_TABLE, measurementDateFrom, measurementDateTo, nextPaginationKey);
        if(bpReadings.Count > 0) {
            response.data.bp = bpReadings.Items.filter(({ attributes }) => Number(attributes.deleteFlag) !== 1)
            .map(({
                    measurementDate,
                    attributes: {systolic, diastolic, pulse, irregularHB,timeZone, ...rest }
            }) => ( {
                measurementDate, timeZone,
                systolic: Number(systolic),
                diastolic: Number(diastolic), 
                pulse: Number(pulse),
                irregularHB: Number(irregularHB)
            } ));
            
            if(bpReadings.LastEvaluatedKey) {
                response.bp.nextPaginationKey = bpReadings.LastEvaluatedKey;
            }
        }

        let bpAlerts = await DB.getBpAlerts(userId, hospitalTableDetails.ALERT_BP_TABLE);        
        if(bpAlerts.Count > 0) {
            bpAlerts = bpAlerts.Items;
           // bpAlerts = bpAlerts.Items.filter(({ alerts }) => (alerts.bp.resolveStatus) !== '1')
            for(let i=0; i<bpAlerts.length; i++) {
                alertObject = {
                    "resolveStatus" : bpAlerts[i].alerts.bp.resolveStatus,
                    "sysDiff" : Number(bpAlerts[i].alerts.bp.sysDiff),
                    "diaDiff" : Number(bpAlerts[i].alerts.bp.diaDiff),
                    "pulseDiff" : Number(bpAlerts[i].alerts.bp.pulseDiff),
                    "notes" : bpAlerts[i].alerts.bp.Notes,
                    "measurementDate" : bpAlerts[i].measurementDate,
                    "resolvedDate": bpAlerts[i].alerts.bp.resolveStatus == '1' ? bpAlerts[i].alerts.bp.resolvedDate : null,
                    "type" : CONSTANTS.TYPE.BP
                }
                finalBpAlertArray.push(alertObject);
            }
        }
        finalAlertArray = finalNonBpAlertArray.concat(finalBpAlertArray);
        response.data.alerts = finalAlertArray;
        response.data.bp = filterBpReadings(response.data.bp, measurementDateFrom, measurementDateTo, timeZone);
        
        return Promise.resolve(response);
    } catch (error) {
        return Promise.resolve({
            success: false,
            message: error,
            errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
        });
    }
};

exports.handler = async (event, context, callback) => {
    console.log(event);
    let response = await applyValidation(event);

    callback(null, response);
};