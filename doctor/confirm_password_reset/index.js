
const crypto = require('crypto');
const AWS = require('aws-sdk');
const https = require('https');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true});
var lambda = new AWS.Lambda({
    region: process.env.REGION //change to your region
});

let invalidateTokens = async ( emailAddress ) => {
    const payLoad = {
        "emailAddress" : emailAddress
    };
    return new Promise((resolve, reject) => {
            lambda.invoke({
            FunctionName: process.env.CLIENT_LAMBDA,
            Payload: JSON.stringify(payLoad) // pass params
        }, function(error, data) {
            if(error){
                reject(error);
            } else{
                resolve(data);
            }
        });
});
};

const cognitoUpdatePassword = async (emailAddress, password, resetCode) => {
    return new Promise((resolve) => {
            new AWS.CognitoIdentityServiceProvider().confirmForgotPassword({
                Username: emailAddress,
                ClientId : process.env.CLIENT_ID,
                ConfirmationCode: resetCode,
                Password: password
            }, (err, res) => {
                console.log(err, res);
    if(err){
        if(err.code === "ExpiredCodeException"){
            err.message = "Reset password link is expired or invalid. Please request another password reset email from the login page."
        }
        resolve({ success: false, message: err.message });
    } else {
        resolve({ success: true, message: 'Password updated successfully.' });
    }
});
});
};

const applyValidation = async ({ emailAddress, password, resetCode }) => {
    let response = await cognitoUpdatePassword(emailAddress, password, resetCode);
    if(response.success) {
        await invalidateTokens(emailAddress);
    }

    return response;
};

exports.handler = async (event, context, callback) => {
    const response = await applyValidation(event);
    callback(null, response);
};