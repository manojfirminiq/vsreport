const DB = require("./dynamodb");
const CONSTANTS = require("./CONSTANTS");

const getYearMonth = timestamp => {
  const date = new Date(timestamp);
  let month = date.getMonth() + 1;
  month = month < CONSTANTS.TENTH ? CONSTANTS.ZERO + month : month;
  const year = date.getFullYear().toString();
  return year + month;
};

const getClaimData = async (
  hospitalID,
  CLAIM_TABLE,
  financialStartDate,
  financialEndDate,
  doctorPatientsData
) => {
  let claimData = [];
  let startYearMonth = parseInt(getYearMonth(parseInt(financialStartDate)));
  let endYearMonth = parseInt(getYearMonth(parseInt(financialEndDate)));
  let claimRecords = await DB.getClaimData(
    hospitalID,
    CLAIM_TABLE,
    startYearMonth,
    endYearMonth
  );
  if (claimRecords.Count > 0 && claimRecords.Items) {
    let claimUserData = claimRecords.Items.map(data => {
      let claimYearMonth = {
        count:
          data.attributes && data.attributes.countOfUsers
            ? data.attributes.countOfUsers
            : null,
        yearMonth: data.yearMonth ? data.yearMonth : null,
        claimTimestamp: data.attributes.claimTimestamp
          ? data.attributes.claimTimestamp
          : null,
        status: data.attributes.status
          ? data.attributes.status
          : CONSTANTS.READY_FOR_SUBMISSION,
        users: []
      };
      if (
        data.attributes &&
        data.attributes.userIDBilled &&
        data.attributes.userIDBilled.length != 0
      ) {
        let billedUserData = data.attributes.userIDBilled.map(billedDUserId => {
          let patientsNames = doctorPatientsData.filter(
            PatientsData =>
              PatientsData.userID && PatientsData.userID == billedDUserId
          );
          if (patientsNames.length != 0) {
            return {
              patientName: patientsNames[0].name ? patientsNames[0].name : null,
              userID: billedDUserId ? billedDUserId : null
            };
          }
        });
        claimYearMonth.users = billedUserData;
      }
      claimData.push(claimYearMonth);
    });
  }
  return Promise.resolve({
    success: true,
    claimData: claimData
  });
};

const get_99453Data = (_99453Details, financialStartDate, financialEndDate) => {
  return _99453Details
    .filter(
      patientData =>
        patientData.attributes &&
        patientData.attributes.transaction &&
        patientData.attributes.transaction.dateOfService &&
        parseInt(patientData.attributes.transaction.dateOfService) >=
          parseInt(financialStartDate) &&
        parseInt(patientData.attributes.transaction.dateOfService) <=
          parseInt(financialEndDate)
    )
    .map(data => ({
      patientName: data.name ? data.name : null,
      status: data.attributes.status
        ? data.attributes.status
        : CONSTANTS.READY_FOR_SUBMISSION,
      claimTimestamp: data.attributes.claimTimestamp
        ? data.attributes.claimTimestamp
        : null,
      dateOfService: data.attributes.transaction.dateOfService
        ? data.attributes.transaction.dateOfService
        : null,
      userID: data.userID ? data.userID : null
    }));
};

const getFinanceData = async (
  doctorPatientsData,
  financialStartDate,
  financialEndDate,
  FINANCE_99453_TABLE,
  FINANCE_99454_TABLE
) => {
  let _99453Details = [];
  let financeData = [];
  for (let i = 0; i < doctorPatientsData.length; i++) {
    let userID = doctorPatientsData[i].userID;
    let _99453UserData = await DB.get_99453_Data(userID, FINANCE_99453_TABLE);
    if (_99453UserData.length > 0) {
      _99453UserData[0].name = doctorPatientsData[i].name
        ? doctorPatientsData[i].name
        : null;
      _99453Details.push(_99453UserData[0]);
    }

    let financeRecords = await DB.getFinanceData(userID, FINANCE_99454_TABLE);

    if (financeRecords.Count > 0 && financeRecords.Items) {
      let financeUserData = financeRecords.Items.filter(
        data =>
          parseInt(data.billingEndDate) >= parseInt(financialStartDate) &&
          parseInt(data.billingEndDate) <= parseInt(financialEndDate)
      ).map(data => {
        return {
          patientName: doctorPatientsData[i].name
            ? doctorPatientsData[i].name
            : null,
          orderID: data.attributes.orderID ? data.attributes.orderID : null,
          status: data.attributes.status
            ? data.attributes.status
            : CONSTANTS.READY_FOR_SUBMISSION,
          billingStartDate: data.billingStartDate
            ? data.billingStartDate
            : null,
          billingEndDate: data.billingEndDate ? data.billingEndDate : null,
          claimTimestamp: data.attributes.claimTimestamp
            ? data.attributes.claimTimestamp
            : null,
          userID: data.userID ? data.userID : null
        };
      });
      financeData = [...financeData, ...financeUserData];
    }
  }
  return Promise.resolve({
    success: true,
    financeData: financeData,
    _99453Details: _99453Details
  });
};

const getDoctorPatients = async (doctorId, hospitalTable) => {
  let { Items } = await DB.getPatient(doctorId, hospitalTable);
  if (Items.length < 0) {
    return Promise.resolve({
      success: false
    });
  } else {
    let patients = Items.map(
      ({
        userID,
        createdDate,
        modifiedDate,
        patientCode,
        attributes: { name, gender, dob, ...attributesRest },
        ...rest
      }) => ({ userID, name, gender, dob, patientCode })
    );
    return Promise.resolve({
      success: true,
      data: patients
    });
  }
};

const applyValidation = async ({
  doctorId,
  hospitalID,
  financialStartDate,
  financialEndDate
}) => {
  try {
    if (!hospitalID) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
        errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
      });
    }

    let hospitalTableDetails = await DB.verifyHospital(hospitalID);
    if (!hospitalTableDetails) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
        errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
      });
    }

    if (!financialStartDate && financialEndDate) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.START_DATE_REQ.MESSAGE,
        errorCode: CONSTANTS.ERRORS.START_DATE_REQ.CODE
      });
    }
    if (!financialEndDate && financialStartDate) {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.END_DATE_REQ.MESSAGE,
        errorCode: CONSTANTS.ERRORS.END_DATE_REQ.CODE
      });
    }

    if (!financialStartDate && !financialEndDate) {
      let currentDate = new Date();
      financialEndDate = currentDate.getTime();
      currentDate.setFullYear(currentDate.getFullYear() - 1);
      financialStartDate = currentDate.getTime();
    }

    let _99453Details = [];
    let financeData = [];
    let claimData = [];
    let doctorPatientsData;
    let _99453Data = [];

    let doctorPatientsRecords = await getDoctorPatients(
      doctorId,
      hospitalTableDetails.DOCTOR_PATIENT
    );
    if (doctorPatientsRecords.success) {
      doctorPatientsData = doctorPatientsRecords.data;

      let financeRecords = await getFinanceData(
        doctorPatientsData,
        financialStartDate,
        financialEndDate,
        hospitalTableDetails.FINANCE_99453_TABLE,
        hospitalTableDetails.FINANCE_99454_TABLE
      );

      if (financeRecords.success) {
        _99453Details = financeRecords._99453Details;
        financeData = financeRecords.financeData;
      }

      let claimRecords = await getClaimData(
        hospitalID,
        hospitalTableDetails.CLAIM_TABLE,
        financialStartDate,
        financialEndDate,
        doctorPatientsData
      );

      if (claimRecords.success) {
        claimData = claimRecords.claimData;
      }

      _99453Data = get_99453Data(
        _99453Details,
        financialStartDate,
        financialEndDate
      );

      return Promise.resolve({
        success: true,
        data: {
          _99453: _99453Data,
          _99454: financeData,
          monthly: claimData
        }
      });
    } else {
      return Promise.resolve({
        success: false,
        message: CONSTANTS.ERRORS.PATIENTS_NOT_FOUND.MESSAGE,
        errorCode: CONSTANTS.ERRORS.PATIENTS_NOT_FOUND.CODE
      });
    }
  } catch (error) {
    return Promise.resolve({
      success: false,
      message: error,
      errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
    });
  }
};

exports.handler = async (event, context, callback) => {
  console.log(event);
  let response = await applyValidation(event);
  callback(null, response);
};
