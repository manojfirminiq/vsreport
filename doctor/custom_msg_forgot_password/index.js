exports.handler = (event, context, callback) => {
    console.log(event.triggerSource);
    if(event.userPoolId === process.env.USERPOOLID) {
        let url = `${process.env.DOMAIN}?token=${event.request.codeParameter}&emailAddress=${event.request.userAttributes.email}`;
        console.log(url);
        // Identify why was this function invoked
        if(event.triggerSource === "CustomMessage_ForgotPassword") {
            // Ensure that your message contains event.request.codeParameter. This is the placeholder for code that will be sent
            event.response.emailSubject = "Omron - Password Reset Requested";
            event.response.emailMessage = `
            Omron received a request to reset the password for the user ${event.request.userAttributes.email}.<br><br>
            To complete the request, click <a href='${url}'> here</a><br><br>
            <a href='${url}'> ${url}</a><br><br>
            ©2018 Omron Healthcare, Inc. All Rights Reserved.<br>
            1925 W Field Court, Lake Forest, IL 60045 | 866.216.1333 | OmronHealthcare.com<br><br>
            This email message was sent from a notification only address that cannot accept incoming email. Do not reply to this message.`;
        } else if(event.triggerSource === "CustomMessage_SignUp" || event.triggerSource === "CustomMessage_ResendCode") {
            console.log("inside custom");
            url = `${process.env.DOMAIN_EMAIL_VERIFY}?userName=${event.userName}&confirmationCode=${event.request.codeParameter}&clientId=${process.env.CLIENT_ID}`;
            console.log(url);
            console.log(event);
            event.response.emailSubject = "Omron - Email Verification Required";
            event.response.emailMessage = `
            Welcome to Omron! In order to complete your registration we need to validate that the email you provided during registration is actually owned by you.
            Please click the link below to complete the process:
            <br><br>
            To verify, click <a href="${url}">here</a> 
            <br><br>
            ©2018 Omron Healthcare, Inc. All Rights Reserved.<br>
            1925 W Field Court, Lake Forest, IL 60045 | 866.216.1333 | OmronHealthcare.com
            <br><br>
            This email message was sent from a notification only address that cannot accept incoming email. Do not reply to this message.`;
            console.log("----------",event.response.emailMessage);
        }
        // Create custom message for other events
    }
    // Customize messages for other user pools

    // Return to Amazon Cognito
    callback(null, event);
};