exports.VALIDATION_MESSAGES = {
    EMAIL_REQ : "emailAddress is required",
    PASSWORD_REQ : "password is required",
    INVALID_INPUT: "Please check the input parameters!",
    DEVICENAME_REQ: "Please provide a devicelocalName",
    USER_NOT_FOUND: "User not found",
    USER_ALREADY_EXIST: "User already exist",
    INVALID_PASSWORD: "Password not long enough",
    INVALID_STUDY_CODE: 'Invalid study code',
    INVALID_REGION: "Invalid Region.",
    USER_NOT_CONFIRMED: "USER_NOT_CONFIRMED",
    INVALID_GRANT: 'Incorrect username or password',
    INVALID_CREDS_VERIFICATION: "INVALID_CREDS_VERIFICATION"
};

exports.ERROR_CODES = {
    "USER_ALREADY_EXISTS": "USER_ALREADY_EXISTS",
    "INVALID_CREDS_VERIFICATION": "INVALID_CREDS_VERIFICATION",
    "INVALID_GRANT": "INVALID_GRANT",
    "USER_NOT_CONFIRMED": "USER_NOT_CONFIRMED"
};

exports.PASSWORD_LENGTH = 8;
exports.MESSAGES = {
    SUCCESS: "We have sent you a verification mail to the email address you provided. Please check your inbox and click on the verification link in the mail to proceed with the account setup"
};

exports.RESET_PASSWORD_MESSAGES = {
    SUCCESS: "Check your inbox for the code to reset your password"
};

exports.COGNITO_ERROR = {
	USER_NOT_CONFIRMED: 'UserNotConfirmedException',
	USER_NOT_FOUND: 'UserNotFoundException',
	INVALID_GRANT: 'NotAuthorizedException'
};
