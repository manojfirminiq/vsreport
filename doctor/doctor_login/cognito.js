'use strict';
const crypto = require('crypto');
const CLIENT_ID = process.env.CLIENTID;
const USER_POOL_ID = process.env.USERPOOLID;
const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
let cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const CONSTANTS = require('./CONSTANTS');

const USER_NOT_CONFIRMED = "User is not confirmed.";

const LOGIN_ERROR = "Incorrect username or password.";

const RESET_PASSWORD = "Password reset required for the user";

const reformatToken = (token) => {
    token = JSON.parse(JSON.stringify(token));

    return {
        "success": true,
        "accessToken": token.IdToken,
        "refreshToken": token.RefreshToken,
        "expiresIn": token.ExpiresIn,
        "tokenType": token.TokenType
    };
};

const changePassword = (email, password, userType) => {
    return new Promise(function (resolve, reject) {
        let params = {
            Password: password,
            UserPoolId: process.env.USER_POOL_ID,
            Username: email,
            Permanent: true
        };
        cognitoidentityserviceprovider.adminSetUserPassword(params, function (err, data) {
            if (err) {
                console.log(err);
                reject(err)
            }
            else{
                resolve();
            }
        });
    });
};


const login = async ({emailAddress, password, refreshToken}) => {
    let params = {
        AuthFlow: 'USER_PASSWORD_AUTH',
        ClientId: CLIENT_ID,
        AuthParameters: {USERNAME: emailAddress, PASSWORD: password}
    };
    
    cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
        region:process.env.REGION
    });

    if(refreshToken) {
        params.AuthFlow = "REFRESH_TOKEN";
        params.AuthParameters = {REFRESH_TOKEN: refreshToken};
    }

    let auth = {};
    try{
        auth = await cognitoidentityserviceprovider.initiateAuth(params).promise();
        if(auth.error){
            // if error throw error
            throw auth.error;
        } else if(auth.ChallengeName && auth.ChallengeName.length > 0) {
            throw auth;
        }
        
        return reformatToken(auth.AuthenticationResult);
    } catch (error){
        console.log(error.code);
        
        const checkInRegion_2 = process.env.CHECKIN_REGION_2 ? Number(process.env.CHECKIN_REGION_2) : false;
        if(checkInRegion_2 && error.code === CONSTANTS.COGNITO_ERROR.INVALID_GRANT) {
            cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
                region:process.env.REGION_2
            });
            params.ClientId = process.env.CLIENT_ID_2;
            auth = await cognitoidentityserviceprovider.initiateAuth(params).promise();
            if(!auth.error && (!auth.ChallengeName || auth.ChallengeName.length === 0)){
                // if exists migrate user to region 1
                try{
                    await changePassword(emailAddress, password);
                    
                     cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
                        region:process.env.REGION
                    });
                    params.ClientId = CLIENT_ID;
                    auth = await cognitoidentityserviceprovider.initiateAuth(params).promise();
                    return reformatToken(auth.AuthenticationResult);
                } catch(err) {
                    console.log(err);
                }
            }
            
        }
        let response = {
            success: false,
            errorCode: "",
            message: error.message
        };
        switch(error.code){
            case CONSTANTS.COGNITO_ERROR.INVALID_GRANT:
            case CONSTANTS.COGNITO_ERROR.USER_NOT_FOUND:
                response.errorCode = CONSTANTS.ERROR_CODES.INVALID_GRANT;
                response.message = CONSTANTS.VALIDATION_MESSAGES.INVALID_GRANT;
                break;
            case CONSTANTS.COGNITO_ERROR.USER_NOT_CONFIRMED:
                response.errorCode = CONSTANTS.ERROR_CODES.USER_NOT_CONFIRMED;
                break;
		}
        return Promise.reject(response);
    }
};

module.exports.login = login;