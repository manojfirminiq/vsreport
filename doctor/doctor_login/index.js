const crypto = require('crypto');
const CONSTANTS = require('./CONSTANTS.js');
const COGNITO = require('./cognito');


const applyValidation = async ({ emailAddress, password, refreshToken}) => {
    try {
        if(refreshToken)
        {
            let tokens = await COGNITO.login({refreshToken});
            if(tokens.errorCode){
                return Promise.resolve({"success": false, "message":"Invalid Refresh Token", "errorCode": "INVALID_REFRESH_TOKEN"});
            } else {
                tokens['refreshToken'] = refreshToken;
                return Promise.resolve(tokens);
             }
        }
        else{
            if( !emailAddress ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.EMAIL_REQ, "errorCode": "REQUIRED_PARAMETER"});
            if( !password ) return Promise.resolve({"success": false, "message": CONSTANTS.VALIDATION_MESSAGES.PASSWORD_REQ, "errorCode": "REQUIRED_PARAMETER"});
            emailAddress = emailAddress.toLowerCase();
            return await COGNITO.login({emailAddress, password});
        }

    } catch (error) {
        console.log(error);
        return Promise.resolve({"success": false, "message": error.message ? error.message : error, "errorCode": error.errorCode ? error.errorCode : "INTERNAL_SERVER_ERROR"});
    }
};

exports.handler = async (event, context, callback) => {
    const response = await applyValidation(event);
    console.log(response);
    callback(null, response);
};