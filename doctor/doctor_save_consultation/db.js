const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION});
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_CONSULTATION = CONSTANTS.DOCTOR_CONSULTATION;
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;


let verifyHospital = async (hospitalId) =>{
    let paramsTable = {
        TableName : HOSPITAL_TABLE,
        KeyConditionExpression: "#hospital = :id",
        ExpressionAttributeNames:{
        "#hospital": "hospitalID"
        },
        ExpressionAttributeValues: {
        ":id": hospitalId
        }
    };
    const {Count, Items} = await dbClient.query(paramsTable).promise();
    if(Items.length == 0){
        return false;
    }else{
         return process.env.STAGE + '_' + CONSTANTS.RPM + '_' + Items[0].dbIdentifier + `_`+ DOCTOR_CONSULTATION;
    }
};

const updatePatientConsultation = async (consultation, hospitalTable) => {
    consultation.modifiedDate = new Date().getTime();
    let params = {
        TableName: hospitalTable,
        Item: consultation
    };
    return dbClient.put(params).promise();
};

const getAllConsultation = async (doctorId, hospitalTable) => {
    let params = {
        TableName : hospitalTable,
        KeyConditionExpression : "#doctorId = :doctorId",
        ExpressionAttributeValues: {
            ':doctorId': doctorId,
            ':isdeletedValue': true
        },
        ExpressionAttributeNames: {
            '#doctorId': 'doctorID',
            '#isdeleted': 'isdeleted'
        },
        FilterExpression : `#isdeleted <> :isdeletedValue`,
        ScanForwardIndex:false
    };
    return dbClient.query(params).promise();
};

module.exports.getAllConsultation = getAllConsultation;
module.exports.updatePatientConsultation = updatePatientConsultation;
module.exports.verifyHospital = verifyHospital;