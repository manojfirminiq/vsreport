### table-trigger provisioning script

#!/bin/sh
set -x

Region1="us-west-2"
Region2="us-east-1"

while getopts e:h: flag
do
    case "${flag}" in
        e) env=${OPTARG};;
        h) hospital=${OPTARG};;
    esac
done
echo "environment: ${env}";
echo "hospital Name: ${hospital}";

## Global Table 1

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_bp_readings --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=measurementDate,AttributeType=N --key-schema AttributeName=userID,KeyType=HASH AttributeName=measurementDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_bp_readings --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=measurementDate,AttributeType=N --key-schema AttributeName=userID,KeyType=HASH AttributeName=measurementDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_bp_readings --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 2

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_bp_readings_alerts --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=measurementDate,AttributeType=N --key-schema AttributeName=userID,KeyType=HASH AttributeName=measurementDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings_alerts" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings_alerts" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings_alerts" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings_alerts" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_bp_readings_alerts --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=measurementDate,AttributeType=N --key-schema AttributeName=userID,KeyType=HASH AttributeName=measurementDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings_alerts" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings_alerts" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings_alerts" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_bp_readings_alerts" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_bp_readings_alerts --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 3

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_claims --attribute-definitions AttributeName=ehrID,AttributeType=S AttributeName=yearMonth,AttributeType=N --key-schema AttributeName=ehrID,KeyType=HASH AttributeName=yearMonth,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_claims" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_claims" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_claims" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_claims" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_claims --attribute-definitions AttributeName=ehrID,AttributeType=S AttributeName=yearMonth,AttributeType=N --key-schema AttributeName=ehrID,KeyType=HASH AttributeName=yearMonth,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_claims" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_claims" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_claims" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_claims" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_claims --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 4

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_bp_average --attribute-definitions AttributeName=userID,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_bp_average" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_bp_average" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_bp_average" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_bp_average" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_bp_average --attribute-definitions AttributeName=userID,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_bp_average" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_bp_average" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_bp_average" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_bp_average" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_doctor_bp_average --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 5

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_consultation --attribute-definitions AttributeName=doctorID,AttributeType=S AttributeName=userID_startTime,AttributeType=S --key-schema AttributeName=doctorID,KeyType=HASH AttributeName=userID_startTime,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_consultation" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_consultation" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_consultation" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_consultation" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_consultation --attribute-definitions AttributeName=doctorID,AttributeType=S AttributeName=userID_startTime,AttributeType=S --key-schema AttributeName=doctorID,KeyType=HASH AttributeName=userID_startTime,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_consultation" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_consultation" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_consultation" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_consultation" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_doctor_consultation --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 6

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_notes --attribute-definitions AttributeName=doctorID,AttributeType=S AttributeName=userID_startDate,AttributeType=S AttributeName=userID,AttributeType=S AttributeName=startDate,AttributeType=N --key-schema AttributeName=doctorID,KeyType=HASH AttributeName=userID_startDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=userID-startDate-index,KeySchema=["{AttributeName=userID,KeyType=HASH}","{AttributeName=startDate,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=doctorID-userID-index,KeySchema=["{AttributeName=doctorID,KeyType=HASH}","{AttributeName=userID,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/userID-startDate-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/userID-startDate-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/userID-startDate-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/userID-startDate-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_notes --attribute-definitions AttributeName=doctorID,AttributeType=S AttributeName=userID_startDate,AttributeType=S AttributeName=userID,AttributeType=S AttributeName=startDate,AttributeType=N --key-schema AttributeName=doctorID,KeyType=HASH AttributeName=userID_startDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=userID-startDate-index,KeySchema=["{AttributeName=userID,KeyType=HASH}","{AttributeName=startDate,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=doctorID-userID-index,KeySchema=["{AttributeName=doctorID,KeyType=HASH}","{AttributeName=userID,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/userID-startDate-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/userID-startDate-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/userID-startDate-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/userID-startDate-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_notes/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_doctor_notes --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 7

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_patient --attribute-definitions AttributeName=doctorID,AttributeType=S AttributeName=userID,AttributeType=S --key-schema AttributeName=doctorID,KeyType=HASH AttributeName=userID,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=userID-doctorID-index,KeySchema=["{AttributeName=userID,KeyType=HASH}","{AttributeName=doctorID,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=doctorID-userID-index,KeySchema=["{AttributeName=doctorID,KeyType=HASH}","{AttributeName=userID,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/userID-doctorID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/userID-doctorID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/userID-doctorID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/userID-doctorID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_patient --attribute-definitions AttributeName=doctorID,AttributeType=S AttributeName=userID,AttributeType=S --key-schema AttributeName=doctorID,KeyType=HASH AttributeName=userID,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=userID-doctorID-index,KeySchema=["{AttributeName=userID,KeyType=HASH}","{AttributeName=doctorID,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=doctorID-userID-index,KeySchema=["{AttributeName=doctorID,KeyType=HASH}","{AttributeName=userID,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/userID-doctorID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/userID-doctorID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/userID-doctorID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/userID-doctorID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_patient/index/doctorID-userID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_doctor_patient --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 8

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_profile --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=npID,AttributeType=S AttributeName=doctorID,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=npID-index,KeySchema=["{AttributeName=npID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=doctorID-index,KeySchema=["{AttributeName=doctorID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/npID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/npID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/npID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/npID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/doctorID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/doctorID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/doctorID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/doctorID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_doctor_profile --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=npID,AttributeType=S AttributeName=doctorID,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=npID-index,KeySchema=["{AttributeName=npID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=doctorID-index,KeySchema=["{AttributeName=doctorID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/npID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/npID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/npID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/npID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/doctorID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/doctorID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/doctorID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_doctor_profile/index/doctorID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_doctor_profile --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 9

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_ehr_fin_transaction --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=billingStartDate,AttributeType=N --key-schema AttributeName=userID,KeyType=HASH AttributeName=billingStartDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_transaction" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_transaction" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_transaction" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_transaction" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_ehr_fin_transaction --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=billingStartDate,AttributeType=N --key-schema AttributeName=userID,KeyType=HASH AttributeName=billingStartDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_transaction" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_transaction" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_transaction" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_transaction" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_ehr_fin_transaction --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


##Global Table 10

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_ehr_mapping --attribute-definitions AttributeName=sourceID_destinationID_dataModel,AttributeType=S AttributeName=ehrID,AttributeType=S AttributeName=dataModel,AttributeType=S --key-schema AttributeName=sourceID_destinationID_dataModel,KeyType=HASH AttributeName=ehrID,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=ehrID-dataModel-index,KeySchema=["{AttributeName=ehrID,KeyType=HASH}","{AttributeName=dataModel,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping/index/ehrID-dataModel-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping/index/ehrID-dataModel-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping/index/ehrID-dataModel-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping/index/ehrID-dataModel-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_ehr_mapping --attribute-definitions AttributeName=sourceID_destinationID_dataModel,AttributeType=S AttributeName=ehrID,AttributeType=S AttributeName=dataModel,AttributeType=S --key-schema AttributeName=sourceID_destinationID_dataModel,KeyType=HASH AttributeName=ehrID,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=ehrID-dataModel-index,KeySchema=["{AttributeName=ehrID,KeyType=HASH}","{AttributeName=dataModel,KeyType=RANGE}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping/index/ehrID-dataModel-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping/index/ehrID-dataModel-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping/index/ehrID-dataModel-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_mapping/index/ehrID-dataModel-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_ehr_mapping --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 11

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_ehr_user --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=imei,AttributeType=N AttributeName=ehrID,AttributeType=S AttributeName=npID,AttributeType=S AttributeName=haUserID,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=imei-index,KeySchema=["{AttributeName=imei,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=ehrID-index,KeySchema=["{AttributeName=ehrID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=npID-index,KeySchema=["{AttributeName=npID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=haUserID-index,KeySchema=["{AttributeName=haUserID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/imei-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/imei-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/imei-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/imei-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/ehrID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/ehrID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/ehrID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/ehrID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/npID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/npID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/npID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/npID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/haUserID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/haUserID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/haUserID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/haUserID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_ehr_user --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=imei,AttributeType=N AttributeName=ehrID,AttributeType=S AttributeName=npID,AttributeType=S AttributeName=haUserID,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --global-secondary-indexes IndexName=imei-index,KeySchema=["{AttributeName=imei,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=ehrID-index,KeySchema=["{AttributeName=ehrID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=npID-index,KeySchema=["{AttributeName=npID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" IndexName=haUserID-index,KeySchema=["{AttributeName=haUserID,KeyType=HASH}"],Projection="{ProjectionType=ALL}",ProvisionedThroughput="{ReadCapacityUnits=5,WriteCapacityUnits=5}" --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/imei-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/imei-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/imei-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/imei-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/ehrID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/ehrID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/ehrID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/ehrID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/npID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/npID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/npID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/npID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/haUserID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/haUserID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/haUserID-index" --scalable-dimension "dynamodb:index:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_user/index/haUserID-index" --scalable-dimension "dynamodb:index:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_ehr_user --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 12

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_non_bp_readings --attribute-definitions AttributeName=userId_type_deviceLocalName,AttributeType=S AttributeName=measurementDate,AttributeType=N --key-schema AttributeName=userId_type_deviceLocalName,KeyType=HASH AttributeName=measurementDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_non_bp_readings --attribute-definitions AttributeName=userId_type_deviceLocalName,AttributeType=S AttributeName=measurementDate,AttributeType=N --key-schema AttributeName=userId_type_deviceLocalName,KeyType=HASH AttributeName=measurementDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_non_bp_readings --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 13

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_non_bp_readings_alerts --attribute-definitions AttributeName=userId_type_deviceLocalName,AttributeType=S AttributeName=measurementDate,AttributeType=N --key-schema AttributeName=userId_type_deviceLocalName,KeyType=HASH AttributeName=measurementDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings_alerts" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings_alerts" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings_alerts" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings_alerts" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_non_bp_readings_alerts --attribute-definitions AttributeName=userId_type_deviceLocalName,AttributeType=S AttributeName=measurementDate,AttributeType=N --key-schema AttributeName=userId_type_deviceLocalName,KeyType=HASH AttributeName=measurementDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings_alerts" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings_alerts" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings_alerts" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_non_bp_readings_alerts" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_non_bp_readings_alerts --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 14

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_order_details --attribute-definitions AttributeName=orderID,AttributeType=S --key-schema AttributeName=orderID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_details" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_details" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_details" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_details" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_order_details --attribute-definitions AttributeName=orderID,AttributeType=S --key-schema AttributeName=orderID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_details" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_details" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_details" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_details" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_order_details --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 15

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_order_processing --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=orderID_createdDate,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH AttributeName=orderID_createdDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_processing" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_processing" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_processing" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_processing" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_order_processing --attribute-definitions AttributeName=userID,AttributeType=S AttributeName=orderID_createdDate,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH AttributeName=orderID_createdDate,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_processing" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_processing" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_processing" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_order_processing" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_order_processing --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Global Table 16

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_ehr_fin_99453_transaction --attribute-definitions AttributeName=userID,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region1}

if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_99453_transaction" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_99453_transaction" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_99453_transaction" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region1}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_99453_transaction" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region1}

fi


if [ "$env" = "stg" -o "$env" = "prd" ]
then

aws dynamodb create-table --table-name ${env}_global_rpm_${hospital}_ehr_fin_99453_transaction --attribute-definitions AttributeName=userID,AttributeType=S --key-schema AttributeName=userID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType=NEW_AND_OLD_IMAGES --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_99453_transaction" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling register-scalable-target --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_99453_transaction" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --min-capacity 5 --max-capacity 40000 --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_99453_transaction" --scalable-dimension "dynamodb:table:WriteCapacityUnits" --policy-name "DynamoDBWriteCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policywrite.json --region ${Region2}

aws application-autoscaling put-scaling-policy --service-namespace dynamodb --resource-id "table/${env}_global_rpm_${hospital}_ehr_fin_99453_transaction" --scalable-dimension "dynamodb:table:ReadCapacityUnits" --policy-name "DynamoDBReadCapacityUtilization" --policy-type "TargetTrackingScaling" --target-tracking-scaling-policy-configuration file://scaling-policyread.json --region ${Region2}

aws dynamodb create-global-table --global-table-name ${env}_global_rpm_${hospital}_ehr_fin_99453_transaction --replication-group RegionName=${Region1} RegionName=${Region2} --region ${Region1}

fi


## Enable PITR

if [ "$env" = "prd" ]
then

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_bp_readings --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_bp_readings_alerts --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_claims --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_doctor_bp_average --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_doctor_consultation --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_doctor_notes --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_doctor_patient --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_doctor_profile --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_ehr_fin_transaction --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_ehr_mapping --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_ehr_user --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_non_bp_readings --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_non_bp_readings_alerts --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_order_details --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_order_processing --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

aws dynamodb update-continuous-backups --table-name ${env}_global_rpm_${hospital}_ehr_fin_99453_transaction --point-in-time-recovery-specification PointInTimeRecoveryEnabled=true --region ${Region1}

fi


## Lambda Triggers Region1

stream1=$( aws dynamodbstreams list-streams --table-name ${env}_global_rpm_${hospital}_bp_readings --query 'Streams[*].[StreamArn]' --region ${Region1} --output text )

aws lambda create-event-source-mapping --function-name ${env}_global_rpm_common_doctor_bp_trigger_cron --batch-size 100 --event-source-arn $stream1 --starting-position LATEST --region ${Region1}

aws lambda create-event-source-mapping --function-name ${env}_global_rpm_common_save_readings_alerts --batch-size 100 --event-source-arn $stream1 --starting-position LATEST --region ${Region1}


stream2=$( aws dynamodbstreams list-streams --table-name ${env}_global_rpm_${hospital}_non_bp_readings --query 'Streams[*].[StreamArn]' --region ${Region1} --output text )

aws lambda create-event-source-mapping --function-name ${env}_global_rpm_common_doctor_bp_trigger_cron --batch-size 100 --event-source-arn $stream2 --starting-position LATEST --region ${Region1}

aws lambda create-event-source-mapping --function-name ${env}_global_rpm_common_save_readings_alerts --batch-size 100 --event-source-arn $stream2 --starting-position LATEST --region ${Region1}


stream3=$( aws dynamodbstreams list-streams --table-name ${env}_global_rpm_${hospital}_ehr_fin_transaction --query 'Streams[*].[StreamArn]' --region ${Region1} --output text )

aws lambda create-event-source-mapping --function-name ${env}_global_rpm_common_submit_claims --batch-size 100 --event-source-arn $stream3 --starting-position LATEST --region ${Region1}
