#!/bin/bash
export PM2_HOME=/root/.pm2
pm2 delete report
cd /data/stg-vitalsightreport/v2/doctor/generate_user_report
pm2 start app.js --name "report"