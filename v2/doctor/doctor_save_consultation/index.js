const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');
const trackPatient = require('track_patients_record');

const fetchPatientConsulation = (consultations, userId) => {
	return consultations
		.filter(consultation => {
			return userId === consultation.userID;
		})
		.sort(function (a, b) {
			return b.startTime - a.startTime;
		});
};

const checkConsultation = async (
	doctorId,
	hospitalTable,
	userId,
	start,
	end,
	notes,
	isManual,
	reason,
	contactPatient,
	notRecordedConfirmation,
	updateUserData
) => {
	contactPatient = contactPatient || 0;
	notRecordedConfirmation = notRecordedConfirmation || 0;
	const { Count, Items } = await DB.getAllConsultation(doctorId, hospitalTable);
	if (Count) {
		const patientConsulations = fetchPatientConsulation(Items, userId);
		if (patientConsulations && patientConsulations.length) {
			if (
				!end &&
				patientConsulations &&
				patientConsulations[0] &&
				!patientConsulations[0].endTime &&
				!notes
			) {
				throw new Error(CONSTANTS.ACTIVE_CONSULTATION_PRESENT);
			}
			// update consultation
			for (const patientConsulation of patientConsulations) {
				if (patientConsulation.startTime === start) {
					if (notes) patientConsulation.notes = notes;
					if (reason) patientConsulation.reason = reason;
					patientConsulation.updates = patientConsulation.updates || [];
					patientConsulation.updates.push(updateUserData);
					if (end) {
						patientConsulation.endTime = end;
						patientConsulation.consultationTime = Math.round(
							(patientConsulation.endTime - patientConsulation.startTime) / 1000
						);
						patientConsulation.consultationUnit = CONSTANTS.SECONDS;
					}
					return patientConsulation;
				}
			}

			// add consultation
			// consultation should not overlap
			if (end) {
				if (!patientConsulations[0].endTime) {
					if (
						patientConsulations[0].startTime < start ||
						patientConsulations[0].startTime < end
					) {
						throw new Error(CONSTANTS.CONSULTATION_OVERLAP);
					} else patientConsulations.splice(0, 1);
				}
				for (const patientConsulation of patientConsulations) {
					if (
						(patientConsulation.startTime < start &&
							patientConsulation.endTime >= start) ||
						(patientConsulation.startTime < end &&
							patientConsulation.endTime >= end)
					) {
						throw new Error(CONSTANTS.CONSULTATION_OVERLAP);
					}
				}
			} else if (start < patientConsulations[0].endTime) {
				throw new Error(CONSTANTS.CONSULTATION_OVERLAP);
			}
		}
	}
	return generateParams(
		doctorId,
		userId,
		start,
		end,
		notes,
		isManual,
		reason,
		contactPatient,
		notRecordedConfirmation,
		updateUserData
	);
};
const generateParams = (
	doctorId,
	userId,
	start,
	end,
	notes,
	isManual,
	reason,
	contactPatient,
	notRecordedConfirmation,
	updateUserData
) => {
	const params = {
		doctorID: doctorId,
		userID_startTime: userId + '_' + start,
		startTime: start,
		userID: userId,
		contactPatient: contactPatient,
		notRecordedConfirmation: notRecordedConfirmation,
		notes: notes || '',
		createdDate: Date.now(),
		consultationUnit: CONSTANTS.MINUTES,
		isdeleted: CONSTANTS.isdeleted,
		isManual: isManual || false,
		reason: reason || '',
		updates: []
	};
	params.updates.push(updateUserData);
	if (end) {
		params.endTime = end;
		params.consultationTime = Math.round((end - start) / 1000);
		params.consultationUnit = CONSTANTS.SECONDS;
	}
	return params;
};

const applyValidation = async ({
	doctorId,
	hospitalID,
	updateUserData,
	body: {
		userId,
		start,
		end,
		notes,
		isManual,
		reason,
		contactPatient,
		notRecordedConfirmation
	}
}) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
			});
		}
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		if (!start) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.START_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.START_REQUIRED.CODE
			});
		}
		const isValidStartDate = (new Date(start)).getTime() > 0;
		if (!isValidStartDate) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.MESSAGE,
				errorCode: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.CODE
			});
		}
		if (end) {
			const isValidEndDate = (new Date(end)).getTime() > 0;
			if (!isValidEndDate) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.MESSAGE,
					errorCode: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.CODE
				});
			}
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const params = await checkConsultation(
			doctorId,
			hospitalTable,
			userId,
			start,
			end,
			notes,
			isManual,
			reason,
			contactPatient,
			notRecordedConfirmation,
			updateUserData
		);
		await DB.updatePatientConsultation(params, hospitalTable);

		return Promise.resolve({
			success: true
		});
	} catch (error) {
		console.log(error);
		if (error.message === CONSTANTS.ACTIVE_CONSULTATION_PRESENT) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.ACTIVE_CONSULTATION_PRESENT.CODE,
				message: CONSTANTS.ERRORS.ACTIVE_CONSULTATION_PRESENT.MESSAGE
			});
		} else if (error.message === CONSTANTS.CONSULTATION_OVERLAP) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.CONSULTATION_OVERLAP.CODE,
				message: CONSTANTS.ERRORS.CONSULTATION_OVERLAP.MESSAGE
			});
		}
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
