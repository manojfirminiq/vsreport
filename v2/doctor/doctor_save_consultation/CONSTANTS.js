exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_NOT_FOUND: {
		CODE: 'USER_NOT_FOUND',
		MESSAGE: 'User not found'
	},
	USER_ID_REQUIRED: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'User Id is required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	START_REQUIRED: {
		CODE: 'START_REQUIRED',
		MESSAGE: 'start is required'
	},
	ACTIVE_CONSULTATION_PRESENT: {
		CODE: 'ACTIVE_CONSULTATION_PRESENT',
		MESSAGE: 'Consultation already active'
	},
	CONSULTATION_OVERLAP: {
		CODE: 'CONSULTATION_OVERLAP',
		MESSAGE: 'Consultation time is overlapping'
	},
	INPUT_VALIDATION_ERROR: {
		CODE: 'INPUT_VALIDATION_ERROR',
		MESSAGE: 'Error in input provided'
	}
};
exports.RPM = 'rpm';
exports.SECONDS = 'seconds';
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.isdeleted = false;
exports.ACTIVE_CONSULTATION_PRESENT = 'ACTIVE_CONSULTATION_PRESENT';
exports.CONSULTATION_OVERLAP = 'CONSULTATION_OVERLAP';
