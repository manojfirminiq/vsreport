const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const connectionTable = `${CONSTANTS.STAGE}${CONSTANTS.RPM}${CONSTANTS.TIMER_CONNECTION_TABLE}`;

const getConnectionDetails = async (connectionId) => {
	const params = {
		TableName: connectionTable,
		KeyConditionExpression: '#connectionID = :id',
		ExpressionAttributeNames: {
			'#connectionID': 'connectionID'
		},
		ExpressionAttributeValues: {
			':id': connectionId
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count === 0) return false;
	return Items[0];
};

const updatePatientConsultation = async (consultation, consultationTable) => {
	consultation.modifiedDate = new Date().getTime();
	const params = {
		TableName: consultationTable,
		Item: consultation
	};
	return await dbClient.put(params).promise();
};

const getLastConsultation = async (doctorId, consultationTable, userId) => {
	const params = {
		TableName: consultationTable,
		KeyConditions: {
			userID_startTime: {
    			ComparisonOperator: 'BEGINS_WITH',
				AttributeValueList: [
					userId
				]
			},
			doctorID: {
    			ComparisonOperator: 'EQ',
				AttributeValueList: [
					doctorId
				]
			}
		},
		ScanIndexForward: false
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count ? Items[0] : false;
};

module.exports.getConnectionDetails = getConnectionDetails;
module.exports.getLastConsultation = getLastConsultation;
module.exports.updatePatientConsultation = updatePatientConsultation;
