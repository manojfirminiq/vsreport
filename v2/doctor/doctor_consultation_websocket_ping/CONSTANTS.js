exports.TIMER_CONNECTION_TABLE = process.env.TIMER_CONNECTION_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = '_rpm_';
exports.TIME_OUT = 5000;
exports.SECONDS = 'seconds';
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	CONNECTION_ID_ERROR: {
		CODE: 'CONNECTION_ID_ERROR',
		MESSAGE: 'A connection with given connection Id doesn\'t exist'
	}
};
