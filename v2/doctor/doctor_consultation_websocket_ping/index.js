const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');

const applyValidation = async (event) => {
	try {
		const connectionDetails = await DB.getConnectionDetails(event.requestContext.connectionId);
		if (connectionDetails) {
			const {
				doctorID,
				userID,
				dbIdentifier,
				offSetInMilliSecs
			} = connectionDetails;
			console.log('connection details fetched');
			console.log('connectionID', event.requestContext.connectionId);
			const consultationTable = `${CONSTANTS.STAGE}${CONSTANTS.RPM}${dbIdentifier}_${CONSTANTS.DOCTOR_CONSULTATION}`;
			const requestEpochWithOffset = (Number(event.requestContext.requestTimeEpoch) + offSetInMilliSecs);
			if (!consultationTable) {
				console.log('no consultation table');
				return Promise.resolve(
					{
						statusCode: 200,
						body: JSON.stringify({
							success: false,
							errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE,
							message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE
						})
					});
			}
			const patientConsultation = await DB.getLastConsultation(doctorID, consultationTable, userID);
			if (patientConsultation && !patientConsultation.endTime && patientConsultation.tempEndTime) {
				const elapsedTimeFromLastUpdate = requestEpochWithOffset - patientConsultation.tempEndTime;
				if (elapsedTimeFromLastUpdate > CONSTANTS.TIME_OUT) {
					if (patientConsultation.startTime === patientConsultation.tempEndTime) {
						patientConsultation.isdeleted = true;
					}
					patientConsultation.endTime = patientConsultation.tempEndTime;
					delete patientConsultation.tempEndTime;
					delete patientConsultation.connectionID;
					patientConsultation.consultationTime = (patientConsultation.endTime - patientConsultation.startTime) / 1000;
					patientConsultation.consultationUnit = CONSTANTS.SECONDS;
					const response = {
						statusCode: 200,
						body: JSON.stringify({
							success: true,
							start: patientConsultation.startTime,
							end: patientConsultation.endTime
						})
					};
					await DB.updatePatientConsultation(patientConsultation, consultationTable);
					console.log('made last active consultation inactive');
					return Promise.resolve(response);
				}
				patientConsultation.tempEndTime = requestEpochWithOffset;

				await DB.updatePatientConsultation(patientConsultation, consultationTable);
				console.log('updated tempEnd time');
				const response = {
					statusCode: 200,
					body: JSON.stringify({
						success: true
					})
				};
				return Promise.resolve(response);
			}
			console.log('no consultations or no active consultation');
			const response = {
				statusCode: 200,
				body: JSON.stringify({
					success: true
				})
			};
			return Promise.resolve(response);
		} else {
			console.log('no connection details');
			return Promise.resolve({
				statusCode: 200,
				body: JSON.stringify({
					success: false,
					errorCode: CONSTANTS.ERRORS.CONNECTION_ID_ERROR.CODE,
					message: CONSTANTS.ERRORS.CONNECTION_ID_ERROR.MESSAGE
				})
			});
		}
	} catch (error) {
		console.log('websocket timer ping error', error);
		return Promise.resolve({
			statusCode: 200,
			body: JSON.stringify({
				success: false,
				errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
				message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE
			})
		});
	}
};

exports.handler = async (event, context, callback) => {
	const response = await applyValidation(event);
	callback(null, response);
};
