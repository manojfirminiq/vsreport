const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');

const getDoctorPatients = async (table, nextPaginationKey) => {
	const params = {
		TableName: table
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = nextPaginationKey;
	}

	return dbClient.scan(params).promise();
};

const getEhrUser = async (userId, table) => {
	const paramsTable = {
		TableName: table,
		KeyConditionExpression: '#userID = :id',
		ExpressionAttributeNames: {
			'#userID': 'userID'
		},
		ExpressionAttributeValues: {
			':id': userId
		}
	};
	return dbClient.query(paramsTable).promise();
};

const updateItem = async (item, table) => {
	const params = {
		TableName: table,
		Item: item
	};
	return dbClient.put(params).promise();
};

const getHospitals = async () => {
	const params = {
		TableName: CONSTANTS.MASTER_HOSPITAL_TABLE
	};

	const { Items } = await dbClient.scan(params).promise();
	const hospitals = [];
	for (const item of Items) {
		hospitals.push(item);
	}

	return hospitals;
};

exports.getDoctorPatients = getDoctorPatients;
exports.getEhrUser = getEhrUser;
exports.updateItem = updateItem;
exports.getHospitals = getHospitals;
