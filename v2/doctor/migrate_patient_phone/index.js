const DB = require('./db.js');
const CONSTANTS = require('./CONSTANTS.js');

exports.handler = async event => {
	const hospitals = await DB.getHospitals();
	for (const hospital of hospitals) {
		if (!hospital.isEHR && hospital.isActive) {
			let nextPaginationKey = null;
			const TABLES = {
				DOCTOR_PATIENT:
					process.env.STAGE +
					CONSTANTS.RPM +
					hospital.dbIdentifier +
					'_' +
					CONSTANTS.DOCTOR_PATIENT_TABLE,
				EHR_USER:
					process.env.STAGE +
					CONSTANTS.RPM +
					hospital.dbIdentifier +
					'_' +
					CONSTANTS.EHR_USER_TABLE
			};
			do {
				const patients = await DB.getDoctorPatients(
					TABLES.DOCTOR_PATIENT,
					nextPaginationKey
				);
				nextPaginationKey = patients.LastEvaluatedKey;
				for (const patient of patients.Items) {
					const ehrUser = await DB.getEhrUser(patient.userID, TABLES.EHR_USER);
					if (ehrUser.Items.length) {
						const currentUser = ehrUser.Items[0];
						let homePhone =
							currentUser.attributes && currentUser.attributes.homePhone
								? currentUser.attributes.homePhone
								: null;
						let mobilePhone =
							currentUser.attributes && currentUser.attributes.mobilePhone
								? currentUser.attributes.mobilePhone
								: null;
						if (homePhone) {
							homePhone = homePhone.replace(/[()-]+/g, '');
							patient.attributes.homePhone = homePhone;
						}
						if (mobilePhone) {
							mobilePhone = mobilePhone.replace(/[()-]+/g, '');
							patient.attributes.mobilePhone = mobilePhone;
						}
						if (homePhone || mobilePhone) {
							await DB.updateItem(patient, TABLES.DOCTOR_PATIENT);
						}
					}
				}
			} while (nextPaginationKey);
		}
	}

	return { success: true };
};
