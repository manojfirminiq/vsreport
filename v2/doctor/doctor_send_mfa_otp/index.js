const AWS = require('aws-sdk');
const USER_POOL_ID = process.env.USER_POOL_ID;
AWS.config.update({ region: process.env.REGION });
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const CONSTANTS = require('./CONSTANTS.js');

const addUserToPool = (emailAddress, phoneNumber) => {
	const params = {
		UserAttributes: [
			{
				Name: 'phone_number',
				Value: phoneNumber
			}
		],
		UserPoolId: USER_POOL_ID,
		Username: emailAddress
	};
	return new Promise((resolve, reject) => {
		cognitoidentityserviceprovider.adminUpdateUserAttributes(
			params,
			(err, data) => {
				if (err) reject(err);
				else resolve(data);
			}
		);
	});
};

const sendOtp = async updateToken => {
	try {
		const params = {
			AccessToken: updateToken,
			AttributeName: 'phone_number'
		};

		const response = await cognitoidentityserviceprovider
			.getUserAttributeVerificationCode(params)
			.promise();

		if (response) {
			return Promise.resolve({ success: true, message: CONSTANTS.OTP_SENT });
		}
	} catch (err) {
		console.log(err);
		const response = {
			success: false,
			errorCode: '',
			message: ''
		};
		switch (err.code) {
		case CONSTANTS.AUTHORIZED_ERROR.CODE:
			response.errorCode = CONSTANTS.AUTHORIZED_ERROR.CODE;
			response.message = CONSTANTS.AUTHORIZED_ERROR.MESSAGE;
			break;
		case CONSTANTS.CODE_DELIVERY_FAILURE.CODE:
			response.errorCode = CONSTANTS.CODE_DELIVERY_FAILURE.CODE;
			response.message = CONSTANTS.CODE_DELIVERY_FAILURE.MESSAGE;
			break;
		case CONSTANTS.INVALID_PARAMETER_ERROR.CODE:
			response.errorCode = CONSTANTS.INVALID_PARAMETER_ERROR.CODE;
			response.message = CONSTANTS.INVALID_PARAMETER_ERROR.MESSAGE;
			break;
		case CONSTANTS.LIMIT_EXCEEDED_ERROR.CODE:
			response.errorCode = CONSTANTS.LIMIT_EXCEEDED_ERROR.CODE;
			response.message = CONSTANTS.LIMIT_EXCEEDED_ERROR.MESSAGE;
			break;
		case CONSTANTS.TOO_MANY_REQUESTS_ERROR.CODE:
			response.errorCode = CONSTANTS.TOO_MANY_REQUESTS_ERROR.CODE;
			response.message = CONSTANTS.TOO_MANY_REQUESTS_ERROR.MESSAGE;
			break;
		default:
			response.errorCode = CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE;
			response.message = CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE;
		}
		return response;
	}
};

const applyValidation = async event => {
	try {
		const { phoneNumber, updateToken } = event.body;

		if (!phoneNumber) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.PHONE_NUMBER_REQ.CODE,
				message: CONSTANTS.ERRORS.PHONE_NUMBER_REQ.MESSAGE
			});
		}

		if (!updateToken) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.TOKEN_REQ.CODE,
				message: CONSTANTS.ERRORS.TOKEN_REQ.MESSAGE
			});
		}
		await addUserToPool(event.emailAddress, phoneNumber);
		const response = await sendOtp(updateToken);
		return Promise.resolve(response);
	} catch (err) {
		console.log(JSON.stringify(err));
		return Promise.resolve({
			success: false,
			message: err.message,
			errorCode: err.errorCode
		});
	}
};

exports.handler = async (event, context, callback) => {
	const response = await applyValidation(event);
	callback(null, response);
};
