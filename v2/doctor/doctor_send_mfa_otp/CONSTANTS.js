exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	PHONE_NUMBER_REQ: {
		CODE: 'PHONE_NUMBER_REQ',
		MESSAGE: 'Phone number is required'
	},
	TOKEN_REQ: {
		CODE: 'TOKEN_REQ',
		MESSAGE: 'Token is required'
	}
};

exports.CODE_DELIVERY_FAILURE = {
	CODE: 'CodeDeliveryFailureException',
	MESSAGE: 'Failed to send otp'
};

exports.INVALID_PARAMETER_ERROR = {
	CODE: 'InvalidParameterException',
	MESSAGE: 'Invalid Parameter'
};

exports.LIMIT_EXCEEDED_ERROR = {
	CODE: 'LimitExceededException',
	MESSAGE: 'Limit Exceeded'
};

exports.TOO_MANY_REQUESTS_ERROR = {
	CODE: 'TooManyRequestsException',
	MESSAGE: 'Too many requests'
};

exports.AUTHORIZED_ERROR = {
	CODE: 'NotAuthorizedException',
	MESSAGE: 'Unauthorized'
};

exports.OTP_SENT = 'OTP sent successfully';
