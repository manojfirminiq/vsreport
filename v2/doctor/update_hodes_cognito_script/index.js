const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

const getDoctors = async nextPaginationKey => {
	const params = {
		TableName: process.env.HODES_DOCTOR_TABLE
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = nextPaginationKey;
	}

	return dbClient.scan(params).promise();
};

const updateDynamoDB = async doctor => {
	doctor.subscriptionFlag = doctor.subscriptionFlag
		? doctor.subscriptionFlag
		: true;
	const params = {
		TableName: process.env.HODES_DOCTOR_TABLE,
		Item: doctor
	};
	return dbClient.put(params).promise();
};

const updateCognito = async (emailAddress, hospitalID) => {
	const params = {
		UserAttributes: [
			{
				Name: 'custom:hospitalID',
				Value: hospitalID
			}
		],
		UserPoolId: process.env.COGNITO_POOL_ID,
		Username: emailAddress
	};
	try {
		await cognitoidentityserviceprovider
			.adminUpdateUserAttributes(params)
			.promise();
		return Promise.resolve();
	} catch (error) {
		console.log('error', error);
		return Promise.reject(error);
	}
};

exports.handler = async event => {
	let nextPaginationKey = null;

	do {
		const { Items, LastEvaluatedKey } = await getDoctors(nextPaginationKey);
		nextPaginationKey = LastEvaluatedKey;
		for (const doctor of Items) {
			await updateCognito(doctor.emailAddress, doctor.hospitalID);
			await updateDynamoDB(doctor);
		}
	} while (nextPaginationKey);

	const response = {
		statusCode: 200,
		body: JSON.stringify({ success: true })
	};
	return response;
};
