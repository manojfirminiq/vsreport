const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Count, Items } = await dbClient.query(paramsTable).promise();
	if (Count <= 0) {
		return false;
	} else {
		return (
			process.env.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			Items[0].dbIdentifier +
			'_' +
			CONSTANTS.NOTES_TABLE
		);
	}
};

const getNotesStartDate = async (params, hospitalTable) => {
	const paramsTable = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctor = :id AND #userID_start = :userID_start',
		ExpressionAttributeNames: {
			'#doctor': 'doctorID',
			'#userID_start': 'userID_startDate'
		},
		ExpressionAttributeValues: {
			':id': params.doctorID,
			':userID_start': params.userID_startDate
		}
	};
	const { Count, Items } = await dbClient.query(paramsTable).promise();

	if (Count <= 0) {
		console.log('Count: ', Count);
		return false;
	} else {
		console.log('Count: ', Items);
		return Items;
	}
};

const saveNotes = async (notes, hospitalTable) => {
	notes.modifiedDate = new Date().getTime();
	const params = {
		TableName: hospitalTable,
		Item: notes
	};
	return dbClient.put(params).promise();
};

module.exports.saveNotes = saveNotes;
module.exports.verifyHospital = verifyHospital;
module.exports.getNotesStartDate = getNotesStartDate;
