exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_NOT_FOUND: {
		CODE: 'USER_NOT_FOUND',
		MESSAGE: 'User not found'
	},
	USER_ID_REQUIRED: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'User Id is required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	START_REQUIRED: {
		CODE: 'START_REQUIRED',
		MESSAGE: 'start is required'
	},
	INVALID_FAVORITE: {
		CODE: 'INVALID_FAVORITE',
		MESSAGE: 'Favorite is not valid'
	},
	INPUT_VALIDATION_ERROR: {
		CODE: 'INPUT_VALIDATION_ERROR',
		MESSAGE: 'Error in input provided'
	}
};
exports.RPM = 'rpm';
exports.POST = 'post';
exports.NOTES_TABLE = process.env.NOTES_TABLE;
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
