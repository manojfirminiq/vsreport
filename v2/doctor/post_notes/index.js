const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');
const trackPatient = require('track_patients_record');

const applyValidation = async ({
	doctorId,
	hospitalID,
	method,
	updateUserData,
	body: { userId, notes, start, favorite }
}) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
			});
		}
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		if (start) {
			const isValidStartDate = (new Date(start)).getTime() > 0;
			if (!isValidStartDate) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.MESSAGE,
					errorCode: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.CODE
				});
			}
		}

		if (!favorite) {
			favorite = 0;
		} else {
			const validateFavorite = new RegExp(/^[0|1]{1}$/).test(favorite);
			if (!validateFavorite) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.INVALID_FAVORITE.MESSAGE,
					errorCode: CONSTANTS.ERRORS.INVALID_FAVORITE.CODE
				});
			}
		}

		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const params = {
			doctorID: doctorId,
			userID_startDate: userId + '_' + start,
			notes: notes,
			favorite: Number(favorite),
			userID: userId,
			createdDate: Date.now()
		};

		if (!start) {
			const date = new Date();
			date.setMinutes(0);
			date.setSeconds(0);
			const startDate = date.setMilliseconds(0);
			params.userID_startDate = userId + '_' + startDate;
			params.startDate = startDate;
			params.updates = [];
		} else {
			params.startDate = start;
			params.userID_startDate = userId + '_' + start;
			const note = await DB.getNotesStartDate(params, hospitalTable);
			params.updates = (note && note[0].updates) || [];
		}
		params.updates.push(updateUserData);
		await DB.saveNotes(params, hospitalTable);

		return Promise.resolve({
			success: true
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
