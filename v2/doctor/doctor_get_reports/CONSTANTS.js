
exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};

exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.DOCTOR_REPORTS_TABLE = process.env.DOCTOR_REPORTS_TABLE;
exports.RPM = '_rpm_';
exports.S3_KEY_EXPIRES_IN = 604800;
exports.S3_BUCKET = process.env.S3_BUCKET;
