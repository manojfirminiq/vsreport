const DB = require('./db');
const CONSTANTS = require('./CONSTANTS');

const applyValidation = async ({
	doctorId,
	nextPaginationKey,
	hospitalID
}) => {
	if (!hospitalID) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
			errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
		});
	}

	try {
		const reportTable = await DB.verifyHospital(hospitalID);
		if (!reportTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const doctorReports = await DB.getDoctorReports(doctorId, reportTable, nextPaginationKey);
		const response = {
			success: true,
			data: doctorReports
		};

		if (doctorReports && doctorReports.length > 0 && doctorReports.LastEvaluatedKey) {
			response.nextPaginationKey = doctorReports.LastEvaluatedKey;
		}

		return Promise.resolve(response);
	} catch (error) {
		console.log('error', error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
