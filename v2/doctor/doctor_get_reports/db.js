const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const s3Client = new AWS.S3();
const CONSTANTS = require('./CONSTANTS.js');

const getDoctorReportSignedUrl = report => {
	if (report) {
		return s3Client.getSignedUrl('getObject', {
			Bucket: CONSTANTS.S3_BUCKET,
			Key: report,
			Expires: CONSTANTS.S3_KEY_EXPIRES_IN
		});
	}
};

const verifyHospital = async (hospitalId) => {
	const paramsTable = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		return `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${CONSTANTS.DOCTOR_REPORTS_TABLE}`;
	}
};

const getDoctorReports = async (doctorId, reportTable, nextPaginationKey) => {
	const params = {
		TableName: reportTable,
		KeyConditionExpression: '#doctorId = :id',
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID'
		},
		ExpressionAttributeValues: {
			':id': doctorId
		}
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = Number(nextPaginationKey);
	}
	params.ScanIndexForward = false;
	let { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return [];
	}
	Items = Items.map(
		({
			createdDate,
			reportUrl,
			status,
			type,
			user,
			...rest
		}) => ({
			createdDate,
			reportUrl: getDoctorReportSignedUrl(reportUrl) || '',
			status,
			type,
			user
		})
	);
	return Items;
};

module.exports.verifyHospital = verifyHospital;
module.exports.getDoctorReports = getDoctorReports;
