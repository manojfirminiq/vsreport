const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_PATIENT = CONSTANTS.DOCTOR_PATIENT;
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};

	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		return (
			process.env.STAGE + '_rpm_' + Items[0].dbIdentifier + '_' + DOCTOR_PATIENT
		);
	}
};

const getThreshold = async (doctorId, patientId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #userId = :userId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userId': patientId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userId': 'userID'
		}
	};

	const { Count, Items } = await dbClient.query(params).promise();
	if (Count <= 0) {
		return false;
	} else {
		return Items;
	}
};

module.exports.verifyHospital = verifyHospital;
module.exports.getThreshold = getThreshold;
