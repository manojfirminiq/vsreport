const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');

const getThreshold = async ({ doctorId, userId, hospitalID }) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQ.CODE
			});
		}
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const Items = await DB.getThreshold(doctorId, userId, hospitalTable);

		if (!Items) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
			});
		}
		const newData = {};
		newData.diaLow = Items[0].attributes.threshold.diaLow;
		newData.diaHigh = Items[0].attributes.threshold.diaHigh;
		newData.sysLow = Items[0].attributes.threshold.sysLow;
		newData.sysHigh = Items[0].attributes.threshold.sysHigh;
		// newData.weightLowerLimit = Items[0].attributes.threshold.weightLowerLimit;
		// newData.weightUpperLimit = Items[0].attributes.threshold.weightUpperLimit;
		newData.weight24hr = Items[0].attributes.threshold.weight24hr
			? Items[0].attributes.threshold.weight24hr
			: '';
		newData.weight72hr = Items[0].attributes.threshold.weight72hr
			? Items[0].attributes.threshold.weight72hr
			: '';

		const response = {
			success: true,
			data: newData
		};

		return Promise.resolve(response);
	} catch (error) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await getThreshold(event);
	callback(null, response);
};
