const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');
const STAFF_TABLE = CONSTANTS.DOCTOR_STAFF_TABLE;
const HOSPITAL_MASTER_TABLE = CONSTANTS.HOSPITAL_MASTER_TABLE;
const RPM = CONSTANTS.RPM;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			Items,
			STAFF_TABLE:
				process.env.STAGE + RPM + Items[0].dbIdentifier + '_' + STAFF_TABLE
		};
		return obj;
	}
};

const getStaffMember = async (doctorID, hospitalTable) => {
	const params = {
		TableName: hospitalTable.STAFF_TABLE,
		KeyConditionExpression: '#doctorID = :doctorIDVal',
		ExpressionAttributeValues: {
			':doctorIDVal': doctorID
		},
		ExpressionAttributeNames: {
			'#doctorID': 'doctorID'
		}
	};
	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		return Items;
	}
};

exports.getStaffMember = getStaffMember;
exports.verifyHospital = verifyHospital;
