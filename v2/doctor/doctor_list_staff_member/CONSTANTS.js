exports.DOCTOR_STAFF_TABLE = process.env.DOCTOR_STAFF_TABLE;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.PROFILE_BUCKET = process.env.PROFILE_BUCKET;
exports.S3_KEY_EXPIRES_IN = 60 * 60;
exports.RPM = '_rpm_';
exports.DELETEFLAG = 0;
exports.SUCCESS_MSG = 'Staff member deleted successfully';

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	STAFF_ID_NOT_FOUND: {
		CODE: 'STAFF_ID_NOT_FOUND',
		MESSAGE: 'staffID not found'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};
