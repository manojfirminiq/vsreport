const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./dynamodb');
const s3Client = new AWS.S3();

const getUserProfileUrl = profilePicture => {
	return s3Client.getSignedUrl('getObject', {
		Bucket: CONSTANTS.PROFILE_BUCKET,
		Key: profilePicture,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	});
};

const applyValidation = async ({ doctorId, hospitalID }) => {
	try {
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const Items = await DB.getStaffMember(doctorId, hospitalTable);
		let response = [];
		if (Items) {
			response = Items.filter(data => !data.deleteFlag).map(data => ({
				staffID: data.staffID ? data.staffID : null,
				name:
					data.attributes && data.attributes.name ? data.attributes.name : null,
				profilePicture:
					data.attributes && data.attributes.profilePicture
						? getUserProfileUrl(data.attributes.profilePicture)
						: null,
				qualification:
					data.attributes && data.attributes.qualification
						? data.attributes.qualification
						: null
			}));
		}
		return {
			success: true,
			data: response
		};
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
