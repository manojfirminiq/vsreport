const { uuid } = require('uuidv4');
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./dynamodb');
const COGNITO = require('./cognito');

const generateUserId = () => {
	return uuid();
};

const signup = async params => {
	try {
		const hospitalTable = await DB.getHospital(params.hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.HOSPITAL_ID_INVALID,
				errorCode: CONSTANTS.ERROR_CODES.HOSPITAL_ID_INVALID
			});
		}
		const npCounter = await DB.getNpIdCounter(
			hospitalTable,
			String(params.npID)
		);
		console.log('NP counter', npCounter);
		if (npCounter && npCounter > 0) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.NPID_ALREADY_EXIST,
				errorCode: CONSTANTS.ERROR_CODES.NPID_ALREADY_EXIST
			});
		}
		params.app = CONSTANTS.APP;

		await COGNITO.addUserToPool(params);
		await DB.postUserData(params, hospitalTable);

		return Promise.resolve({
			success: true,
			message: CONSTANTS.MESSAGES.SUCCESS
		});
	} catch (error) {
		console.log('Error in signup', error);
		return Promise.reject(error);
	}
};

const applyValidation = async ({
	emailAddress,
	password,
	npID,
	phoneNumber,
	accountNumber,
	timeZone,
	...rest
}) => {
	try {
		if (!emailAddress) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INVALID_INPUT,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		if (!password) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INVALID_INPUT,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		if (!npID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INVALID_INPUT,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		if (!phoneNumber) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INVALID_INPUT,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		if (!accountNumber) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INVALID_INPUT,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		if (password.trim().length < CONSTANTS.PASSWORD_LENGTH) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INVALID_INPUT,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		const hospitalMaster = await DB.fetchHospitalID(accountNumber);
		if (!hospitalMaster) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.ACCOUNT_NUMBER_INVALID,
				errorCode: CONSTANTS.ERROR_CODES.ACCOUNT_NUMBER_INVALID
			});
		}
		const hospitalID = hospitalMaster[0].hospitalID;

		emailAddress = emailAddress.toLowerCase();
		const userId = generateUserId();
		const response = await signup({
			emailAddress,
			password,
			userId,
			hospitalID,
			npID,
			phoneNumber,
			accountNumber,
			timeZone,
			...rest
		});
		return response;
	} catch (error) {
		console.log(error, 'in catch');
		return Promise.resolve({
			success: false,
			message: error.message,
			errorCode: error.errorCode ? error.errorCode : 'INTERNAL_SERVER_ERROR'
		});
	}
};

exports.handler = async (event, context, callback) => {
	if (event.body) {
		console.log(event.sourceIP);
		event.body.sourceIP = event.sourceIP;
		event = event.body;
	}
	const response = await applyValidation(event);
	callback(null, response);
};
