const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_TABLE_NAME = process.env.DOCTOR_TABLE_NAME;
const HOSPITAL_TABLE_NAME = process.env.HOSPITAL_TABLE_NAME;
const DOCTOR_COUNTER_TABLE_NAME = process.env.DOCTOR_COUNTER_TABLE;

const paddNumber = number => {
	const padlen = 5;
	const padChar = '0';
	const pad = new Array(1 + padlen).join(padChar);
	return (pad + number).slice(-pad.length);
};

const saveItem = async (Item, table) => {
	const params = {
		TableName: table,
		Item: Item
	};
	return dbClient.put(params).promise();
};

const fetchHospitalID = async facilityCode => {
	const params = {
		TableName: HOSPITAL_TABLE_NAME,
		IndexName: CONSTANTS.FACILITYCODEINDEX,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': facilityCode
		},
		ExpressionAttributeNames: {
			'#uid': 'facilityCode'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	}
	return Items;
};

const getHospital = async hospitalID => {
	const params = {
		TableName: HOSPITAL_TABLE_NAME,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': hospitalID
		},
		ExpressionAttributeNames: {
			'#uid': 'hospitalID'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_TABLE_NAME,
			DOCTOR_COUNTER_TABLE: DOCTOR_COUNTER_TABLE_NAME
		};
		return obj;
	}
};

const getDoctorCounter = async hospitalTable => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': CONSTANTS.USER_TYPE
		},
		ExpressionAttributeNames: {
			'#uid': 'userType'
		},
		Limit: 1,
		ScanIndexForward: false
	};

	const { Items, Count } = await dbClient.query(params).promise();

	return Count > 0 ? Items[0].count : 0;
};

const getNpIdCounter = async (hospitalTable, npID) => {
	const params = {
		TableName: hospitalTable.DOCTOR_TABLE,
		IndexName: CONSTANTS.NPIDINDEX,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': npID
		},
		ExpressionAttributeNames: {
			'#uid': CONSTANTS.NPID
		}
	};

	const { Count } = await dbClient.query(params).promise();
	return Count;
};

const postUserData = async (params, hospitalTable) => {
	const currentDate = new Date().getTime();
	const doctorCount = await getDoctorCounter(
		hospitalTable.DOCTOR_COUNTER_TABLE
	);

	const doctorID = paddNumber(doctorCount + 1);
	const data = {
		userID: params.userId,
		createdDate: currentDate,
		modifiedDate: currentDate,
		emailAddress: params.emailAddress,
		phoneNumber: params.phoneNumber,
		doctorID: doctorID,
		app: CONSTANTS.APP,
		hospitalID: params.hospitalID,
		npID: params.npID,
		subscriptionFlag: params.subscriptionFlag
			? CONSTANTS.SUBSCRIBEFLAG
			: CONSTANTS.UNSUBSCRIBEFLAG,
		attributes: {
			name: params.name ? params.name : null,
			timeZone: params.timeZone || null
		}
	};

	await saveItem(data, hospitalTable.DOCTOR_TABLE);
	await saveItem(
		{ userType: CONSTANTS.USER_TYPE, count: doctorCount + 1 },
		hospitalTable.DOCTOR_COUNTER_TABLE
	);

	return Promise.resolve({
		success: true,
		message: CONSTANTS.MESSAGES.SUCCESS
	});
};

module.exports.postUserData = postUserData;
module.exports.getHospital = getHospital;
module.exports.getNpIdCounter = getNpIdCounter;
module.exports.fetchHospitalID = fetchHospitalID;
