const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const lambda = new AWS.Lambda({
	region: process.env.REGION // change to your region
});
const ses = new AWS.SES({
	region: process.env.REGION
});

const errorFunctions = [];

const LAMBDA_FN = [
	`${process.env.STAGE}_rpm_common_v2_doctor_bp_average`,
	`${process.env.STAGE}_rpm_common_v2_doctor_custom_authorization`,
	`${process.env.STAGE}_rpm_common_v2_doctor_forgot_password`,
	`${process.env.STAGE}_rpm_common_v2_doctor_get_patients`,
	`${process.env.STAGE}_rpm_common_v2_doctor_hospital`,
	`${process.env.STAGE}_rpm_common_v2_doctor_login`,
	`${process.env.STAGE}_rpm_common_v2_doctor_patient_summary`,
	`${process.env.STAGE}_rpm_common_v2_doctor_signup`,
	`${process.env.STAGE}_rpm_common_v2_doctor_update_password`,
	`${process.env.STAGE}_rpm_common_v2_doctor_update_patient_code`,
	`${process.env.STAGE}_rpm_common_v2_get_doctor_profile`,
	`${process.env.STAGE}_rpm_common_v2_cognito_custom_msg_forgot_password`,
	`${process.env.STAGE}_rpm_common_v2_doctor_manage_threshold`,
	`${process.env.STAGE}_rpm_common_v2_doctor_get_manage_threshold`,
	`${process.env.STAGE}_rpm_common_v2_doctor_get_active_consultation`,
	`${process.env.STAGE}_rpm_common_v2_doctor_save_consultation`,
	`${process.env.STAGE}_rpm_common_v2_update_financial_status`,
	`${process.env.STAGE}_rpm_common_v2_patient_registration`,
	`${process.env.STAGE}_rpm_common_v2_update_bp_alerts`,
	`${process.env.STAGE}_rpm_common_v2_verify_user`,
	`${process.env.STAGE}_rpm_common_v2_get_doctor_notes`,
	`${process.env.STAGE}_rpm_common_v2_post_doctor_notes`,
	`${process.env.STAGE}_rpm_common_v2_delete_doctor_notes`,
	`${process.env.STAGE}_rpm_common_v2_doctor_resend_verification`,
	`${process.env.STAGE}_rpm_common_v2_forgot_password_view`,
	`${process.env.STAGE}_rpm_common_v2_confirm_password_reset`,
	`${process.env.STAGE}_rpm_common_v2_verify_user`,
	`${process.env.STAGE}_rpm_common_v2_doctor_delete_consultation`,
	`${process.env.STAGE}_rpm_common_v2_save_readings_alerts`,
	`${process.env.STAGE}_rpm_common_v2_get_financial_data`,
	`${process.env.STAGE}_rpm_common_v2_alerts_subscription`,
	`${process.env.STAGE}_rpm_common_v2_search_icd_code`,
	`${process.env.STAGE}_rpm_common_v2_get_report`,
	`${process.env.STAGE}_rpm_common_doctor_get_to_do`,
	`${process.env.STAGE}_rpm_common_v2_delete_staff_member`,
	`${process.env.STAGE}_rpm_common_v2_list_staff_member`,
	`${process.env.STAGE}_rpm_common_v2_doctor_update_to_do`,
	`${process.env.STAGE}_rpm_common_v2_init_user`,
	`${process.env.STAGE}_rpm_common_v2_doctor_mfa`,
	`${process.env.STAGE}_rpm_common_v2_get_order_details`,
	`${process.env.STAGE}_rpm_common_v2_doctor_cancel_order`,
	`${process.env.STAGE}_rpm_common_v2_doctor_update_patient`,
	`${process.env.STAGE}_rpm_common_doctor_post_to_do`,
	`${process.env.STAGE}_rpm_common_v2_update_doctor_profile`,
	`${process.env.STAGE}_rpm_common_v2_doctor_update_order`,
	`${process.env.STAGE}_rpm_common_v2_doctor_get_finance_summary`,
	`${process.env.STAGE}_rpm_common_v2_doctor_static_content`,
	`${process.env.STAGE}_rpm_common_v2_doctor_get_alerts`,
	`${process.env.STAGE}_rpm_common_v2_doctor_consultation_connect`,
	`${process.env.STAGE}_rpm_common_v2_doctor_consultation_disconnect`,
	`${process.env.STAGE}_rpm_common_v2_doctor_consultation_ping`,
	`${process.env.STAGE}_rpm_common_v2_doctor_save_consultation_timer`,
	`${process.env.STAGE}_rpm_common_v2_doctor_stop_consultation_timer`,
	`${process.env.STAGE}_rpm_common_v2_doctor_consultation_default`,
	`${process.env.STAGE}_rpm_common_v2_websocket_doctor_custom_authorization`
];

const sendMail = async (data, subject) => {
	if (data && subject) {
		const toAdrress = process.env.NOTIFICATION_EMAIL_TO.split(',');
		const eParams = {
			Destination: {
				ToAddresses: toAdrress
			},
			Message: {
				Body: {
					Html: {
						Data: data
					}
				},
				Subject: {
					Data: `[${process.env.STAGE}] - ${subject}`
				}
			},
			Source: process.env.NOTIFICATION_EMAIL_FROM
		};
		return new Promise((resolve, reject) => {
			ses.sendEmail(eParams, function (err, data) {
				if (err) {
					console.log(err);
				}
				resolve();
			});
		});
	}
};

const invokeLambda = async name => {
	return new Promise((resolve, reject) => {
		// console.log(name, "=====");
		lambda.invoke(
			{
				FunctionName: name,
				Payload: JSON.stringify({ type: 'sample' }) // pass params
			},
			(error, data) => {
				// console.log(error);
				if (error) {
					errorFunctions.push({ name: name, error: error });
				}
				// data = JSON.parse(data.Payload);
				resolve({ success: true });
			}
		);
	});
};

exports.handler = async (event, context, callback) => {
	try {
		for (const fn of LAMBDA_FN) {
			await invokeLambda(`${fn}`);
		}
	} catch (error) {
		console.log(error);
	}
	if (errorFunctions.length > 0) {
		let message = 'Following lambda functions returned with error<ol>';
		for (const fn of errorFunctions) {
			message += `<li><b>Function: ${
				fn.name
			}</b><br> <b>Error:</b> ${JSON.stringify(fn.error)}</li>`;
		}
		message += '</ol>';

		sendMail(message, `[${process.env.STAGE}] Error invoking Lamda`);
	}
	callback(null);
};
