const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');
const trackPatient = require('track_patients_record');

const applyValidation = async ({
	doctorId,
	hospitalID,
	updateUserData,
	body: { userId, patientCode }
}) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
			});
		}
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		if (!patientCode) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.PATIENT_CODE_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.PATIENT_CODE_REQUIRED.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		const { Count, Items } = await DB.getPatient(
			doctorId,
			userId,
			hospitalTable
		);
		if (Count === 0) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
			});
		}

		Items[0].attributes.patientCode = patientCode.toString();
		Items[0].updates = Items[0].updates || [];
		Items[0].updates.push(updateUserData);
		await DB.updatePatientCode(Items[0], hospitalTable);

		return Promise.resolve({
			success: true
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
