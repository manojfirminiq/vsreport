const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const BP_TABLE = process.env.BP_TABLE;
const NON_BP_TABLE = process.env.NON_BP_TABLE;
const DOCTOR_TABLE = process.env.DOCTOR_TABLE;
const ALERT_NON_BP_TABLE = process.env.ALERT_NON_BP_TABLE;
const ALERT_BP_TABLE = process.env.ALERT_BP_TABLE;
const TODO_TABLE = process.env.TODO_TABLE;
const DOCTOR_PROFILE = process.env.DOCTOR_PROFILE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: DOCTOR_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			Items,
			DOCTOR_PATIENT:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT_TABLE,
			BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				BP_TABLE,
			NON_BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				NON_BP_TABLE,
			ALERT_BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				ALERT_BP_TABLE,
			ALERT_NON_BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				ALERT_NON_BP_TABLE,
			TODO_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				TODO_TABLE,
			DOCTOR_PROFILE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PROFILE
		};
		return obj;
	}
};

const getPatient = async (doctorId, userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #userId = :userId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userId': userId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userId': 'userID'
		}
	};

	return dbClient.query(params).promise();
};

const getTodo = async (userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		IndexName: CONSTANTS.TODO_USERID_INDEX,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeNames: {
			'#uid': 'userID'
		},
		ExpressionAttributeValues: {
			':id': userId
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return [];
	}
	return Items;
};

const getBpReadings = async (userId, BP_TABLE, from, to, nextPaginationKey) => {
	const fromDate = new Date(Number(from));
	fromDate.setDate(fromDate.getDate() - 1);

	const toDate = new Date(Number(to));
	toDate.setDate(toDate.getDate() + 1);

	const params = {
		TableName: BP_TABLE,
		KeyConditionExpression:
			'#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': fromDate.getTime(),
			':maxDate': toDate.getTime()
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = {
			userID: userId,
			measurementDate: Number(nextPaginationKey)
		};
	}
	params.ScanIndexForward = false;

	return dbClient.query(params).promise();
};

const getBpAlerts = async (userId, ALERT_BP_TABLE) => {
	const params = {
		TableName: ALERT_BP_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': userId
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};

	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

const getNonBpAlerts = async (userId, ALERT_NON_BP_TABLE) => {
	const params = {
		TableName: ALERT_NON_BP_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': userId + CONSTANTS.WEIGHT_MANUAL
		},
		ExpressionAttributeNames: {
			'#uid': 'userId_type_deviceLocalName'
		}
	};

	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

const getNonBpReadings = async (
	userId,
	NON_BP_TABLE,
	from,
	to,
	nextPaginationKey
) => {
	const fromDate = new Date(Number(from));
	fromDate.setDate(fromDate.getDate() - 1);

	const toDate = new Date(Number(to));
	toDate.setDate(toDate.getDate() + 1);

	const params = {
		TableName: NON_BP_TABLE,
		KeyConditionExpression:
			'#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId + CONSTANTS.WEIGHT_MANUAL,
			':minDate': fromDate.getTime(),
			':maxDate': toDate.getTime()
		},
		ExpressionAttributeNames: {
			'#uid': 'userId_type_deviceLocalName'
		}
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = {
			userID: userId,
			measurementDate: Number(nextPaginationKey)
		};
	}
	params.ScanIndexForward = false;

	return dbClient.query(params).promise();
};

const getDoctorData = async (
	doctorID,
	hospitalTable,
	accountNumber,
	hospitalName
) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorID
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		const Item = Items.map(
			({
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			}) => ({
				createdDate,
				accountNumber,
				...rest,
				...attributesRest,
				hospitalName
			})
		)[0];

		return Item;
	} else {
		return null;
	}
};

module.exports.getPatient = getPatient;
module.exports.getNonBpReadings = getNonBpReadings;
module.exports.getBpReadings = getBpReadings;
module.exports.verifyHospital = verifyHospital;
module.exports.getBpAlerts = getBpAlerts;
module.exports.getNonBpAlerts = getNonBpAlerts;
module.exports.getTodo = getTodo;
module.exports.getDoctorData = getDoctorData;
