const moment = require('moment');
const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS.js');
const AWS = require('aws-sdk');
const s3Client = new AWS.S3();

const getUserProfileUrl = profilePicture => {
	return s3Client.getSignedUrl('getObject', {
		Bucket: CONSTANTS.PROFILE_BUCKET,
		Key: profilePicture,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	});
};

const filterReadings = (readings, from, to, timeZone) => {
	const deviceStartTime = moment(Number(from))
		.utcOffset(Number(timeZone) / 60)
		.format('YYYYMMDDHHmmssSSS');
	const deviceEndTime = moment(Number(to))
		.utcOffset(Number(timeZone) / 60)
		.format('YYYYMMDDHHmmssSSS');
	const validReadings = [];
	for (const reading of readings) {
		let localTime = moment(reading.measurementDate);
		let readingTimeZone = timeZone;
		if (reading.timeZone) {
			readingTimeZone = reading.timeZone;
		}
		localTime = localTime.utcOffset(Number(readingTimeZone) / 60);
		localTime = localTime.format('YYYYMMDDHHmmssSSS');
		if (
			Number(localTime) >= Number(deviceStartTime) &&
			Number(localTime) <= Number(deviceEndTime)
		) {
			validReadings.push(reading);
		}
	}

	return validReadings;
};

const applyValidation = async ({
	doctorId,
	userId,
	measurementDateFrom,
	measurementDateTo,
	nextPaginationKey,
	timeZone,
	hospitalID
}) => {
	if (!userId) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE,
			errorCode: CONSTANTS.ERRORS.USER_ID_REQ.CODE
		});
	}
	if (!hospitalID) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
			errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
		});
	}
	if (!timeZone) {
		timeZone = 0;
	}
	if (!measurementDateFrom) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.FROM_DATE.MESSAGE,
			errorCode: CONSTANTS.ERRORS.FROM_DATE.CODE
		});
	}
	if (!measurementDateTo) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.TO_DATE.MESSAGE,
			errorCode: CONSTANTS.ERRORS.TO_DATE.CODE
		});
	}
	const patientId = userId;
	try {
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);

		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		let { Items } = await DB.getPatient(
			doctorId,
			userId,
			hospitalTableDetails.DOCTOR_PATIENT
		);

		if (Items.length === 0 || Items[0].deleteFlag) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
			});
		} else {
			// if hospital is hodes, get readings using hubId, else use userId
			if (hospitalTableDetails.DOCTOR_PATIENT.includes(CONSTANTS.HODES)) {
				userId = Items[0].hubID ? Items[0].hubID : Items[0].userID;
			}

			Items = Items.map(
				({
					createdDate,
					modifiedDate,
					patientCode,
					processStatus,
					attributes: {
						name,
						firstName,
						lastName,
						gender,
						profilePicture,
						dob,
						icdCode,
						mobilePhone,
						quickNotes,
						emailAddress,
						consent,
						contactVai,
						threshold,
						lastReportedDate,
						homePhone,
						...attributesRest
					},
					...rest
				}) => ({
					name,
					createdDate,
					firstName,
					lastName,
					gender,
					dob,
					phoneNumber: mobilePhone || homePhone || null,
					patientCode,
					profilePicture,
					threshold,
					quickNotes,
					emailAddress: emailAddress || null,
					consent,
					contactVai,
					icdCode,
					lastReportedDate,
					processStatus
				})
			);
			if (Items[0].name && (!Items[0].firstName || !Items[0].lastName)) {
				const name = Items[0].name.split(' ');
				Items[0].firstName = name[0];
				Items[0].lastName =
					name && name.length > 1 ? name[name.length - 1] : '';
			}
			Items[0].icdCode = Items[0].icdCode ? Items[0].icdCode : [];
			Items[0].threshold = Items[0].threshold || {};
			Items[0].enrollmentDate = Items[0].createdDate || null;

			const doctorDetails = await DB.getDoctorData(
				doctorId,
				hospitalTableDetails.DOCTOR_PROFILE,
				hospitalTableDetails.Items[0].facilityCode,
				hospitalTableDetails.Items[0].name
			);
			let inActivePeriod;

			if (doctorDetails && doctorDetails.inActivePeriod) {
				inActivePeriod = doctorDetails.inActivePeriod;
			} else {
				inActivePeriod = CONSTANTS.INACTIVE_PERIOD;
			}
			const now = new Date();
			const inActiveDayThreshold = Date.parse(
				new Date(now.setDate(now.getDate() - inActivePeriod))
			);
			if (
				Items[0].lastReportedDate &&
				Items[0].lastReportedDate < inActiveDayThreshold
			) {
				Items[0].activeStatus = false;
			} else {
				Items[0].activeStatus = true;
			}

			Items[0].profilePicture = Items[0].profilePicture
				? getUserProfileUrl(Items[0].profilePicture)
				: null;
		}

		let getToDoItems = null;
		let nonBpAlerts = null;
		let bpReadings = null;
		let bpAlerts = null;
		let weight = null;

		return await Promise.all([
			DB.getTodo(patientId, hospitalTableDetails.TODO_TABLE),
			DB.getNonBpReadings(
				userId,
				hospitalTableDetails.NON_BP_TABLE,
				measurementDateFrom,
				measurementDateTo,
				nextPaginationKey
			),
			DB.getNonBpAlerts(userId, hospitalTableDetails.ALERT_NON_BP_TABLE),
			DB.getBpReadings(
				userId,
				hospitalTableDetails.BP_TABLE,
				measurementDateFrom,
				measurementDateTo,
				nextPaginationKey
			),
			DB.getBpAlerts(userId, hospitalTableDetails.ALERT_BP_TABLE)
		])
			.then(values => {
				getToDoItems = values[0];
				getToDoItems = getToDoItems.map(
					({
						createdDate,
						dueDate,
						userID,
						doctorID,
						attributes: { description, itemType, notes, resolveStatus }
					}) => ({
						createdDate,
						dueDate,
						userID,
						doctorID,
						description,
						itemType,
						notes,
						resolveStatus:
							Number(resolveStatus) || CONSTANTS.DEFAULT_RESOLVE_STATUS
					})
				);

				const openToDoCount = getToDoItems.filter(
					todo => todo.resolveStatus === CONSTANTS.DEFAULT_RESOLVE_STATUS
				).length;

				const response = {
					success: true,
					data: {
						user: Items[0],
						bp: [],
						averageBpSystolic: 0,
						averageBpDiastolic: 0,
						ihbCount: 0,
						alertCount: 0,
						toDo: getToDoItems,
						openToDoCount: openToDoCount
					}
				};
				response.data.weight = [];
				response.data.alerts = [];
				let alertObject;
				let finalAlertArray = [];
				const finalBpAlertArray = [];
				const finalNonBpAlertArray = [];

				weight = values[1];
				if (weight.Count > 0) {
					response.data.weight = weight.Items.filter(
						({ attributes }) => Number(attributes.deleteFlag) !== 1
					).map(
						({
							measurementDate,
							attributes: {
								bmiValue,
								weight,
								bodyFatPercentage,
								have24hrAlert,
								have72hrAlert,
								timeZone,
								...rest
							}
						}) => ({
							measurementDate,
							timeZone,
							bmiValue: Number(bmiValue),
							have24hrAlert: have24hrAlert || 0,
							have72hrAlert: have72hrAlert || 0,
							weight: Number(weight),
							bodyFatPercentage: Number(bodyFatPercentage)
						})
					);

					if (weight.LastEvaluatedKey) {
						response.weight.nextPaginationKey = weight.LastEvaluatedKey;
					}
					response.data.weight = filterReadings(
						response.data.weight,
						measurementDateFrom,
						measurementDateTo,
						timeZone
					);
				}
				nonBpAlerts = values[2];
				const filteredNonBpAlerts = nonBpAlerts.Items.filter(
					({ alerts }) =>
						alerts.weight.resolveStatus !== CONSTANTS.RESOLVE_STATUS
				);
				if (nonBpAlerts.Count > 0) {
					nonBpAlerts = nonBpAlerts.Items;
					// nonBpAlerts = nonBpAlerts.Items.filter(({ alerts }) => (alerts.weight.resolveStatus) !== CONSTANTS.RESOLVE_STATUS)
					for (let i = 0; i < nonBpAlerts.length; i++) {
						alertObject = {
							resolveStatus: nonBpAlerts[i].alerts.weight.resolveStatus,
							notes: nonBpAlerts[i].alerts.weight.Notes,
							measurementDate: nonBpAlerts[i].measurementDate,
							resolvedDate:
								nonBpAlerts[i].alerts.weight.resolveStatus ===
								CONSTANTS.RESOLVE_STATUS
									? nonBpAlerts[i].alerts.weight.resolvedDate
									: null,
							thresholdType24hr: nonBpAlerts[i].alerts.weight.thresholdType24hr
								? nonBpAlerts[i].alerts.weight.thresholdType24hr
								: null,
							weightDiff24hr: nonBpAlerts[i].alerts.weight.weightDiff24hr
								? nonBpAlerts[i].alerts.weight.weightDiff24hr
								: null,
							thresholdType72hr: nonBpAlerts[i].alerts.weight.thresholdType72hr
								? nonBpAlerts[i].alerts.weight.thresholdType72hr
								: null,
							weightDiff72hr: nonBpAlerts[i].alerts.weight.weightDiff72hr
								? nonBpAlerts[i].alerts.weight.weightDiff72hr
								: null,
							thresholdWeightUnit: nonBpAlerts[i].alerts.thresholdWeightUnit
								? nonBpAlerts[i].alerts.thresholdWeightUnit
								: null,
							threshold: nonBpAlerts[i].alerts.threshold
								? nonBpAlerts[i].alerts.threshold
								: null,
							type: CONSTANTS.TYPE.WEIGHT,
							weight24hr:
								nonBpAlerts[i].alerts &&
								nonBpAlerts[i].alerts.threshold &&
								nonBpAlerts[i].alerts.threshold.weight24hr
									? nonBpAlerts[i].alerts.threshold.weight24hr
									: null,
							weight72hr:
								nonBpAlerts[i].alerts &&
								nonBpAlerts[i].alerts.threshold &&
								nonBpAlerts[i].alerts.threshold.weight72hr
									? nonBpAlerts[i].alerts.threshold.weight72hr
									: null
						};
						finalNonBpAlertArray.push(alertObject);
					}
				}

				bpReadings = values[3];
				if (bpReadings.Count > 0) {
					response.data.bp = bpReadings.Items.filter(
						({ attributes }) => Number(attributes.deleteFlag) !== 1
					).map(
						({
							measurementDate,
							attributes: {
								systolic,
								diastolic,
								alerts,
								pulse,
								irregularHB,
								haveAlert,
								movementError,
								timeZone,
								...rest
							}
						}) => ({
							measurementDate,
							timeZone,
							systolic: Number(systolic),
							diastolic: Number(diastolic),
							alerts: alerts || {},
							pulse: Number(pulse),
							irregularHB: Number(irregularHB),
							haveAlert: haveAlert || 0,
							movementError: Number(movementError)
						})
					);

					if (bpReadings.LastEvaluatedKey) {
						response.bp.nextPaginationKey = bpReadings.LastEvaluatedKey;
					}
				}

				bpAlerts = values[4];
				const filteredBpAlerts = bpAlerts.Items.filter(
					({ alerts }) => alerts.bp.resolveStatus !== CONSTANTS.RESOLVE_STATUS
				);
				if (bpAlerts.Count > 0) {
					bpAlerts = bpAlerts.Items;
					// bpAlerts = bpAlerts.Items.filter(({ alerts }) => (alerts.bp.resolveStatus) !== CONSTANTS.RESOLVE_STATUS)
					for (let i = 0; i < bpAlerts.length; i++) {
						alertObject = {
							resolveStatus: bpAlerts[i].alerts.bp.resolveStatus,
							sysDiff: Number(bpAlerts[i].alerts.bp.sysDiff),
							diaDiff: Number(bpAlerts[i].alerts.bp.diaDiff),
							pulseDiff: Number(bpAlerts[i].alerts.bp.pulseDiff),
							notes: bpAlerts[i].alerts.bp.Notes,
							measurementDate: bpAlerts[i].measurementDate,
							resolvedDate:
								bpAlerts[i].alerts.bp.resolveStatus === CONSTANTS.RESOLVE_STATUS
									? bpAlerts[i].alerts.bp.resolvedDate
									: null,
							type: CONSTANTS.TYPE.BP,
							averageType:
								(bpAlerts[i].attributes &&
									bpAlerts[i].attributes.averageType) ||
								null,
							alertType:
								(bpAlerts[i].attributes && bpAlerts[i].attributes.alertType) ||
								null
						};
						finalBpAlertArray.push(alertObject);
					}
				}
				finalAlertArray = finalNonBpAlertArray.concat(finalBpAlertArray);
				response.data.alerts = finalAlertArray;
				response.data.alerts = filterReadings(
					response.data.alerts,
					measurementDateFrom,
					measurementDateTo,
					timeZone
				);
				response.data.bp = filterReadings(
					response.data.bp,
					measurementDateFrom,
					measurementDateTo,
					timeZone
				);

				response.data.ihbCount = response.data.bp.filter(
					data => data.irregularHB > 0
				).length;
				response.data.averageBpSystolic = Math.round(
					response.data.bp.reduce(
						(total, nextItem) => total + nextItem.systolic,
						0
					) / response.data.bp.length
				);
				response.data.averageBpDiastolic = Math.round(
					response.data.bp.reduce(
						(total, nextItem) => total + nextItem.diastolic,
						0
					) / response.data.bp.length
				);
				response.data.alertCount =
					filteredBpAlerts.length + filteredNonBpAlerts.length;
				return Promise.resolve(response);
			})
			.catch(reason => {
				console.log(reason);
				return Promise.resolve({
					success: false,
					message: reason,
					errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
				});
			});
	} catch (error) {
		console.log('error', error);
		return Promise.resolve({
			success: false,
			message: error,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);

	callback(null, response);
};
