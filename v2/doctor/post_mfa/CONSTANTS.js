exports.TYPES = {
	ENABLE: 'enable',
	DISABLE: 'disable'
};

exports.ERROR_CODES = {
	REQUIRED_PARAMETER: 'REQUIRED_PARAMETER',
	USER_NOT_FOUND: 'USER_NOT_FOUND',
	UNEXPECTED_ERROR: 'UNEXPECTED_ERROR'
};

exports.VALIDATION_MESSAGES = {
	USER_NOT_FOUND: 'User does not exist',
	UNEXPECTED_ERROR: 'unexpected error found'
};

exports.COGNITO_ERROR = {
	REQUIRED_PARAMETER: 'InvalidParameterException',
	USER_NOT_FOUND: 'UserNotFoundException'
};

exports.AUTHORIZED_ERROR = {
	CODE: 'NotAuthorizedException',
	MESSAGE: 'Unauthorized'
};

exports.OTP_ERROR = {
	CODE: 'CodeMismatchException',
	MESSAGE: 'Invalid verification code provided'
};

exports.REQUIRED_KEYS = {
	CODE: 'MissingRequiredParameter',
	MESSAGE: 'Missing required key'
};

exports.EXPIRE_CODE_ERROR = {
	CODE: 'ExpiredCodeException',
	MESSAGE: 'OTP code has expired'
};

exports.INVALID_PARAMETER_ERROR = {
	CODE: 'InvalidParameterException',
	MESSAGE: 'Invalid Parameter'
};

exports.LIMIT_EXCEEDED_ERROR = {
	CODE: 'LimitExceededException',
	MESSAGE: 'Limit Exceeded'
};

exports.TOO_MANY_REQUESTS_ERROR = {
	CODE: 'TooManyRequestsException',
	MESSAGE: 'Too many requests'
};

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	INVALID_TYPE: {
		CODE: 'INVALID_TYPE',
		MESSAGE: 'Type is invalid'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	PHONE_NUMBER_INVALID: {
		CODE: 'PHONE_NUMBER_INVALID',
		MESSAGE: 'Invalid Phone Number'
	}
};

exports.SUCCESS_MESSAGE = {
	MFA_ENABLE: 'mfa enabled successfully',
	MFA_DISABLE: 'mfa disabled successfully'
};

exports.PHONE_NUMBER_ATTR = 'phone_number';
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.STAFF_PROFILE_TABLE = process.env.STAFF_PROFILE_TABLE;
exports.RPM = '_rpm_';
exports.DISABLE_MFA = false;
exports.ENABLE_MFA = true;
