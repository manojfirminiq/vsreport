'use strict';
const AWS = require('aws-sdk');
const USER_POOL_ID = process.env.USER_POOL_ID;
AWS.config.update({ region: process.env.REGION });
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./db');

const enableMFA = async (type, emailAddress, otp, updateToken, profileTable, staffID, doctorId) => {
	const getUserParams = {
		AccessToken: updateToken
	};
	let phoneNumber;
	const getUserData = await cognitoidentityserviceprovider.getUser(getUserParams).promise();
	if (getUserData) {
		const userAttributes = getUserData.UserAttributes.find(attr => {
			return attr.Name === CONSTANTS.PHONE_NUMBER_ATTR;
		});
		phoneNumber = userAttributes && userAttributes.Value;
		if (!phoneNumber) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.PHONE_NUMBER_INVALID.CODE,
				message: CONSTANTS.ERRORS.PHONE_NUMBER_INVALID.MESSAGE
			});
		}
	}
	const params = {
		AccessToken: updateToken,
			  AttributeName: CONSTANTS.PHONE_NUMBER_ATTR,
			  Code: otp
	};
	const verifyOtp = await cognitoidentityserviceprovider.verifyUserAttribute(params).promise();
	if (Object.keys(verifyOtp).length === 0) {
		const response = await mfa(emailAddress, type);
		if (response) {
			if (staffID) {
				console.log();
				await DB.updateStaffProfile(profileTable.STAFF_TABLE, doctorId, staffID, phoneNumber, CONSTANTS.ENABLE_MFA);
			} else {
				await DB.updateDoctorProfile(profileTable.DOCTOR_TABLE, doctorId, phoneNumber, CONSTANTS.ENABLE_MFA);
			}
			return Promise.resolve({
				success: true,
				message: CONSTANTS.SUCCESS_MESSAGE.MFA_ENABLE
			});
		}
	}
};

const disableMFA = async (type, emailAddress, profileTable, staffID, doctorId) => {
	const response = await mfa(emailAddress, type);
	if (response) {
		if (staffID) {
			await DB.disableStaffMFA(profileTable.STAFF_TABLE, doctorId, staffID, CONSTANTS.DISABLE_MFA);
		} else {
			await DB.disableDoctorMFA(profileTable.DOCTOR_TABLE, doctorId, CONSTANTS.DISABLE_MFA);
		}
		return Promise.resolve({
			success: true,
			message: CONSTANTS.SUCCESS_MESSAGE.MFA_DISABLE
		});
	}
};

const applyValidation = async event => {
	const { doctorId, hospitalID, staffID, emailAddress } = event;
	const { type, otp, updateToken } = event.body;
	try {
		if (staffID && staffID !== null) {
			const profileTable = await DB.getProfileTable(hospitalID);
			if (!profileTable) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
					errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
				});
			}
			if (type && type.toLowerCase() === CONSTANTS.TYPES.ENABLE) {
				const response = await enableMFA(type, emailAddress, otp, updateToken, profileTable, staffID, doctorId);
				return Promise.resolve(response);
			} else if (type && type.toLowerCase() === CONSTANTS.TYPES.DISABLE) {
				const response = await disableMFA(type, emailAddress, profileTable, staffID, doctorId);
				return Promise.resolve(response);
			} else {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.INVALID_TYPE.MESSAGE,
					errorCode: CONSTANTS.ERRORS.INVALID_TYPE.CODE
				});
			}
		} else {
			const profileTable = await DB.getProfileTable(hospitalID);
			if (!profileTable) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
					errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
				});
			}
			if (type && type.toLowerCase() === CONSTANTS.TYPES.ENABLE) {
				const response = await enableMFA(type, emailAddress, otp, updateToken, profileTable, staffID, doctorId);
				return Promise.resolve(response);
			} else if (type && type.toLowerCase() === CONSTANTS.TYPES.DISABLE) {
				const response = await disableMFA(type, emailAddress, profileTable, staffID, doctorId);
				return Promise.resolve(response);
			} else {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.INVALID_TYPE.MESSAGE,
					errorCode: CONSTANTS.ERRORS.INVALID_TYPE.CODE
				});
			}
		}
	} catch (err) {
		console.log(JSON.stringify(err));
		const response = {
			success: false,
			errorCode: '',
			message: ''
		};
		switch (err.code) {
		case CONSTANTS.AUTHORIZED_ERROR.CODE:
			response.errorCode = CONSTANTS.AUTHORIZED_ERROR.CODE;
			response.message = CONSTANTS.AUTHORIZED_ERROR.MESSAGE;
			break;
		case CONSTANTS.OTP_ERROR.CODE:
			response.errorCode = CONSTANTS.OTP_ERROR.CODE;
			response.message = CONSTANTS.OTP_ERROR.MESSAGE;
			break;
		case CONSTANTS.REQUIRED_KEYS.CODE:
			response.errorCode = CONSTANTS.REQUIRED_KEYS.CODE;
			response.message = CONSTANTS.REQUIRED_KEYS.MESSAGE;
			break;
		case CONSTANTS.EXPIRE_CODE_ERROR.CODE:
			response.errorCode = CONSTANTS.EXPIRE_CODE_ERROR.CODE;
			response.message = CONSTANTS.EXPIRE_CODE_ERROR.MESSAGE;
			break;
		case CONSTANTS.INVALID_PARAMETER_ERROR.CODE:
			response.errorCode = CONSTANTS.INVALID_PARAMETER_ERROR.CODE;
			response.message = CONSTANTS.INVALID_PARAMETER_ERROR.MESSAGE;
			break;
		case CONSTANTS.LIMIT_EXCEEDED_ERROR.CODE:
			response.errorCode = CONSTANTS.LIMIT_EXCEEDED_ERROR.CODE;
			response.message = CONSTANTS.LIMIT_EXCEEDED_ERROR.MESSAGE;
			break;
		case CONSTANTS.TOO_MANY_REQUESTS_ERROR.CODE:
			response.errorCode = CONSTANTS.TOO_MANY_REQUESTS_ERROR.CODE;
			response.message = CONSTANTS.TOO_MANY_REQUESTS_ERROR.MESSAGE;
			break;
		case CONSTANTS.COGNITO_ERROR.USER_NOT_FOUND:
			response.errorCode = CONSTANTS.ERROR_CODES.USER_NOT_FOUND;
			response.message = CONSTANTS.VALIDATION_MESSAGES.USER_NOT_FOUND;
			break;
		default:
			response.errorCode = CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE;
			response.message = CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE;
		}
		return response;
	}
};

const mfa = async (emailAddress, type) => {
	let params;
	if (type === CONSTANTS.TYPES.ENABLE) {
		params = {
			UserPoolId: USER_POOL_ID,
			Username: emailAddress,
			SMSMfaSettings: {
				Enabled: true,
				PreferredMfa: true
			}
		};
	} else if (type === CONSTANTS.TYPES.DISABLE) {
		params = {
			UserPoolId: USER_POOL_ID,
			Username: emailAddress,
			SMSMfaSettings: {
				Enabled: false,
				PreferredMfa: false
			}
		};
	}
	const response = await cognitoidentityserviceprovider.adminSetUserMFAPreference(params).promise();
	return response;
};

exports.handler = async (event, context, callback) => {
	const response = await applyValidation(event);
	callback(null, response);
};
