const AWS = require('aws-sdk');
const CONSTANTS = require('./CONSTANTS');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const getProfileTable = async hospitalId => {
	const params = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': hospitalId
		},
		ExpressionAttributeNames: {
			'#uid': 'hospitalID'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.DOCTOR_PROFILE_TABLE,
			STAFF_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.STAFF_PROFILE_TABLE
		};
		return obj;
	}
};

const disableDoctorMFA = async (
	doctorProfileTable,
	doctorId,
	mfaEnabled
) => {
	const params = {
		TableName: doctorProfileTable,
		Key: {
			userID: doctorId
		},
		UpdateExpression: 'set #attributes.#mfaEnabled = :mfaEnabled',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#mfaEnabled': 'mfaEnabled'
		},
		ExpressionAttributeValues: {
			':mfaEnabled': mfaEnabled
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return await dbClient.update(params).promise();
};

const disableStaffMFA = async (
	staffProfileTable,
	doctorId,
	staffID,
	mfaEnabled
) => {
	const params = {
		TableName: staffProfileTable,
		Key: {
			doctorID: doctorId,
			staffID: staffID
		},
		UpdateExpression: 'set #attributes.#mfaEnabled = :mfaEnabled',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#mfaEnabled': 'mfaEnabled'
		},
		ExpressionAttributeValues: {
			':mfaEnabled': mfaEnabled
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return await dbClient.update(params).promise();
};

const updateDoctorProfile = async (
	doctorProfileTable,
	doctorId,
	phoneNumber,
	mfaEnabled
) => {
	const params = {
		TableName: doctorProfileTable,
		Key: {
			userID: doctorId
		},
		UpdateExpression: 'set #attributes.#mfaEnabled = :mfaEnabled, #phoneNumber = :phNo',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#mfaEnabled': 'mfaEnabled',
			'#phoneNumber': 'phoneNumber'
		},
		ExpressionAttributeValues: {
			':mfaEnabled': mfaEnabled,
			':phNo': phoneNumber
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return await dbClient.update(params).promise();
};

const updateStaffProfile = async (
	staffProfileTable,
	doctorId,
	staffID,
	phoneNumber,
	mfaEnabled
) => {
	const params = {
		TableName: staffProfileTable,
		Key: {
			doctorID: doctorId,
			staffID: staffID
		},
		UpdateExpression: 'set #attributes.#mfaEnabled = :mfaEnabled, #phoneNumber = :phNo',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#mfaEnabled': 'mfaEnabled',
			'#phoneNumber': 'phoneNumber'
		},
		ExpressionAttributeValues: {
			':mfaEnabled': mfaEnabled,
			':phNo': phoneNumber
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return await dbClient.update(params).promise();
};

module.exports.getProfileTable = getProfileTable;
module.exports.disableStaffMFA = disableStaffMFA;
module.exports.disableDoctorMFA = disableDoctorMFA;
module.exports.updateStaffProfile = updateStaffProfile;
module.exports.updateDoctorProfile = updateDoctorProfile;
