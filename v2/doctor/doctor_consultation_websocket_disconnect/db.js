const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const connectionTable = `${CONSTANTS.STAGE}${CONSTANTS.RPM}${CONSTANTS.TIMER_CONNECTION_TABLE}`;

const getConnectionParams = async (connectionID) => {
	const params = {
		TableName: connectionTable,
		KeyConditionExpression: '#connectionID = :connectionID',
		ExpressionAttributeValues: {
			':connectionID': connectionID

		},
		ExpressionAttributeNames: {
			'#connectionID': 'connectionID'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	}
	return Items[0];
};

const getLastConsultation = async (doctorId, consultationTable, userId) => {
	const params = {
		TableName: consultationTable,
		KeyConditions: {
			userID_startTime: {
    			ComparisonOperator: 'BEGINS_WITH', /* required */
				AttributeValueList: [
					userId
				]
			},
			doctorID: {
    			ComparisonOperator: 'EQ', /* required */
				AttributeValueList: [
					doctorId
				]
			}
		},
		ScanIndexForward: false
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count ? Items[0] : false;
};

const updatePatientConsultation = async (params, consultationTable) => {
	const dbParams = {
		TableName: consultationTable,
		Item: params
	};
	await dbClient.put(dbParams).promise();
};

const deleteConnection = async (connectionId) => {
	const params = {
		TableName: connectionTable,
		Key: {
			connectionID: connectionId
		}
	};
	await dbClient.delete(params).promise();
};

module.exports.getConnectionParams = getConnectionParams;
module.exports.getLastConsultation = getLastConsultation;
module.exports.updatePatientConsultation = updatePatientConsultation;
module.exports.deleteConnection = deleteConnection;
