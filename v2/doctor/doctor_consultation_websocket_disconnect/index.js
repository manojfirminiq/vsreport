const CONSTANTS = require('./CONSTANTS.js');
const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});
const DB = require('./db.js');

exports.handler = async (event) => {
	const connectionID = event.requestContext.connectionId;
	console.log('connectionID', connectionID);
	try {
		const connectionDetails = await DB.getConnectionParams(connectionID);
		if (!connectionDetails) {
			console.log('no connection details');
			throw new Error(CONSTANTS.ERRORS.CONNECTION_ID_INVALID.MESSAGE);
		}
		await DB.deleteConnection(connectionID);
		console.log('deleted connection ID ', connectionID, ' from connection table');
		const {
			doctorID,
			userID,
			dbIdentifier
		} = connectionDetails;
		if (!dbIdentifier) {
			throw new Error(CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE);
		}
		const consultationTable = `${CONSTANTS.STAGE}${CONSTANTS.RPM}${dbIdentifier}_${CONSTANTS.DOCTOR_CONSULTATION}`;
		console.log('input params of getlastconsultation');
		console.log(doctorID, consultationTable, userID);
		const patientConsultation = await DB.getLastConsultation(doctorID, consultationTable, userID);
		if (patientConsultation && !patientConsultation.endTime && patientConsultation.tempEndTime && patientConsultation.connectionID) {
			if (patientConsultation.connectionID === connectionID) {
				console.log('same patient connection id');
				if (patientConsultation.startTime === patientConsultation.tempEndTime) {
					patientConsultation.isdeleted = true;
				}
				patientConsultation.endTime = patientConsultation.tempEndTime;
				delete patientConsultation.tempEndTime;
				delete patientConsultation.connectionID;
				patientConsultation.consultationTime = (patientConsultation.endTime - patientConsultation.startTime) / 1000;
				patientConsultation.consultationUnit = CONSTANTS.SECONDS;
				await DB.updatePatientConsultation(patientConsultation, consultationTable);
				console.log('made last active consultation inactive');
			}
		}
		const response = {
			statusCode: 200,
			body: JSON.stringify(CONSTANTS.SUCCESS_MESSAGE)
		};
		console.log('reached end of disconnect');
		return response;
	} catch (error) {
		console.log('websocket disconnect error', error);
		if (error.message === CONSTANTS.ERRORS.CONNECTION_ID_INVALID.MESSAGE) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.CONNECTION_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.CONNECTIONL_ID_INVALID.CODE
			});
		} else if (error.message === CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE
		});
	}
};
