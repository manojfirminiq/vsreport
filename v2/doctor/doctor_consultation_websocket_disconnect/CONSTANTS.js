exports.TIMER_CONNECTION_TABLE = process.env.TIMER_CONNECTION_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = '_rpm_';
exports.SUCCESS_MESSAGE = {
	success: 'true',
	message: 'consultation has been terminated'
};
exports.SECONDS = 'seconds';
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;
exports.ERRORS = {
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	CONNECTION_ID_INVALID: {
		CODE: 'CONNECTION_ID_INVALID',
		MESSAGE: 'Connection Id is invalid'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	}
};
