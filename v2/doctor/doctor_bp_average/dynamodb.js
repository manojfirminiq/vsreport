const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_BP_AVERAGE_TABLE = process.env.DOCTOR_BP_AVERAGE_TABLE;
const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const DOCTOR_TABLE = process.env.DOCTOR_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: DOCTOR_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		return {
			BP_AVERAGE_TABLE:
				process.env.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_BP_AVERAGE_TABLE,
			DOCTOR_PATIENT_TABLE:
				process.env.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT_TABLE
		};
	}
};

const getBpAverage = async (userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#userId = :userId',
		ExpressionAttributeValues: {
			':userId': userId
		},
		ExpressionAttributeNames: {
			'#userId': 'userID'
		}
	};
	return dbClient.query(params).promise();
};

const fetchDoctorPatient = async (userID, doctorPatientTable) => {
	const params = {
		TableName: doctorPatientTable,
		IndexName: 'userID-doctorID-index',
		KeyConditionExpression: 'userID = :id',
		ExpressionAttributeValues: {
			':id': userID
		},
		ScanIndexForward: false
	};
	const { Items, Count } = await dbClient.query(params).promise();
	return Count ? Items[0] : false;
};

module.exports.fetchDoctorPatient = fetchDoctorPatient;
module.exports.getBpAverage = getBpAverage;
module.exports.verifyHospital = verifyHospital;
