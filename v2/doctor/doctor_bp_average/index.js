const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');

const getBpAverage = async ({ userId, hospitalID }) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQ.CODE
			});
		}
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		const userDetails = await DB.fetchDoctorPatient(
			userId,
			hospitalTable.DOCTOR_PATIENT_TABLE
		);

		if (!userDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
			});
		}
		// if hospital is hodes, get readings using hubId, else use userId
		if (hospitalTable.DOCTOR_PATIENT_TABLE.includes(CONSTANTS.HODES)) {
			userId = userDetails.hubID ? userDetails.hubID : userDetails.userID;
		}
		let { Items } = await DB.getBpAverage(
			userId,
			hospitalTable.BP_AVERAGE_TABLE
		);
		Items = Items.map(
			({
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			}) => ({ ...attributesRest })
		);

		const response = {
			success: true,
			data: Items[0] ? Items[0] : {}
		};

		return Promise.resolve(response);
	} catch (error) {
		console.log('error', error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await getBpAverage(event);
	callback(null, response);
};
