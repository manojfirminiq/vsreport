const AWS = require('aws-sdk');
const CONSTANTS = require('CONSTANTS');
AWS.config.update({ region: process.env.REGION });
const { saveReading } = require('./save_alerts_db.js');

exports.handler = async (event, context) => {
	const userIds = new Set();
	if (event.Records) {
		// trigger flow
		for (const record of event.Records) {
			const recordToCheck =
				record.eventName === CONSTANTS.constants.insert ||
				record.eventName === CONSTANTS.constants.modify
					? AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage)
					: AWS.DynamoDB.Converter.unmarshall(record.dynamodb.OldImage);
			if (recordToCheck.attributes.isDoctorLinked) {
				await saveReading(event.Records[0].eventSourceARN, recordToCheck);
			}
		}
	}
	return `Successfully processed ${userIds.size} records.`;
};
