const aws = require('aws-sdk');
aws.config.update({ region: process.env.REGION });
const ses = new aws.SES({ region: process.env.REGION });

exports.sendEmailToDoctor = async (toAddresses, base64data, value) => {
	try {
		const params = {
			Destination: {
				ToAddresses: toAddresses
			},
			Message: {
				Body: {
					Text: {
						Data: `Please click here to view your patients alert on the VitalSight program. ${process.env.FE_URL}patient/${base64data}?view=${value}`
					}
				},
				Subject: {
					Data: 'VitalSight Alert Notification'
				}
			},
			Source: `${process.env.SENDER_EMAIL}`
		};
		ses.sendEmail(params, function (err, data) {
			if (err) {
				console.log(err);
				return err;
			} else {
				return data;
			}
		});
	} catch (err) {
		console.log(err);
	}
};
