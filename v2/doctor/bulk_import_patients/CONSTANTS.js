exports.RPM_HOSPITAL_MASTER = process.env.RPM_HOSPITAL_MASTER;
exports.RPM = 'rpm';
exports.MESSAGES = {
	NP_ID_NOT_FOUND: 'npID not found',
	HOSPITAL_ID_NOT_FOUND: 'Hospital ID not found'
};
exports.DEFAULT_COUNTRY = 'US';
exports.ORDER_PROCESSING = process.env.ORDER_PROCESSING;
exports.ORDER_DETAIL = process.env.ORDER_DETAIL;
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.EHR_USER = process.env.EHR_USER;
exports.WEIGHT_UNIT = 'lbs';
exports.HUB = 'Hub';
exports.OMRON = 'Omron';
exports.SCALE = 'Scale';
exports.MRN = 'MRN';
exports.NPI = 'NPI';
exports.STAGE = process.env.STAGE;
exports.REGULAR = 'Regular';
exports.LARGE = 'Large';
exports.OMRON_SMALL_BP = 'Omron small BP';
exports.OMRON_LARGE_BP = 'Omron large BP';
exports.OMRON_SMALL_SCALE = 'Omron small Scale';
exports.OMRON_LARGE_SCALE = 'Omron large Scale';
exports.DOCTOR_PROFILE = process.env.DOCTOR_PROFILE;
exports.BP_MODELS = ['BP7250', 'HEM-9210T'];
exports.WEIGHT_MODELS = ['BCM500', 'HN-290T'];
