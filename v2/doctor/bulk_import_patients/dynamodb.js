const AWS = require('aws-sdk');
const CONSTANTS = require('./CONSTANTS.js');
const crypto = require('crypto');

AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true });

const generateUserId = mrn => {
	let userID = crypto
		.createHash('sha256')
		.update(mrn.toString())
		.digest('hex');
	userID = crypto
		.createHash('sha256')
		.update(userID)
		.digest('hex');
	return userID;
};

const saveItem = (item, TABLE_NAME) => {
	const params = {
		TableName: TABLE_NAME,
		Item: item
	};
	return dbClient.put(params).promise();
};

const getkitType = async (bpdevicetype, weightdevicetype) => {
	let kitno = CONSTANTS.RPM.toUpperCase();
	if (bpdevicetype.toLowerCase().includes(CONSTANTS.REGULAR.toLowerCase())) {
		kitno = kitno.concat('1');
	} else {
		kitno = kitno.concat('2');
	}
	if (
		weightdevicetype.toLowerCase().includes(CONSTANTS.REGULAR.toLowerCase())
	) {
		kitno = kitno.concat('1');
	} else if (
		weightdevicetype.toLowerCase().includes(CONSTANTS.LARGE.toLowerCase())
	) {
		kitno = kitno.concat('2');
	} else {
		kitno = kitno.concat('3');
	}
	kitno = kitno.concat('1');
	if (!weightdevicetype) {
		kitno = kitno.concat(1);
	} else {
		kitno = kitno.concat(
			weightdevicetype.toLowerCase().includes(CONSTANTS.LARGE.toLowerCase())
				? '2'
				: '1'
		);
	}
	return kitno;
};

const formatPhoneNumber = phoneNumberString => {
	console.log('phoneNumberString', phoneNumberString);

	phoneNumberString = phoneNumberString
		.toString()
		.trim()
		.replace(/\s+/, '');
	if (phoneNumberString.includes('-')) {
		return phoneNumberString;
	} else {
		const cleaned = ('' + phoneNumberString).replace(/\D/g, '');
		const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
		if (match) {
			const intlCode = match[1] ? '+1' : '';
			return [intlCode, '(', match[2], ')-', match[3], '-', match[4]].join('');
		}
		return '';
	}
};

module.exports = {
	saveOrderProcessingDetail: async (
		patient,
		doctor,
		dbIdentifier,
		hospitalID,
		orderIdentifier
	) => {
		const currentDate = new Date().getTime();
		const userID = await generateUserId(
			patient.patientMRN
				.toString()
				.trim()
				.replace(/\s+/, '')
				.toLowerCase()
		);
		const clinicalInfo = [];
		if (patient.bpDevice && patient.bpDevice !== 'N/A') {
			clinicalInfo.push({
				code: 1,
				description: patient.bpDevice
					.toLowerCase()
					.includes(CONSTANTS.LARGE.toLowerCase())
					? `${CONSTANTS.OMRON_LARGE_BP}`
					: `${CONSTANTS.OMRON_SMALL_BP}`
			});
		}
		if (patient.weightScale && patient.weightScale !== 'N/A') {
			clinicalInfo.push({
				code: 2,
				description: patient.weightScale
					.toLowerCase()
					.includes(CONSTANTS.LARGE.toLowerCase())
					? `${CONSTANTS.OMRON_LARGE_SCALE}`
					: `${CONSTANTS.OMRON_SMALL_SCALE}`
			});
		}
		clinicalInfo.push({ code: 3, description: CONSTANTS.HUB });

		const item = {
			userID: userID,
			orderID_createdDate: orderIdentifier + currentDate + '_' + currentDate,
			modifiedDate: currentDate,
			createdDate: currentDate,
			attributes: {},
			processStatus: 1,
			orderDate: currentDate,
			orderTZ: 0 // local time, hence keeping it 0
		};

		const name = patient.firstName + ' ' + patient.lastName;
		const attributes = {
			patientIdentifier: '',
			bpModelNo:
				!patient.bpDevice || patient.bpDevice === 'N/A'
					? ''
					: patient.bpDevice
						.toLowerCase()
						.includes(CONSTANTS.LARGE.toLowerCase())
						? CONSTANTS.BP_MODELS[1]
						: CONSTANTS.BP_MODELS[0],
			scaleModelNo:
				!patient.weightScale || patient.weightScale === 'N/A'
					? ''
					: patient.weightScale
						.toLowerCase()
						.includes(CONSTANTS.LARGE.toLowerCase())
						? CONSTANTS.WEIGHT_MODELS[1]
						: CONSTANTS.WEIGHT_MODELS[0],
			transactionDateTime: currentDate,
			applicationOrderID: orderIdentifier + currentDate + '_' + currentDate,
			collectionDateTime: currentDate,
			eventDateTime: currentDate,
			status: '',
			clinicalInfo: clinicalInfo,
			mrNumberType: CONSTANTS.MRN,
			firstName: patient.firstName,
			middleName: '',
			lastName: patient.lastName,
			name: name,
			dateOfBirth: patient.dateOfBirth
				? `${new Date(patient.dateOfBirth).getUTCFullYear()}-${
					new Date(patient.dateOfBirth).getUTCMonth() + 1 <= 9
						? `0${new Date(patient.dateOfBirth).getUTCMonth() + 1}`
						: new Date(patient.dateOfBirth).getUTCMonth() + 1
				  }-${
					new Date(patient.dateOfBirth).getUTCDate() <= 9
						? `0${new Date(patient.dateOfBirth).getUTCDate()}`
						: new Date(patient.dateOfBirth).getUTCDate()
				  }`
				: '',
			gender: patient.gender ? patient.gender : '',
			homePhone:
				!patient.phoneNumber ||
				patient.phoneNumber === 'N/A' ||
				patient.phoneNumber === ''
					? ''
					: formatPhoneNumber(patient.phoneNumber),
			officePhone: '',
			mobilePhone:
				!patient['cell phone number'] ||
				patient['cell phone number'] === 'N/A' ||
				patient['cell phone number'] === ''
					? ''
					: formatPhoneNumber(patient['cell phone number']),
			mrNumber: patient.patientMRN,
			ehrID: hospitalID,
			address: {
				city: patient.city,
				country: patient.country ? patient.country : CONSTANTS.DEFAULT_COUNTRY,
				county: patient.county ? patient.county : '',
				state: patient.state,
				streetAddress: `${
					patient['address line 1'] ? `${patient['address line 1']}` : ''
				}${patient['address line 2'] ? `,${patient['address line 2']}` : ''}`,
				zip: String(patient.zip)
			},
			npID: patient.NPID,
			ehrEmailAddress: [],
			notes: [],
			threshold: {},
			weightPrefUnit: CONSTANTS.WEIGHT_UNIT,
			language: patient.language.toUpperCase(),
			race: '',
			maritalStatus: '',
			visit: {
				visitNumber: '',
				accountNumber: '',
				visitDateTime: ''
			},
			diagnosis: [],
			code: '',
			codeSet: '',
			procedure: {
				code: '',
				codeSet: ''
			},
			provider: {
				ID: doctor.npID,
				type: CONSTANTS.NPI,
				firstName: doctor.attributes.name.split(' ')[0],
				lastName: doctor.attributes.name.split(' ')[1]
					? doctor.attributes.name.split(' ')[1]
					: ''
			},
			facilityCode: ''
		};
		attributes.ehrEmailAddress.push(patient.ehrEmailAddress);
		item.attributes = attributes;
		item.attributes.isDoctorLinked = 1;
		const tableName =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			dbIdentifier +
			'_' +
			CONSTANTS.ORDER_PROCESSING;
		await saveItem(item, tableName);
		return item;
	},
	saveOrderDetail: async (orderprocess, dbIdentifier, patient) => {
		const currentDate = new Date().getTime();
		const item = {
			userID: orderprocess.userID,
			modifiedDate: currentDate,
			createdDate: currentDate,
			attributes: {},
			orderTZ: 0, // local time, hence keeping it 0,
			uniqueKeyType: 'imei',
			uniqueKey: patient.hubID,
			orderID: orderprocess.orderID_createdDate.split('_')[0],
			name: orderprocess.attributes.name
		};
		const attributes = {
			applicationOrderID: orderprocess.orderID_createdDate,
			bpModelNo: '',
			clinicalInfo: orderprocess.attributes.clinicalInfo,
			code: '',
			codeSet: '',
			collectionDateTime: currentDate,
			diagnosis: [],
			eventDateTime: currentDate,
			imei: item.uniqueKey,
			kitType: await getkitType(
				patient.bpDevice,
				patient.weightScale ? patient.weightScale : ''
			),
			notes: [],
			procedure: orderprocess.attributes.procedure,
			provider: orderprocess.attributes.provider,
			scaleModelNo: '',
			scaleSerialNo: '',
			status: null,
			transactionDateTime: currentDate,
			visit: orderprocess.attributes.visit
		};

		item.attributes = attributes;
		// let tableName = CONSTANTS.STAGE + '_' + CONSTANTS.RPM + '_' + dbIdentifier + '_' + CONSTANTS.ORDER_DETAIL;
		// await saveItem(item, tableName);
		return item;
	},
	saveEhrUser: async (patient, doctor, dbIdentifier, order, hospitalID) => {
		const currentDate = new Date().getTime();
		const item = {
			app: CONSTANTS.RPM.toUpperCase(),
			createdDate: currentDate,
			dateOfBirth: patient.dateOfBirth,
			ehrID: hospitalID,
			firstName: order.attributes.name,
			gender: patient.gender,
			goals: {
				dia: 0,
				pulse: 0,
				sys: 0,
				weight: 0
			},
			haUserID: order.userID,
			imei: order.uniqueKey,
			lastName: patient.firstName,
			middleName: patient.lastName,
			modifiedDate: currentDate,
			mrNumber: patient.patientMRN,
			mrNumberType: CONSTANTS.MRN,
			name: patient.name,
			npID: doctor.npID,
			orderID: order.orderID,
			threshold: {},
			thresholdWeightUnit: 'lbs',
			userID: order.userID,
			attributes: {}
		};

		const attributes = {
			address: {
				city: patient.city,
				country: patient.country,
				county: patient.county,
				state: patient.state,
				streetAddress: patient.address,
				zip: patient.zip
			},
			ehrEmailAddress: [],
			facilityCode: null,
			homePhone: patient.phoneNumber,
			language: null,
			maritalStatus: null,
			mobilePhone:
				patient['cell phone number'] === 'N/A' ||
				patient['cell phone number'] === ''
					? ''
					: patient['cell phone number'],
			officePhone: null,
			patientIdentifier: null,
			race: null,
			status: 1
		};
		item.attributes = attributes;
		// let tableName = CONSTANTS.STAGE + '_' + CONSTANTS.RPM + '_' + dbIdentifier + '_' + CONSTANTS.EHR_USER;
		// await saveItem(item, tableName);
		return item;
	},
	saveDoctorPatient: async (patient, doctor, dbIdentifier, order) => {
		const currentDate = new Date().getTime();
		const item = {
			createdDate: currentDate,
			doctorID: doctor.userID,
			hubID: order.uniqueKey,
			modifiedDate: currentDate,
			patientCode: patient.patientMRN,
			userID: order.userID,
			attributes: {}
		};

		const attributes = {
			averageBp: {
				diastolic: null,
				pulse: null,
				systolic: null
			},
			dob: patient.dateOfBirth,
			gender: patient.gender,
			irregularHeartBeat: 0,
			lastReportedDate: currentDate,
			name: order.name,
			threshold: {
				diaHigh: null,
				diaLow: null,
				sysHigh: null,
				sysLow: null,
				weightLowerLimit: null,
				weightUpperLimit: null
			},
			thresholdBpUnit: 'mmHg',
			thresholdWeightUnit: 'lbs',
			timeZone: 0
		};
		item.attributes = attributes;
		// let tableName = CONSTANTS.STAGE + '_' + CONSTANTS.RPM + '_' + dbIdentifier + '_' + CONSTANTS.DOCTOR_PATIENT;
		// await saveItem(item, tableName);
	}
};
