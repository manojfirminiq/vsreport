const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const XLSX = require('xlsx');
const { RPM_HOSPITAL_MASTER, MESSAGES, RPM } = require('./CONSTANTS.js');
const { saveOrderProcessingDetail } = require('./dynamodb.js');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient({ convertEmptyValues: true });
const findHospital = async hospitalID => {
	try {
		const paramsTable = {
			TableName: RPM_HOSPITAL_MASTER,
			KeyConditionExpression: '#hospitalID = :hospitalID',
			ExpressionAttributeValues: {
				':hospitalID': hospitalID
			},
			ExpressionAttributeNames: {
				'#hospitalID': 'hospitalID'
			}
		};
		const { Items } = await dbClient.query(paramsTable).promise();
		return Items;
	} catch (e) {
		console.log('error', e);
	}
};

const findDoctorProfile = async (tablename, npID) => {
	try {
		const paramsTable = {
			TableName: tablename,
			IndexName: 'npID-index',
			FilterExpression: '#npID = :npID',
			ExpressionAttributeNames: {
				'#npID': 'npID'
			},
			ExpressionAttributeValues: {
				':npID': npID.toString()
			}
		};
		const { Items } = await dbClient.scan(paramsTable).promise();
		return Items;
	} catch (e) {
		console.log('error', e);
	}
};

const formatData = async data => {
	try {
		const arr = Array.isArray(data) ? data : [data];
		for (let index = 0; index < arr.length; index++) {
			if (!arr[0].hospitalID) {
				return Promise.resolve({
					success: false,
					errorCode: 'HOSPITAL_ID_NOT_FOUND',
					message: MESSAGES.HOSPITAL_ID_NOT_FOUND
				});
			}
			const hospitalrecords = await findHospital(arr[0].hospitalID);
			if (hospitalrecords.length === 0) {
				return Promise.resolve({
					success: false,
					errorCode: 'HOSPITAL_ID_NOT_FOUND',
					message: MESSAGES.HOSPITAL_ID_NOT_FOUND
				});
			}
			const doctortable = `${process.env.STAGE}_${RPM}_${hospitalrecords[0].dbIdentifier}_${process.env.DOCTOR_PROFILE}`;
			const doctorrecords = await findDoctorProfile(
				doctortable,
				arr[index].NPID
			);
			if (doctorrecords && doctorrecords.length > 0) {
				await saveOrderProcessingDetail(
					arr[index],
					doctorrecords[0],
					hospitalrecords[0].dbIdentifier,
					arr[0].hospitalID,
					hospitalrecords[0].orderIdentifier
				);
				//  const orderDetails = await saveOrderDetail(orderProcessing, hospitalrecords[0].dbIdentifier ,arr[index]);
				//  const ehrUser = await saveEhrUser(arr[index], doctorrecords[0] ,hospitalrecords[0].dbIdentifier ,orderDetails,arr[0].hospitalID);
				//  const savedoctorpatient = await saveDoctorPatient(arr[index], doctorrecords[0] ,hospitalrecords[0].dbIdentifier ,orderDetails);
				console.log('done');
			} else {
				return Promise.resolve({
					success: false,
					errorCode: 'NP_ID_NOT_FOUND',
					message: MESSAGES.NP_ID_NOT_FOUND
				});
			}
		}
	} catch (e) {
		console.log(e);
	}
};

const readExcel = async function (record) {
	try {
		if (
			record &&
			record.s3 &&
			record.s3.object &&
			record.s3.bucket.name &&
			record.s3.object.key
		) {
			const { Body: fileData } = await s3
				.getObject({ Bucket: record.s3.bucket.name, Key: record.s3.object.key })
				.promise();
			const workbook = XLSX.read(fileData, {
				type: 'buffer',
				cellDates: true
			});
			const sheetNames = workbook.SheetNames;
			let data = [];
			sheetNames.forEach(sheetName => {
				const sheetData = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {
					dateNF: 'dd-mm-yyyy'
				});
				data = [...data, ...sheetData];
			});
			return data;
		}
	} catch (err) {
		console.log(err);
	}
};

exports.handler = async (event, context, callback) => {
	try {
		const promiseArr = [];
		if (event && event.Records) {
			event.Records.forEach(record => {
				promiseArr.push(readExcel(record));
			});
		}

		let data = await Promise.all(promiseArr);
		data = data.flat();
		await formatData(data);
	} catch (error) {
		console.log(error);
	}
};
