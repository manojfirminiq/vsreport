exports.ELASTICSEARCH_DOMAIN = process.env.ELASTICSEARCH_DOMAIN;
exports.REGION = process.env.REGION;
exports.ES_INDEX = process.env.ES_INDEX;
exports.AUTH_TYPE = 'Basic ';
exports.ENCODE_TYPE = 'base64';
exports.USER_NAME = process.env.USER_NAME;
exports.PASSWORD = process.env.PASSWORD;
exports.SEARCH_ERROR_MESSAGE = 'ERROR IN SEARCHING ICD CODE';
