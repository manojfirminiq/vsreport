
const AWS = require('aws-sdk');
const {
	ELASTICSEARCH_DOMAIN, REGION, ES_INDEX,
	AUTH_TYPE, USER_NAME, PASSWORD, ENCODE_TYPE, SEARCH_ERROR_MESSAGE
} = require('./CONSTANTS');

const auth = AUTH_TYPE + Buffer.from(USER_NAME + ':' + PASSWORD).toString(ENCODE_TYPE);
const endPoint = new AWS.Endpoint(ELASTICSEARCH_DOMAIN);
const httpClient = new AWS.HttpClient();

/**
 * Sends a request to elastic search
 */
const sendElasticSearchRequest = async ({ httpMethod, requestPath, payload }) => {
	const request = new AWS.HttpRequest(endPoint, REGION);

	request.method = httpMethod;
	request.path += requestPath;
	request.body = payload;
	request.headers['Content-Type'] = 'application/json';
	request.headers.host = ELASTICSEARCH_DOMAIN;
	request.headers['Content-Length'] = Buffer.byteLength(request.body);
	request.headers.Authorization = auth;

	return new Promise((resolve, reject) => {
		httpClient.handleRequest(request, null, (response) => {
			let responseBody = '';

			response.on('data', chunk => {
				responseBody += chunk;
			});

			response.on('end', (chunk) => {
				resolve(responseBody);
			});
		}, err => {
			console.log('Error while sending request to es', err);
			reject(err);
		});
	});
};

const frameSearchQuery = ({ search, group }) => {
	let query = null;
	if (search) {
		query = {
			_source: false,
			query: {
				nested: {
					path: 'codes',
					query: {
						bool: {
							must: {
								multi_match: {
									query: search,
									fields: [
										'codes.id',
										'codes.description'
									],
									operator: 'AND'
								}
							}
						}
					},
					inner_hits: {
						size: '100'
					}
				}
			}
		};
		return query;
	} else if (group) {
		query = {
			query: {
				match: {
					group: {
						query: group,
						operator: 'AND'
					}
				}
			}
		};
		return query;
	} else {
		query = {
			_source: ['group'],
			query: {
				match_all: {}
			}
		};
		return query;
	}
};

const frameSearchResponse = (event, searchResult) => {
	let result;
	if (event.search) {
		// eslint-disable-next-line camelcase
		const resultArr = searchResult.hits.hits.map(({ inner_hits }) => inner_hits.codes.hits.hits);
		result = resultArr.reduce((icdCodeArr, item) => icdCodeArr.concat(
			item.map(({ _source: { id, description } }) => ({ code: id, description }))
		), []);
		return result;
	}
	if (event.group) {
		const resultArr = searchResult.hits.hits.map(({ _source: { codes } }) => (codes));
		result = resultArr.reduce((icdCodeArr, item) => icdCodeArr.concat(
			item.map(({ id, description }) => ({ code: id, description }))
		), []);
		return result;
	}
	result = searchResult.hits.hits.map(({ _id }) => (_id));
	return result;
};

const searchIcdCode = async (event) => {
	try {
		const query = frameSearchQuery(event);
		const params = {
			httpMethod: 'POST',
			requestPath: ES_INDEX + '/_search',
			payload: JSON.stringify(query)
	    };

		const searchResult = await sendElasticSearchRequest(params);
		const response = frameSearchResponse(event, JSON.parse(searchResult));
		return Promise.resolve({ success: true, data: response });
	} catch (e) {
		console.log('Error in searching icd code', e);
		return Promise.resolve({ success: false, error: SEARCH_ERROR_MESSAGE });
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await searchIcdCode(event);
	callback(null, response);
};
