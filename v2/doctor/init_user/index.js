const AWS = require('aws-sdk');
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./dynamodb');
const s3Client = new AWS.S3();

const getUserProfileUrl = profilePicture => {
	return s3Client.getSignedUrl('getObject', {
		Bucket: CONSTANTS.PROFILE_BUCKET,
		Key: profilePicture,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	});
};

function groupBy (Items, userID) {
	let i = 0;
	let val;
	const result = {};
	for (; i < Items.length; i++) {
		val = Items[i][userID];
		if (result[val]) {
			result[val].push(Items[i]);
		} else {
			result[val] = [];
			result[val].push(Items[i]);
		}
	}
	return result;
}

const getIndicators = patient => {
	const threshold = {
		diaHigh:
			patient.threshold && patient.threshold.diaHigh
				? patient.threshold.diaHigh
				: null,
		diaLow:
			patient.threshold && patient.threshold.diaLow
				? patient.threshold.diaLow
				: null,
		sysHigh:
			patient.threshold && patient.threshold.sysHigh
				? patient.threshold.sysHigh
				: null,
		sysLow:
			patient.threshold && patient.threshold.sysLow
				? patient.threshold.sysLow
				: null
	};

	const indicators = {
		lastSevenDays: { systolic: null, diastolic: null },
		lastFourteenDays: { systolic: null, diastolic: null },
		lastTwentyOneDays: { systolic: null, diastolic: null },
		lastTen: { systolic: null, diastolic: null },
		lastFifteen: { systolic: null, diastolic: null },
		lastTwenty: { systolic: null, diastolic: null }
	};

	if (patient.averageBp && patient.averageBp.lastTwenty && threshold.sysHigh) {
		if (patient.averageBp.lastTwenty.systolic > threshold.sysHigh) {
			indicators.lastTwenty.systolic = CONSTANTS.HIGH;
		} else {
			indicators.lastTwenty.systolic = CONSTANTS.LOW;
		}
		if (patient.averageBp.lastTwenty.diastolic > threshold.diaHigh) {
			indicators.lastTwenty.diastolic = CONSTANTS.HIGH;
		} else {
			indicators.lastTwenty.diastolic = CONSTANTS.LOW;
		}
	}

	if (patient.averageBp && patient.averageBp.lastFifteen && threshold.sysHigh) {
		if (patient.averageBp.lastFifteen.systolic > threshold.sysHigh) {
			indicators.lastFifteen.systolic = CONSTANTS.HIGH;
		} else {
			indicators.lastFifteen.systolic = CONSTANTS.LOW;
		}
		if (patient.averageBp.lastFifteen.diastolic > threshold.diaHigh) {
			indicators.lastFifteen.diastolic = CONSTANTS.HIGH;
		} else {
			indicators.lastFifteen.diastolic = CONSTANTS.LOW;
		}
	}

	if (patient.averageBp && patient.averageBp.lastTen && threshold.sysHigh) {
		if (patient.averageBp.lastTen.systolic > threshold.sysHigh) {
			indicators.lastTen.systolic = CONSTANTS.HIGH;
		} else {
			indicators.lastTen.systolic = CONSTANTS.LOW;
		}
		if (patient.averageBp.lastTen.diastolic > threshold.diaHigh) {
			indicators.lastTen.diastolic = CONSTANTS.HIGH;
		} else {
			indicators.lastTen.diastolic = CONSTANTS.LOW;
		}
	}

	if (
		patient.averageBp &&
		patient.averageBp.lastTwentyOneDays &&
		threshold.sysHigh
	) {
		if (patient.averageBp.lastTwentyOneDays.systolic > threshold.sysHigh) {
			indicators.lastTwentyOneDays.systolic = CONSTANTS.HIGH;
		} else {
			indicators.lastTwentyOneDays.systolic = CONSTANTS.LOW;
		}
		if (patient.averageBp.lastTwentyOneDays.diastolic > threshold.diaHigh) {
			indicators.lastTwentyOneDays.diastolic = CONSTANTS.HIGH;
		} else {
			indicators.lastTwentyOneDays.diastolic = CONSTANTS.LOW;
		}
	}

	if (
		patient.averageBp &&
		patient.averageBp.lastFourteenDays &&
		threshold.sysHigh
	) {
		if (patient.averageBp.lastFourteenDays.systolic > threshold.sysHigh) {
			indicators.lastFourteenDays.systolic = CONSTANTS.HIGH;
		} else {
			indicators.lastFourteenDays.systolic = CONSTANTS.LOW;
		}
		if (patient.averageBp.lastFourteenDays.diastolic > threshold.diaHigh) {
			indicators.lastFourteenDays.diastolic = CONSTANTS.HIGH;
		} else {
			indicators.lastFourteenDays.diastolic = CONSTANTS.LOW;
		}
	}

	if (
		patient.averageBp &&
		patient.averageBp.lastSevenDays &&
		threshold.sysHigh
	) {
		if (patient.averageBp.lastSevenDays.systolic > threshold.sysHigh) {
			indicators.lastSevenDays.systolic = CONSTANTS.HIGH;
		} else {
			indicators.lastSevenDays.systolic = CONSTANTS.LOW;
		}
		if (patient.averageBp.lastSevenDays.diastolic > threshold.diaHigh) {
			indicators.lastSevenDays.diastolic = CONSTANTS.HIGH;
		} else {
			indicators.lastSevenDays.diastolic = CONSTANTS.LOW;
		}
	}
	return indicators;
};

const patientListData = async (doctorId, hospitalTableDetails, limit) => {
	let patientListObject;
	const finalpatientListArray = [];
	const patientObject = {};
	const patientList = await DB.getPatient(
		doctorId,
		hospitalTableDetails.DOCTOR_PATIENT
	);
	const doctorDetails = await DB.getDoctorData(
		doctorId,
		hospitalTableDetails.DOCTOR_PROFILE,
		hospitalTableDetails.Items[0].facilityCode,
		hospitalTableDetails.Items[0].name
	);

	let inActivePeriod;
	let prevLoginTime;

	if (doctorDetails && doctorDetails.inActivePeriod) {
		inActivePeriod = doctorDetails.inActivePeriod;
	} else {
		inActivePeriod = CONSTANTS.INACTIVE_PERIOD;
	}

	if (doctorDetails && doctorDetails.prevLoginTime) {
		prevLoginTime = new Date(doctorDetails.prevLoginTime);
	} else {
		prevLoginTime = new Date();
	}

	let lastPatientId = null;
	let activePatientCount = 0;
	let inActiveCount = 0;
	let activePatientWithThreshold = 0;
	if (patientList.Count > 0) {
		const patientListArray = patientList.Items;
		const now = new Date();
		const inActiveDayThreshold = Date.parse(
			new Date(now.setDate(now.getDate() - inActivePeriod))
		);
		const prevLoginInActiveThreshold = Date.parse(
			new Date(prevLoginTime.setDate(prevLoginTime.getDate() - inActivePeriod))
		);

		for (let i = 0; i < patientListArray.length; i++) {
			const {
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			} = patientListArray[i];
			patientListObject = {
				enrollmentDate: createdDate || null,
				...rest,
				...attributesRest
			};
			if (
				patientListObject.name &&
				(!patientListObject.firstName || !patientListObject.lastName)
			) {
				const name = patientListObject.name.split(' ');
				patientListObject.firstName = name[0];
				patientListObject.lastName =
					name && name.length > 1 ? name[name.length - 1] : '';
			}
			if (
				patientListObject.profilePicture &&
				(patientListObject.profilePicture != null ||
					patientListObject.profilePicture !== undefined)
			) {
				patientListObject.profilePicture = getUserProfileUrl(
					patientListObject.profilePicture
				);
			}
			patientListObject.isAssistedLiving = false;
			if (
				doctorDetails.hospitalID === CONSTANTS.GERMA &&
				patientListObject.shippingAddress
			) {
				const { streetAddress } = patientListObject.shippingAddress;
				if (
					streetAddress &&
					streetAddress.toLowerCase().includes(CONSTANTS.TIMBERS)
				) {
					patientListObject.isAssistedLiving = true;
				}
			}
			if (patientListObject.deleteFlag !== true) {
				if (
					patientListObject.lastReportedDate &&
					patientListObject.lastReportedDate < inActiveDayThreshold
				) {
					patientListObject.activeStatus = false;
					patientListObject.prevActive = false;
					if (
						patientListObject.lastReportedDate - prevLoginInActiveThreshold >=
						0
					) {
						patientListObject.prevActive = true;
						inActiveCount++;
					}
				} else if (
					!patientListObject.lastReportedDate ||
					patientListObject.lastReportedDate === ''
				) {
					patientListObject.activeStatus = null;
				} else {
					if (
						patientListObject.lastReportedDate &&
						patientListObject.lastReportedDate !== ''
					) {
						activePatientCount++;
						if (
							patientListObject.threshold &&
							(patientListObject.threshold.diaLow ||
								patientListObject.threshold.diaHigh ||
								patientListObject.threshold.sysLow ||
								patientListObject.threshold.sysHigh)
						) {
							patientListObject.haveThreshold = true;
							activePatientWithThreshold++;
						}
					}
					patientListObject.activeStatus = true;
				}
			}
			patientObject[patientListObject.userID] = patientListObject;

			if (!limit || finalpatientListArray.length < Number(limit)) {
				finalpatientListArray.push(patientListObject);
				lastPatientId = patientListObject.userID;
			}
			finalpatientListArray.sort(function (a, b) {
				return new Date(b.lastReportedDate) - new Date(a.lastReportedDate);
			});
		}
	}
	return Promise.resolve({
		finalpatientListArray: finalpatientListArray,
		lastPatientId: lastPatientId,
		patientCount: activePatientCount,
		inActiveCount: inActiveCount,
		patientObject: patientObject,
		activePatientWithThreshold: activePatientWithThreshold
	});
};

const bpAlertsData = async (doctorId, hospitalTableDetails, patientObject) => {
	let bpAlerts = await DB.getBpAlerts(
		doctorId,
		hospitalTableDetails.ALERT_BP_TABLE
	);
	let alertObject;
	const finalBpAlertArray = [];
	let alertCount = 0;
	let userAlerts = {};
	if (bpAlerts.Count > 0) {
		bpAlerts = bpAlerts.Items;
		for (let i = 0; i < bpAlerts.length; i++) {
			const userID = bpAlerts[i].actualUserID
				? bpAlerts[i].actualUserID
				: bpAlerts[i].userID;
			let patientName = '';
			let firstName = '';
			let lastName = '';
			let deleteFlag = false;
			if (patientObject[userID]) {
				patientName = patientObject[userID].name;
				firstName = patientObject[userID].firstName
					? patientObject[userID].firstName
					: '';
				lastName = patientObject[userID].lastName
					? patientObject[userID].lastName
					: '';
				deleteFlag = patientObject[userID].deleteFlag
					? patientObject[userID].deleteFlag
					: false;
			}
			if (patientName && (firstName === '' || lastName === '')) {
				const name = patientName.split(' ');
				firstName = name[0];
				lastName = name && name.length > 1 ? name[name.length - 1] : '';
			}
			alertObject = {
				resolveStatus: bpAlerts[i].alerts.bp.resolveStatus,
				userID: userID,
				patientName: patientName,
				firstName: firstName,
				lastName: lastName,
				sysDiff: Number(bpAlerts[i].alerts.bp.sysDiff),
				diaDiff: Number(bpAlerts[i].alerts.bp.diaDiff),
				pulseDiff: Number(bpAlerts[i].alerts.bp.pulseDiff),
				notes: bpAlerts[i].alerts.bp.Notes,
				measurementDate: bpAlerts[i].measurementDate,
				resolvedDate:
					bpAlerts[i].alerts.bp.resolveStatus === CONSTANTS.RESOLVE_STATUS
						? bpAlerts[i].alerts.bp.resolvedDate
						: null,
				type: CONSTANTS.TYPE.BP,
				averageType:
					(bpAlerts[i].attributes && bpAlerts[i].attributes.averageType) ||
					null,
				alertType:
					(bpAlerts[i].attributes && bpAlerts[i].attributes.alertType) || null
			};
			if (alertObject.patientName !== '' && deleteFlag !== true) {
				finalBpAlertArray.push(alertObject);
			}
			if (
				alertObject.resolveStatus !== CONSTANTS.RESOLVE_STATUS &&
				alertObject.patientName &&
				deleteFlag !== true
			) {
				alertCount++;
			}
		}

		userAlerts = groupBy(finalBpAlertArray, 'userID');
	}
	return Promise.resolve({
		finalBpAlertArray: finalBpAlertArray,
		alertCount: alertCount,
		userAlerts: userAlerts
	});
};

const nonBpAlertsData = async (
	doctorId,
	hospitalTableDetails,
	patientObject
) => {
	let nonBpAlerts = await DB.getNonBpAlerts(
		doctorId,
		hospitalTableDetails.ALERT_NON_BP_TABLE
	);
	let alertObject;
	const finalNonBpAlertArray = [];
	let alertCount = 0;
	let userAlerts = {};
	if (nonBpAlerts.Count > 0) {
		nonBpAlerts = nonBpAlerts.Items;
		for (let i = 0; i < nonBpAlerts.length; i++) {
			const userID = nonBpAlerts[i].actualUserID
				? nonBpAlerts[i].actualUserID
				: nonBpAlerts[i].userId_type_deviceLocalName.split('_')[0];
			let patientName = '';
			let firstName = '';
			let lastName = '';
			let deleteFlag = false;
			if (patientObject[userID]) {
				patientName = patientObject[userID].name;
				firstName = patientObject[userID].firstName
					? patientObject[userID].firstName
					: '';
				lastName = patientObject[userID].lastName
					? patientObject[userID].lastName
					: '';
				deleteFlag = patientObject[userID].deleteFlag
					? patientObject[userID].deleteFlag
					: false;
			}
			if (patientName && (firstName === '' || lastName === '')) {
				const name = patientName.split(' ');
				firstName = name[0];
				lastName = name && name.length > 1 ? name[name.length - 1] : '';
			}
			alertObject = {
				resolveStatus: nonBpAlerts[i].alerts.weight.resolveStatus,
				userID: userID,
				patientName: patientName,
				firstName: firstName,
				lastName: lastName,
				notes: nonBpAlerts[i].alerts.weight.Notes,
				measurementDate: nonBpAlerts[i].measurementDate,
				resolvedDate:
					nonBpAlerts[i].alerts.weight.resolveStatus ===
					CONSTANTS.RESOLVE_STATUS
						? nonBpAlerts[i].alerts.weight.resolvedDate
						: null,
				thresholdType24hr: nonBpAlerts[i].alerts.weight.thresholdType24hr
					? nonBpAlerts[i].alerts.weight.thresholdType24hr
					: null,
				weightDiff24hr: nonBpAlerts[i].alerts.weight.weightDiff24hr
					? nonBpAlerts[i].alerts.weight.weightDiff24hr
					: null,
				thresholdType72hr: nonBpAlerts[i].alerts.weight.thresholdType72hr
					? nonBpAlerts[i].alerts.weight.thresholdType72hr
					: null,
				weightDiff72hr: nonBpAlerts[i].alerts.weight.weightDiff72hr
					? nonBpAlerts[i].alerts.weight.weightDiff72hr
					: null,
				thresholdWeightUnit: nonBpAlerts[i].alerts.thresholdWeightUnit
					? nonBpAlerts[i].alerts.thresholdWeightUnit
					: null,
				threshold: nonBpAlerts[i].alerts.threshold
					? nonBpAlerts[i].alerts.threshold
					: null,
				type: CONSTANTS.TYPE.WEIGHT
			};
			if (alertObject.patientName !== '' && deleteFlag !== true) {
				finalNonBpAlertArray.push(alertObject);
			}
			if (
				alertObject.resolveStatus !== CONSTANTS.RESOLVE_STATUS &&
				alertObject.patientName &&
				deleteFlag !== true
			) {
				alertCount++;
			}
		}
		userAlerts = groupBy(finalNonBpAlertArray, 'userID');
	}
	return Promise.resolve({
		finalNonBpAlertArray: finalNonBpAlertArray,
		alertCount: alertCount,
		userAlerts: userAlerts
	});
};

const getData = async (
	doctorId,
	hospitalTableDetails,
	limit,
	hospitalFeatures,
	staffID
) => {
	let doctorData = {};
	let staffData = {};
	let patientList = [];
	let bpAlerts = [];
	let nonBpAlerts = [];
	let toDo = [];
	let start = new Date();
	start.setHours(0, 0, 0, 0);
	start = start.getTime();
	patientList = await patientListData(doctorId, hospitalTableDetails, limit);
	const promiseArr = [
		bpAlertsData(doctorId, hospitalTableDetails, patientList.patientObject),
		nonBpAlertsData(doctorId, hospitalTableDetails, patientList.patientObject),
		DB.getToDoData(doctorId, hospitalTableDetails.TO_DO_TABLE),
		DB.getDoctorData(
			doctorId,
			hospitalTableDetails.DOCTOR_PROFILE,
			hospitalTableDetails.Items[0].facilityCode,
			hospitalTableDetails.Items[0].name
		)
	];
	if (staffID) {
		promiseArr.push(
			DB.getStaffData(
				doctorId,
				staffID,
				hospitalTableDetails.STAFF_PROFILE,
				hospitalTableDetails.Items[0].facilityCode,
				hospitalTableDetails.Items[0].name
			)
		);
	}
	return await Promise.all(promiseArr)
		.then(values => {
			bpAlerts = values[0];
			nonBpAlerts = values[1];
			const getToDo = values[2];
			toDo = getToDo.Items;
			doctorData = values[3];
			staffData = values[4] || {};
			let openToDoCount = toDo.length;
			const toDoArray = [];

			for (let i = 0; i < toDo.length; i++) {
				if (
					toDo[i].dueDate >= start &&
					(!toDo[i].userID ||
						(toDo[i].userID &&
							patientList.patientObject[toDo[i].userID] &&
							patientList.patientObject[toDo[i].userID].deleteFlag !== true))
				) {
					toDoArray.push(toDo[i]);
				}
				if (
					patientList.patientObject[toDo[i].userID] &&
					patientList.patientObject[toDo[i].userID].deleteFlag === true
				) {
					openToDoCount--;
				}
			}

			toDo = toDoArray.map(
				({
					createdDate,
					modifiedDate,
					dueDate,
					doctorID,
					userID,
					attributes: {
						description,
						itemType,
						notes,
						resolveStatus,
						...attributesRest
					},
					...rest
				}) => ({
					createdDate,
					modifiedDate,
					dueDate,
					doctorID,
					userID,
					attributes: {
						description,
						itemType,
						notes,
						resolveStatus:
							Number(resolveStatus) || CONSTANTS.DEFAULT_RESOLVE_STATUS
					}
				})
			);

			if (doctorData.profilePicture) {
				doctorData.profilePicture = doctorData.profilePicture
					? getUserProfileUrl(doctorData.profilePicture)
					: null;
			}

			let inActivePeriod;
			if (doctorData && doctorData.inActivePeriod) {
				inActivePeriod = doctorData.inActivePeriod;
			} else {
				inActivePeriod = CONSTANTS.INACTIVE_PERIOD;
			}
			const now = new Date();
			const inActiveDayThreshold = Date.parse(
				new Date(now.setDate(now.getDate() - inActivePeriod))
			);

			const combinedAlerts = []
				.concat(bpAlerts.finalBpAlertArray, nonBpAlerts.finalNonBpAlertArray)
				.filter(alert => alert.resolveStatus !== CONSTANTS.RESOLVE_STATUS);
			if (combinedAlerts && combinedAlerts.length) {
				combinedAlerts.sort(function (a, b) {
					return new Date(b.measurementDate) - new Date(a.measurementDate);
				});
			}
			const patientStatus = {
				withinThreshold: 0,
				outSideThreshold: 0
			};

			for (const patient of patientList.finalpatientListArray) {
				const patientBpAlert = bpAlerts.userAlerts[patient.userID]
					? bpAlerts.userAlerts[patient.userID]
					: [];
				const patientNonBpAlert = nonBpAlerts.userAlerts[patient.userID]
					? nonBpAlerts.userAlerts[patient.userID]
					: [];
				let individualAerts = [].concat(patientBpAlert, patientNonBpAlert);
				individualAerts = individualAerts.sort(function (a, b) {
					return b.measurementDate - a.measurementDate;
				});

				patient.alertCount = individualAerts.filter(
					x => x.resolveStatus !== CONSTANTS.RESOLVE_STATUS
				).length;
				if (
					patient.activeStatus &&
					patient.haveThreshold &&
					individualAerts.length > 0 &&
					individualAerts[0].measurementDate > inActiveDayThreshold
				) {
					patientStatus.outSideThreshold++;
				}
				patient.indicator = getIndicators(patient);
			}
			patientStatus.withinThreshold =
				patientList.activePatientWithThreshold - patientStatus.outSideThreshold;

			const result = {
				doctorData: doctorData,
				staffData: staffData,
				patientList: patientList.finalpatientListArray,
				patientCount: patientList.patientCount,
				inActiveCount: patientList.inActiveCount,
				nextpaginationKey: patientList.lastPatientId,
				alerts: combinedAlerts.splice(0, 3),
				alertsCount: bpAlerts.alertCount + nonBpAlerts.alertCount,
				toDo: toDo.splice(0, 3),
				openToDoCount: openToDoCount,
				patientStatus: patientStatus,
				hospitalFeatures: hospitalFeatures
			};
			return Promise.resolve({ success: true, data: result });
		})
		.catch(reason => {
			console.log(reason);
			return Promise.resolve(reason);
		});
};

const applyValidation = async ({ doctorId, staffID, hospitalID, limit }) => {
	try {
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const hospitalFeatures =
			hospitalTableDetails.Items[0].hospitalFeatures ||
			CONSTANTS.HOSPITAL_FEATURES;
		return await getData(
			doctorId,
			hospitalTableDetails,
			limit,
			hospitalFeatures,
			staffID
		);
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
