exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};
exports.TYPE = {
	BP: 'bp',
	WEIGHT: 'weight'
};

exports.HODES = 'hodes';
exports.RPM = '_rpm_';
exports.S3_KEY_EXPIRES_IN = 60 * 60;
exports.PROFILE_BUCKET = process.env.PROFILE_BUCKET;
exports.INACTIVE_PERIOD = 7;
exports.HIGH = 'high';
exports.LOW = 'low';
exports.RESOLVE_STATUS = '1';
exports.DEFAULT_RESOLVE_STATUS = 0;
exports.HOSPITAL_FEATURES = {
	alerts: 1,
	billing: 1,
	documentation: 1,
	editProfile: 1,
	enrollPatient: 1,
	inviteTeamMember: 1,
	manageThreshold: 1,
	mfa: 1,
	myCareTeam: 1,
	timer: 1
};
exports.TIMBERS = 'timbers';
exports.GERMA = 'germa001';
