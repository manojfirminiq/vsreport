const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
const DOCTOR_TABLE = process.env.DOCTOR_TABLE;
const ALERT_NON_BP_TABLE = process.env.ALERT_NON_BP_TABLE;
const ALERT_BP_TABLE = process.env.ALERT_BP_TABLE;
const TO_DO_TABLE = process.env.TO_DO_TABLE;
const STAFF_PROFILE = process.env.STAFF_PROFILE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: DOCTOR_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			Items,
			DOCTOR_PROFILE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PROFILE_TABLE,
			DOCTOR_PATIENT:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT_TABLE,
			ALERT_BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				ALERT_BP_TABLE,
			ALERT_NON_BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				ALERT_NON_BP_TABLE,
			TO_DO_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				TO_DO_TABLE,
			STAFF_PROFILE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				STAFF_PROFILE
		};
		return obj;
	}
};

const getDoctorData = async (
	doctorID,
	hospitalTable,
	accountNumber,
	hospitalName
) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorID
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		const Item = Items.map(
			({
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			}) => ({
				createdDate,
				accountNumber,
				...rest,
				...attributesRest,
				hospitalName
			})
		)[0];

		return Item;
	} else {
		return null;
	}
};

const getPatient = async (
	doctorId,
	DOCTOR_PATIENT_TABLE,
	nextPaginationKey
) => {
	const params = {
		TableName: DOCTOR_PATIENT_TABLE,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID'
		}
	};
	if (nextPaginationKey) {
		params.ExclusiveStartKey = {
			doctorID: doctorId,
			userID: nextPaginationKey
		};
	}
	return dbClient.query(params).promise();
};

const getBpAlerts = async (doctorId, ALERT_BP_TABLE) => {
	const params = {
		TableName: ALERT_BP_TABLE,
		IndexName: 'doctorID-measurementDate-index',
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorId
		},
		ExpressionAttributeNames: {
			'#uid': 'doctorID'
		}
	};

	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

const getNonBpAlerts = async (doctorId, ALERT_NON_BP_TABLE) => {
	const params = {
		TableName: ALERT_NON_BP_TABLE,
		IndexName: 'doctorID-measurementDate-index',
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorId
		},
		ExpressionAttributeNames: {
			'#uid': 'doctorID'
		}
	};

	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

const getToDoData = async (doctorID, TO_DO_TABLE) => {
	const paramsTable = {
		TableName: TO_DO_TABLE,
		KeyConditionExpression: '#doctorID = :doctorID',
		FilterExpression: '#attributes.#resolveStatus <> :resolveStatus',
		ExpressionAttributeNames: {
			'#doctorID': 'doctorID',
			'#attributes': 'attributes',
			'#resolveStatus': 'resolveStatus'
		},
		ExpressionAttributeValues: {
			':doctorID': doctorID,
			':resolveStatus': Number(CONSTANTS.RESOLVE_STATUS)
		}
	};
	return dbClient.query(paramsTable).promise();
};

const getPatientDetails = async (doctorId, userID, DOCTOR_PATIENT) => {
	const params = {
		TableName: DOCTOR_PATIENT,
		KeyConditionExpression: '#doctorId = :doctorId AND #userID = :userID',
		FilterExpression: '#deleteFlag <> :deleteFlag',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userID': userID,
			':deleteFlag': true
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userID': 'userID',
			'#deleteFlag': 'deleteFlag'
		}
	};
	return dbClient.query(params).promise();
};

const getStaffData = async (
	doctorID,
	staffID,
	hospitalTable,
	accountNumber,
	hospitalName
) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id AND #staffID = :staffID',
		ExpressionAttributeValues: {
			':id': doctorID,
			':staffID': staffID
		},
		ExpressionAttributeNames: {
			'#uid': 'doctorID',
			'#staffID': 'staffID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		const Item = Items.map(
			({
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			}) => ({
				createdDate,
				accountNumber,
				...rest,
				...attributesRest,
				hospitalName
			})
		)[0];

		return Item;
	} else {
		return null;
	}
};

module.exports.getPatient = getPatient;
module.exports.getDoctorData = getDoctorData;
module.exports.verifyHospital = verifyHospital;
module.exports.getBpAlerts = getBpAlerts;
module.exports.getNonBpAlerts = getNonBpAlerts;
module.exports.getToDoData = getToDoData;
module.exports.getPatientDetails = getPatientDetails;
module.exports.getStaffData = getStaffData;
