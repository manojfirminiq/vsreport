/* eslint-disable standard/no-callback-literal */
const crypto = require('crypto');
const AWS = require('aws-sdk');
const dbClient = new AWS.DynamoDB.DocumentClient();
AWS.config.update({ region: process.env.REGION });
const jwt = require('jsonwebtoken');
const https = require('https');
const jwkToPem = require('jwk-to-pem');
const TOKEN_TRACK_TABLE = process.env.TOKEN_TRACK_TABLE;
const REGION = process.env.REGION;
const USERPOOLID = process.env.USERPOOLID;

const generateUserId = email => {
	let userID = crypto
		.createHash('sha256')
		.update(email)
		.digest('hex');
	userID = crypto
		.createHash('sha256')
		.update(userID)
		.digest('hex');
	console.log(userID);
	return userID;
};

const checkIfTokenUpdated = async (emailAddress, tokenGeneratedTime) => {
	const userId = generateUserId(emailAddress);
	const params = {
		TableName: TOKEN_TRACK_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': userId
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	let passwordUpdated = false;
	const { Items } = await dbClient.query(params).promise();
	if (Items.length > 0) {
		if (Items[0].updatedAt > tokenGeneratedTime) {
			passwordUpdated = true;
		}
	}
	return passwordUpdated;
};

const getPem = async () => {
	const url = `https://cognito-idp.${REGION}.amazonaws.com/${USERPOOLID}/.well-known/jwks.json`;

	return new Promise((resolve, reject) => {
		https
			.get(url, resp => {
				let data = '';

				// A chunk of data has been recieved.
				resp.on('data', chunk => {
					data += chunk;
				});

				// The whole response has been received. Print out the result.
				resp.on('end', () => {
					const pems = {};
					const body = JSON.parse(data);
					const keys = body.keys;
					for (let i = 0; i < keys.length; i++) {
						const keyId = keys[i].kid;
						const modulus = keys[i].n;
						const exponent = keys[i].e;
						const keyType = keys[i].kty;
						const jwk = { kty: keyType, n: modulus, e: exponent };
						const pem = jwkToPem(jwk);
						pems[keyId] = pem;
					}
					resolve(pems);
				});
			})
			.on('error', err => {
				// eslint-disable-next-line prefer-promise-reject-errors
				reject('Error: ' + err.message);
			});
	});
};

exports.handler = async function (event, context, callback) {
	const token = event.authorizationToken;
	if (!token) {
		console.log('Token not found');
		callback('Unauthorized');
	} else {
		try {
			const decodedJwt = jwt.decode(token, { complete: true });

			if (
				!decodedJwt ||
				!decodedJwt.payload ||
				!decodedJwt.payload.exp ||
				!decodedJwt.payload.email
			) {
				callback('Unauthorized');
			} else {
				try {
					const expiryTime = new Date(decodedJwt.payload.exp * 1000).getTime();
					const generatedTime = new Date(
						decodedJwt.payload.auth_time * 1000
					).getTime();
					const currentTime = new Date().getTime();
					const email = decodedJwt.payload.email;
					const userId = decodedJwt.payload['custom:userId'] || null;
					const hospitalID = decodedJwt.payload['custom:hospitalID'] || null;
					const staffID = decodedJwt.payload['custom:staffID'] || null;
					await Promise.all([
						getPem(),
						checkIfTokenUpdated(email, generatedTime)
					]).then(values => {
						const pems = values[0];
						const pem = pems[decodedJwt.header.kid];
						const passwordUpdated = values[1];
						console.log('expiryTime', 'currentTime', 'generatedTime');
						console.log(expiryTime, currentTime, generatedTime);
						if (passwordUpdated || expiryTime < currentTime) {
							callback('Unauthorized');
						} else {
							jwt.verify(token, pem, function (err, payload) {
								if (err) {
									console.log('Invalid Token.');
									callback('Unauthorized');
								} else {
									console.log('Valid Token.');
									callback(
										null,
										generatePolicy(
											'user',
											'Allow',
											event.methodArn,
											email,
											userId,
											hospitalID,
											staffID
										)
									);
								}
							});
						}
					});
				} catch (error) {
					console.log('Invalid signature', error);
					callback('Unauthorized');
				}
			}
		} catch (error) {
			console.log(error);
			callback('Unauthorized');
		}
	}
};

// Help function to generate an IAM policy
const generatePolicy = function (
	principalId,
	effect,
	resource,
	email,
	userId,
	hospitalID,
	staffID
) {
	const authResponse = {};
	authResponse.principalId = principalId;
	if (effect && resource) {
		const policyDocument = {};
		policyDocument.Version = '2012-10-17';
		policyDocument.Statement = [];
		const statementOne = {};
		statementOne.Action = 'execute-api:Invoke';
		statementOne.Effect = effect;
		statementOne.Resource = resource;
		policyDocument.Statement[0] = statementOne;
		authResponse.policyDocument = policyDocument;
	}
	// Optional output with custom properties of the String, Number or Boolean type.
	authResponse.context = {
		email: email,
		userId: userId,
		hospitalID: hospitalID,
		staffID: staffID
	};
	return authResponse;
};
