exports.ERRORS = {
	NP_ID_NOT_FOUND: {
		CODE: 'NP_ID_NOT_FOUND',
		MESSAGE: 'npID not found in Doctor Profile Table'
	},
	DB_IDENTIFIER_NOT_FOUND: {
		CODE: 'DB_IDENTIFIER_NOT_FOUND',
		MESSAGE: 'dbIdentifier for the hospital is not found'
	},
	PATIENT_REGISTRATION_FAILED: {
		CODE: 'PATIENT_REGISTRATION_FAILED',
		MESSAGE: 'Patient registration failed'
	},
	ORDER_ALREADY_PROCESSED: {
		CODE: 'ORDER_ALREADY_PROCESSED',
		MESSAGE: 'Order already processed'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR'
	},
	INVALID_PACEMAKER: {
		CODE: 'INVALID_PACEMAKER',
		MESSAGE: 'Pacemaker value is not valid'
	}
};

exports.VALIDATION_MESSAGES = {
	FIRST_NAME_KEY_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'Patient First Name is required'
	},
	LAST_NAME_KEY_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'Patient Last Name is required'
	},
	ADDRESS_KEY_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'Patient Address is required'
	},
	MRN_KEY_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'MRN Number is required'
	},
	DEVICE_KEY_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'Device Type is required'
	},
	HOSPITAL_ID_KEY_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'Hospital ID is required'
	},
	DOCTOR_ID_KEY_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'Doctor ID is required'
	},
	MOBILE_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'Mobile Number is required'
	},
	GENDER_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'Gender is required'
	},
	SHIPPING_ADDRESS_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'Shipping Address is required'
	},
	PATIENT_ALREADY_EXISTS: {
		CODE: 'PATIENT_ALREADY_EXISTS',
		MESSAGE: 'Patient with same MRN already exists'
	},
	INPUT_VALIDATION_ERROR: {
		CODE: 'INPUT_VALIDATION_ERROR',
		MESSAGE: 'Error in input provided'
	}
};

exports.SUCCESS = {
	SUBMIT_DETAILS: 'Sucessfully submitted details',
	PATIENT_REGISTERED: 'Patient Registered successfully',
	ORDER_PROCESSING_UPDATE: 'Order Processing Updated successfully',
	DOCTOR_PATIENT: 'Doctor Patient Updated successfully'
};

exports.ORDER_PROCESSING = process.env.ORDER_PROCESSING;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = 'rpm';
exports.ORDER_PROCESSING = 'order_processing';
exports.WEIGHT_UNIT = 'lbs';
exports.BP = 'BP';
exports.HUB = 'Hub';
exports.OMRON = 'Omron';
exports.SCALE = 'Scale';
exports.MRN = 'MRN';
exports.NPI = 'NPI';
exports.LARGE = 'large';
exports.BP_MODELS = ['BP7250', 'HEM-9210T'];
exports.WEIGHT_MODELS = ['BCM500', 'HN-290T'];
exports.PROCESS_STATUS = 1;
exports.ORDER_PROCESSED_STATUS = 4;
exports.GENDER = ['m', 'f'];
