const crypto = require('crypto');
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');
const trackPatient = require('track_patients_record');

const saveItem = (item, TABLE_NAME) => {
	const params = {
		TableName: TABLE_NAME,
		Item: item
	};
	return dbClient.put(params).promise();
};

const getPatientDetails = async (userID, docPatientTable) => {
	const params = {
		TableName: docPatientTable,
		IndexName: 'userID-doctorID-index',
		KeyConditionExpression: '#userID = :userID ',
		ExpressionAttributeValues: {
			':userID': userID
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const { Items } = await dbClient.query(params).promise();
	return Items;
};

const getOrderDetails = async (userId, orderProcessingTable) => {
	const params = {
		TableName: orderProcessingTable,
		KeyConditionExpression: '#userID = :userID',
		ExpressionAttributeValues: {
			':userID': userId
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const getDoctorPatientDetails = async (doctorID, userID, docPatientTable) => {
	const params = {
		TableName: docPatientTable,
		IndexName: 'userID-doctorID-index',
		KeyConditionExpression: '#doctorID = :doctorID AND #userID = :userID ',
		ExpressionAttributeValues: {
			':doctorID': doctorID,
			':userID': userID
		},
		ExpressionAttributeNames: {
			'#doctorID': 'doctorID',
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const updateOrderProcessing = async (
	userID,
	orderIDCreatedDate,
	attributes,
	TABLE_NAME,
	updateUserData
) => {
	const params = {
		TableName: TABLE_NAME,
		Key: {
			userID: userID,
			orderID_createdDate: orderIDCreatedDate
		},
		UpdateExpression:
			'SET attributes = :attributes, modifiedDate = :modifiedDate, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':attributes': attributes,
			':modifiedDate': new Date().getTime(),
			':updateUserData': [updateUserData],
			':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const updateDoctorPatient = async (
	doctorID,
	userID,
	attributesDoctorPatient,
	tableName,
	processStatus,
	updateUserData
) => {
	const params = {
		TableName: tableName,
		Key: {
			doctorID: doctorID,
			userID: userID
		},
		UpdateExpression:
			'SET attributes = :attributes, modifiedDate = :modifiedDate, processStatus = :processStatus, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':attributes': attributesDoctorPatient,
			':modifiedDate': new Date().getTime(),
			':processStatus': processStatus,
			':updateUserData': [updateUserData],
			':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const generateUserId = mrn => {
	let userID = crypto
		.createHash('sha256')
		.update(mrn)
		.digest('hex');
	userID = crypto
		.createHash('sha256')
		.update(userID)
		.digest('hex');
	return userID;
};

const getNpIDFromDoctorProfile = async (doctorID, table) => {
	const params = {
		TableName: table,
		KeyConditionExpression: '#userID = :userID',
		ExpressionAttributeValues: {
			':userID': doctorID
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const getDbIdenitifierFromHospital = async hospitalID => {
	const params = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospitalID = :hospitalID',
		ExpressionAttributeValues: {
			':hospitalID': hospitalID
		},
		ExpressionAttributeNames: {
			'#hospitalID': 'hospitalID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();

	return Count > 0 ? Items : false;
};

const saveOrderProcessingDetail = async (
	userID,
	hospitalDetail,
	firstName,
	lastName,
	patientAddress,
	patientMRN,
	pacemaker,
	deviceType,
	hospitalID,
	doctorID,
	email,
	dob,
	threshold,
	gender,
	language,
	icdCode,
	officePhone,
	homePhone,
	mobilePhone,
	shippingAddress,
	updateUserData
) => {
	const currentDate = new Date().getTime();
	const docProfileTable =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PROFILE_TABLE;
	const npIDDetails = await getNpIDFromDoctorProfile(doctorID, docProfileTable);
	if (npIDDetails && hospitalDetail) {
		const clinicalInfo = [];
		let bpModelNo = '';
		let weightModelNo = '';
		for (const device of deviceType) {
			if (device.type === CONSTANTS.BP) {
				clinicalInfo.push({
					code: 1,
					description: CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.BP
				});
				if (device.size.toLowerCase() === CONSTANTS.LARGE) {
					bpModelNo = CONSTANTS.BP_MODELS[1];
				} else {
					bpModelNo = CONSTANTS.BP_MODELS[0];
				}
			} else {
				clinicalInfo.push({
					code: 2,
					description:
						CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.SCALE
				});
				if (device.size.toLowerCase() === CONSTANTS.LARGE) {
					weightModelNo = CONSTANTS.WEIGHT_MODELS[1];
				} else {
					weightModelNo = CONSTANTS.WEIGHT_MODELS[0];
				}
			}
		}
		clinicalInfo.push({ code: 3, description: CONSTANTS.HUB });
		const item = {
			userID: userID,
			orderID_createdDate:
				hospitalDetail[0].orderIdentifier + currentDate + '_' + currentDate,
			modifiedDate: currentDate,
			createdDate: currentDate,
			attributes: {},
			processStatus: CONSTANTS.PROCESS_STATUS,
			orderDate: currentDate,
			orderTZ: 0, // local time, hence keeping it 0
			updates: []
		};
		if (!threshold) {
			threshold = {};
		}
		const attributes = {
			patientIdentifier: '',
			bpModelNo: bpModelNo,
			scaleModelNo: weightModelNo,
			transactionDateTime: currentDate,
			applicationOrderID: item.orderID_createdDate,
			collectionDateTime: currentDate,
			eventDateTime: currentDate,
			status: '',
			clinicalInfo: clinicalInfo,
			mrNumberType: CONSTANTS.MRN,
			firstName: firstName,
			middleName: '',
			lastName: lastName,
			name: `${firstName} ${lastName}`,
			dateOfBirth: dob,
			gender: gender || '',
			homePhone: homePhone,
			officePhone: officePhone || '',
			mobilePhone: mobilePhone,
			mrNumber: patientMRN,
			pacemaker: Number(pacemaker),
			ehrID: hospitalID,
			address: patientAddress,
			npID: npIDDetails[0].npID,
			ehrEmailAddress: [],
			notes: [],
			threshold: {
				diaUpperLimit: threshold.diaUpperLimit
					? Number(threshold.diaUpperLimit)
					: null,
				diaLowerLimit: threshold.diaLowerLimit
					? Number(threshold.diaLowerLimit)
					: null,
				sysUpperLimit: threshold.sysUpperLimit
					? Number(threshold.sysUpperLimit)
					: null,
				sysLowerLimit: threshold.sysLowerLimit
					? Number(threshold.sysLowerLimit)
					: null,
				diaLowerAverage: threshold.diaLowerAverage
					? Number(threshold.diaLowerAverage)
					: null,
				diaUpperAverage: threshold.diaUpperAverage
					? Number(threshold.diaUpperAverage)
					: null,
				sysLowerAverage: threshold.sysLowerAverage
					? Number(threshold.sysLowerAverage)
					: null,
				sysUpperAverage: threshold.sysUpperAverage
					? Number(threshold.sysUpperAverage)
					: null,
				bpAverage: threshold.bpAverage ? threshold.bpAverage : null,
				weight24hr: threshold.weight24hr ? Number(threshold.weight24hr) : null,
				weight72hr: threshold.weight72hr ? Number(threshold.weight72hr) : null
			},
			weightPrefUnit: CONSTANTS.WEIGHT_UNIT,
			goals: {
				sys: 0,
				dia: 0,
				pulse: 0,
				weight: 0
			},
			language: language,
			race: '',
			maritalStatus: '',
			visit: {
				visitNumber: '',
				accountNumber: '',
				visitDateTime: ''
			},
			diagnosis: [],
			code: '',
			codeSet: '',
			procedure: {
				code: '',
				codeSet: ''
			},
			provider: {
				ID: npIDDetails[0].npID,
				type: CONSTANTS.NPI,
				firstName: npIDDetails[0].attributes.name.split(' ')[0],
				lastName: npIDDetails[0].attributes.name.split(' ')[1]
			},
			shippingAddress: shippingAddress,
			icdCode: icdCode,
			facilityCode: ''
		};
		attributes.ehrEmailAddress.push(email);

		item.attributes = attributes;
		item.attributes.isDoctorLinked = 1;
		const tableName =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			hospitalDetail[0].dbIdentifier +
			'_' +
			CONSTANTS.ORDER_PROCESSING;
		item.updates.push(updateUserData);
		await saveItem(item, tableName);
		return Promise.resolve({
			success: true,
			message: CONSTANTS.SUCCESS.SUBMIT_DETAILS
		});
	} else {
		if (!npIDDetails) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.CODE,
				message: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.MESSAGE
			});
		}
		if (!hospitalDetail) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.CODE,
				message: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.MESSAGE
			});
		}
	}
};

const saveDoctorPatientDetails = async (
	userID,
	hospitalDetail,
	firstName,
	lastName,
	patientAddress,
	patientMRN,
	pacemaker,
	deviceType,
	hospitalID,
	doctorID,
	email,
	dob,
	threshold,
	gender,
	language,
	icdCode,
	officePhone,
	homePhone,
	mobilePhone,
	shippingAddress,
	updateUserData,
	consent,
	contactVai
) => {
	const currentDate = new Date().getTime();
	const docProfileTable =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PROFILE_TABLE;
	const npIDDetails = await getNpIDFromDoctorProfile(doctorID, docProfileTable);
	if (npIDDetails && hospitalDetail) {
		const clinicalInfo = [];
		for (const device of deviceType) {
			if (device.type === CONSTANTS.BP) {
				clinicalInfo.push({
					code: 1,
					description: CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.BP
				});
			} else {
				clinicalInfo.push({
					code: 2,
					description:
						CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.SCALE
				});
			}
		}
		clinicalInfo.push({ code: 3, description: CONSTANTS.HUB });
		const item = {
			userID: userID,
			modifiedDate: currentDate,
			createdDate: currentDate,
			doctorID: doctorID,
			processStatus: CONSTANTS.PROCESS_STATUS,
			patientCode: '',
			hubID: '',
			attributes: {},
			updates: []
		};
		if (!threshold) {
			threshold = {};
		}
		const attributesDoctorPatient = {
			dob: dob,
			gender: gender,
			irregularHeartBeat: '',
			lastReportedDate: '',
			language: language,
			firstName: firstName,
			lastName: lastName,
			pacemaker: Number(pacemaker),
			name: `${firstName} ${lastName}`,
			threshold: {
				diaHigh: threshold.diaUpperLimit
					? Number(threshold.diaUpperLimit)
					: null,
				diaLow: threshold.diaLowerLimit
					? Number(threshold.diaLowerLimit)
					: null,
				sysHigh: threshold.sysUpperLimit
					? Number(threshold.sysUpperLimit)
					: null,
				sysLow: threshold.sysLowerLimit
					? Number(threshold.sysLowerLimit)
					: null,
				diaLowAverage: threshold.diaLowerAverage
					? Number(threshold.diaLowerAverage)
					: null,
				diaHighAverage: threshold.diaUpperAverage
					? Number(threshold.diaUpperAverage)
					: null,
				sysLowAverage: threshold.sysLowerAverage
					? Number(threshold.sysLowerAverage)
					: null,
				sysHighAverage: threshold.sysUpperAverage
					? Number(threshold.sysUpperAverage)
					: null,
				bpAverage: threshold.bpAverage ? threshold.bpAverage : null,
				weight24hr: threshold.weight24hr ? Number(threshold.weight24hr) : null,
				weight72hr: threshold.weight72hr ? Number(threshold.weight72hr) : null
			},
			thresholdBpUnit: '',
			thresholdWeightUnit: '',
			timeZone: 0,
			icdCode: icdCode,
			shippingAddress: shippingAddress,
			emailAddress: email || '',
			officePhone: officePhone || '',
			homePhone: homePhone,
			mobilePhone: mobilePhone,
			consent,
			contactVai
		};

		item.attributes = attributesDoctorPatient;
		item.attributes.isDoctorLinked = 1;
		const tableName =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			hospitalDetail[0].dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PATIENT;
		item.updates.push(updateUserData);
		await saveItem(item, tableName);
		return Promise.resolve({
			success: true,
			userId: userID,
			message: CONSTANTS.SUCCESS.SUBMIT_DETAILS
		});
	} else {
		if (!npIDDetails) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.CODE,
				message: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.MESSAGE
			});
		}
		if (!hospitalDetail) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.CODE,
				message: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.MESSAGE
			});
		}
	}
};

const updateOrderProcessingDetail = async (
	userId,
	hospitalDetail,
	firstName,
	lastName,
	patientAddress,
	patientMRN,
	pacemaker,
	deviceType,
	hospitalID,
	doctorID,
	email,
	dob,
	threshold,
	gender,
	language,
	icdCode,
	officePhone,
	homePhone,
	mobilePhone,
	shippingAddress,
	updateUserData
) => {
	const orderProcessingTable =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.ORDER_PROCESSING;
	const orderDetails = await getOrderDetails(userId, orderProcessingTable);
	if (
		orderDetails &&
		orderDetails[0].processStatus !== CONSTANTS.ORDER_PROCESSED_STATUS
	) {
		const orderIDCreatedDate = orderDetails[0].orderID_createdDate;
		const currentDate = new Date().getTime();
		const docProfileTable =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			hospitalDetail[0].dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PROFILE_TABLE;
		const npIDDetails = await getNpIDFromDoctorProfile(
			doctorID,
			docProfileTable
		);
		if (npIDDetails && hospitalDetail) {
			const clinicalInfo = [];
			let bpModelNo = '';
			let weightModelNo = '';
			for (const device of deviceType) {
				if (device.type === CONSTANTS.BP) {
					clinicalInfo.push({
						code: 1,
						description:
							CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.BP
					});
					if (device.size.toLowerCase() === CONSTANTS.LARGE) {
						bpModelNo = CONSTANTS.BP_MODELS[1];
					} else {
						bpModelNo = CONSTANTS.BP_MODELS[0];
					}
				} else {
					clinicalInfo.push({
						code: 2,
						description:
							CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.SCALE
					});
					if (device.size.toLowerCase() === CONSTANTS.LARGE) {
						weightModelNo = CONSTANTS.WEIGHT_MODELS[1];
					} else {
						weightModelNo = CONSTANTS.WEIGHT_MODELS[0];
					}
				}
			}
			clinicalInfo.push({ code: 3, description: CONSTANTS.HUB });

			const attributes = {
				patientIdentifier: '',
				bpModelNo: bpModelNo || orderDetails[0].bpModelNo,
				scaleModelNo: weightModelNo || orderDetails[0].scaleModelNo,
				applicationOrderID: orderDetails[0].orderID_createdDate,
				collectionDateTime: currentDate,
				eventDateTime: currentDate,
				status: '',
				clinicalInfo: clinicalInfo,
				mrNumberType: CONSTANTS.MRN,
				firstName: firstName,
				middleName: '',
				lastName: lastName,
				name: `${firstName} ${lastName}` || orderDetails[0].name,
				dateOfBirth: dob || orderDetails[0].dateOfBirth,
				gender: gender || orderDetails[0].gender,
				officePhone: officePhone || orderDetails[0].officePhone,
				homePhone: homePhone || orderDetails[0].homePhone,
				mobilePhone: mobilePhone || orderDetails[0].mobilePhone,
				mrNumber: patientMRN || orderDetails[0].mrNumber,
				pacemaker: Number(pacemaker),
				ehrID: hospitalID || orderDetails[0].ehrID,
				address: patientAddress || orderDetails[0].address,
				npID: npIDDetails[0].npID ? npIDDetails[0].npID : orderDetails.npID,
				ehrEmailAddress: [],
				notes: [],
				threshold: threshold || orderDetails[0].threshold,
				weightPrefUnit: CONSTANTS.WEIGHT_UNIT,
				goals: {
					sys: 0,
					dia: 0,
					pulse: 0,
					weight: 0
				},
				language: language || orderDetails[0].language,
				race: '',
				maritalStatus: '',
				visit: {
					visitNumber: '',
					accountNumber: '',
					visitDateTime: ''
				},
				diagnosis: [],
				code: '',
				codeSet: '',
				procedure: {
					code: '',
					codeSet: ''
				},
				provider: {
					ID: npIDDetails[0].npID,
					type: CONSTANTS.NPI,
					firstName: npIDDetails[0].attributes.name.split(' ')[0],
					lastName: npIDDetails[0].attributes.name.split(' ')[1]
				},
				shippingAddress: shippingAddress || orderDetails[0].shippingAddress,
				icdCode: icdCode || orderDetails[0].icdCode,
				facilityCode: ''
			};
			attributes.ehrEmailAddress.push(email);

			const tableName =
				CONSTANTS.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				hospitalDetail[0].dbIdentifier +
				'_' +
				CONSTANTS.ORDER_PROCESSING;
			await updateOrderProcessing(
				userId,
				orderIDCreatedDate,
				attributes,
				tableName,
				updateUserData
			);
			return Promise.resolve({
				success: true,
				message: CONSTANTS.SUCCESS.ORDER_PROCESSING_UPDATE
			});
		} else {
			if (!npIDDetails) {
				return Promise.resolve({
					success: false,
					errorCode: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.CODE,
					message: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.MESSAGE
				});
			}
			if (!hospitalDetail) {
				return Promise.resolve({
					success: false,
					errorCode: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.CODE,
					message: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.MESSAGE
				});
			}
		}
	} else if (!orderDetails) {
		saveOrderProcessingDetail(
			userId,
			hospitalDetail,
			firstName,
			lastName,
			patientAddress,
			patientMRN,
			pacemaker,
			deviceType,
			hospitalID,
			doctorID,
			email,
			dob,
			threshold,
			gender,
			language,
			icdCode,
			officePhone,
			homePhone,
			mobilePhone,
			shippingAddress,
			updateUserData
		);
	} else {
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.ORDER_ALREADY_PROCESSED.CODE,
			message: CONSTANTS.ERRORS.ORDER_ALREADY_PROCESSED.MESSAGE
		});
	}
};

const updateDoctorPatientDetails = async (
	userId,
	hospitalDetail,
	firstName,
	lastName,
	patientAddress,
	patientMRN,
	pacemaker,
	deviceType,
	hospitalID,
	doctorID,
	email,
	dob,
	threshold,
	gender,
	language,
	icdCode,
	officePhone,
	homePhone,
	mobilePhone,
	shippingAddress,
	updateUserData,
	consent,
	contactVai
) => {
	const docPatientTable =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PATIENT;
	const doctorPatientDetails = await getDoctorPatientDetails(
		doctorID,
		userId,
		docPatientTable
	);
	if (
		doctorPatientDetails &&
		doctorPatientDetails[0].processStatus !== CONSTANTS.ORDER_PROCESSED_STATUS
	) {
		const hospitalDetail = await getDbIdenitifierFromHospital(hospitalID);
		const docProfileTable =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			hospitalDetail[0].dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PROFILE_TABLE;
		const npIDDetails = await getNpIDFromDoctorProfile(
			doctorID,
			docProfileTable
		);
		if (npIDDetails && hospitalDetail) {
			const clinicalInfo = [];
			for (const device of deviceType) {
				if (device.type === CONSTANTS.BP) {
					clinicalInfo.push({
						code: 1,
						description:
							CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.BP
					});
				} else {
					clinicalInfo.push({
						code: 2,
						description:
							CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.SCALE
					});
				}
			}
			clinicalInfo.push({ code: 3, description: CONSTANTS.HUB });
			const processStatus = doctorPatientDetails[0].processStatus;
			if (!threshold) {
				threshold = {};
			}
			const attributesDoctorPatient = {
				dob: dob || doctorPatientDetails[0].dob,
				gender: gender || doctorPatientDetails[0].gender,
				irregularHeartBeat: '',
				lastReportedDate: '',
				language: language,
				firstName: firstName,
				lastName: lastName,
				pacemaker: Number(pacemaker),
				name:
					`${firstName} ${lastName}` || doctorPatientDetails[0].attributes.name,
				threshold: {
					diaHigh: threshold.diaUpperLimit
						? Number(threshold.diaUpperLimit)
						: null,
					diaLow: threshold.diaLowerLimit
						? Number(threshold.diaLowerLimit)
						: null,
					sysHigh: threshold.sysUpperLimit
						? Number(threshold.sysUpperLimit)
						: null,
					sysLow: threshold.sysLowerLimit
						? Number(threshold.sysLowerLimit)
						: null,
					diaLowAverage: threshold.diaLowerAverage
						? Number(threshold.diaLowerAverage)
						: null,
					diaHighAverage: threshold.diaUpperAverage
						? Number(threshold.diaUpperAverage)
						: null,
					sysLowAverage: threshold.sysLowerAverage
						? Number(threshold.sysLowerAverage)
						: null,
					sysHighAverage: threshold.sysUpperAverage
						? Number(threshold.sysUpperAverage)
						: null,
					bpAverage: threshold.bpAverage ? threshold.bpAverage : null,
					weight24hr: threshold.weight24hr
						? Number(threshold.weight24hr)
						: null,
					weight72hr: threshold.weight72hr ? Number(threshold.weight72hr) : null
				},
				thresholdBpUnit: '',
				thresholdWeightUnit: '',
				timeZone: 0,
				icdCode: icdCode || doctorPatientDetails[0].icdCode,
				shippingAddress:
					shippingAddress || doctorPatientDetails[0].shippingAddress,
				officePhone: officePhone || doctorPatientDetails[0].officePhone,
				homePhone: homePhone || doctorPatientDetails[0].homePhone,
				mobilePhone: mobilePhone || doctorPatientDetails[0].mobilePhone,
				emailAddress: email || doctorPatientDetails[0].emailAddress,
				consent,
				contactVai
			};

			const tableName =
				CONSTANTS.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				hospitalDetail[0].dbIdentifier +
				'_' +
				CONSTANTS.DOCTOR_PATIENT;
			await updateDoctorPatient(
				doctorID,
				userId,
				attributesDoctorPatient,
				tableName,
				processStatus,
				updateUserData
			);
			return Promise.resolve({
				success: true,
				userId: userId,
				message: CONSTANTS.SUCCESS.DOCTOR_PATIENT
			});
		} else {
			if (!npIDDetails) {
				return Promise.resolve({
					success: false,
					errorCode: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.CODE,
					message: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.MESSAGE
				});
			}
			if (!hospitalDetail) {
				return Promise.resolve({
					success: false,
					errorCode: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.CODE,
					message: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.MESSAGE
				});
			}
		}
	} else if (!doctorPatientDetails) {
		const response = await saveDoctorPatientDetails(
			userId,
			hospitalDetail,
			firstName,
			lastName,
			patientAddress,
			patientMRN,
			pacemaker,
			deviceType,
			hospitalID,
			doctorID,
			email,
			dob,
			threshold,
			gender,
			language,
			icdCode,
			officePhone,
			homePhone,
			mobilePhone,
			shippingAddress,
			updateUserData,
			consent,
			contactVai
		);
		return Promise.resolve({
			success: true,
			userId: response.userId,
			message: CONSTANTS.SUCCESS.DOCTOR_PATIENT
		});
	} else {
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.ORDER_ALREADY_PROCESSED.CODE,
			message: CONSTANTS.ERRORS.ORDER_ALREADY_PROCESSED.MESSAGE
		});
	}
};

const applyValidation = async ({
	doctorID,
	hospitalID,
	updateUserData,
	body: {
		firstName,
		lastName,
		pacemaker,
		patientAddress,
		patientMRN,
		patientDOB,
		patientEmail,
		device,
		patientThreshold,
		gender,
		language,
		icdCode,
		officePhone,
		homePhone,
		mobilePhone,
		shippingAddress,
		consent = false,
		contactVai
	}
}) => {
	try {
		if (!firstName) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.FIRST_NAME_KEY_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.FIRST_NAME_KEY_REQ.CODE
			});
		}
		const isValidFirstName = new RegExp(/^[a-z0-9 ,.'-]+$/i).test(firstName);
		if (!isValidFirstName) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.CODE
			});
		}
		if (!lastName) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.LAST_NAME_KEY_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.LAST_NAME_KEY_REQ.CODE
			});
		}
		const isValidLastName = new RegExp(/^[a-z0-9 ,.'-]+$/i).test(lastName);
		if (!isValidLastName) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.CODE
			});
		}
		if (!patientAddress) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.ADDRESS_KEY_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.ADDRESS_KEY_REQ.CODE
			});
		}
		if (!patientMRN) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.MRN_KEY_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.MRN_KEY_REQ.CODE
			});
		}
		const isValidPatientMRN = new RegExp(/^[a-zA-Z0-9 ]{3,50}$/).test(
			patientMRN
		);
		if (!isValidPatientMRN) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.CODE
			});
		}
		if (patientEmail) {
			const isValidEmail = new RegExp(
				/^[a-zA-Z0-9](?!.*?[^\na-zA-Z0-9]{2})[^\s@]+@[^\s@]+\.[^\s@]+[a-zA-Z0-9]$/
			).test(patientEmail);
			if (!isValidEmail) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.MESSAGE,
					errorCode: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.CODE
				});
			}
		}
		if (!device) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.DEVICE_KEY_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.DEVICE_KEY_REQ.CODE
			});
		}
		if (!gender) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.GENDER_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.GENDER_REQ.CODE
			});
		}
		const isValidGender = CONSTANTS.GENDER.includes(gender.toLowerCase());
		if (!isValidGender) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.CODE
			});
		}
		if (!doctorID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.DOCTOR_ID_KEY_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.DOCTOR_ID_KEY_REQ.CODE
			});
		}
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.HOSPITAL_ID_KEY_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.HOSPITAL_ID_KEY_REQ.CODE
			});
		}
		if (!mobilePhone) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.MOBILE_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.MOBILE_REQ.CODE
			});
		}
		const isValidMobile = new RegExp(/^(\+(91|1)\d{10})$/).test(mobilePhone);
		if (!isValidMobile) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.CODE
			});
		}
		if (!shippingAddress) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.SHIPPING_ADDRESS_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.SHIPPING_ADDRESS_REQ.CODE
			});
		}

		if (homePhone) {
			const isValidHomePhone = new RegExp(/^(\+(91|1)\d{10})$/).test(homePhone);
			if (!isValidHomePhone) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.MESSAGE,
					errorCode: CONSTANTS.VALIDATION_MESSAGES.INPUT_VALIDATION_ERROR.CODE
				});
			}
		}

		if (!pacemaker) {
			pacemaker = 0;
		} else {
			const validatePacemaker = new RegExp(/^[0|1]{1}$/).test(pacemaker);
			if (!validatePacemaker) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.INVALID_PACEMAKER.MESSAGE,
					errorCode: CONSTANTS.ERRORS.INVALID_PACEMAKER.CODE
				});
			}
		}

		if (patientThreshold && patientThreshold.weight24) {
			patientThreshold.weight24hr = patientThreshold.weight24;
			delete patientThreshold.weight24;
		}

		if (patientThreshold && patientThreshold.weight72) {
			patientThreshold.weight72hr = patientThreshold.weight72;
			delete patientThreshold.weight72;
		}
		const hospitalDetail = await getDbIdenitifierFromHospital(hospitalID);
		const docPatientTable =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			hospitalDetail[0].dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PATIENT;

		const userID = await generateUserId(patientMRN.toLowerCase());
		const patientDetails = await getPatientDetails(userID, docPatientTable);

		if (patientDetails.length > 0) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.PATIENT_ALREADY_EXISTS.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.PATIENT_ALREADY_EXISTS.CODE
			});
		}

		const obj = {
			updateOrderProcessingDetail: await updateOrderProcessingDetail(
				userID,
				hospitalDetail,
				firstName,
				lastName,
				patientAddress,
				patientMRN,
				pacemaker,
				device,
				hospitalID,
				doctorID,
				patientEmail,
				patientDOB,
				patientThreshold,
				gender,
				language,
				icdCode,
				officePhone,
				homePhone,
				mobilePhone,
				shippingAddress,
				updateUserData
			),
			updateDoctorPatientDetails: await updateDoctorPatientDetails(
				userID,
				hospitalDetail,
				firstName,
				lastName,
				patientAddress,
				patientMRN,
				pacemaker,
				device,
				hospitalID,
				doctorID,
				patientEmail,
				patientDOB,
				patientThreshold,
				gender,
				language,
				icdCode,
				officePhone,
				homePhone,
				mobilePhone,
				shippingAddress,
				updateUserData,
				consent,
				contactVai
			)
		};
		if (obj) {
			return Promise.resolve({
				success: true,
				userId:
					(obj &&
						obj.updateDoctorPatientDetails &&
						obj.updateDoctorPatientDetails.userId) ||
					null,
				message: CONSTANTS.SUCCESS.PATIENT_REGISTERED
			});
		} else {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.PATIENT_REGISTRATION_FAILED
			});
		}
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: error.message,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	event.doctorId = event.doctorID;
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
		delete event.doctorId;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
