const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');
const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});

const checkConsultation = async (
	doctorId,
	hospitalTable,
	userId,
	connectionID,
	requestTimeEpochWithOffset
) => {
	console.log('connectionID', connectionID);
	const patientConsultation = await DB.getLastConsultation(doctorId, hospitalTable, userId);
	if (patientConsultation && !patientConsultation.endTime && patientConsultation.connectionID === connectionID) {
		if (patientConsultation.startTime === patientConsultation.tempEndTime) {
			patientConsultation.isdeleted = true;
		}
		patientConsultation.endTime = requestTimeEpochWithOffset;

		delete patientConsultation.tempEndTime;
		delete patientConsultation.connectionID;
		patientConsultation.consultationTime = (patientConsultation.endTime - patientConsultation.startTime) / 1000;
		patientConsultation.consultationUnit = CONSTANTS.SECONDS;
		await DB.updatePatientConsultation(patientConsultation, hospitalTable);
		console.log('made last active consultation inactive');
		return {
			endTime: patientConsultation.endTime,
			startTime: patientConsultation.startTime
		};
	}
	console.log('no patient consultations');
	console.log('no consultations');
	return null;
};

const applyValidation = async (event) => {
	console.log('created api management gateway');
	const connectionID = event.connectionId;
	const apigwManagementApi = new AWS.ApiGatewayManagementApi({
		apiVersion: '2018-11-29',
		endpoint: `${CONSTANTS.WEBSOCKET_DOMAIN}/${CONSTANTS.WEBSOCKET_STAGE}`
	});
	try {
		console.log('connectionID', connectionID);
		const connectionDetails = await DB.getConnectionDetails(connectionID);
		if (!connectionDetails) {
			console.log('no connection details');
			throw new Error(CONSTANTS.ERRORS.CONNECTION_ID_INVALID.MESSAGE);
		}
		let end;
		let start;
		const {
			doctorID,
			userID,
			dbIdentifier,
			offSetInMilliSecs
		} = connectionDetails;
		const consultationTable = `${CONSTANTS.STAGE}${CONSTANTS.RPM}${dbIdentifier}_${CONSTANTS.DOCTOR_CONSULTATION}`;
		if (!consultationTable) {
			console.log('no consultation table found');
			throw new Error(CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE);
		}
		const requestTimeEpochWithOffset = (Number(event.requestTimeEpoch) + offSetInMilliSecs);

		if (userID) {
			const consultation = await checkConsultation(doctorID, consultationTable, userID, connectionID, requestTimeEpochWithOffset);
			if (consultation) {
				end = consultation.endTime;
				start = consultation.startTime;
			}
		}

		const response = {
			success: true
		};
		if (end && start) {
			console.log('start and end present');
			response.start = start;
			response.end = end;
		}

		const postParams = {
			ConnectionId: connectionDetails.connectionID,
			Data: JSON.stringify(response)
		};
		await apigwManagementApi.postToConnection(postParams).promise();
		console.log('response sent to client');
		if (event.body.isDisconnected === true) {
			const deleteConnectionParams = {
				ConnectionId: connectionDetails.connectionID
			};
			console.log('reached end of stop consultation');
			await apigwManagementApi.deleteConnection(deleteConnectionParams).promise();
			console.log('disconnected');
		}
	} catch (error) {
		console.log('stop consultation websocket error', error);
		let postErrorMessage;
		let postErrorParams;
		if (error.message === CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE) {
			postErrorMessage = {
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			};
			postErrorParams = {
				ConnectionId: connectionID,
				Data: JSON.stringify(postErrorMessage)
			};
		} else if (error.message === CONSTANTS.ERRORS.CONNECTION_ID_INVALID.MESSAGE) {
			postErrorMessage = {
				success: false,
				message: CONSTANTS.ERRORS.CONNECTION_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.CONNECTION_ID_INVALID.CODE
			};
			postErrorParams = {
				ConnectionId: connectionID,
				Data: JSON.stringify(postErrorMessage)
			};
		} else {
			postErrorMessage = {
				success: false,
				message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
				errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
			};
			postErrorParams = {
				ConnectionId: connectionID,
				Data: JSON.stringify(postErrorMessage)
			};
		}
		await apigwManagementApi.postToConnection(postErrorParams).promise();
	}
};

exports.handler = async (event, context, callback) => {
	const response = await applyValidation(event);
	callback(null, response);
};
