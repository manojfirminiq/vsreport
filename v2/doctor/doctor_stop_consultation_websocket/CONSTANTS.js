exports.TIMER_CONNECTION_TABLE = process.env.TIMER_CONNECTION_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = '_rpm_';
exports.TIME_OUT = 10000;
exports.WEBSOCKET_DOMAIN = process.env.WEBSOCKET_DOMAIN;
exports.WEBSOCKET_STAGE = process.env.WEBSOCKET_STAGE;
exports.SECONDS = 'seconds';
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;

exports.ERRORS = {
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	CONNECTION_ID_INVALID: {
		CODE: 'CONNECTION_ID_INVALID',
		MESSAGE: 'Connection Id is invalid'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	}
};
