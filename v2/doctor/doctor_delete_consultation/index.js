const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./db');
const trackPatient = require('track_patients_record');

const applyValidation = async params => {
	try {
		if (!params.userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
			});
		}
		if (!params.timestamp) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.TIMESTAMP_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.TIMESTAMP_NOT_FOUND.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(params.hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		await DB.deletePatientConsultation(
			params,
			hospitalTable.DOCTOR_CONSULTATION
		);

		return Promise.resolve({
			success: true
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
