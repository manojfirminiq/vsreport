const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_CONSULTATION = CONSTANTS.DOCTOR_CONSULTATION;
const DOCTOR_TABLE = CONSTANTS.DOCTOR_TABLE;

const verifyHospital = async hospitalID => {
	const paramsTable = {
		TableName: DOCTOR_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalID
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		return {
			DOCTOR_CONSULTATION:
				process.env.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_CONSULTATION
		};
	}
};

const deletePatientConsultation = async (params, DOCTOR_CONSULTATION) => {
	const updateUserData = params.updateUserData;
	const paramsTable = {
		TableName: DOCTOR_CONSULTATION,
		Key: {
			doctorID: params.doctorId,
			userID_startTime: `${params.userId}_${params.timestamp}`
		},
		UpdateExpression:
			'set isdeleted = :isdeleted, modifiedDate = :modifiedDate, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':isdeleted': true,
			':modifiedDate': Date.now(),
			':updateUserData': [updateUserData],
      		':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		}
	};
	return await dbClient.update(paramsTable).promise();
};

module.exports.deletePatientConsultation = deletePatientConsultation;
module.exports.verifyHospital = verifyHospital;
