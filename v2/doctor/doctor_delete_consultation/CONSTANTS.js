exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	TIMESTAMP_NOT_FOUND: {
		CODE: 'TIMESTAMP_NOT_FOUND',
		MESSAGE: 'Timestamp not found'
	},
	USER_ID_REQUIRED: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'User Id is required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;
exports.DOCTOR_TABLE = process.env.DOCTOR_TABLE;
exports.RPM = 'rpm';
