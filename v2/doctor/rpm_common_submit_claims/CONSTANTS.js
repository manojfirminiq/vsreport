exports.RPM_CLAIM_TABLE = process.env.RPM_CLAIM_TABLE;
exports.RPM_USER_TABLE = process.env.RPM_USER_TABLE;
exports.RPM_ORDER_TABLE = process.env.RPM_ORDER_TABLE;
exports.DATA_MODEL_FIN = 'FINANCIAL';
exports.RPM = 'rpm';
exports.HOSPITAL_MASTER = process.env.HOSPITAL_MASTER;
