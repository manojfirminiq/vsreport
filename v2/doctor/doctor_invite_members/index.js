'use strict';
const crypto = require('crypto');
const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');
const AWS = require('aws-sdk');
const trackPatient = require('track_patients_record');
const AmazonCognitoIdentity = new AWS.CognitoIdentityServiceProvider();

const getEndTime = startTime => {
	const expireHour = parseInt(CONSTANTS.EXPIRE_HOUR);
	const endTime = startTime + 1 * expireHour * 60 * 60 * 1000;
	return endTime;
};

function uuid () {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		const r = (Math.random() * 16) | 0;
		const v = c === 'x' ? r : (r & 0x3) | 0x8;
		return v.toString(16);
	});
}

const encrypt = text => {
	const algorithm = CONSTANTS.ENCRYPTION.ALGORITHM;
	const key = crypto
		.createHash('sha256')
		.update(String(CONSTANTS.ENCRYPTION.PASSWORD))
		.digest('base64')
		.substr(0, 32);

	const iv = crypto
		.createHash('sha256')
		.update(String(CONSTANTS.ENCRYPTION.PASSWORD))
		.digest('base64')
		.substr(0, 16);
	const cipher = crypto.createCipheriv(algorithm, Buffer.from(key), iv);
	const encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');

	return encrypted;
};

const checkIfUserAlreadyExist = async email => {
	const payload = {
		AttributesToGet: [CONSTANTS.EMAILS],
		Filter: `email ^= "${email}"`,
		UserPoolId: CONSTANTS.USER_POOL_ID
	};
	return new Promise((resolve, reject) => {
		AmazonCognitoIdentity.listUsers(payload, (err, data) => {
			if (err) {
				reject(err);
			} else {
				if (data && data.Users && data.Users.length > 0) {
					resolve(true);
				} else {
					resolve(false);
				}
			}
		});
	});
};

const sendMemberEmail = async (emailAddress, inviteCode, doctorData) => {
	let params = {};
	params = {
		...CONSTANTS.EMAIL_TEMPLATE
	};
	const jsonToken = {
		inviteCode: inviteCode,
		hospitalID: doctorData[0].hospitalID,
		emailAddress: emailAddress
	};

	const encryptedToken = encrypt(JSON.stringify(jsonToken));

	params.Destination.ToAddresses = [emailAddress];
	params.Message.Body.Html.Data = `${CONSTANTS.EMAIL.TEXT_1}${CONSTANTS.EMAIL.TEXT_2}${CONSTANTS.EMAIL.LINK_TEXT_1}${CONSTANTS.WEB_DNS_URL}/${CONSTANTS.WEB_ROUTER_PATH}?invite-code=${encryptedToken}${CONSTANTS.EMAIL.LINK_TEXT_2}`;
	const mailResult = await DB.toMember(params);
	if (mailResult.MessageId) {
		return Promise.resolve({
			success: true,
			data: mailResult.data
		});
	} else {
		return Promise.resolve({
			success: false
		});
	}
};

const sendInvitation = async (
	doctorId,
	emailList,
	hospitalTable,
	doctorData,
	updateUserData
) => {
	const promisesResponse = emailList.map(async email => {
		const currentTime = new Date().getTime();
		const expiryTime = getEndTime(currentTime);
		const Items = {};
		const attributes = {};
		Items.inviteCode = uuid();
		Items.doctorID = doctorId;
		Items.createdDate = currentTime;
		Items.modifiedDate = currentTime;
		attributes.emailAddress = email;
		attributes.expiryTime = expiryTime;
		attributes.npID = doctorData[0].npID;
		Items.attributes = attributes;
		Items.updates = [];
		Items.updates.push(updateUserData);

		const checkUser = await checkIfUserAlreadyExist(email);
		let emailResponse = {
			success: false
		};

		if (!checkUser) {
			try {
				await DB.saveMemberInvitation(Items, hospitalTable.INVITATION_TABLE);
				emailResponse = await sendMemberEmail(
					email,
					Items.inviteCode,
					doctorData
				);
			} catch (err) {
				console.log(err);
			}
		}
		const output = {};
		output[email] = false;

		if (emailResponse.success) {
			Items.attributes.status = CONSTANTS.EMAIL_SENT;
			await DB.updateMemberInvitation(Items, hospitalTable.INVITATION_TABLE);
			output[email] = true;
		}

		return output;
	});

	const response = await Promise.all(promisesResponse);
	return Promise.resolve({
		success: true,
		data: response
	});
};

const applyValidation = async ({
	doctorId,
	emailList,
	hospitalID,
	updateUserData
}) => {
	try {
		if (!emailList) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.EMAIL_LIST_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.EMAIL_LIST_REQ.CODE
			});
		} else {
			if (Array.isArray(emailList)) {
				if (emailList.length > CONSTANTS.EMAIL_LIST_LENGTH) {
					return Promise.resolve({
						success: false,
						message: CONSTANTS.ERRORS.EMAIL_LIST_LENGTH.MESSAGE,
						errorCode: CONSTANTS.ERRORS.EMAIL_LIST_LENGTH.CODE
					});
				}
			} else {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.EMAIL_LIST_INVALID.MESSAGE,
					errorCode: CONSTANTS.ERRORS.EMAIL_LIST_INVALID.CODE
				});
			}
		}

		const hospitalTable = await DB.getHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const { Items } = await DB.getDoctorData(
			doctorId,
			hospitalTable.DOCTOR_TABLE
		);

		emailList = [...new Set(emailList)];
		return await sendInvitation(
			doctorId,
			emailList,
			hospitalTable,
			Items,
			updateUserData
		);
	} catch (error) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	event = {
		...event,
		...event.body
	};
	delete event.body;
	const response = await applyValidation(event);
	callback(null, response);
};
