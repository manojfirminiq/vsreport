exports.WEB_DNS_URL = process.env.WEB_URL;
exports.WEB_ROUTER_PATH = process.env.WEB_PATH;
exports.RPM = '_rpm_';
exports.EXPIRE_HOUR = 24;
exports.STAGE = process.env.STAGE;
exports.EMAIL = {
	TEXT_1: 'Hello,',
	TEXT_2:
		'<br> <br> You are invited to join OMRON’s Remote Patient Monitoring program, VitalSight™. As a practitioner, you will be able to view patient data within the portal.  Please follow the link below to complete your sign up for VitalSight™.  If you have any questions or concerns please contact the VitalSight™ team at 1-877-510-5902.',
	LINK_TEXT_1: '<br> <br> <a class="ulink" href="',
	LINK_TEXT_2:
		'" target="_blank">OMRON VitalSight Staff Registration</a>.'
};

const SOURCE_EMAIL = process.env.SOURCE_EMAIL;

exports.EMAIL_TEMPLATE = {
	Destination: {
		ToAddresses: []
	},
	Message: {
		Body: {
			Html: {
				Data: ''
			},
			Text: {
				Data: ''
			}
		},
		Subject: {
			Data: 'Welcome to OMRON-VitalSight™'
		}
	},
	Source: `${SOURCE_EMAIL}`
};

exports.EMAIL_LIST_LENGTH = 5;

exports.ERRORS = {
	EMAIL_LIST_REQ: {
		CODE: 'EMAIL_LIST_REQ',
		MESSAGE: 'Email list required'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	EMAIL_LIST_LENGTH: {
		CODE: 'EMAIL_LIST_LENGTH',
		MESSAGE: 'Only 5 emails allowed in a list'
	},
	EMAIL_INVITATION_FAIL: {
		CODE: 'EMAIL_INVITATION_FAIL',
		MESSAGE: 'Email sending failed'
	}
};

exports.HOSPITAL_TABLE_NAME = process.env.HOSPITAL_TABLE_NAME;
exports.INVITATION_TABLE_NAME = process.env.INVITATION_TABLE_NAME;
exports.DOCTOR_TABLE_NAME = process.env.DOCTOR_TABLE_NAME;
exports.ENCRYPTION = {
	ALGORITHM: 'aes256',
	PASSWORD: process.env.HASH_KEY
};
exports.USER_POOL_ID = process.env.USER_POOL_ID;
exports.EMAILS = 'email';
exports.EMAIL_SENT = 'EMAIL_SENT';
