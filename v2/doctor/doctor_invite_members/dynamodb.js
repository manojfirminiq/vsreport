const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');
const ses = new AWS.SES({
	region: process.env.REGION
});

const HOSPITAL_TABLE_NAME = CONSTANTS.HOSPITAL_TABLE_NAME;
const INVITATION_TABLE_NAME = CONSTANTS.INVITATION_TABLE_NAME;
const STAGE = CONSTANTS.STAGE;
const RPM = CONSTANTS.RPM;
const DOCTOR_TABLE_NAME = CONSTANTS.DOCTOR_TABLE_NAME;

const getDoctorData = async (doctorID, DOCTOR_TABLE) => {
	const params = {
		TableName: DOCTOR_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': String(doctorID)
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	return dbClient.query(params).promise();
};

const getHospital = async hospitalId => {
	const params = {
		TableName: HOSPITAL_TABLE_NAME,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': hospitalId
		},
		ExpressionAttributeNames: {
			'#uid': 'hospitalID'
		}
	};

	const { Count, Items } = await dbClient.query(params).promise();
	if (Count === 0) {
		return false;
	} else {
		return {
			Items: Items[0],
			INVITATION_TABLE:
				STAGE + RPM + Items[0].dbIdentifier + '_' + INVITATION_TABLE_NAME,
			DOCTOR_TABLE:
				STAGE + RPM + Items[0].dbIdentifier + '_' + DOCTOR_TABLE_NAME
		};
	}
};

const saveMemberInvitation = async (Items, TABLE_NAME) => {
	const params = {
		TableName: TABLE_NAME,
		Item: Items
	};
	return dbClient.put(params).promise();
};

const updateMemberInvitation = async (item, TABLE_NAME) => {
	const params = {
		TableName: TABLE_NAME,
		Item: item
	};
	return dbClient.put(params).promise();
};

exports.getHospital = getHospital;
exports.saveMemberInvitation = saveMemberInvitation;
exports.getDoctorData = getDoctorData;
exports.updateMemberInvitation = updateMemberInvitation;
exports.toMember = async params => {
	try {
		return ses.sendEmail(params).promise();
	} catch (err) {
		return Promise.resolve({
			success: false,
			data: err
		});
	}
};
