const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');

const groupBy = (Items, userID) => {
	let i = 0;
	let val;
	const result = {};
	for (; i < Items.length; i++) {
		val = Items[i][userID];
		if (result[val]) {
			result[val].push(Items[i]);
		} else {
			result[val] = [];
			result[val].push(Items[i]);
		}
	}
	return result;
};

const getPatientObject = patientList => {
	const patientObject = {};
	let patientListObject;

	if (patientList.Count > 0) {
		const patientListArray = patientList.Items;
		for (let i = 0; i < patientListArray.length; i++) {
			const {
				attributes: { name },
				userID
			} = patientListArray[i];
			patientListObject = { name, userID };
			patientObject[patientListObject.userID] = patientListObject;
		}
	}
	return patientObject;
};

const bpAlertsData = async (doctorId, hospitalTableDetails, patientObject) => {
	let bpAlerts = await DB.getBpAlerts(
		doctorId,
		hospitalTableDetails.ALERT_BP_TABLE
	);
	let alertObject;
	const finalBpAlertArray = [];
	let userAlerts = {};
	if (bpAlerts.Count > 0) {
		bpAlerts = bpAlerts.Items;
		for (let i = 0; i < bpAlerts.length; i++) {
			const userID = bpAlerts[i].actualUserID
				? bpAlerts[i].actualUserID
				: bpAlerts[i].userID;
			let patientName = '';
			if (patientObject[userID]) {
				patientName = patientObject[userID].name;
			}
			alertObject = {
				resolveStatus: bpAlerts[i].alerts.bp.resolveStatus,
				userID: userID,
				patientName: patientName,
				measurementDate: bpAlerts[i].measurementDate
			};
			if (alertObject.patientName !== '') {
				finalBpAlertArray.push(alertObject);
			}
		}

		userAlerts = groupBy(finalBpAlertArray, 'userID');
	}
	return Promise.resolve({
		userAlerts: userAlerts
	});
};

const nonBpAlertsData = async (
	doctorId,
	hospitalTableDetails,
	patientObject
) => {
	let nonBpAlerts = await DB.getNonBpAlerts(
		doctorId,
		hospitalTableDetails.ALERT_NON_BP_TABLE
	);
	let alertObject;
	const finalNonBpAlertArray = [];
	let userAlerts = {};
	if (nonBpAlerts.Count > 0) {
		nonBpAlerts = nonBpAlerts.Items;
		for (let i = 0; i < nonBpAlerts.length; i++) {
			const userID = nonBpAlerts[i].actualUserID
				? nonBpAlerts[i].actualUserID
				: nonBpAlerts[i].userId_type_deviceLocalName.split('_')[0];
			let patientName = '';
			if (patientObject[userID]) {
				patientName = patientObject[userID].name;
			}
			alertObject = {
				resolveStatus: nonBpAlerts[i].alerts.weight.resolveStatus,
				userID: userID,
				patientName: patientName,
				measurementDate: nonBpAlerts[i].measurementDate
			};
			if (alertObject.patientName !== '') {
				finalNonBpAlertArray.push(alertObject);
			}
		}
		userAlerts = groupBy(finalNonBpAlertArray, 'userID');
	}
	return Promise.resolve({
		userAlerts: userAlerts
	});
};

const applyValidation = async ({ doctorId, hospitalID }) => {
	try {
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		const doctorDetails = await DB.getDoctorData(
			doctorId,
			hospitalTableDetails.DOCTOR_PROFILE
		);
		let inActivePeriod;
		if (doctorDetails && doctorDetails.inActivePeriod) {
			inActivePeriod = doctorDetails.inActivePeriod;
		} else {
			inActivePeriod = CONSTANTS.INACTIVE_PERIOD;
		}

		const now = new Date();
		const inActiveDayThreshold = Date.parse(
			new Date(now.setDate(now.getDate() - inActivePeriod))
		);

		let patientsList = await DB.getPatients(
			doctorId,
			hospitalTableDetails.DOCTOR_PATIENT
		);
		const patientObject = await getPatientObject(patientsList);
		const promiseArr = [
			bpAlertsData(doctorId, hospitalTableDetails, patientObject),
			nonBpAlertsData(doctorId, hospitalTableDetails, patientObject)
		];
		const promiseResults = await Promise.all(promiseArr);
		const bpAlerts = promiseResults[0];
		const nonBpAlerts = promiseResults[1];
		if (patientsList.Count > 0) {
			patientsList = patientsList.Items;
			const data = [];
			for (let i = 0; i < patientsList.length; i++) {
				const {
					patientCode,
					userID,
					attributes: {
						name,
						gender,
						profilePicture,
						dob,
						lastReportedDate,
						lastDiastolic,
						lastSystolic,
						weight,
						threshold
					}
				} = patientsList[i];

				if (lastReportedDate && lastReportedDate < inActiveDayThreshold) {
					continue;
				} else {
					if (
						!lastReportedDate ||
						!threshold ||
						(!threshold.diaLow &&
							!threshold.diaHigh &&
							!threshold.sysLow &&
							!threshold.sysHigh)
					) {
						continue;
					}
				}

				const patientBpAlert = bpAlerts.userAlerts[patientsList[i].userID]
					? bpAlerts.userAlerts[patientsList[i].userID]
					: [];
				const patientNonBpAlert = nonBpAlerts.userAlerts[patientsList[i].userID]
					? nonBpAlerts.userAlerts[patientsList[i].userID]
					: [];
				let individualAlerts = [].concat(patientBpAlert, patientNonBpAlert);
				individualAlerts = individualAlerts.sort((a, b) => {
					return b.measurementDate - a.measurementDate;
				});

				if (
					individualAlerts.length > 0 &&
					individualAlerts[0].measurementDate > inActiveDayThreshold
				) {
					continue;
				}

				data.push({
					name: name || null,
					userID: userID,
					gender: gender || null,
					patientCode: patientCode || null,
					dob: dob || null,
					profilePicture: profilePicture
						? DB.getUserProfileUrl(profilePicture)
						: null,
					lastReportedDate: lastReportedDate ? Number(lastReportedDate) : null,
					lastDiastolic: lastDiastolic ? Number(lastDiastolic) : null,
					lastSystolic: lastSystolic ? Number(lastSystolic) : null,
					weight: weight ? Number(weight) : null
				});
			}
			if (data.length > 0) {
				return {
					success: true,
					data: data
				};
			}
		}
		return {
			success: true,
			data: []
		};
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
