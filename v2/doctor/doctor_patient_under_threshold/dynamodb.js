const AWS = require('aws-sdk');
const CONSTANTS = require('./CONSTANTS');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const s3Client = new AWS.S3();
const MASTER_HOSPITAL_TABLE = CONSTANTS.MASTER_HOSPITAL_TABLE;
const DOCTOR_PATIENT_TABLE = CONSTANTS.DOCTOR_PATIENT_TABLE;
const DOCTOR_PROFILE_TABLE = CONSTANTS.DOCTOR_PROFILE_TABLE;
const ALERT_NON_BP_TABLE = CONSTANTS.ALERT_NON_BP_TABLE;
const ALERT_BP_TABLE = CONSTANTS.ALERT_BP_TABLE;

const getUserProfileUrl = profilePicture => {
	return s3Client.getSignedUrl('getObject', {
		Bucket: CONSTANTS.PROFILE_BUCKET,
		Key: profilePicture,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	});
};

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: MASTER_HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_PROFILE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PROFILE_TABLE,
			DOCTOR_PATIENT:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT_TABLE,
			ALERT_BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				ALERT_BP_TABLE,
			ALERT_NON_BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				ALERT_NON_BP_TABLE
		};
		return obj;
	}
};

const getDoctorData = async (doctorID, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorID
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		const Item = Items.map(
			({
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			}) => ({
				createdDate,
				...rest,
				...attributesRest
			})
		)[0];

		return Item;
	} else {
		return null;
	}
};

const getPatients = async (doctorId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId',
		FilterExpression: '#deleteFlag <> :deleteFlag',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':deleteFlag': true
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#deleteFlag': 'deleteFlag'
		}
	};

	return dbClient.query(params).promise();
};

const getBpAlerts = async (doctorId, ALERT_BP_TABLE) => {
	const params = {
		TableName: ALERT_BP_TABLE,
		IndexName: 'doctorID-measurementDate-index',
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorId
		},
		ExpressionAttributeNames: {
			'#uid': 'doctorID'
		}
	};

	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

const getNonBpAlerts = async (doctorId, ALERT_NON_BP_TABLE) => {
	const params = {
		TableName: ALERT_NON_BP_TABLE,
		IndexName: 'doctorID-measurementDate-index',
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorId
		},
		ExpressionAttributeNames: {
			'#uid': 'doctorID'
		}
	};

	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

module.exports.getUserProfileUrl = getUserProfileUrl;
module.exports.verifyHospital = verifyHospital;
module.exports.getDoctorData = getDoctorData;
module.exports.getPatients = getPatients;
module.exports.getBpAlerts = getBpAlerts;
module.exports.getNonBpAlerts = getNonBpAlerts;
