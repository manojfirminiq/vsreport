exports.RPM = '_rpm_';
exports.DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.MASTER_HOSPITAL_TABLE = process.env.MASTER_HOSPITAL_TABLE;
exports.ALERT_NON_BP_TABLE = process.env.ALERT_NON_BP_TABLE;
exports.ALERT_BP_TABLE = process.env.ALERT_BP_TABLE;
exports.INACTIVE_PERIOD = 7;

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};

exports.RESOLVE_STATUS = '1';
exports.PROFILE_BUCKET = process.env.PROFILE_BUCKET;
exports.S3_KEY_EXPIRES_IN = 60 * 60;
