const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS');
const STAFF_TABLE_NAME = CONSTANTS.STAFF_TABLE_NAME;
const HOSPITAL_TABLE_NAME = CONSTANTS.HOSPITAL_TABLE_NAME;
const INVITATION_TABLE_NAME = CONSTANTS.INVITATION_TABLE_NAME;
const RPM = CONSTANTS.RPM;
const STAGE = CONSTANTS.STAGE;

const saveItem = async (Item, table) => {
	const params = {
		TableName: table,
		Item: Item
	};
	return dbClient.put(params).promise();
};

const fetchHospitalID = async facilityCode => {
	const params = {
		TableName: HOSPITAL_TABLE_NAME,
		IndexName: CONSTANTS.FACILITYCODEINDEX,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': facilityCode
		},
		ExpressionAttributeNames: {
			'#uid': 'facilityCode'
		}
	};
	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	}
	return Items;
};

const getHospital = async hospitalID => {
	const params = {
		TableName: HOSPITAL_TABLE_NAME,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': hospitalID
		},
		ExpressionAttributeNames: {
			'#uid': 'hospitalID'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			STAFF_TABLE: STAGE + RPM + Items[0].dbIdentifier + '_' + STAFF_TABLE_NAME,
			INVITATION_TABLE:
				STAGE + RPM + Items[0].dbIdentifier + '_' + INVITATION_TABLE_NAME
		};
		return obj;
	}
};

const postUserData = async (params, hospitalTable) => {
	const data = {
		staffID: params.userId,
		doctorID: params.doctorID,
		app: CONSTANTS.APP,
		createdDate: new Date().getTime(),
		modifiedDate: new Date().getTime(),
		attributes: {
			name: params.name ? params.name : null,
			emailAddress: params.emailAddress,
			hospitalID: params.hospitalID,
			npID: params.npID,
			phoneNumber: params.phoneNumber,
			qualification: params.qualification
		}
	};

	await saveItem(data, hospitalTable.STAFF_TABLE);
	return Promise.resolve({
		success: true,
		message: CONSTANTS.MESSAGES.SUCCESS
	});
};

const checkInviteCode = async (inviteCode, hospitalTable) => {
	const params = {
		TableName: hospitalTable.INVITATION_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': inviteCode
		},
		ExpressionAttributeNames: {
			'#uid': 'inviteCode'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	}
	return Items[0];
};

const updateInviteCode = async (item, hospitalTable) => {
	const params = {
		TableName: hospitalTable.INVITATION_TABLE,
		Item: item
	};
	return dbClient.put(params).promise();
};

exports.postUserData = postUserData;
exports.checkInviteCode = checkInviteCode;
exports.getHospital = getHospital;
exports.fetchHospitalID = fetchHospitalID;
exports.updateInviteCode = updateInviteCode;
