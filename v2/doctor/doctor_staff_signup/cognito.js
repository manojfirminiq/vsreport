const AWS = require('aws-sdk');
const AmazonCognitoIdentity = new AWS.CognitoIdentityServiceProvider();
const CONSTANTS = require('./CONSTANTS');
const CLIENT_POOL_ID = CONSTANTS.CLIENT_ID;

const addUserToPool = async ({
	emailAddress,
	password,
	userId,
	doctorID,
	app,
	hospitalID,
	phoneNumber
}) => {
	const attributeList = [];
	attributeList.push({
		Name: 'custom:appName',
		Value: app
	});
	attributeList.push({
		Name: 'custom:userId',
		Value: doctorID
	});
	attributeList.push({
		Name: 'custom:hospitalID',
		Value: hospitalID
	});
	attributeList.push({
		Name: 'phone_number',
		Value: phoneNumber
	});
	attributeList.push({
		Name: 'custom:staffID',
		Value: userId
	});
	const params = {
		ClientId: CLIENT_POOL_ID,
		Username: emailAddress,
		Password: password,
		UserAttributes: attributeList
	};

	return new Promise((resolve, reject) => {
		AmazonCognitoIdentity.signUp(params, (err, result) => {
			if (err) {
				const response = {
					success: false,
					errorCode: '',
					message: ''
				};
				switch (err.code) {
				case CONSTANTS.COGNITO_ERROR.USER_ALREADY_EXISTS:
					response.errorCode = CONSTANTS.ERROR_CODES.USER_ALREADY_EXISTS;
					break;
				case CONSTANTS.COGNITO_ERROR.PASSWORD_CONSTRAINT:
					response.errorCode = CONSTANTS.ERROR_CODES.INVALID_PASSWORD;
					break;
				default:
					response.errorCode = CONSTANTS.ERROR_CODES.INTERNAL_SERVER_ERROR;
					break;
				}
				response.message = err.message;
				reject(response);
			} else {
				resolve(result);
			}
		});
	});
};

exports.addUserToPool = addUserToPool;
