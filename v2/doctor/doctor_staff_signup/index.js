const crypto = require('crypto');
const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');
const COGNITO = require('./cognito');

function generateUserId () {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		const r = (Math.random() * 16) | 0;
		const v = c === 'x' ? r : (r & 0x3) | 0x8;
		return v.toString(16);
	});
}

const decrypt = text => {
	const algorithm = CONSTANTS.ENCRYPTION.ALGORITHM;
	const key = crypto
		.createHash('sha256')
		.update(String(CONSTANTS.ENCRYPTION.PASSWORD))
		.digest('base64')
		.substr(0, 32);
	let response = '';

	try {
		const iv = crypto
			.createHash('sha256')
			.update(String(CONSTANTS.ENCRYPTION.PASSWORD))
			.digest('base64')
			.substr(0, 16);
		const decipher = crypto.createDecipheriv(algorithm, Buffer.from(key), iv);
		response = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8');
	} catch (error) {
		console.log(error);
	}

	return response;
};

const getToken = encryptedToken => {
	const decryptedCode = decrypt(encryptedToken);
	const inviteJson = {
		inviteCode: 'empty',
		hospitalID: 'empty'
	};

	try {
		const decryptedJson = JSON.parse(decryptedCode);
		if (decryptedJson.inviteCode) {
			inviteJson.inviteCode = decryptedJson.inviteCode;
		}
		if (decryptedJson.hospitalID) {
			inviteJson.hospitalID = decryptedJson.hospitalID;
		}
	} catch (error) {
		console.log(error);
	}

	return inviteJson;
};

const checkValidInvite = (item, emailAddress) => {
	let valid = true;
	const currentTime = new Date().getTime();
	if (
		!item.attributes ||
		!item.attributes.expiryTime ||
		currentTime > item.attributes.expiryTime
	) {
		valid = false;
	}
	if (
		!item.attributes ||
		!item.attributes.emailAddress ||
		item.attributes.emailAddress !== emailAddress
	) {
		valid = false;
	}

	if (
		!item.attributes ||
		!item.attributes.emailAddress ||
		item.attributes.status === CONSTANTS.REGISTERED
	) {
		valid = false;
	}

	return valid;
};

const signup = async params => {
	try {
		const inviteJson = getToken(params.inviteCode);

		const hospitalTable = await DB.getHospital(inviteJson.hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.HOSPITAL_ID_INVALID,
				errorCode: CONSTANTS.ERROR_CODES.HOSPITAL_ID_INVALID
			});
		}

		const Items = await DB.checkInviteCode(
			inviteJson.inviteCode,
			hospitalTable
		);
		// eslint-disable-next-line no-unused-vars
		const currentTime = new Date().getTime();
		if (Items && checkValidInvite(Items, params.emailAddress)) {
			params.app = CONSTANTS.APP;
			params.doctorID = Items.doctorID;
			params.npID = Items.attributes.npID;
			params.hospitalID = inviteJson.hospitalID;
			await COGNITO.addUserToPool(params);
			await DB.postUserData(params, hospitalTable);
			Items.attributes.status = CONSTANTS.REGISTERED;
			await DB.updateInviteCode(Items, hospitalTable);
			return Promise.resolve({
				success: true,
				message: CONSTANTS.MESSAGES.SUCCESS
			});
		} else {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.REGISTRATION_LINK_EXPIRE,
				errorCode: CONSTANTS.ERROR_CODES.REGISTRATION_LINK_EXPIRE
			});
		}
	} catch (error) {
		return Promise.reject(error);
	}
};

const applyValidation = async ({
	inviteCode,
	emailAddress,
	password,
	npID,
	phoneNumber,
	accountNumber,
	...rest
}) => {
	try {
		if (!inviteCode) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.INVITE_CODE_REQ,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		if (!emailAddress) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.EMAIL_REQ,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		if (!password) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.PASSWORD_REQ,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		if (!phoneNumber) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.PHONE_NUMBER_REQ,
				errorCode: CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER
			});
		}
		if (password.trim().length < CONSTANTS.PASSWORD_LENGTH) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.PASSWORD_REQ,
				errorCode: CONSTANTS.ERROR_CODES.INVALID_PASSWORD
			});
		}

		emailAddress = emailAddress.toLowerCase();
		const userId = generateUserId();
		const response = await signup({
			inviteCode,
			emailAddress,
			password,
			userId,
			phoneNumber,
			...rest
		});
		return response;
	} catch (error) {
		return Promise.resolve({
			success: false,
			message: error.message,
			errorCode: error.errorCode
		});
	}
};

exports.handler = async (event, context, callback) => {
	if (event.body) {
		event.body.sourceIP = event.sourceIP;
		event = event.body;
	}
	const response = await applyValidation(event);
	callback(null, response);
};
