exports.VALIDATION_MESSAGES = {
	EMAIL_REQ: 'Please provide a valid emailAddress',
	PASSWORD_REQ: 'Please provide a valid password',
	INVITE_CODE_REQ: 'Invitation code is invalid',
	USER_NOT_FOUND: 'User not found',
	USER_ALREADY_EXIST: 'User already exist',
	INVALID_INPUT: 'Please check the input parameters!',
	INVALID_PASSWORD: 'Password not long enough',
	CREATION_ERROR: 'Error while creating user',
	HOSPITAL_ID_INVALID: 'Please provide a valid hospital Id',
	PHONE_NUMBER_REQ: 'Please provide a Phone Number',
	ACCOUNT_NUMBER_REQ: 'Please provide a Account Number',
	ACCOUNT_NUMBER_INVALID: 'Account Number Invalid',
	REGISTRATION_LINK_EXPIRE: 'Registration link expired or invalid',
	DOCTORID_REQ: 'Please provide a doctorID'
};

exports.ERROR_CODES = {
	USER_ALREADY_EXISTS: 'USER_ALREADY_EXISTS',
	INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
	INVALID_PASSWORD: 'INVALID_PASSWORD',
	REQUIRED_PARAMETER: 'REQUIRED_PARAMETER',
	HOSPITAL_ID_INVALID: 'INVALID_HOSPITAL_ID',
	ACCOUNT_NUMBER_INVALID: 'ACCOUNT_NUMBER_INVALID',
	REGISTRATION_LINK_EXPIRE: 'REGISTRATION_LINK_EXPIRE'
};

exports.COGNITO_ERROR = {
	USER_ALREADY_EXISTS: 'UsernameExistsException',
	PASSWORD_CONSTRAINT: 'InvalidParameterException'
};

exports.PASSWORD_LENGTH = 8;

exports.MESSAGES = {
	SUCCESS:
		'We have sent you a verification mail to the email address you provided. Please check your inbox and click on the verification link in the mail to proceed with the account setup'
};

exports.RESET_PASSWORD_MESSAGES = {
	SUCCESS: 'Check your inbox for the code to reset your password'
};

exports.RPM = '_rpm_';
exports.APP = 'DWS';
exports.USER_TYPE = 'doctor';
exports.NPID = 'npID';
exports.FACILITYCODEINDEX = 'facilityCode-index';
exports.STAFF_TABLE_NAME = process.env.STAFF_TABLE_NAME;
exports.HOSPITAL_TABLE_NAME = process.env.HOSPITAL_TABLE_NAME;
exports.INVITATION_TABLE_NAME = process.env.INVITATION_TABLE_NAME;
exports.STAGE = process.env.STAGE;
exports.CLIENT_ID = process.env.CLIENT_ID;
exports.ENCRYPTION = {
	ALGORITHM: 'aes256',
	PASSWORD: process.env.HASH_KEY
};
exports.REGISTERED = 'REGISTERED';
