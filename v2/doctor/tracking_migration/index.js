const db = require('./dynamodb');

const fechHospitals = () => {
	return Promise.resolve(db.getHospitals());
};

const fetchDoctors = hospitalId => {
	return Promise.resolve(db.getDoctors(hospitalId));
};

const fetchPatients = (doctorId, hospitalId) => {
	return Promise.resolve(db.getPatients(doctorId, hospitalId));
};

const fetchEhrDetails = (patientId, hospitalId) => {
	return Promise.resolve(db.getEhrDetails(patientId, hospitalId));
};

const fetchOrders = (orderId, hospitalId) => {
	return Promise.resolve(db.getOrders(orderId, hospitalId));
};

const updatePatientRecord = (
	doctorId,
	userId,
	hospitalId,
	attributes,
	createdDate
) => {
	return Promise.resolve(
		db.updatePatient(doctorId, userId, attributes, createdDate, hospitalId)
	);
};

const shipmantMigration = async hospitalId => {
	try {
		const getAllHospitals = await fechHospitals();
		if (getAllHospitals.length && getAllHospitals.length) {
			const patientOrdersArray = getAllHospitals
				.filter(hospital => {
					if (hospitalId) {
						return (
							(!hospital.isEHR || hospital.isEHR !== 1) &&
							hospital.isActive &&
							hospitalId === hospital.hospitalID
						);
					} else {
						return (!hospital.isEHR || hospital.isEHR !== 1) && hospital.isActive;
					}
				})
				.map(hospital => {
					return Promise.resolve(fetchDoctors(hospital.dbIdentifier))
						.then(doctors => {
							if (doctors && doctors.Items && doctors.Items.length) {
								return doctors.Items.map(doctor => {
									return Promise.resolve(
										fetchPatients(doctor.userID, hospital.dbIdentifier)
									)
										.then(patientList => {
											if (
												patientList &&
												patientList.Items &&
												patientList.Items.length
											) {
												return patientList.Items.map(patient => {
													return Promise.resolve(
														fetchEhrDetails(
															patient.userID,
															hospital.dbIdentifier
														)
													).then(ehrDetails => {
														if (
															ehrDetails.Items.length &&
															ehrDetails.Items[0].orderID
														) {
															return Promise.resolve(
																fetchOrders(
																	ehrDetails.Items[0].orderID,
																	hospital.dbIdentifier
																)
															).then(orders => {
																if (
																	orders &&
																	orders.Items &&
																	orders.Items.length
																) {
																	const newPatient = Object.assign({}, patient);
																	newPatient.attributes.shipmentTracking =
																		orders.Items[0].shipmentTracking;
																	newPatient.attributes.bpSerialNo =
																		orders.Items[0].attributes.bpSerialNo;
																	newPatient.attributes.bpModelNo =
																		orders.Items[0].attributes.bpModelNo;
																	newPatient.attributes.scaleModelNo =
																		orders.Items[0].attributes.scaleModelNo;
																	newPatient.attributes.scaleSerialNo =
																		orders.Items[0].attributes.scaleSerialNo;
																	newPatient.createdDate =
																		ehrDetails.Items[0].createdDate;
																	return Promise.resolve(
																		updatePatientRecord(
																			doctor.userID,
																			patient.userID,
																			hospital.dbIdentifier,
																			newPatient.attributes,
																			newPatient.createdDate
																		)
																	)
																		.then(() => {
																			return {
																				patientId: patient.userID,
																				orders: orders.length,
																				updated: true,
																				data: newPatient
																			};
																		})
																		.catch(e => {
																			console.log('erorr in update>>>>', e);
																			return {
																				patientId: patient.userID,
																				orders: orders.length,
																				updated: false,
																				data: newPatient
																			};
																		});
																} else {
																	return {
																		patientId: patient.userID,
																		orders: orders,
																		updated: false,
																		order: 0,
																		data: {}
																	};
																}
															});
														} else {
															return null;
														}
													});
												});
											} else {
												return [];
											}
										})
										.then(patientData => {
											return Promise.all(patientData).then(patientList => {
												return {
													doctorId: doctor.userID,
													patientList: patientList.filter(
														patient =>
															patient != null &&
															patient !== '' &&
															patient !== {}
													)
												};
											});
										});
								});
							} else {
								return [];
							}
						})
						.then(doctorData => {
							return Promise.all(doctorData).then(doctorList => {
								return {
									hospital: hospital.dbIdentifier,
									doctorList
								};
							});
						});
				});

			Promise.all(patientOrdersArray).then(patientOrdersDetails => {
				console.log(
					'patientOrdersDetails',
					JSON.stringify(patientOrdersDetails)
				);
			});
		}
	} catch (e) {
		console.log('error in Migration>>>>', e);
	}
};

exports.handler = async (event, context, callback) => {
	if (event.hospitalId) {
		callback(null, shipmantMigration(event.hospitalId));
	} else {
		callback(null, shipmantMigration());
	}
};
