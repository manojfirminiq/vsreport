const CONSTANTS = require('./CONSTANTS.js');
const AWS = require('aws-sdk');
AWS.config.update(CONSTANTS.AWS_CONFIG);
const dbClient = new AWS.DynamoDB.DocumentClient();

const getHospitals = async () => {
	const params = {
		TableName: CONSTANTS.MASTER_HOSPITAL_TABLE
	};

	const { Items } = await dbClient.scan(params).promise();
	const hospitals = [];
	for (const item of Items) {
		hospitals.push(item);
	}

	return hospitals;
};

const getDoctors = async hospitalIdentifire => {
	const params = {
		TableName:
			process.env.STAGE +
			'_rpm_' +
			hospitalIdentifire +
			'_' +
			CONSTANTS.DOCTOR_TABLE_NAME
	};
	const doctors = await dbClient.scan(params).promise();
	return doctors;
};

const getPatients = async (doctorId, hospitalIdentifire) => {
	const params = {
		TableName:
			process.env.STAGE +
			'_rpm_' +
			hospitalIdentifire +
			'_' +
			CONSTANTS.DOCTOR_PATIENT_TABLE_NAME,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorId
		},
		ExpressionAttributeNames: {
			'#uid': 'doctorID'
		}
	};
	const allPatients = await dbClient.query(params).promise();
	return allPatients;
};

const getEhrDetails = async (patientId, hospitalIdentifire) => {
	const params = {
		TableName:
			process.env.STAGE +
			'_rpm_' +
			hospitalIdentifire +
			'_' +
			CONSTANTS.EHR_USER_TABLE_NAME,
		KeyConditionExpression: '#userID = :userID',
		ExpressionAttributeValues: {
			':userID': patientId
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const ehrDetails = await dbClient.query(params).promise();
	return ehrDetails;
};

const getOrders = async (orderId, hospitalIdentifire) => {
	const params = {
		TableName:
			process.env.STAGE +
			'_rpm_' +
			hospitalIdentifire +
			'_' +
			CONSTANTS.ORDER_DETAILS_TABLE_NAME,
		KeyConditionExpression: '#orderID = :orderID',
		ExpressionAttributeValues: {
			':orderID': orderId
		},
		ExpressionAttributeNames: {
			'#orderID': 'orderID'
		}
	};
	const orderDetails = await dbClient.query(params).promise();
	return orderDetails;
};

const updatePatient = async (
	doctorID,
	userID,
	attributesDoctorPatient,
	createdDate,
	hospitalIdentifire
) => {
	const params = {
		TableName:
			process.env.STAGE +
			'_rpm_' +
			hospitalIdentifire +
			'_' +
			CONSTANTS.DOCTOR_PATIENT_TABLE_NAME,
		Key: {
			doctorID: doctorID,
			userID: userID
		},
		UpdateExpression:
			'SET attributes = :attributes, createdDate = :createdDate',
		ExpressionAttributeValues: {
			':attributes': attributesDoctorPatient,
			':createdDate': createdDate
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

exports.getDoctors = getDoctors;
exports.getPatients = getPatients;
exports.getOrders = getOrders;
exports.getEhrDetails = getEhrDetails;
exports.getHospitals = getHospitals;
exports.updatePatient = updatePatient;
