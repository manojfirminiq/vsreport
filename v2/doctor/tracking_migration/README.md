# Migration Script 

node 12.x

Below keys will be migrated from order_details table to doctor_patient table.

  - bpSerialNo
  - bpModelNo
  - scaleModelNo
  - scaleSerialNo
  - shipmentTracking

### enrollmentDate Migrate in doctor_patient Table

  - createdDate in doctor_patient table will be updated from ehr_user table.

### Execution Details

- If script need to be run for specific hospital then we need to pass hospitalId in event for example
```sh
{ hospitalId: 'stats001' }
```
- If need to run script for all hospital then we need not to pass hospitalId in event. The event object should be empty
```sh
{ }
```