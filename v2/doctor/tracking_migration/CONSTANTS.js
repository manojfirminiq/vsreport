exports.MASTER_HOSPITAL_TABLE =
	process.env.MASTER_HOSPITAL_TABLE || 'dev_global_rpm_hospital_master';
exports.DOCTOR_TABLE_NAME = process.env.DOCTOR_TABLE_NAME || 'doctor_profile';
exports.DOCTOR_PATIENT_TABLE_NAME =
	process.env.DOCTOR_PATIENT || 'doctor_patient';
exports.EHR_USER_TABLE_NAME = process.env.EHR_USER || '	ehr_user';
exports.ORDER_DETAILS_TABLE_NAME = process.env.ORDER_DETAILS || 'order_details';
exports.AWS_CONFIG = {
	region: process.env.REGION || 'us-west-2'
};
