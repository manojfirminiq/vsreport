const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');

const applyValidation = async ({ hospitalID }) => {
	try {
		if (!hospitalID) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE });
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE });
		}

		await migratePatientName(hospitalTable);
		return Promise.resolve({ success: true, message: 'Successfully Migrated Patient Name' });
	} catch (e) {
		console.log('Error', e);
		return Promise.resolve({ success: false, errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE, message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE });
	}
};

const splitPatientName = (fullName) => {
	const name = fullName.split(' ');
	const firstName = name[0];
	const lastName = fullName.substring(name[0].length).trim();
	return { firstName, lastName };
};

const migratePatientName = async (hospitalTable) => {
	try {
	    const getPatientsDetails = await DB.getAllPatients(hospitalTable.DOCTOR_PATIENT);
	    for (let i = 0; i < getPatientsDetails.length; i++) {
	    	const fullName = getPatientsDetails[i] && getPatientsDetails[i].attributes && getPatientsDetails[i].attributes.name;
	    	if (fullName) {
	    	const name = splitPatientName(fullName);
	    	await DB.updatePatientName(hospitalTable.DOCTOR_PATIENT, name, getPatientsDetails[i]);
	    	}
	    }
	} catch (error) {
		console.log('Error in migrating patient name', error);
		throw error;
	}
};

exports.handler = async (event, context, callback) => {
	console.log('event', event);
	const response = await applyValidation(event);
	callback(null, response);
};
