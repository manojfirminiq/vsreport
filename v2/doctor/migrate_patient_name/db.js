const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');

const verifyHospital = async (hospitalID) => {
	const paramsTable = {
		TableName: CONSTANTS.HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalID
		}
	};

	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		return {

			DOCTOR_PATIENT: process.env.STAGE + '_' + CONSTANTS.RPM + '_' + Items[0].dbIdentifier + '_' + CONSTANTS.DOCTOR_PATIENT
		};
	}
};

const getAllPatients = async (doctorPatientTable) => {
	const params = {
		TableName: doctorPatientTable
	};

	const scanResults = [];
	let items;
	do {
		items = await dbClient.scan(params).promise();
		items.Items.forEach((item) => scanResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return scanResults;
};

const updatePatientName = async (doctorPatientTable, name, patientData) => {
	const params = {
		TableName: doctorPatientTable,
		Key: {
			doctorID: patientData.doctorID,
			userID: patientData.userID
		},
		UpdateExpression: 'SET #attributes.#firstName = :firstName, #attributes.#lastName = :lastName',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#firstName': 'firstName',
			'#lastName': 'lastName'
		},
		ExpressionAttributeValues: {
			':firstName': name.firstName,
			':lastName': name.lastName
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return await dbClient.update(params).promise();
};

module.exports.verifyHospital = verifyHospital;
module.exports.getAllPatients = getAllPatients;
module.exports.updatePatientName = updatePatientName;
