const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');

const getPatientList = async Items => {
	const patients = Items.map(
		({
			createdDate,
			modifiedDate,
			attributes: { ...attributesRest },
			...rest
		}) => ({ ...rest, ...attributesRest })
	);
	return patients[0];
};

const getNonBpAlerts = async (userId, ALERT_NON_BP_TABLE) => {
	let alertObject;
	const finalNonBpAlertArray = [];
	let nonBpAlerts = await DB.getNonBpAlerts(userId, ALERT_NON_BP_TABLE);
	if (nonBpAlerts.Count > 0) {
		nonBpAlerts = nonBpAlerts.Items;
		// nonBpAlerts = nonBpAlerts.Items.filter(({ alerts }) => (alerts.weight.resolveStatus) !== '1')
		for (let i = 0; i < nonBpAlerts.length; i++) {
			alertObject = {
				resolveStatus: nonBpAlerts[i].alerts.weight.resolveStatus,
				notes: nonBpAlerts[i].alerts.weight.Notes,
				measurementDate: nonBpAlerts[i].measurementDate,
				resolvedDate:
					nonBpAlerts[i].alerts.weight.resolveStatus === '1'
						? nonBpAlerts[i].alerts.weight.resolvedDate
						: null,
				thresholdType24hr: nonBpAlerts[i].alerts.weight.thresholdType24hr
					? nonBpAlerts[i].alerts.weight.thresholdType24hr
					: null,
				weightDiff24hr: nonBpAlerts[i].alerts.weight.weightDiff24hr
					? nonBpAlerts[i].alerts.weight.weightDiff24hr
					: null,
				thresholdType72hr: nonBpAlerts[i].alerts.weight.thresholdType72hr
					? nonBpAlerts[i].alerts.weight.thresholdType72hr
					: null,
				weightDiff72hr: nonBpAlerts[i].alerts.weight.weightDiff72hr
					? nonBpAlerts[i].alerts.weight.weightDiff72hr
					: null,
				thresholdWeightUnit: nonBpAlerts[i].alerts.thresholdWeightUnit
					? nonBpAlerts[i].alerts.thresholdWeightUnit
					: null,
				threshold: nonBpAlerts[i].alerts.threshold
					? nonBpAlerts[i].alerts.threshold
					: null,
				type: CONSTANTS.TYPE.WEIGHT
			};
			finalNonBpAlertArray.push(alertObject);
		}
	}
	return finalNonBpAlertArray;
};

const getBpAlerts = async (userId, ALERT_BP_TABLE) => {
	let alertObject;
	const finalBpAlertArray = [];
	let bpAlerts = await DB.getBpAlerts(userId, ALERT_BP_TABLE);
	if (bpAlerts.Count > 0) {
		bpAlerts = bpAlerts.Items;
		// bpAlerts = bpAlerts.Items.filter(({ alerts }) => (alerts.bp.resolveStatus) !== '1')
		for (let i = 0; i < bpAlerts.length; i++) {
			alertObject = {
				resolveStatus: bpAlerts[i].alerts.bp.resolveStatus,
				sysDiff: Number(bpAlerts[i].alerts.bp.sysDiff),
				diaDiff: Number(bpAlerts[i].alerts.bp.diaDiff),
				pulseDiff: Number(bpAlerts[i].alerts.bp.pulseDiff),
				notes: bpAlerts[i].alerts.bp.Notes,
				measurementDate: bpAlerts[i].measurementDate,
				resolvedDate:
					bpAlerts[i].alerts.bp.resolveStatus === '1'
						? bpAlerts[i].alerts.bp.resolvedDate
						: null,
				type: CONSTANTS.TYPE.BP
			};
			finalBpAlertArray.push(alertObject);
		}
	}
	return finalBpAlertArray;
};

const getPatients = async ({ doctorId, hospitalID }) => {
	let nextPaginationKey = null;
	const patientList = [];
	try {
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		do {
			const { Items, LastEvaluatedKey } = await DB.getPatients(
				doctorId,
				nextPaginationKey,
				hospitalTableDetails.DOCTOR_PATIENT
			);

			let finalAlertArray = [];
			for (let i = 0; i < Items.length; i++) {
				let userId;
				// if hospital is hodes, get readings using hubId, else use userId
				if (hospitalTableDetails.DOCTOR_PATIENT.includes(CONSTANTS.HODES)) {
					userId = Items[i].hubID;
				} else {
					userId = Items[i].userID;
				}

				const nonBpAlerts = await getNonBpAlerts(
					userId,
					hospitalTableDetails.ALERT_NON_BP_TABLE
				);
				const bpAlerts = await getBpAlerts(
					userId,
					hospitalTableDetails.ALERT_BP_TABLE
				);
				finalAlertArray = [...nonBpAlerts, ...bpAlerts];

				const patientData = await getPatientList([Items[i]]);
				patientData.alerts = finalAlertArray;
				patientList.push(patientData);
			}
			nextPaginationKey = LastEvaluatedKey;
		} while (nextPaginationKey !== undefined);
		return Promise.resolve({
			success: true,
			data: patientList
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await getPatients(event);
	callback(null, response);
};
