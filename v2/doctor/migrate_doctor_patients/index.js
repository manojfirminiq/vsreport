const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');

const migrateData = async (fromDoctorId, fromHospitalTables, toDoctorId, toHospitalTables) => {
	const patients = await DB.getItems(fromDoctorId, fromHospitalTables.DOCTOR_PATIENT_TABLE);
	if (Array.isArray(patients) && patients.length > 0) {
		for (let i = 0; i < patients.length; i++) {
			await DB.saveItems(toDoctorId, toHospitalTables.DOCTOR_PATIENT_TABLE, patients[i]);
			await DB.deletePatient(fromDoctorId, fromHospitalTables.DOCTOR_PATIENT_TABLE, patients[i]);
		}
	}

	const notes = await DB.getItems(fromDoctorId, fromHospitalTables.NOTES_TABLE);

	if (Array.isArray(notes) && notes.length > 0) {
		for (let i = 0; i < notes.length; i++) {
			await DB.saveItems(toDoctorId, toHospitalTables.NOTES_TABLE, notes[i]);
			await DB.deleteNote(fromDoctorId, fromHospitalTables.NOTES_TABLE, notes[i]);
		}
	}

	const consultation = await DB.getItems(fromDoctorId, fromHospitalTables.DOCTOR_CONSULTATION_TABLE);
	if (Array.isArray(consultation) && consultation.length > 0) {
		for (let i = 0; i < consultation.length; i++) {
			await DB.saveItems(toDoctorId, toHospitalTables.DOCTOR_CONSULTATION_TABLE, consultation[i]);
			await DB.deleteConsultation(fromDoctorId, fromHospitalTables.DOCTOR_CONSULTATION_TABLE, consultation[i]);
		}
	}

	const bpAlerts = await DB.getAlerts(fromDoctorId, fromHospitalTables.BP_READINGS_ALERTS_TABLE);
	if (Array.isArray(bpAlerts) && bpAlerts.length > 0) {
		for (let i = 0; i < bpAlerts.length; i++) {
			await DB.saveItems(toDoctorId, toHospitalTables.BP_READINGS_ALERTS_TABLE, bpAlerts[i]);
			await DB.deleteBpAlert(fromHospitalTables.BP_READINGS_ALERTS_TABLE, bpAlerts[i]);
		}
	}

	const nonBpAlerts = await DB.getAlerts(fromDoctorId, fromHospitalTables.NON_BP_READINGS_ALERTS_TABLE);
	if (Array.isArray(nonBpAlerts) && nonBpAlerts.length > 0) {
		for (let i = 0; i < nonBpAlerts.length; i++) {
			await DB.saveItems(toDoctorId, toHospitalTables.NON_BP_READINGS_ALERTS_TABLE, nonBpAlerts[i]);
			await DB.deleteNonBpAlert(fromHospitalTables.NON_BP_READINGS_ALERTS_TABLE, nonBpAlerts[i]);
		}
	}
};

const applyValidation = async ({ fromHospitalId, fromDoctorId, toHospitalId, toDoctorId }) => {
	try {
	    if (!fromHospitalId) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.FROM_HOSPITAL_ID_REQ.MESSAGE, errorCode: CONSTANTS.ERRORS.FROM_HOSPITAL_ID_REQ.CODE });

	    if (!toHospitalId) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.TO_HOSPITAL_ID_REQ.MESSAGE, errorCode: CONSTANTS.ERRORS.TO_HOSPITAL_ID_REQ.CODE });

		if (!fromDoctorId) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.FROM_DOCTOR_ID_REQ.MESSAGE, errorCode: CONSTANTS.ERRORS.FROM_DOCTOR_ID_REQ.CODE });

		if (!toDoctorId) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.TO_DOCTOR_ID_REQ.MESSAGE, errorCode: CONSTANTS.ERRORS.TO_DOCTOR_ID_REQ.CODE });

		const fromHospitalTables = await DB.verifyHospital(fromHospitalId);
		if (!fromHospitalTables) {
			return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.FROM_HOSPITAL_ID_INVALID.MESSAGE, errorCode: CONSTANTS.ERRORS.FROM_HOSPITAL_ID_INVALID.CODE });
		}

		const isValidFromDoctorId = await DB.verifyDoctor(fromDoctorId, fromHospitalTables.DOCTOR_PROFILE_TABLE);
		if (!isValidFromDoctorId) {
		    return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.FROM_DOCTOR_NOT_FOUND.MESSAGE, errorCode: CONSTANTS.ERRORS.FROM_DOCTOR_NOT_FOUND.CODE });
		}

		const toHospitalTables = await DB.verifyHospital(toHospitalId);
		if (!toHospitalTables) {
			return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.TO_HOSPITAL_ID_INVALID.MESSAGE, errorCode: CONSTANTS.ERRORS.TO_HOSPITAL_ID_INVALID.CODE });
		}

		const isValidToDoctorId = await DB.verifyDoctor(toDoctorId, toHospitalTables.DOCTOR_PROFILE_TABLE);
		if (!isValidToDoctorId) {
		    return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.TO_DOCTOR_NOT_FOUND.MESSAGE, errorCode: CONSTANTS.ERRORS.TO_DOCTOR_NOT_FOUND.CODE });
		}

		await migrateData(fromDoctorId, fromHospitalTables, toDoctorId, toHospitalTables);
		return Promise.resolve({ success: true, message: 'Successfully Migrated Patients' });
	} catch (e) {
		console.log('Error in migrating patients', e);
		return Promise.resolve({ success: false, errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE, message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE });
	}
};

exports.handler = async (event, context, callback) => {
	const response = await applyValidation(event);
	callback(null, response);
};
