exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	FROM_DOCTOR_ID_REQ: {
		CODE: 'FROM_DOCTOR_ID_REQ',
		MESSAGE: 'From Doctor id required'
	},
	FROM_HOSPITAL_ID_REQ: {
		CODE: 'FROM_HOSPITAL_ID_REQ',
		MESSAGE: 'From Hospital id required'
	},
	TO_DOCTOR_ID_REQ: {
		CODE: 'TO_DOCTOR_ID_REQ',
		MESSAGE: 'To Doctor Id is invalid'
	},
	TO_HOSPITAL_ID_REQ: {
		CODE: 'TO_HOSPITAL_ID_REQ',
		MESSAGE: 'To Hospital id required'
	},
	FROM_HOSPITAL_ID_INVALID: {
		CODE: 'FROM_HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	TO_HOSPITAL_ID_INVALID: {
		CODE: 'TO_HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	FROM_DOCTOR_NOT_FOUND: {
		CODE: 'FROM_DOCTOR_NOT_FOUND',
		MESSAGE: 'Doctor Not Found'
	},
	TO_DOCTOR_NOT_FOUND: {
		CODE: 'TO_DOCTOR_NOT_FOUND',
		MESSAGE: 'Doctor Not Found'
	}
};

exports.DOCTOR_ID_MEASURMENT_DATE_INDEX = 'doctorID-measurementDate-index';
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.RPM = '_rpm_';
exports.DELETE_FLAG = true;
