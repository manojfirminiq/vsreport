const AWS = require('aws-sdk');
const CONSTANTS = require('./CONSTANTS');

AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
const DOCTOR_TABLE_NAME = process.env.DOCTOR_TABLE_NAME;
const DOCTOR_PATIENT_TABLE_NAME = process.env.DOCTOR_PATIENT_TABLE_NAME;
const NOTES_TABLE_NAME = process.env.NOTES_TABLE_NAME;
const DOCTOR_CONSULTATION_TABLE_NAME = process.env.DOCTOR_CONSULTATION_TABLE_NAME;
const BP_READINGS_ALERTS_TABLE_NAME = process.env.BP_READINGS_ALERTS_TABLE_NAME;
const NON_BP_READINGS_ALERTS_TABLE_NAME = process.env.NON_BP_READINGS_ALERTS_TABLE_NAME;

const verifyHospital = async (hospitalId) => {
	const params = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': hospitalId
		},
		ExpressionAttributeNames: {
			'#uid': 'hospitalID'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_PROFILE_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${DOCTOR_TABLE_NAME}`,
			DOCTOR_PATIENT_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${DOCTOR_PATIENT_TABLE_NAME}`,
			NOTES_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${NOTES_TABLE_NAME}`,
			DOCTOR_CONSULTATION_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${DOCTOR_CONSULTATION_TABLE_NAME}`,
			BP_READINGS_ALERTS_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${BP_READINGS_ALERTS_TABLE_NAME}`,
			NON_BP_READINGS_ALERTS_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${NON_BP_READINGS_ALERTS_TABLE_NAME}`
		};
		return obj;
	}
};

const verifyDoctor = async (doctorID, doctorTable) => {
	const params = {
		TableName: doctorTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorID
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	}
	return true;
};

const getItems = async (doctorId, table) => {
	const params = {
		TableName: table,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID'
		}
	};

	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach((item) => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return queryResults;
};

const saveItems = async (doctorId, table, item) => {
	item.modifiedDate = new Date().getTime();
	item.doctorID = doctorId;
	const params = {
		TableName: table,
		Item: item
	};
	return await dbClient.put(params).promise();
};

const deletePatient = async (doctorId, table, item) => {
	const params = {
		TableName: table,
		Key: {
			doctorID: doctorId,
			userID: item.userID
		},
		UpdateExpression:
			'SET deleteFlag = :deleteFlag, deletedAt = :deletedAt',
		ExpressionAttributeValues: {
			':deleteFlag': CONSTANTS.DELETE_FLAG,
			':deletedAt': new Date().getTime()
		}
	};
	return await dbClient.update(params).promise();
};

const deleteNote = async (doctorId, table, item) => {
	const params = {
		TableName: table,
		Key: {
			doctorID: doctorId,
			userID_startDate: `${item.userID}_${item.startDate}`
		},
		UpdateExpression:
			'SET deleteFlag = :deleteFlag, deletedAt = :deletedAt',
		ExpressionAttributeValues: {
			':deleteFlag': CONSTANTS.DELETE_FLAG,
			':deletedAt': new Date().getTime()
		}
	};
	return await dbClient.update(params).promise();
};

const deleteConsultation = async (doctorId, table, item) => {
	const params = {
		TableName: table,
		Key: {
			doctorID: doctorId,
			userID_startTime: `${item.userID}_${item.startTime}`
		},
		UpdateExpression:
			'SET deleteFlag = :deleteFlag, deletedAt = :deletedAt',
		ExpressionAttributeValues: {
			':deleteFlag': CONSTANTS.DELETE_FLAG,
			':deletedAt': new Date().getTime()
		}
	};
	return await dbClient.update(params).promise();
};

const deleteBpAlert = async (table, item) => {
	const params = {
		TableName: table,
		Key: {
			userID: item.userID,
			measurementDate: item.measurementDate
		},
		UpdateExpression:
			'SET deleteFlag = :deleteFlag, deletedAt = :deletedAt',
		ExpressionAttributeValues: {
			':deleteFlag': CONSTANTS.DELETE_FLAG,
			':deletedAt': new Date().getTime()
		}
	};
	return await dbClient.update(params).promise();
};

const deleteNonBpAlert = async (table, item) => {
	const params = {
		TableName: table,
		Key: {
			userId_type_deviceLocalName: `${item.userID}_weight_Manual`,
			measurementDate: item.measurementDate
		},
		UpdateExpression:
			'SET deleteFlag = :deleteFlag, deletedAt = :deletedAt',
		ExpressionAttributeValues: {
			':deleteFlag': CONSTANTS.DELETE_FLAG,
			':deletedAt': new Date().getTime()
		}
	};
	return await dbClient.update(params).promise();
};

const getAlerts = async (doctorId, table) => {
	const params = {
		TableName: table,
		IndexName: CONSTANTS.DOCTOR_ID_MEASURMENT_DATE_INDEX,
		KeyConditionExpression:
			'#doctor = :id',
		ExpressionAttributeNames: {
			'#doctor': 'doctorID'
		},
		ExpressionAttributeValues: {
			':id': doctorId
		}
	};
	params.ScanIndexForward = false;
	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach((item) => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return queryResults;
};

module.exports.verifyDoctor = verifyDoctor;
module.exports.verifyHospital = verifyHospital;
module.exports.getItems = getItems;
module.exports.saveItems = saveItems;
module.exports.deletePatient = deletePatient;
module.exports.deleteNote = deleteNote;
module.exports.deleteConsultation = deleteConsultation;
module.exports.deleteBpAlert = deleteBpAlert;
module.exports.deleteNonBpAlert = deleteNonBpAlert;
module.exports.getAlerts = getAlerts;
