const DB = require('./db');
const CONSTANTS = require('./CONSTANTS');
const AWS = require('aws-sdk');
const s3Client = new AWS.S3();

const getUserProfileUrl = profilePicture => {
	return s3Client.getSignedUrl('getObject', {
		Bucket: CONSTANTS.PROFILE_BUCKET,
		Key: profilePicture,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	});
};

const getTodo = async ({ doctorId, hospitalID, fromDate, toDate }) => {
	try {
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		let getToDoItems = await DB.getToDo(
			hospitalTable.TO_DO_TABLE,
			doctorId,
			fromDate,
			toDate
		);
		getToDoItems = getToDoItems.map(
			({
				createdDate,
				modifiedDate,
				dueDate,
				doctorID,
				userID,
				attributes: {
					description,
					itemType,
					notes,
					resolveStatus,
					...attributesRest
				},
				...rest
			}) => ({
				createdDate,
				modifiedDate,
				dueDate,
				doctorID,
				userID,
				description,
				itemType,
				notes,
				resolveStatus: resolveStatus || CONSTANTS.DEFAULT_RESOLVE_STATUS
			})
		);

		const finalArray = [];
		let patientDetails = [];
		for (let i = 0; i < getToDoItems.length; i++) {
			let deletedUserFlag = false;
			if (getToDoItems[i].userID) {
				patientDetails = await DB.getPatient(
					doctorId,
					getToDoItems[i].userID,
					hospitalTable.DOCTOR_PATIENT
				);

				if (patientDetails.Items.length !== 0) {
					patientDetails = patientDetails.Items.map(
						({
							patientCode,
							attributes: {
								icdCode,
								name,
								lastName,
								gender,
								dob,
								profilePicture,
								...attributesRest
							},
							...rest
						}) => ({ patientCode, icdCode, name, lastName: lastName || '', gender, dob, profilePicture })
					);
					patientDetails[0].profilePicture = patientDetails[0].profilePicture
						? getUserProfileUrl(patientDetails[0].profilePicture)
						: null;
				} else {
					deletedUserFlag = true;
				}
			} else {
				patientDetails[0] = {};
			}

			if (!deletedUserFlag) {
				const finalArrayObj = {
					...patientDetails[0],
					...getToDoItems[i]
				};
				finalArray.push(finalArrayObj);
			}
		}

		const response = {
			success: true,
			data: finalArray
		};

		return response;
	} catch (error) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await getTodo(event);
	callback(null, response);
};
