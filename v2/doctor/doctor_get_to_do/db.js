const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Count, Items } = await dbClient.query(paramsTable).promise();
	if (Count <= 0) {
		return false;
	} else {
		const obj = {
			TO_DO_TABLE:
				process.env.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.TO_DO_TABLE,
			DOCTOR_PATIENT:
				process.env.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.DOCTOR_PATIENT
		};
		return obj;
	}
};

const getPatient = async (doctorId, userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #userId = :userId',
		FilterExpression: '#deleteFlag <> :deleteFlag',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userId': userId,
			':deleteFlag': true
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userId': 'userID',
			'#deleteFlag': 'deleteFlag'
		}
	};
	const Items = await dbClient.query(params).promise();
	return Items;
};

const getToDo = async (hospitalTable, doctorID, start, end) => {
	const paramsTable = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorID = :doctorID',
		ExpressionAttributeNames: {
			'#doctorID': 'doctorID'
		},
		ExpressionAttributeValues: {
			':doctorID': doctorID
		}
	};
	if (start || end) {
		if (start && end) {
			paramsTable.KeyConditionExpression +=
				' AND #sortKey BETWEEN :minDate AND :maxDate';
			paramsTable.ExpressionAttributeValues[':minDate'] = parseInt(start);
			paramsTable.ExpressionAttributeValues[':maxDate'] = parseInt(end);
		} else if (!start) {
			paramsTable.KeyConditionExpression += ' AND #sortKey <= :maxDate';
			paramsTable.ExpressionAttributeValues[':maxDate'] = parseInt(end);
		} else {
			paramsTable.KeyConditionExpression += ' AND #sortKey >= :minDate';
			paramsTable.ExpressionAttributeValues[':minDate'] = parseInt(start);
		}
		paramsTable.ExpressionAttributeNames['#sortKey'] = 'dueDate';
	}
	const { Items } = await dbClient.query(paramsTable).promise();
	return Items;
};

module.exports.verifyHospital = verifyHospital;
module.exports.getToDo = getToDo;
module.exports.getPatient = getPatient;
