exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_ID_REQ: {
		CODE: 'USER_ID_REQ',
		MESSAGE: 'User id required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};

exports.TO_DO_TABLE = process.env.TO_DO_TABLE;
exports.RPM = 'rpm';
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.S3_KEY_EXPIRES_IN = 60 * 5;
exports.PROFILE_BUCKET = process.env.PROFILE_BUCKET;
exports.DEFAULT_RESOLVE_STATUS = 0;
