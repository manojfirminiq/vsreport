exports.RPM = '_rpm_';
exports.STAGE = process.env.STAGE;
exports.SUBSCRIBE_MSG = 'Subscribe successfully for alerts';
exports.UNSUBSCRIBE_MSG = 'Unsubscribe successfully for alerts';

exports.SUBSCRIPTION = {
	UNSUBSCRIBE: 'UNSUBSCRIBE',
	SUBSCRIBE: 'SUBSCRIBE',
	SUBSCRIBEFLAG: true,
	UNSUBSCRIBEFLAG: false
};

exports.ERRORS = {
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	DOCTOR_ID_NOT_FOUND: {
		CODE: 'DOCTOR_ID_NOT_FOUND',
		MESSAGE: 'Doctor Id not found'
	},
	STATUS_INVALID: {
		CODE: 'STATUS_INVALID',
		MESSAGE: 'Status is not valid'
	},
	STATUS_REQ: {
		CODE: 'STATUS_REQ',
		MESSAGE: 'Status required'
	},
	DOCTOR_NOT_SUBSCRIBED: {
		CODE: 'DOCTOR_NOT_SUBSCRIBED',
		MESSAGE: 'Doctor not subscribed'
	},
	DOCTOR_ALREADY_SUBSCRIBED: {
		CODE: 'DOCTOR_ALREADY_SUBSCRIBED',
		MESSAGE: 'Doctor already subscribed'
	}
};

exports.DOCTOR_TABLE_NAME = process.env.DOCTOR_TABLE_NAME;
exports.HOSPITAL_TABLE_NAME = process.env.HOSPITAL_TABLE_NAME;
