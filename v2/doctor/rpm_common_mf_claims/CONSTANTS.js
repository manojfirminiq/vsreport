exports.RPM = 'RPM';
exports.MF_END_POINT = process.env.MF_END_POINT;
exports.MF_CONTENT_TYPE = process.env.MF_CONTENT_TYPE;
exports.MF_END_POINT = process.env.MF_END_POINT;
exports.MF_SOURCE_HOST = process.env.MF_SOURCE_HOST;
exports.MF_VERIFICATION_TOKEN = process.env.MF_VERIFICATION_TOKEN;
exports.MF_APPLICATION_NAME = process.env.REDOX_ENGINE;
exports.CLAIM = process.env.CLAIM;
exports.MF_POST = 'POST';
exports.DATA_MODEL_FIN = 'FINANCIAL';
exports.STAGE = process.env.STAGE;
exports.SENT = 'SENT';
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.TEST = 'test';

exports.MF_JSON = {
	Meta: {
		DataModel: 'Inventory',
		EventType: 'Deplete',
		EventDateTime: new Date().toISOString(),
		Source: {
			ID: 'edb9fdce-f966-4325-8d0a-249a3168c0dc',
			Name: 'Omron Foresight'
		},
		Destinations: [
			{
				ID: 'abf8395a-0d1e-458a-be9b-b9f9e961d9d6',
				Name: 'Omron Source'
			}
		],
		Message: {
			ID: Date.now()
		},
		Transmission: {
			ID: Date.now()
		},
		FacilityCode: 'HD001'
	},
	Patient: {
		Identifiers: [],
		Demographics: {
			FirstName: null,
			MiddleName: null,
			LastName: null,
			DOB: null,
			SSN: null,
			Sex: null,
			Race: null,
			IsHispanic: null,
			MaritalStatus: null,
			IsDeceased: null,
			DeathDateTime: null,
			PhoneNumber: {
				Home: null,
				Office: null,
				Mobile: null
			},
			EmailAddresses: [],
			Language: null,
			Citizenship: [],
			Address: {
				StreetAddress: null,
				City: null,
				State: null,
				ZIP: null,
				County: null,
				Country: null
			}
		},
		Notes: []
	},
	Visit: {
		VisitNumber: null
	},
	Items: [
		{
			Identifiers: [
				{
					ID: 'RPM35',
					IDType: 'SKU'
				}
			],
			Description: null,
			Quantity: '',
			Type: null,
			Units: null,
			Procedure: {
				Code: null,
				Codeset: null,
				Modifier: null
			},
			Notes: null,
			Vendor: {
				ID: null,
				Name: null,
				CatalogNumber: null
			},
			WastedQuantity: null,
			UsedQuantity: null,
			SerialNumber: null,
			LotNumber: null,
			Location: {
				Facility: null,
				Department: null,
				ID: null,
				Bin: null
			},
			OrderingProvider: {
				ID: null,
				IDType: null,
				FirstName: null,
				LastName: null,
				Credentials: [],
				Address: {
					StreetAddress: null,
					City: null,
					State: null,
					ZIP: null,
					County: null,
					Country: null
				},
				EmailAddresses: [],
				PhoneNumber: {
					Office: null
				},
				Location: {
					Type: null,
					Facility: null,
					Department: null,
					Room: null
				}
			}
		}
	]
};
