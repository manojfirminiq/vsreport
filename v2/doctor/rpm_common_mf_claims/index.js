const CONSTANT = require('./CONSTANTS.js');
const axios = require('axios');
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const callApi = async payload => {
	const options = {
		method: 'POST',
		headers: {
			'Content-Type': CONSTANT.MF_CONTENT_TYPE,
			'source-host': CONSTANT.MF_SOURCE_HOST,
			'application-name': CONSTANT.MF_APPLICATION_NAME,
			'verification-token': CONSTANT.MF_VERIFICATION_TOKEN
		},
		data: payload,
		url: CONSTANT.MF_END_POINT
	};
	return await axios(options);
};

const updateClaimsStatus = async (yearMonth, ehrID, tablename) => {
	try {
		const queryParams = {
			TableName: tablename,
			Key: { yearMonth, ehrID },
			UpdateExpression:
				'set #attributes.#status = :submitstatus,modifiedDate = :modifiedDate',
			ExpressionAttributeNames: {
				'#attributes': 'attributes',
				'#status': 'status'
			},
			ExpressionAttributeValues: {
				':submitstatus': CONSTANT.SENT,
				':modifiedDate': Date.now()
			}
		};
		await dbClient.update(queryParams).promise();
		return { success: true };
	} catch (e) {
		return { success: false };
	}
};

const findHospitalID = async (hospitalId) => {
	let hospitals = [];
	const paramsTable = {
		TableName: CONSTANT.HOSPITAL_MASTER_TABLE
	};
	if (hospitalId) {
		paramsTable.KeyConditionExpression = 'hospitalID = :id';
		paramsTable.ExpressionAttributeValues = {
		    ':id': hospitalId
		};
		const { Items } = await dbClient.query(paramsTable).promise();
		hospitals = Items;
	} else {
	    const { Items } = await dbClient.scan(paramsTable).promise();
	    hospitals = Items;
	}
	return hospitals;
};

const getPreviousClaimDetails = async (table, ehrID, yearMonth) => {
	try {
		const queryParams = {
			TableName: table,
			KeyConditionExpression: 'yearMonth = :yearMonth AND ehrID = :ehrID',
			ExpressionAttributeValues: {
				':ehrID': ehrID,
				':yearMonth': Number(yearMonth)
			}
		};
		const { Items } = await dbClient.query(queryParams).promise();
		return Items || [];
	} catch (e) {
		console.log(e);
	}
};

const getLastThreeMonths = () => {
	const lastThreeMonths = [];
	const date = new Date();

	for (let i = 0; i < 3; i++) {
		let month = date.getMonth();
		month = month < 10 ? `0${month}` : `${month}`;
		const year = date.getFullYear().toString();
		const yearMonth = year + month;
		lastThreeMonths.push(yearMonth);
		date.setMonth(date.getMonth() - 1);
	}

	return lastThreeMonths;
};

exports.handler = async event => {
	try {
		const manualRunning = event.manualRunning || 0;
		const hospitalId = event.hospitalId || 0;
		let yearMonth = event.yearMonth || 0;
		let yearMonthArray = [];
		if (manualRunning && !yearMonth) {
			yearMonthArray = getLastThreeMonths();
		}
		if (!yearMonth) {
			const date = new Date();
			let month = date.getMonth();
			month = month < 10 ? `0${month}` : `${month}`;
			const year = date.getFullYear().toString();
			yearMonth = year + month;
		}
		if (yearMonthArray.length === 0) {
		    yearMonthArray.push(yearMonth);
		}
		let hospital = [];
		if (hospitalId) {
			hospital = await findHospitalID(hospitalId);
		} else {
			hospital = await findHospitalID();
		}

		const successcount = [];
		const failurecount = [];

		if (hospital.length > 0) {
			for (let i = 0; i < hospital.length; i++) {
			    if (hospital[i].isActive && !hospital[i].isEHR) {
    				for (const currentMonth of yearMonthArray) {
    					try {
    						const tablename = `${CONSTANT.STAGE}_${CONSTANT.RPM.toLowerCase()}_${
    							hospital[i].dbIdentifier
    						}_${CONSTANT.CLAIM}`;
    						const claims = await getPreviousClaimDetails(
    							tablename,
    							hospital[i].hospitalID,
    							currentMonth
    						);
    						if (claims && claims.length > 0) {
    							for (let j = 0; j < claims.length; j++) {
    								try {
    									const mfparams = { ...CONSTANT.MF_JSON };
    									if (hospital[i].claims.SKU.toLowerCase() !== CONSTANT.TEST) {
    										mfparams.Items[0].Quantity =
    											claims[j].attributes.countOfUsers;
    										mfparams.Meta.FacilityCode = hospital[i].claims.facilityCode;
    										mfparams.Items[0].Identifiers[0].ID = hospital[i].claims.SKU;
    										if (claims[j].attributes && !claims[j].attributes.status) {
    											await callApi(mfparams)
    												.then(async response => {
    													if (response && response.data && response.data.status) {
    														const yearMonth = claims[j].yearMonth;
    														const ehrID = claims[j].ehrID;
    														const updatestatus = await updateClaimsStatus(
    															yearMonth,
    															ehrID,
    															tablename
    														);
    														if (updatestatus && updatestatus.success) {
    															successcount.push(claims);
    														} else {
    															failurecount.push(claims);
    														}
    													}
    												})
    												.catch(err => {
    													console.log(err);
    												});
    										}
    									}
    								} catch (errors) {
    									console.log(errors);
    								}
    							}
    						}
    					} catch (e) {
    						console.log(e);
    					}
    				}
			    }
				if (i + 1 === hospital.length) {
					console.log('success count', successcount.length);
					console.log('failurecount count', failurecount.length);
				}
			}
		}
	} catch (error) {
		return Promise.reject(error);
	}

	return Promise.resolve({ success: true });
};
