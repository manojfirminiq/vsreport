'use strict';
const AWS = require('aws-sdk');
const S3 = new AWS.S3();
const excel = require('excel4node');
const moment = require('moment');
const moment1 = require('moment-timezone');

const CONSTANTS = require('./CONSTANTS.js');

const weightToLbs = weight => {
	return (Number(weight) * 2.20462).toFixed(1).toString();
};

const getReportUrl = Key => {
	return S3.getSignedUrl('getObject', {
		Bucket: CONSTANTS.PROFILE_BUCKET,
		Key: Key,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	});
};

const generateXlsReport = async (bpData, weightData, timeZone, Items) => {
	let dob = '';
	if (Items[0].attributes.dob.includes('-')) {
		dob = Items[0].attributes.dob;
	} else {
		dob =
			Items[0].attributes.dob.slice(0, 4) +
			'-' +
			Items[0].attributes.dob.slice(4, 6) +
			'-' +
			Items[0].attributes.dob.slice(6, 8);
	}

	const date = moment1.tz(timeZone).format('LL');
	const workbook = new excel.Workbook();

	if (bpData !== '') {
		const worksheet = workbook.addWorksheet('BP');
		// Headers + Columns
		worksheet
			.cell(1, 1)
			.string(CONSTANTS.NAME)
			.style({ font: { bold: true } });
		worksheet
			.cell(1, 2)
			.string(CONSTANTS.DOB)
			.style({ font: { bold: true } });
		worksheet
			.cell(1, 3)
			.string(CONSTANTS.MRN)
			.style({ font: { bold: true } });
		worksheet
			.cell(4, 1)
			.string(CONSTANTS.DATE)
			.style({ font: { bold: true } });
		worksheet
			.cell(4, 2)
			.string(CONSTANTS.SYSTOLIC)
			.style({ font: { bold: true } });
		worksheet
			.cell(4, 3)
			.string(CONSTANTS.DIASTOLIC)
			.style({ font: { bold: true } });
		worksheet
			.cell(4, 4)
			.string(CONSTANTS.PULSE)
			.style({ font: { bold: true } });
		worksheet
			.cell(4, 5)
			.string(CONSTANTS.ALERT_FLAG)
			.style({ font: { bold: true } });
		// Rows
		bpData.Items.sort((a, b) =>
			a.measurementDate > b.measurementDate ? -1 : 1
		);
		worksheet.cell(2, 1).string((Items[0].attributes.name).toUpperCase());
		worksheet.cell(2, 2).string(dob);
		worksheet.cell(2, 3).string(Items[0].patientCode);
		bpData.Items.forEach((item, i) => {
			worksheet
				.cell(i + 5, 1)
				.string(moment(item.measurementDate).format('lll'));
			worksheet.cell(i + 5, 2).number(Number(item.attributes.systolic));
			worksheet.cell(i + 5, 3).number(Number(item.attributes.diastolic));
			worksheet.cell(i + 5, 4).number(Number(item.attributes.pulse));
			worksheet
				.cell(i + 5, 5)
				.string(
					item.attributes.haveAlert === CONSTANTS.HAVE_ALERT
						? CONSTANTS.HAVE_ALERT_YES
						: CONSTANTS.HAVE_ALERT_NO
				);
		});
	}

	if (weightData !== '') {
		const worksheet2 = workbook.addWorksheet('Weight');
		// Headers + Columns
		worksheet2
			.cell(1, 1)
			.string(CONSTANTS.NAME)
			.style({ font: { bold: true } });
		worksheet2
			.cell(1, 2)
			.string(CONSTANTS.DOB)
			.style({ font: { bold: true } });
		worksheet2
			.cell(1, 3)
			.string(CONSTANTS.MRN)
			.style({ font: { bold: true } });
		worksheet2
			.cell(4, 1)
			.string(CONSTANTS.DATE)
			.style({ font: { bold: true } });
		worksheet2
			.cell(4, 2)
			.string(CONSTANTS.WEIGHT_IN_LBS)
			.style({ font: { bold: true } });
		worksheet2
			.cell(4, 3)
			.string(CONSTANTS.WEIGHT_GAIN_24HR_ALERT)
			.style({ font: { bold: true } });
		worksheet2
			.cell(4, 4)
			.string(CONSTANTS.WEIGHT_GAIN_72HR_ALERT)
			.style({ font: { bold: true } });
		worksheet2
			.cell(4, 5)
			.string(CONSTANTS.WEIGHT_GAIN_24HR)
			.style({ font: { bold: true } });
		worksheet2
			.cell(4, 6)
			.string(CONSTANTS.WEIGHT_GAIN_72HR)
			.style({ font: { bold: true } });
		// Rows
		weightData.Items.sort((a, b) =>
			a.measurementDate > b.measurementDate ? -1 : 1
		);
		worksheet2.cell(2, 1).string((Items[0].attributes.name).toUpperCase());
		worksheet2.cell(2, 2).string(dob);
		worksheet2.cell(2, 3).string(Items[0].patientCode);
		weightData.Items.forEach((item, i) => {
			worksheet2
				.cell(i + 5, 1)
				.string(moment(item.measurementDate).format('lll'));
			worksheet2.cell(i + 5, 2).string(weightToLbs(item.attributes.weight));
			worksheet2
				.cell(i + 5, 3)
				.string(
					Number(item.attributes.have24hrAlert) === CONSTANTS.HAVE_ALERT
						? CONSTANTS.HAVE_ALERT_YES
						: CONSTANTS.HAVE_ALERT_NO
				);
			worksheet2
				.cell(i + 5, 4)
				.string(
					Number(item.attributes.have72hrAlert) === CONSTANTS.HAVE_ALERT
						? CONSTANTS.HAVE_ALERT_YES
						: CONSTANTS.HAVE_ALERT_NO
				);
			const weightGain24Hr =
				(item.attributes.have24hrAlert &&
					item.weight &&
					item.weight.weightDiff24hr &&
					Number(item.weight.weightDiff24hr).toFixed(1)) ||
				'0';
			worksheet2.cell(i + 5, 5).string(weightGain24Hr);

			const weightGain72Hr =
				(item.attributes.have72hrAlert &&
					item.weight &&
					item.weight.weightDiff72hr &&
					Number(item.weight.weightDiff72hr).toFixed(1)) ||
				'0';
			worksheet2.cell(i + 5, 6).string(weightGain72Hr);
		});
	}

	const buffer = await workbook.writeToBuffer();
	if (buffer) {
		const params = {
			Bucket: CONSTANTS.PROFILE_BUCKET,
			Key: `reports/${date} User report.xlsx`,
			Body: buffer
		};
		const response = await S3.upload(params).promise();
		if (response) {
			const s3url = getReportUrl(params.Key);
			return Promise.resolve({
				success: true,
				report: s3url
			});
		} else {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.S3_UPLOAD_FAILED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.S3_UPLOAD_FAILED.CODE
			});
		}
	} else {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.WORKBOOK_CREATION_FAILED.MESSAGE,
			errorCode: CONSTANTS.ERRORS.WORKBOOK_CREATION_FAILED.CODE
		});
	}
};

module.exports.generateXlsReport = generateXlsReport;
