exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	USER_NOT_FOUND: {
		CODE: 'USER_NOT_FOUND',
		MESSAGE: 'User not found'
	},
	S3_UPLOAD_FAILED: {
		CODE: 'S3_UPLOAD_FAILED',
		MESSAGE: 'S3 upload failed'
	},
	WORKBOOK_CREATION_FAILED: {
		CODE: 'WORKBOOK_CREATION_FAILED',
		MESSAGE: 'Workbook creation failed'
	}
};

exports.VALIDATION_MESSAGES = {
	USER_ID_REQ: {
		CODE: 'USER_ID_REQ',
		MESSAGE: 'Ready for Submission'
	},
	FROM_DATE_REQ: {
		CODE: 'FROM_DATE_REQ',
		MESSAGE: 'From date required'
	},
	TO_DATE_REQ: {
		CODE: 'TO_DATE_REQ',
		MESSAGE: 'To date required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	VITAL_TYPE_REQ: {
		CODE: 'VITAL_TYPE_REQ',
		MESSAGE: 'Vital type is required'
	}
};

exports.WEIGHT = 'weight';
exports.BP = 'bp';
exports.HODES = 'hodes';
exports.RPM = '_rpm_';
exports.PROFILE_BUCKET = process.env.PROFILE_BUCKET;
exports.WEIGHT_MANUAL = '_weight_Manual';
exports.S3_KEY_EXPIRES_IN = 60 * 5;

exports.DATE = 'Date';
exports.SYSTOLIC = 'Systolic';
exports.DIASTOLIC = 'Diastolic';
exports.PULSE = 'Pulse';
exports.ALERT_FLAG = 'Alert';
exports.WEIGHT_IN_LBS = 'Weight in lbs';
exports.HAVE_ALERT = 1;
exports.HAVE_ALERT_NO = 'No';
exports.HAVE_ALERT_YES = 'Yes';
exports.WEIGHT_GAIN_24HR_ALERT = '24 hr weight gain alert';
exports.WEIGHT_GAIN_72HR_ALERT = '72 hr weight gain alert';
exports.WEIGHT_GAIN_24HR = '24 hr weight gain';
exports.WEIGHT_GAIN_72HR = '72 hr weight gain';
exports.NAME = 'Name';
exports.DOB = 'DOB';
exports.MRN = 'MRN';
