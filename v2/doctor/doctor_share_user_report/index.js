const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./db.js');
const Util = require('./utils.js');

const generateXlsReport = async (
	doctorId,
	staffID,
	hospitalID,
	userID,
	fromDate,
	toDate,
	vitalType,
	timeZone
) => {
	try {
		let tZone;
		let loggedInUserProfile;
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		if (staffID && staffID !== null) {
			loggedInUserProfile = await DB.getStaffProfile(
				doctorId,
				staffID,
				hospitalTableDetails.STAFF_PROFILE
			);
		} else {
			loggedInUserProfile = await DB.getDoctorProfile(
				doctorId,
				hospitalTableDetails.DOCTOR_PROFILE
			);
		}

		if (loggedInUserProfile) {
			tZone =
				loggedInUserProfile.attributes &&
				loggedInUserProfile.attributes.timeZone
					? loggedInUserProfile.attributes.timeZone
					: timeZone;
		}

		const { Items } = await DB.getPatient(
			doctorId,
			userID,
			hospitalTableDetails.DOCTOR_PATIENT
		);

		if (Items.length === 0) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
			});
		} else {
			// if hospital is hodes, get readings using hubId, else use userId
			if (hospitalTableDetails.DOCTOR_PATIENT.includes(CONSTANTS.HODES)) {
				userID = Items[0].hubID;
			}

			let xlsReport;

			if (vitalType.length === 2) {
				const bpData = await DB.getBpReadings(
					hospitalTableDetails.BP_TABLE,
					userID,
					fromDate,
					toDate
				);
				const weightData = await DB.getNonBpReadings(
					hospitalTableDetails.NON_BP_TABLE,
					userID,
					fromDate,
					toDate
				);
				const weightDataAlerts = await DB.getNonBpReadingsAlerts(
					hospitalTableDetails.NON_BP_ALERTS_TABLE,
					userID,
					fromDate,
					toDate
				);
				const weightJson = {};
				for (const alert of weightDataAlerts.Items) {
					weightJson[alert.measurementDate] = alert;
				}
				for (let i = 0; i < weightData.Items.length; i++) {
					if (
						weightData.Items[i].attributes.have24hrAlert ||
						weightData.Items[i].attributes.have72hrAlert
					) {
						if (weightJson[weightData.Items[i].measurementDate]) {
							weightData.Items[i] = {
								...weightData.Items[i],
								...weightJson[weightData.Items[i].measurementDate].alerts
							};
						}
					}
				}
				xlsReport = await Util.generateXlsReport(
					bpData,
					weightData,
					tZone,
					Items
				);
			} else {
				if (vitalType[0] === CONSTANTS.BP) {
					const bpData = await DB.getBpReadings(
						hospitalTableDetails.BP_TABLE,
						userID,
						fromDate,
						toDate
					);
					const weightData = '';
					xlsReport = await Util.generateXlsReport(
						bpData,
						weightData,
						tZone,
						Items
					);
				} else if (vitalType[0] === CONSTANTS.WEIGHT) {
					const weightData = await DB.getNonBpReadings(
						hospitalTableDetails.NON_BP_TABLE,
						userID,
						fromDate,
						toDate
					);
					const weightDataAlerts = await DB.getNonBpReadingsAlerts(
						hospitalTableDetails.NON_BP_ALERTS_TABLE,
						userID,
						fromDate,
						toDate
					);
					const weightJson = {};
					for (const alert of weightDataAlerts.Items) {
						weightJson[alert.measurementDate] = alert;
					}
					for (let i = 0; i < weightData.Items.length; i++) {
						if (
							weightData.Items[i].attributes.have24hrAlert ||
							weightData.Items[i].attributes.have72hrAlert
						) {
							if (weightJson[weightData.Items[i].measurementDate]) {
								weightData.Items[i] = {
									...weightData.Items[i],
									...weightJson[weightData.Items[i].measurementDate].alerts
								};
							}
						}
					}
					const bpData = '';
					xlsReport = await Util.generateXlsReport(
						bpData,
						weightData,
						tZone,
						Items
					);
				}
			}
			return xlsReport;
		}
	} catch (e) {
		console.log('error', e);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

const applyValidation = async ({
	doctorId,
	hospitalID,
	staffID,
	body: { userID, fromDate, toDate, vitalType, timeZone }
}) => {
	try {
		if (!userID) {
			return Promise.resolve({
				success: false,
				code: CONSTANTS.VALIDATION_MESSAGES.USER_ID_REQ.MESSAGE,
				message: CONSTANTS.VALIDATION_MESSAGES.USER_ID_REQ.MESSAGE
			});
		}
		if (!fromDate) {
			return Promise.resolve({
				success: false,
				code: CONSTANTS.VALIDATION_MESSAGES.FROM_DATE_REQ.MESSAGE,
				message: CONSTANTS.VALIDATION_MESSAGES.FROM_DATE_REQ.MESSAGE
			});
		}
		if (!toDate) {
			return Promise.resolve({
				success: false,
				code: CONSTANTS.VALIDATION_MESSAGES.TO_DATE_REQ.MESSAGE,
				message: CONSTANTS.VALIDATION_MESSAGES.TO_DATE_REQ.MESSAGE
			});
		}
		if (!vitalType) {
			return Promise.resolve({
				success: false,
				code: CONSTANTS.VALIDATION_MESSAGES.VITAL_TYPE_REQ.MESSAGE,
				message: CONSTANTS.VALIDATION_MESSAGES.VITAL_TYPE_REQ.MESSAGE
			});
		}

		return await generateXlsReport(
			doctorId,
			staffID,
			hospitalID,
			userID,
			fromDate,
			toDate,
			vitalType,
			timeZone
		);
	} catch (e) {
		console.log('error', e);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
