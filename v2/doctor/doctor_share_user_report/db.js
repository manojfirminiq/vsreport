const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const BP_TABLE = process.env.BP_TABLE;
const NON_BP_TABLE = process.env.NON_BP_TABLE;
const DOCTOR_TABLE = process.env.DOCTOR_TABLE;
const DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
const STAFF_PROFILE_TABLE = process.env.STAFF_PROFILE_TABLE;
const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const NON_BP_ALERTS_TABLE = process.env.NON_BP_ALERTS_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: DOCTOR_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_PROFILE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PROFILE_TABLE,
			STAFF_PROFILE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				STAFF_PROFILE_TABLE,
			DOCTOR_PATIENT:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT_TABLE,
			BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				BP_TABLE,
			NON_BP_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				NON_BP_TABLE,
			NON_BP_ALERTS_TABLE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				NON_BP_ALERTS_TABLE
		};
		return obj;
	}
};

const getDoctorProfile = async (userID, doctorTable) => {
	const params = {
		TableName: doctorTable,
		KeyConditionExpression: '#userID = :userID',
		ExpressionAttributeValues: {
			':userID': userID
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items[0] : false;
};

const getStaffProfile = async (doctorId, staffID, staffTable) => {
	const params = {
		TableName: staffTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #staffID = :staffID',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':staffID': staffID
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#staffID': 'staffID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items[0] : false;
};

const getPatient = async (doctorId, userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #userId = :userId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userId': userId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userId': 'userID'
		}
	};
	return dbClient.query(params).promise();
};

const getBpReadings = async (BP_TABLE, userId, from, to) => {
	const params = {
		TableName: BP_TABLE,
		KeyConditionExpression:
			'#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': parseInt(from),
			':maxDate': parseInt(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	return dbClient.query(params).promise();
};

const getNonBpReadings = async (NON_BP_TABLE, userId, from, to) => {
	const params = {
		TableName: NON_BP_TABLE,
		KeyConditionExpression:
			'#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId + CONSTANTS.WEIGHT_MANUAL,
			':minDate': parseInt(from),
			':maxDate': parseInt(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userId_type_deviceLocalName'
		}
	};
	return dbClient.query(params).promise();
};

const getNonBpReadingsAlerts = async (
	NON_BP_ALERTS_TABLE,
	userId,
	from,
	to
) => {
	const params = {
		TableName: NON_BP_ALERTS_TABLE,
		KeyConditionExpression:
			'#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId + CONSTANTS.WEIGHT_MANUAL,
			':minDate': parseInt(from),
			':maxDate': parseInt(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userId_type_deviceLocalName'
		}
	};
	return dbClient.query(params).promise();
};

module.exports.getNonBpReadings = getNonBpReadings;
module.exports.getBpReadings = getBpReadings;
module.exports.verifyHospital = verifyHospital;
module.exports.getDoctorProfile = getDoctorProfile;
module.exports.getStaffProfile = getStaffProfile;
module.exports.getPatient = getPatient;
module.exports.getNonBpReadingsAlerts = getNonBpReadingsAlerts;
