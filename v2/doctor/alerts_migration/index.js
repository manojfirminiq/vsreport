const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS');
const HOSPITAL_MASTER_TABLE = CONSTANTS.HOSPITAL_MASTER_TABLE;

const getDbIdenitifierFromHospital = async () => {
	const params = {
		TableName: HOSPITAL_MASTER_TABLE
	};
	return dbClient.scan(params).promise();
};

const getBpAlerts = async ALERT_TABLE => {
	const params = {
		TableName: ALERT_TABLE
	};
	return dbClient.scan(params).promise();
};

const getNonBpAlerts = async ALERT_TABLE => {
	const params = {
		TableName: ALERT_TABLE
	};
	return dbClient.scan(params).promise();
};

const getPatientId = async (DOCTOR_PATIENT_TABLE, userID, hospitalID) => {
	const params = {
		TableName: DOCTOR_PATIENT_TABLE
	};

	if (hospitalID === CONSTANTS.HODES) {
		params.FilterExpression = 'hubID = :hubIDVal';
		params.ExpressionAttributeValues = {
			':hubIDVal': userID
		};
	} else {
		params.FilterExpression = 'userID = :userIDVal';
		params.ExpressionAttributeValues = {
			':userIDVal': userID
		};
	}
	return dbClient.scan(params).promise();
};

const updateAlerts = async (readings, alertTable) => {
	const params = {
		TableName: alertTable,
		Item: readings
	};
	return dbClient.put(params).promise();
};

const migrateAlerts = async () => {
	const hospitalDetails = await getDbIdenitifierFromHospital();
	hospitalDetails.Items.map(async data => {
		const DOCTOR_PATIENT_TABLE =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			data.dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PATIENT;
		const BP_ALERT_TABLE =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			data.dbIdentifier +
			'_' +
			CONSTANTS.BP_ALERT;
		const bpAlertsDetails = await getBpAlerts(BP_ALERT_TABLE);
		bpAlertsDetails.Items.map(async bpdata => {
			const patientDetails = await getPatientId(
				DOCTOR_PATIENT_TABLE,
				bpdata.userID,
				data.dbIdentifier
			);
			if (patientDetails.Items.length > 0) {
				bpdata.doctorID = patientDetails.Items[0].doctorID;
				bpdata.actualUserID = patientDetails.Items[0].userID;
				await updateAlerts(bpdata, BP_ALERT_TABLE);
			} else {
				console.log(
					'No hubId found for bp alert -  patientID =>',
					data.dbIdentifier,
					' - ',
					bpdata.userID
				);
			}
		});

		const NON_BP_ALERT_TABLE =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			data.dbIdentifier +
			'_' +
			CONSTANTS.NON_BP_ALERT;
		const nonBpAlertsDetails = await getNonBpAlerts(NON_BP_ALERT_TABLE);
		nonBpAlertsDetails.Items.map(async nonBpdata => {
			const userID = nonBpdata.userId_type_deviceLocalName.split('_');
			const patientDetails = await getPatientId(
				DOCTOR_PATIENT_TABLE,
				userID[0],
				data.dbIdentifier
			);
			if (patientDetails.Items.length > 0) {
				nonBpdata.doctorID = patientDetails.Items[0].doctorID;
				nonBpdata.actualUserID = patientDetails.Items[0].userID;
				await updateAlerts(nonBpdata, NON_BP_ALERT_TABLE);
			} else {
				console.log(
					'No hubId found for non bp alert - patientID =>',
					data.dbIdentifier,
					' - ',
					userID[0]
				);
			}
		});
	});
	return Promise.resolve({
		success: true
	});
};

exports.handler = async (event, context, callback) => {
	const response = await migrateAlerts();
	callback(null, response);
};
