exports.STAGE = process.env.STAGE;
exports.RPM = 'rpm';
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.BP_ALERT = process.env.BP_ALERT;
exports.NON_BP_ALERT = process.env.NON_BP_ALERT;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.HODES = 'hodes';
