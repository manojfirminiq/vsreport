const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_PATIENT = CONSTANTS.DOCTOR_PATIENT;
const DOCTOR_TABLE = CONSTANTS.DOCTOR_TABLE;
const ORDER_PROCESSING = CONSTANTS.ORDER_PROCESSING;

const verifyHospital = async hospitalID => {
	const paramsTable = {
		TableName: DOCTOR_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalID
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		return {
			ORDER_PROCESSING:
				process.env.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				Items[0].dbIdentifier +
				'_' +
				ORDER_PROCESSING,
			DOCTOR_PATIENT:
				process.env.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT
		};
	}
};

const verifyOrder = async (userID, ORDER_PROCESSING) => {
	const paramsTable = {
		TableName: ORDER_PROCESSING,
		KeyConditionExpression: '#userID = :id',
		ExpressionAttributeNames: {
			'#userID': 'userID'
		},
		ExpressionAttributeValues: {
			':id': userID
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	return Items;
};

const deleteOrderProcessing = async (
	userID,
	orderIdCreatedDate,
	ORDER_PROCESSING,
	reason,
	updateUserData
) => {
	const paramsOrder = {
		TableName: ORDER_PROCESSING,
		Key: {
			userID: userID,
			orderID_createdDate: orderIdCreatedDate
		},
		UpdateExpression:
			'set processStatus = :processStatus, reason = :reason, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':processStatus': CONSTANTS.CANCELLED_STATUS,
			':reason': reason,
			':updateUserData': [updateUserData],
      		':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		}
	};
	return dbClient.update(paramsOrder).promise();
};

const deleteDoctorPatient = async (params, DOCTOR_PATIENT) => {
	const updateUserData = params.updateUserData;
	const paramsDocpatient = {
		TableName: DOCTOR_PATIENT,
		Key: {
			doctorID: params.doctorId,
			userID: params.userId
		},
		UpdateExpression:
			'set processStatus = :processStatus, deleteFlag = :deleteFlag, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':processStatus': CONSTANTS.CANCELLED_STATUS,
			':deleteFlag': CONSTANTS.DELETE_FLAG,
			':updateUserData': [updateUserData],
      		':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		}
	};
	return dbClient.update(paramsDocpatient).promise();
};
module.exports.verifyHospital = verifyHospital;
module.exports.deleteOrderProcessing = deleteOrderProcessing;
module.exports.deleteDoctorPatient = deleteDoctorPatient;
module.exports.verifyOrder = verifyOrder;
