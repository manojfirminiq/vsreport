exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_ID_REQUIRED: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'User Id is required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	ORDER_ALREADY_PROCESSED: {
		CODE: 'ORDER_ALREADY_PROCESSED',
		MESSAGE: 'Order already processed'
	},
	ORDER_ALREADY_CANCELLED: {
		CODE: 'ORDER_ALREADY_CANCELLED',
		MESSAGE: 'Order already cancelled'
	},
	REASON_REQUIRED: {
		CODE: 'REASON_REQUIRED',
		MESSAGE: 'Reason is required'
	}
};
exports.NOT_PROCESSED_STATUS = 1;
exports.CANCELLED_STATUS = 12;
exports.ORDER_PROCESSING = process.env.ORDER_PROCESSING;
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.DOCTOR_TABLE = process.env.DOCTOR_TABLE;
exports.ERROR = 'error';
exports.RPM = 'rpm';
exports.DELETE_FLAG = true;
