const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./db');
const trackPatient = require('track_patients_record');

const applyValidation = async params => {
	try {
		if (!params.userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
			});
		}
		if (!params.reason) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.REASON_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.REASON_REQUIRED.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(params.hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		const verifyOrder = await DB.verifyOrder(
			params.userId,
			hospitalTable.ORDER_PROCESSING
		);
		for (let i = 0; i < verifyOrder.length; i++) {
			if (verifyOrder[i].processStatus === CONSTANTS.NOT_PROCESSED_STATUS) {
				await DB.deleteOrderProcessing(
					verifyOrder[i].userID,
					verifyOrder[i].orderID_createdDate,
					hospitalTable.ORDER_PROCESSING,
					params.reason,
					params.updateUserData
				);
				await DB.deleteDoctorPatient(params, hospitalTable.DOCTOR_PATIENT);
				return Promise.resolve({
					success: true
				});
			} else if (verifyOrder[i].processStatus === CONSTANTS.CANCELLED_STATUS) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.ORDER_ALREADY_CANCELLED.MESSAGE,
					errorCode: CONSTANTS.ERRORS.ORDER_ALREADY_CANCELLED.CODE
				});
			} else {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.ORDER_ALREADY_PROCESSED.MESSAGE,
					errorCode: CONSTANTS.ERRORS.ORDER_ALREADY_PROCESSED.CODE
				});
			}
		}
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
