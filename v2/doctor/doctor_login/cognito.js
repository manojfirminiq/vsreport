/* eslint-disable camelcase */
'use strict';
const CLIENT_ID = process.env.CLIENTID;
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
let cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const CONSTANTS = require('./CONSTANTS');

const reformatToken = token => {
	token = JSON.parse(JSON.stringify(token));

	return {
		success: true,
		accessToken: token.IdToken,
		refreshToken: token.RefreshToken,
		expiresIn: token.ExpiresIn,
		tokenType: token.TokenType,
		updateToken: token.AccessToken
	};
};

const changePassword = (email, password, userType) => {
	return new Promise(function (resolve, reject) {
		const params = {
			Password: password,
			UserPoolId: process.env.USER_POOL_ID,
			Username: email,
			Permanent: true
		};
		cognitoidentityserviceprovider.adminSetUserPassword(params, function (
			err,
			data
		) {
			if (err) {
				console.log(err);
				reject(err);
			} else {
				resolve();
			}
		});
	});
};

const login = async ({ emailAddress, password, refreshToken }) => {
	const params = {
		AuthFlow: 'USER_PASSWORD_AUTH',
		ClientId: CLIENT_ID,
		AuthParameters: { USERNAME: emailAddress, PASSWORD: password }
	};

	cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
		region: process.env.REGION
	});

	if (refreshToken) {
		params.AuthFlow = 'REFRESH_TOKEN';
		params.AuthParameters = { REFRESH_TOKEN: refreshToken };
	}

	let auth = {};
	try {
		auth = await cognitoidentityserviceprovider.initiateAuth(params).promise();

		if (
			auth.ChallengeName === CONSTANTS.SMS_MFA &&
			auth.ChallengeName.length > 0
		) {
			const sessionToken = auth.Session;
			const userName = auth.ChallengeParameters.USER_ID_FOR_SRP;
			const phoneNumber = auth.ChallengeParameters.CODE_DELIVERY_DESTINATION || null; // Response will be +*******0052

			return {
				success: true,
				userName: userName,
				sessionToken: sessionToken,
				phoneNumber: phoneNumber
			};
		} else if (auth.error) {
			throw auth.error;
		}
		return reformatToken(auth.AuthenticationResult);
	} catch (error) {
		console.log(error);

		const checkInRegion_2 = process.env.CHECKIN_REGION_2
			? Number(process.env.CHECKIN_REGION_2)
			: false;
		if (
			checkInRegion_2 &&
			error.code === CONSTANTS.COGNITO_ERROR.INVALID_GRANT
		) {
			cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
				region: process.env.REGION_2
			});
			params.ClientId = process.env.CLIENT_ID_2;
			auth = await cognitoidentityserviceprovider
				.initiateAuth(params)
				.promise();
			if (
				!auth.error &&
				(!auth.ChallengeName || auth.ChallengeName.length === 0)
			) {
				// if exists migrate user to region 1
				try {
					await changePassword(emailAddress, password);

					cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider(
						{
							region: process.env.REGION
						}
					);
					params.ClientId = CLIENT_ID;
					auth = await cognitoidentityserviceprovider
						.initiateAuth(params)
						.promise();
					return reformatToken(auth.AuthenticationResult);
				} catch (err) {
					console.log(err);
				}
			}
		}
		const response = {
			success: false,
			errorCode: '',
			message: error.message
		};
		switch (error.code) {
		case CONSTANTS.COGNITO_ERROR.INVALID_GRANT:
		case CONSTANTS.COGNITO_ERROR.USER_NOT_FOUND:
			response.errorCode = CONSTANTS.ERROR_CODES.INVALID_GRANT;
			response.message = CONSTANTS.VALIDATION_MESSAGES.INVALID_GRANT;
			break;
		case CONSTANTS.COGNITO_ERROR.USER_NOT_CONFIRMED:
			response.errorCode = CONSTANTS.ERROR_CODES.USER_NOT_CONFIRMED;
			break;
		default:
			response.errorCode = CONSTANTS.ERROR_CODES.INVALID_GRANT;
			response.message = CONSTANTS.VALIDATION_MESSAGES.INVALID_GRANT;
		}
		return Promise.reject(response);
	}
};

module.exports.login = login;
