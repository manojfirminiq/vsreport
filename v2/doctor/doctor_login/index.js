const CONSTANTS = require('./CONSTANTS.js');
const COGNITO = require('./cognito');
const jwt = require('jsonwebtoken');
const DB = require('./dynamodb');

const applyValidation = async ({ emailAddress, password, refreshToken, timeZone }) => {
	try {
		if (refreshToken) {
			const tokens = await COGNITO.login({ refreshToken });
			if (tokens && tokens.success && tokens.accessToken) {
				const decodedJwt = jwt.decode(tokens && tokens.accessToken, {
					complete: true
				});
				const userId = decodedJwt.payload['custom:userId'];
				const staffID = decodedJwt.payload['custom:staffID'] || null;
				if (staffID !== null) {
					tokens.staffID = staffID;
					tokens.userType = CONSTANTS.STAFF;
				} else {
					tokens.doctorId = userId;
					tokens.userType = CONSTANTS.DOCTOR;
				}
			}
			if (tokens.errorCode) {
				return Promise.resolve({
					success: false,
					message: 'Invalid Refresh Token',
					errorCode: 'INVALID_REFRESH_TOKEN'
				});
			} else {
				tokens.refreshToken = refreshToken;
				return Promise.resolve(tokens);
			}
		} else {
			let lastloginTime = 0;
			if (!emailAddress) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.VALIDATION_MESSAGES.EMAIL_REQ,
					errorCode: 'REQUIRED_PARAMETER'
				});
			}
			if (!password) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.VALIDATION_MESSAGES.PASSWORD_REQ,
					errorCode: 'REQUIRED_PARAMETER'
				});
			}
			emailAddress = emailAddress.toLowerCase();
			const response = await COGNITO.login({ emailAddress, password });

			if (response && response.success && response.accessToken) {
				const decodedJwt = jwt.decode(response && response.accessToken, {
					complete: true
				});
				const userId = decodedJwt.payload['custom:userId'];
				const hospitalID = decodedJwt.payload['custom:hospitalID'];
				const staffID = decodedJwt.payload['custom:staffID'] || null;
				const hospitalDetail = await DB.getDbIdentifierFromHospital(hospitalID);

				if (staffID !== null) {
					const staffProfile = await DB.getStaffProfile(
						userId,
						hospitalDetail[0].dbIdentifier,
						staffID
					);
					if (
						staffProfile[0].deleteFlag &&
						staffProfile[0].deleteFlag === CONSTANTS.DELETE_FLAG
					) {
						return Promise.resolve({
							success: false,
							message: CONSTANTS.VALIDATION_MESSAGES.INVALID_GRANT,
							errorCode: CONSTANTS.ERROR_CODES.INVALID_GRANT
						});
					}
					lastloginTime = staffProfile[0].lastLoginTime
						? staffProfile[0].lastLoginTime
						: 0;
					await DB.updateStaffProfile(
						userId,
						hospitalDetail[0].dbIdentifier,
						staffID,
						timeZone,
						staffProfile[0]
					);
					response.userType = CONSTANTS.STAFF;
					response.staffID = staffID;
				} else {
					const doctorProfile = await DB.getDoctorProfile(
						userId,
						hospitalDetail[0].dbIdentifier
					);
					lastloginTime = doctorProfile[0].lastLoginTime
						? doctorProfile[0].lastLoginTime
						: 0;
					await DB.updateDoctorProfile(userId, hospitalDetail[0].dbIdentifier, timeZone, doctorProfile[0], lastloginTime);
					response.userType = CONSTANTS.DOCTOR;
					response.doctorId = userId;
				}
			}
			response.lastLoginTime = lastloginTime;
			return response;
		}
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: error.message ? error.message : error,
			errorCode: error.errorCode ? error.errorCode : 'INTERNAL_SERVER_ERROR'
		});
	}
};

exports.handler = async (event, context, callback) => {
	const response = await applyValidation(event);
	callback(null, response);
};
