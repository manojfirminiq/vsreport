const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');

const getDbIdentifierFromHospital = async hospitalID => {
	const params = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospitalID = :hospitalID',
		ExpressionAttributeValues: {
			':hospitalID': hospitalID
		},
		ExpressionAttributeNames: {
			'#hospitalID': 'hospitalID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();

	return Count > 0 ? Items : false;
};

const getDoctorProfile = async (userID, dbIdentifier) => {
	const params = {
		TableName:
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PROFILE,
		KeyConditionExpression: '#userID = :userID',
		ExpressionAttributeValues: {
			':userID': userID
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const getStaffProfile = async (doctorId, dbIdentifier, staffID) => {
	const params = {
		TableName:
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			dbIdentifier +
			'_' +
			CONSTANTS.STAFF_TABLE_NAME,
		KeyConditionExpression: '#doctorId = :doctorId AND #staffID = :staffID',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':staffID': staffID
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#staffID': 'staffID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const updateDoctorProfile = async (doctorId, dbIdentifier, timeZone, doctorProfile, prevLoginTime) => {
	const tZone = doctorProfile.attributes.timeZone ? doctorProfile.attributes.timeZone : String(timeZone);
	const params = {
		TableName:
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PROFILE,
		Key: {
			userID: doctorId
		},
		UpdateExpression: 'SET lastLoginTime = :lastLoginTime, prevLoginTime = :prevLoginTime , #attributes.#timeZone = :timeZone',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#timeZone': 'timeZone'
		},
		ExpressionAttributeValues: {
			':lastLoginTime': new Date().getTime(),
			':timeZone': tZone,
			':prevLoginTime': prevLoginTime

		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const updateStaffProfile = async (doctorId, dbIdentifier, staffID, timeZone, staffProfile) => {
	const tZone = staffProfile.attributes.timeZone ? staffProfile.attributes.timeZone : String(timeZone);
	const params = {
		TableName:
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			dbIdentifier +
			'_' +
			CONSTANTS.STAFF_TABLE_NAME,
		Key: {
			doctorID: doctorId,
			staffID: staffID
		},
		UpdateExpression: 'SET lastLoginTime = :lastLoginTime, #attributes.#timeZone = :timeZone',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#timeZone': 'timeZone'
		},
		ExpressionAttributeValues: {
			':lastLoginTime': new Date().getTime(),
			':timeZone': tZone
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

module.exports.getDbIdentifierFromHospital = getDbIdentifierFromHospital;
module.exports.getDoctorProfile = getDoctorProfile;
module.exports.updateDoctorProfile = updateDoctorProfile;
module.exports.updateStaffProfile = updateStaffProfile;
module.exports.getStaffProfile = getStaffProfile;
