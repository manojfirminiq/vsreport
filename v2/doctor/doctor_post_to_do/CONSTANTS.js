exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};

exports.TO_DO_TABLE = process.env.TO_DO_TABLE;
exports.RPM = 'rpm';
