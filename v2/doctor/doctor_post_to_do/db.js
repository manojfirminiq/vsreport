const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Count, Items } = await dbClient.query(paramsTable).promise();
	if (Count <= 0) {
		return false;
	} else {
		return (
			process.env.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			Items[0].dbIdentifier +
			'_' +
			CONSTANTS.TO_DO_TABLE
		);
	}
};

const saveToDo = async (params, hospitalTable) => {
	const paramsToDo = {
		TableName: hospitalTable,
		Item: params
	};
	return dbClient.put(paramsToDo).promise();
};

module.exports.verifyHospital = verifyHospital;
module.exports.saveToDo = saveToDo;
