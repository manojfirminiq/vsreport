const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');
const trackPatient = require('track_patients_record');

const applyValidation = async ({
	doctorId,
	hospitalID,
	updateUserData,
	body: { itemType, userId, description, dueDate }
}) => {
	try {
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const params = {
			userID: userId,
			attributes: {
				itemType: itemType,
				description: description
			},
			dueDate: dueDate,
			doctorID: doctorId,
			createdDate: new Date().getTime(),
			modifiedDate: new Date().getTime(),
			updates: []
		};

		params.updates.push(updateUserData);

		if (!params.userID) {
			delete params.userID;
		}

		await DB.saveToDo(params, hospitalTable);

		return Promise.resolve({
			success: true
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
