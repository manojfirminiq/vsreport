const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');
const ses = new AWS.SES({
	region: process.env.REGION
});

const toAddresses = [];
let base64data;

const fetchDoctorPatient = async (userID, dbIdentifier) => {
	const tableName =
		process.env.STAGE +
		CONSTANTS.RPM +
		dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PATIENT_TABLE;
	const params = {
		TableName: tableName,
		IndexName: 'userID-doctorID-index',
		KeyConditionExpression: 'userID = :id',
		ExpressionAttributeValues: {
			':id': userID
		},
		ScanIndexForward: false
	};
	const { Items, Count } = await dbClient.query(params).promise();
	return Count ? Items[0] : false;
};

const fetchDoctorProfile = async (userID, dbIdentifier) => {
	const tableName =
		process.env.STAGE +
		CONSTANTS.RPM +
		dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PROFILE_TABLE;
	const params = {
		TableName: tableName,
		KeyConditionExpression: 'userID = :id',
		ExpressionAttributeValues: {
			':id': userID
		},
		ScanIndexForward: false
	};
	const { Items, Count } = await dbClient.query(params).promise();
	return Count ? Items[0] : false;
};

const getHospitals = async () => {
	const params = {
		TableName: CONSTANTS.MASTER_HOSPITAL_TABLE
	};

	const { Items } = await dbClient.scan(params).promise();
	const hospitals = [];
	for (const item of Items) {
		hospitals.push(item);
	}

	return hospitals;
};

const identifyTable = (hospitals, eventSource) => {
	let dbIdentifier = null;
	Object.keys(hospitals).forEach(function (key) {
		if (eventSource.includes(hospitals[key].dbIdentifier)) {
			dbIdentifier = hospitals[key].dbIdentifier;
		}
	});

	return dbIdentifier;
};

const sendEmailToDoctor = async (toAddresses, base64data, value) => {
	try {
		const params = {
			Destination: {
				ToAddresses: toAddresses
			},
			Message: {
				Body: {
					Text: {
						Data: `${CONSTANTS.EMAIL_CONTENT} ${CONSTANTS.FE_URL}patient/${base64data}?view=${value}`
					}
				},
				Subject: {
					Data: CONSTANTS.EMAIL_SUBJECT
				}
			},
			Source: CONSTANTS.SENDER_EMAIL
		};
		try {
			return ses.sendEmail(params).promise();
		} catch (err) {
			return Promise.resolve({
				success: false,
				data: err
			});
		}
	} catch (err) {
		console.log('error', err);
	}
};
exports.handler = async event => {
	if (event.Records) {
		for (const record of event.Records) {
			const recordToCheck =
				record.eventName === CONSTANTS.constants.insert ||
				record.eventName === CONSTANTS.constants.modify
					? AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage)
					: AWS.DynamoDB.Converter.unmarshall(record.dynamodb.OldImage);
			if (
				recordToCheck.attributes.alertType === CONSTANTS.ALERT_TYPE &&
				(recordToCheck.attributes.averageType ===
					CONSTANTS.SEVEN_DAYS_AVG_TYPE ||
					recordToCheck.attributes.averageType ===
						CONSTANTS.TEN_READINGS_AVG_TYPE)
			) {
				console.log('Alert has average type');
				const hospitals = await getHospitals();
				const dbIdentifier = identifyTable(
					hospitals,
					event.Records[0].eventSourceARN
				);
				const docPatient = await fetchDoctorPatient(
					recordToCheck.actualUserID,
					dbIdentifier
				);
				if (docPatient) {
					const docProfile = await fetchDoctorProfile(
						docPatient.doctorID,
						dbIdentifier
					);
					if (
						docProfile &&
						docProfile.emailAddress &&
						docProfile.subscriptionFlag
					) { toAddresses.push(String(docProfile.emailAddress)); }
					if (
						docProfile &&
						docProfile.attributes &&
						docProfile.attributes.secondaryEmailAddress &&
						docProfile.secondaryEmailSubscription
					) {
						toAddresses.push(
							String(docProfile.attributes.secondaryEmailAddress)
						);
					}
					const patientUserID = docPatient.userID;
					base64data = Buffer.from(patientUserID)
						.toString('base64')
						.replace(/=/gi, '');
					try {
						await sendEmailToDoctor(toAddresses, base64data, CONSTANTS.TYPE_BP);
						return Promise.resolve({
							success: true,
							message: CONSTANTS.SUCCESS.MESSAGE
						});
					} catch (error) {
						console.log(
							'Error in sending email, please check whether doctor has emailAddress & subscribed'
						);
						return Promise.resolve({
							success: false,
							message: CONSTANTS.ERROR.MESSAGE
						});
					}
				}
			}
		}
	}
};
