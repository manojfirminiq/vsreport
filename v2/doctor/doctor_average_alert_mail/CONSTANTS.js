exports.RPM = '_rpm_';
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
exports.MASTER_HOSPITAL_TABLE = process.env.MASTER_HOSPITAL_TABLE;
exports.SENDER_EMAIL = process.env.SENDER_EMAIL;
exports.FE_URL = process.env.FE_URL;
exports.ALERT_TYPE = 'average';
exports.SEVEN_DAYS_AVG_TYPE = '7day';
exports.TEN_READINGS_AVG_TYPE = '10readings';
exports.EMAIL_SUBJECT = 'VitalSight Alert Notification';
exports.EMAIL_CONTENT =
	'Please click here to view your patients alert on the VitalSight program.';
exports.SUCCESS = {
	MESSAGE: 'Successfully Sent Mail to Doctor'
};

exports.ERROR = {
	MESSAGE:
		'Error in sending email, please check whether doctor has emailAddress & subscribed'
};

exports.constants = {
	insert: 'INSERT',
	modify: 'MODIFY'
};
exports.TYPE_BP = 'bp';
