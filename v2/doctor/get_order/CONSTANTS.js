exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_ID_REQUIRED: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'User Id is required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital Id is required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	ORDER_NOT_FOUND: {
		CODE: 'ORDER_NOT_FOUND',
		MESSAGE: 'Order not found'
	}
};

exports.RPM = '_rpm_';
exports.DEVICE_SIZE_SMALL = 'Small';
exports.DEVICE_SIZE_LARGE = 'Large';
exports.BP = 'BP';
exports.WEIGHT = 'Weight';
exports.DEVICE_CODE_BP = 1;
exports.DEVICE_CODE_WEIGHT = 2;
