const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
const ORDER_PROCESSING_TABLE = process.env.ORDER_PROCESSING_TABLE;
const STAGE = process.env.STAGE;

const verifyHospital = async hospitalId => {
	const params = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};

	const { Items } = await dbClient.query(params).promise();

	if (Items.length === 0) {
		return false;
	}

	return `${STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${ORDER_PROCESSING_TABLE}`;
};

const getOrderDetails = async (userId, orderProcessingTable) => {
	const params = {
		TableName: orderProcessingTable,
		KeyConditionExpression: '#userId = :uid',
		ExpressionAttributeNames: {
			'#userId': 'userID'
		},
		ExpressionAttributeValues: {
			':uid': userId
		},
		ScanIndexForward: false
	};

	const { Count, Items } = await dbClient.query(params).promise();
	console.log('Count: ', Count);

	if (Items.length === 0) return false;
	return Items[0].attributes ? Items[0].attributes : null;
};

module.exports.verifyHospital = verifyHospital;
module.exports.getOrderDetails = getOrderDetails;
