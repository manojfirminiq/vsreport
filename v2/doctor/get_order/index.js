const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');

const applyValidation = async ({ hospitalID, userID }) => {
	try {
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		if (!userID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
			});
		}

		const orderProcessingTable = await DB.verifyHospital(hospitalID);
		if (!orderProcessingTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const orderDBResp = await DB.getOrderDetails(userID, orderProcessingTable);

		if (!orderDBResp) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.ORDER_NOT_FOUND.CODE,
				message: CONSTANTS.ERRORS.ORDER_NOT_FOUND.MESSAGE
			});
		}

		const order = formatOrder(orderDBResp);
		return Promise.resolve({
			success: true,
			order: order
		});
	} catch (error) {
		console.log('Error', error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

const formatOrder = order => {
	const response = {
		patientName: order.name || null,
		patientAddress: order.address || null,
		patientMRN: order.mrNumber || null,
		pacemaker: order.pacemaker || 0,
		patientDOB: order.dateOfBirth || null,
		patientEmail: order.ehrEmailAddress ? order.ehrEmailAddress[0] : null,
		device: null,
		patientThreshold: order.threshold || null,
		gender: order.gender || null,
		language: order.language || null,
		icdCode: order.icdCode || null,
		officePhone: order.officePhone || null,
		homePhone: order.homePhone || null,
		mobilePhone: order.mobilePhone || null,
		shippingAddress: order.shippingAddress || null
	};

	const devices = [];

	if (order.clinicalInfo) {
		for (const device of order.clinicalInfo) {
			const deviceObj = {
				type: null,
				size: null
			};
			switch (device.code) {
			case CONSTANTS.DEVICE_CODE_BP:
				deviceObj.type = CONSTANTS.BP;
				break;
			case CONSTANTS.DEVICE_CODE_WEIGHT:
				deviceObj.type = CONSTANTS.WEIGHT;
				break;
			default:
				break;
			}

			if (
				device.description &&
				device.description
					.toLowerCase()
					.includes(CONSTANTS.DEVICE_SIZE_SMALL.toLowerCase())
			) {
				deviceObj.size = CONSTANTS.DEVICE_SIZE_SMALL;
				devices.push(deviceObj);
			} else if (
				device.description &&
				device.description
					.toLowerCase()
					.includes(CONSTANTS.DEVICE_SIZE_LARGE.toLowerCase())
			) {
				deviceObj.size = CONSTANTS.DEVICE_SIZE_LARGE;
				devices.push(deviceObj);
			}
		}

		response.device = devices;
	}
	return response;
};
exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
