exports.ERRORS = {
	INVALID_OLD_PASSWORD: {
		CODE: 'INVALID_OLD_PASSWORD',
		MESSAGE: 'Invalid or wrong password'
	},
	WEAK_PASSWORD: {
		CODE: 'WEAK_PASSWORD',
		MESSAGE: 'Password should contain Number, Special character, Uppercase and Lowercase letter'
	},
	LIMIT_EXCEEDED: {
		CODE: 'LIMIT_EXCEEDED',
		MESSAGE: 'Attempt limit exceeded, please try after some time'
	},
	TOO_MANY_REQUESTS: {
		CODE: 'TOO_MANY_REQUESTS',
		MESSAGE: 'Too many requests, please try after some time'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal server error.'
	}
};

exports.COGNITO_ERROR = {
	INVALID_OLD_PASSWORD: 'InvalidParameterException', // fails to satisfy constraint - Member must have length greater than or equal to 6
	WEAK_PASSWORD: 'InvalidPasswordException',
	LIMIT_EXCEEDED: 'LimitExceededException',
	PASSWORD_EXCEPTION: 'NotAuthorizedException', // throws when mismatch occurs on password - Incorrect username or password.
	TOO_MANY_REQUESTS: 'TooManyRequestsException'
};
