const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const CONSTANTS = require('./CONSTANTS');

const cognitoUpdatePassword = async (password, oldPassword, accessToken) => {
	try {
		const params = {
			AccessToken: accessToken,
			PreviousPassword: oldPassword,
			ProposedPassword: password
		};
		await new AWS.CognitoIdentityServiceProvider()
			.changePassword(params)
			.promise();
		return {
			success: true,
			message: 'Password updated succesfully.'
		};
	} catch (error) {
		console.log('error while changing password', error);
		const response = {
			success: false
			// message: error.message
		};
		if (
			error.code &&
			(error.code === CONSTANTS.COGNITO_ERROR.INVALID_OLD_PASSWORD ||
				error.code === CONSTANTS.COGNITO_ERROR.PASSWORD_EXCEPTION)
		) {
			response.errorCode = CONSTANTS.ERRORS.INVALID_OLD_PASSWORD.CODE;
			response.message = CONSTANTS.ERRORS.INVALID_OLD_PASSWORD.MESSAGE;
		} else if (
			error.code &&
			error.code === CONSTANTS.COGNITO_ERROR.WEAK_PASSWORD
		) {
			response.errorCode = CONSTANTS.ERRORS.WEAK_PASSWORD.CODE;
			response.message = CONSTANTS.ERRORS.WEAK_PASSWORD.MESSAGE;
		} else if (
			error.code &&
			error.code === CONSTANTS.COGNITO_ERROR.LIMIT_EXCEEDED
		) {
			response.errorCode = CONSTANTS.ERRORS.LIMIT_EXCEEDED.CODE;
			response.message = CONSTANTS.ERRORS.LIMIT_EXCEEDED.MESSAGE;
		} else if (
			error.code &&
			error.code === CONSTANTS.COGNITO_ERROR.TOO_MANY_REQUESTS
		) {
			response.errorCode = CONSTANTS.ERRORS.TOO_MANY_REQUESTS.CODE;
			response.message = CONSTANTS.ERRORS.TOO_MANY_REQUESTS.MESSAGE;
		} else {
			response.errorCode = CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE;
			response.message = CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE;
		}

		return response;
	}
};

const applyValidation = async ({
	accessToken,
	emailAddress,
	body: { password, oldPassword }
}) => {
	const response = await cognitoUpdatePassword(
		password,
		oldPassword,
		accessToken
	);
	return response;
};

exports.handler = async (event, context, callback) => {
	const accessToken = event.body.updateToken;
	event.accessToken = accessToken;
	const response = await applyValidation(event);
	callback(null, response);
};
