const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');

const connectionTable = `${CONSTANTS.STAGE}${CONSTANTS.RPM}${CONSTANTS.TIMER_CONNECTION_TABLE}`;

const storeConnectionDetails = async (params) => {
	const dbParams = {
		TableName: connectionTable,
		Item: params
	};
	return dbClient.put(dbParams).promise();
};

const verifyHospital = async hospitalId => {
	const params = {
		TableName: CONSTANTS.HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	}
	return {
		doctorProfileTable: `${CONSTANTS.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${CONSTANTS.DOCTOR_PROFILE}`,
		dbIdentifier: Items[0].dbIdentifier
	};
};

const getDoctorProfile = async (doctorID, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorID
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		return Items[0];
	}
	return false;
};

module.exports.storeConnectionDetails = storeConnectionDetails;
module.exports.verifyHospital = verifyHospital;
module.exports.getDoctorProfile = getDoctorProfile;
