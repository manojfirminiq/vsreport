exports.TIMER_CONNECTION_TABLE = process.env.TIMER_CONNECTION_TABLE;
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = '_rpm_';
exports.DOCTOR_PROFILE = process.env.DOCTOR_PROFILE;
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;
