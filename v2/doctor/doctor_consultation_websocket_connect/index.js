const DB = require('./db');
const momentTimezone = require('moment-timezone');

exports.handler = async (event, context, callback) => {
	try {
		const { doctorID, hospitalID } = event.requestContext && event.requestContext.authorizer;
		console.log('connectionID', event.requestContext.connectionId);
		const { doctorProfileTable, dbIdentifier } = await DB.verifyHospital(hospitalID);
		const doctor = await DB.getDoctorProfile(doctorID, doctorProfileTable);
		let offSetInMilliSecs = 0;

		if (doctor && doctor.attributes && doctor.attributes.timeZone) {
			const timeZone = doctor.attributes.timeZone;
			offSetInMilliSecs = (momentTimezone.tz(timeZone).utcOffset()) * 60000;
		}

		const params = {
			doctorID: doctorID,
			hospitalID: hospitalID,
			connectionID: event.requestContext.connectionId,
			offSetInMilliSecs: offSetInMilliSecs,
			dbIdentifier: dbIdentifier
		};

		await DB.storeConnectionDetails(params);
		console.log('stored connection details in db');
		const secWebsocketProtocol = event.headers['Sec-WebSocket-Protocol'];
		const response = {
			statusCode: 200,
			headers: {
				'Sec-WebSocket-Protocol': secWebsocketProtocol
			}
		};
		console.log('reached end of connect');
		return response;
	} catch (err) {
		console.log('Socket Connnection Error', err);
		return { statusCode: 500 };
	}
};
