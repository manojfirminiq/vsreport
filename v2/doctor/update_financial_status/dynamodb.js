const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
const FINANCIAL_TRANSACTION_TABLE = process.env.FINANCIAL_TRANSACTION_TABLE;
const FINANCIAL_TRANSACTION_99453_TABLE =
	process.env.FINANCIAL_TRANSACTION_99453_TABLE;
const FINANCIAL_TRANSACTION_99457_TABLE =
	process.env.FINANCIAL_TRANSACTION_99457_TABLE;
const FINANCIAL_TRANSACTION_99458_TABLE =
	process.env.FINANCIAL_TRANSACTION_99458_TABLE;
const DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;

const verifyHospital = async hospitalId => {
	const params = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_PATIENT:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT_TABLE,
			DOCTOR_PROFILE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PROFILE_TABLE,
			FINANCIAL_TRANSACTION_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				FINANCIAL_TRANSACTION_TABLE,
			FINANCIAL_TRANSACTION_99453_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				FINANCIAL_TRANSACTION_99453_TABLE,
			FINANCIAL_TRANSACTION_99457_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				FINANCIAL_TRANSACTION_99457_TABLE,
			FINANCIAL_TRANSACTION_99458_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				FINANCIAL_TRANSACTION_99458_TABLE
		};
		return obj;
	}
};

const getFinTransaction99453Details = async (
	userID,
	FINANCIAL_TRANSACTION_99453_TABLE
) => {
	try {
		const params = {
			TableName: FINANCIAL_TRANSACTION_99453_TABLE,
			KeyConditionExpression: '#userID = :userID',
			ExpressionAttributeValues: {
				':userID': userID
			},
			ExpressionAttributeNames: {
				'#userID': 'userID'
			}
		};
		const { Count, Items } = await dbClient.query(params).promise();
		return Count > 0 ? Items : [];
	} catch (e) {
		console.log('error', e);
		throw e;
	}
};

const getFinTransaction99457Details = async (
	userID,
	endDate,
	FINANCIAL_TRANSACTION_99457_TABLE
) => {
	try {
		const params = {
			TableName: FINANCIAL_TRANSACTION_99457_TABLE,
			KeyConditionExpression: '#userID = :userID AND #endDate  = :endDate ',
			ExpressionAttributeValues: {
				':userID': userID,
				':endDate': endDate
			},
			ExpressionAttributeNames: {
				'#userID': 'userID',
				'#endDate': 'endDate'
			}
		};
		const { Count, Items } = await dbClient.query(params).promise();
		return Count > 0 ? Items : [];
	} catch (e) {
		console.log('error', e);
		throw e;
	}
};

const getFinTransaction99458Details = async (
	userID,
	createdDate,
	FINANCIAL_TRANSACTION_99458_TABLE
) => {
	try {
		const params = {
			TableName: FINANCIAL_TRANSACTION_99458_TABLE,
			KeyConditionExpression:
				'#userID = :userID AND #createdDate  = :createdDate ',
			ExpressionAttributeValues: {
				':userID': userID,
				':createdDate': createdDate
			},
			ExpressionAttributeNames: {
				'#userID': 'userID',
				'#createdDate': 'createdDate'
			}
		};
		const { Count, Items } = await dbClient.query(params).promise();
		return Count > 0 ? Items : [];
	} catch (e) {
		console.log('error', e);
		throw e;
	}
};

const getEhrFinTransactionDetails = async (
	userID,
	FINANCIAL_TRANSACTION_TABLE,
	billingStartDate
) => {
	try {
		const params = {
			TableName: FINANCIAL_TRANSACTION_TABLE,
			KeyConditionExpression:
				'#userID = :userID AND #billingStartDate = :billingStartDate',
			ExpressionAttributeValues: {
				':userID': userID,
				':billingStartDate': billingStartDate
			},
			ExpressionAttributeNames: {
				'#billingStartDate': 'billingStartDate',
				'#userID': 'userID'
			}
		};
		const { Count, Items } = await dbClient.query(params).promise();
		return Count > 0 ? Items : [];
	} catch (e) {
		console.log('error', e);
		throw e;
	}
};

const updateFinTransaction99453Details = async (
	userId,
	FINANCIAL_TRANSACTION_99453_TABLE,
	attributes,
	updateUserData
) => {
	const params = {
		TableName: FINANCIAL_TRANSACTION_99453_TABLE,
		Key: {
			userID: userId
		},
		UpdateExpression:
			'SET attributes = :attributes, modifiedDate = :modifiedDate, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':attributes': attributes,
			':modifiedDate': new Date().getTime(),
			':updateUserData': [updateUserData],
			':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const updateEhrFinTransactionTable = async (
	userID,
	FINANCIAL_TRANSACTION_TABLE,
	attributes,
	billingStartDate,
	updateUserData
) => {
	const params = {
		TableName: FINANCIAL_TRANSACTION_TABLE,
		Key: {
			userID: userID,
			billingStartDate: billingStartDate
		},
		UpdateExpression:
			'SET attributes = :attributes, modifiedDate = :modifiedDate, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':attributes': attributes,
			':modifiedDate': new Date().getTime(),
			':updateUserData': [updateUserData],
			':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const updateFinTransaction99457Details = async (
	userID,
	FINANCIAL_TRANSACTION_99457_TABLE,
	attributes,
	endDate,
	updateUserData
) => {
	const params = {
		TableName: FINANCIAL_TRANSACTION_99457_TABLE,
		Key: {
			userID: userID,
			endDate: endDate
		},
		UpdateExpression:
			'SET attributes = :attributes, modifiedDate = :modifiedDate, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':attributes': attributes,
			':modifiedDate': new Date().getTime(),
			':updateUserData': [updateUserData],
			':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const updateFinTransaction99458Details = async (
	userID,
	FINANCIAL_TRANSACTION_99458_TABLE,
	attributes,
	createdDate,
	updateUserData
) => {
	const params = {
		TableName: FINANCIAL_TRANSACTION_99458_TABLE,
		Key: {
			userID: userID,
			createdDate: createdDate
		},
		UpdateExpression:
			'SET attributes = :attributes, modifiedDate = :modifiedDate, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':attributes': attributes,
			':modifiedDate': new Date().getTime(),
			':updateUserData': [updateUserData],
			':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const getPatient = async (doctorId, userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #userId = :userId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userId': userId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userId': 'userID'
		}
	};
	return dbClient.query(params).promise();
};

module.exports.getPatient = getPatient;
module.exports.verifyHospital = verifyHospital;
module.exports.getFinTransaction99453Details = getFinTransaction99453Details;
module.exports.getEhrFinTransactionDetails = getEhrFinTransactionDetails;
module.exports.updateFinTransaction99453Details = updateFinTransaction99453Details;
module.exports.updateEhrFinTransactionTable = updateEhrFinTransactionTable;
module.exports.getFinTransaction99457Details = getFinTransaction99457Details;
module.exports.updateFinTransaction99457Details = updateFinTransaction99457Details;
module.exports.getFinTransaction99458Details = getFinTransaction99458Details;
module.exports.updateFinTransaction99458Details = updateFinTransaction99458Details;
