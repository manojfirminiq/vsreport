const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');
const trackPatient = require('track_patients_record');

const updateFinancialStatus = async (
	hospitalID,
	doctorId,
	userId,
	status,
	updateUserData,
	billingDetails
) => {
	let userID;
	try {
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		const { Items } = await DB.getPatient(
			doctorId,
			userId,
			hospitalTableDetails.DOCTOR_PATIENT
		);
		if (Items.length <= 0) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
			});
		} else {
			userID = Items[0].userID;

			for (let i = 0; i < billingDetails.length; i++) {
				if (billingDetails[i].code === CONSTANTS.CODE.PATIENT_CODE_99453) {
					const ehrFin99453Details = await DB.getFinTransaction99453Details(
						userID,
						hospitalTableDetails.FINANCIAL_TRANSACTION_99453_TABLE,
						billingDetails[i].dateOfService
					);
					if (ehrFin99453Details.length > 0) {
						const attributes = ehrFin99453Details[0].attributes
							? ehrFin99453Details[0].attributes
							: {};
						attributes.status = status.toUpperCase();
						attributes.claimTimestamp = new Date().getTime();
						await DB.updateFinTransaction99453Details(
							userID,
							hospitalTableDetails.FINANCIAL_TRANSACTION_99453_TABLE,
							attributes,
							updateUserData,
							billingDetails[i].dateOfService
						);
					} else {
						return Promise.resolve({
							success: false,
							message:
								CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION_99453.MESSAGE,
							errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION_99453.CODE
						});
					}
				} else if (
					billingDetails[i].code === CONSTANTS.CODE.PATIENT_CODE_99454
				) {
					const ehrFinDetails = await DB.getEhrFinTransactionDetails(
						userID,
						hospitalTableDetails.FINANCIAL_TRANSACTION_TABLE,
						parseInt(billingDetails[i].billingStartDate)
					);
					if (ehrFinDetails.length > 0) {
						const attributes = ehrFinDetails[0].attributes
							? ehrFinDetails[0].attributes
							: {};
						attributes.status = status.toUpperCase();
						attributes.claimTimestamp = new Date().getTime();
						await DB.updateEhrFinTransactionTable(
							userID,
							hospitalTableDetails.FINANCIAL_TRANSACTION_TABLE,
							attributes,
							parseInt(billingDetails[i].billingStartDate),
							updateUserData
						);
					} else {
						return Promise.resolve({
							success: false,
							message: CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION.MESSAGE,
							errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION.CODE
						});
					}
				} else if (
					billingDetails[i].code === CONSTANTS.CODE.PATIENT_CODE_99457
				) {
					const ehrFin99457Details = await DB.getFinTransaction99457Details(
						userID,
						parseInt(billingDetails[i].endDate),
						hospitalTableDetails.FINANCIAL_TRANSACTION_99457_TABLE
					);
					if (ehrFin99457Details.length > 0) {
						const attributes = ehrFin99457Details[0].attributes
							? ehrFin99457Details[0].attributes
							: {};
						attributes.status = status.toUpperCase();
						attributes.claimTimestamp = new Date().getTime();
						await DB.updateFinTransaction99457Details(
							userID,
							hospitalTableDetails.FINANCIAL_TRANSACTION_99457_TABLE,
							attributes,
							parseInt(billingDetails[i].endDate),
							updateUserData
						);
					} else {
						return Promise.resolve({
							success: false,
							message: CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION_99457.MESSAGE,
							errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION_99457.CODE
						});
					}
				} else if (
					billingDetails[i].code === CONSTANTS.CODE.PATIENT_CODE_99458
				) {
					const ehrFin99458Details = await DB.getFinTransaction99458Details(
						userID,
						parseInt(billingDetails[i].createdDate),
						hospitalTableDetails.FINANCIAL_TRANSACTION_99458_TABLE
					);
					if (ehrFin99458Details.length > 0) {
						const attributes = ehrFin99458Details[0].attributes
							? ehrFin99458Details[0].attributes
							: {};
						attributes.status = status.toUpperCase();
						attributes.claimTimestamp = new Date().getTime();
						await DB.updateFinTransaction99458Details(
							userID,
							hospitalTableDetails.FINANCIAL_TRANSACTION_99458_TABLE,
							attributes,
							parseInt(billingDetails[i].createdDate),
							updateUserData
						);
					} else {
						return Promise.resolve({
							success: false,
							message: CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION_99458.MESSAGE,
							errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND_TRANSACTION_99458.CODE
						});
					}
				}
			}
		}
		const response = {
			success: true,
			msg: CONSTANTS.STATUS_UPDATE.MESSAGE
		};
		return Promise.resolve(response);
	} catch (error) {
		return Promise.resolve({
			success: false,
			message: error,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

const applyValidation = async ({
	hospitalID,
	doctorId,
	userId,
	status,
	updateUserData,
	billingDetails
}) => {
	if (!hospitalID) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
			errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
		});
	}
	if (!userId) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE,
			errorCode: CONSTANTS.ERRORS.USER_ID_REQ.CODE
		});
	}
	if (!status || status.toUpperCase() !== CONSTANTS.SUBMITTED) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INVALID_STATUS.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INVALID_STATUS.CODE
		});
	}
	return await updateFinancialStatus(
		hospitalID,
		doctorId,
		userId,
		status,
		updateUserData,
		billingDetails
	);
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	if (event.body) {
		event = { ...event, ...event.body };
	}
	const response = await applyValidation(event);
	callback(null, response);
};
