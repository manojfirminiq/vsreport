exports.ERRORS = {
	EXPIRED_LINK: {
		CODE: 'EXPIRED_LINK',
		MESSAGE: 'Reset password link is expired or invalid. Please request another password reset email from the login page.'
	},
	WEAK_PASSWORD: {
		CODE: 'WEAK_PASSWORD',
		MESSAGE: 'Password should contain number, special character, uppercase and lowercase letter'
	},
	INTERNAL_SERVER_ERRROR: {
		CODE: 'INTERNAL_SERVER_ERRROR',
		MESSAGE: 'Internal server error.'
	}
};

exports.COGNITO_ERROR = {
	EXPIRED_LINK: 'ExpiredCodeException',
	WEAK_PASSWORD: 'InvalidPasswordException'

};

exports.SUCCESS_MESSAGE = 'Password updated successfully.';
