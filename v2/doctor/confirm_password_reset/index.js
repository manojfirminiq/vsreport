const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const lambda = new AWS.Lambda({
	region: process.env.REGION // change to your region
});
const CONSTANTS = require('./CONSTANTS');

const invalidateTokens = async emailAddress => {
	const payLoad = {
		emailAddress: emailAddress
	};
	return new Promise((resolve, reject) => {
		lambda.invoke(
			{
				FunctionName: process.env.CLIENT_LAMBDA,
				Payload: JSON.stringify(payLoad) // pass params
			},
			function (error, data) {
				if (error) {
					console.log('Error', error);
					reject(error);
				} else {
					resolve(data);
				}
			}
		);
	});
};

const cognitoUpdatePassword = async (emailAddress, password, resetCode) => {
	return new Promise(resolve => {
		new AWS.CognitoIdentityServiceProvider().confirmForgotPassword(
			{
				Username: emailAddress,
				ClientId: process.env.CLIENT_ID,
				ConfirmationCode: resetCode,
				Password: password
			},
			(err, res) => {
				if (err) {
					console.log('Error', err);
					if (
						err.code &&
						err.code === CONSTANTS.COGNITO_ERROR.EXPIRED_LINK
					) {
						err.errorCode = CONSTANTS.ERRORS.EXPIRED_LINK.CODE;
						err.message = CONSTANTS.ERRORS.EXPIRED_LINK.MESSAGE;
					} else if (
						err.code &&
						err.code === CONSTANTS.COGNITO_ERROR.WEAK_PASSWORD
					) {
						err.errorCode = CONSTANTS.ERRORS.WEAK_PASSWORD.CODE;
						err.message = CONSTANTS.ERRORS.WEAK_PASSWORD.MESSAGE;
					} else {
						err.errorCode = CONSTANTS.ERRORS.INTERNAL_SERVER_ERRROR.CODE;
						err.message = CONSTANTS.ERRORS.INTERNAL_SERVER_ERRROR.MESSAGE;
					}
					resolve({ success: false, code: err.errorCode, message: err.message });
				} else {
					resolve({ success: true, message: CONSTANTS.SUCCESS_MESSAGE });
				}
			}
		);
	});
};

const applyValidation = async ({ emailAddress, password, resetCode }) => {
	const response = await cognitoUpdatePassword(
		emailAddress,
		password,
		resetCode
	);
	if (response.success) {
		await invalidateTokens(emailAddress);
	}

	return response;
};

exports.handler = async (event, context, callback) => {
	const response = await applyValidation(event);
	callback(null, response);
};
