exports.verifyUserType = function (params) {
	const payload = JSON.parse(JSON.stringify(params));
	delete payload.headers;
	const log = {
		USERID: params.staffID ? params.staffID : params.doctorId,
		ACTION: params.body ? params.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	if (params.staffID && params.doctorId) {
		return {
			success: true,
			data: {
				updatedUserType: 'STAFF_MEMBER',
				updatedBy: params.staffID,
				updatedAt: new Date().getTime()
			}
		};
	} else if (params.doctorId) {
		return {
			success: true,
			data: {
				updatedUserType: 'DOCTOR',
				updatedBy: params.doctorId,
				updatedAt: new Date().getTime()
			}
		};
	} else {
		return {
			success: false,
			message: 'User type not found',
			errorCode: 'USERTYPE_NOT_FOUND'
		};
	}
};
