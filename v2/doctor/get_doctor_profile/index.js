const AWS = require('aws-sdk');
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./dynamodb.js');
const s3Client = new AWS.S3();

const getUserProfileUrl = profilePicture => {
	return s3Client.getSignedUrl('getObject', {
		Bucket: CONSTANTS.PROFILE_BUCKET,
		Key: profilePicture,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	});
};

const applyValidation = async ({ doctorId, staffID, hospitalID }) => {
	try {
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		const hospitalTable = await DB.getHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		const response = {
			success: true
		};

		if (staffID) {
			const staffData = await DB.getStaffData(
				doctorId,
				staffID,
				hospitalTable.STAFF_TABLE
			);

			if (!staffData) {
				response.success = false;
				response.errorCode = CONSTANTS.ERRORS.USER_NOT_FOUND.CODE;
				response.message = CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE;
				return Promise.resolve(response);
			} else {
				staffData.subscriptionFlag = staffData.subscriptionFlag
					? CONSTANTS.SUBSCRIBEFLAG
					: CONSTANTS.UNSUBSCRIBEFLAG;
				staffData.qualification = staffData.qualification || null;
				staffData.phoneNumber = staffData.phoneNumber || null;
				staffData.secondaryEmailAddress =
					staffData.secondaryEmailAddress || null;
				staffData.profilePicture = staffData.profilePicture
					? getUserProfileUrl(staffData.profilePicture)
					: null;
				response.data = staffData;
			}
		} else {
			const doctorData = await DB.getDoctorData(
				doctorId,
				hospitalTable.DOCTOR_TABLE
			);
			if (!doctorData) {
				response.success = false;
				response.errorCode = CONSTANTS.ERRORS.USER_NOT_FOUND.CODE;
				response.message = CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE;
				return Promise.resolve(response);
			} else {
				doctorData.subscriptionFlag = doctorData.subscriptionFlag
					? CONSTANTS.SUBSCRIBEFLAG
					: CONSTANTS.UNSUBSCRIBEFLAG;
				doctorData.qualification = doctorData.qualification || null;
				doctorData.phoneNumber = doctorData.phoneNumber || null;
				doctorData.profilePicture = doctorData.profilePicture
					? getUserProfileUrl(doctorData.profilePicture)
					: null;
				response.data = doctorData;
			}
		}
		return response;
	} catch (error) {
		console.log('Error while fetching doctor profile', error);
		return Promise.resolve({
			success: false,
			message: error.message,
			errorCode: error.errorCode ? error.errorCode : 'INTERNAL_SERVER_ERROR'
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
