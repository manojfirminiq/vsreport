const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const DOCTOR_TABLE_NAME = process.env.DOCTOR_TABLE_NAME;
const STAFF_TABLE_NAME = process.env.STAFF_TABLE_NAME;
const HOSPITAL_TABLE_NAME = process.env.HOSPITAL_TABLE_NAME;

const getHospital = async hospitalId => {
	const params = {
		TableName: HOSPITAL_TABLE_NAME,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': hospitalId
		},
		ExpressionAttributeNames: {
			'#uid': 'hospitalID'
		}
	};

	const { Count, Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			Name: Count > 0 ? Items[0].name : null,
			AccountNumber: Items[0].facilityCode || null,
			DOCTOR_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_TABLE_NAME,
			STAFF_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				STAFF_TABLE_NAME
		};
		return obj;
	}
};

const getDoctorData = async (doctorID, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorID
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};

	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		const Item = Items.map(
			({
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			}) => ({ createdDate, ...rest, ...attributesRest })
		)[0];

		if (Item.hospitalID) {
			Item.hospital = await getHospital(Item.hospitalID);
			Item.hospital = Item.hospital.Name;
			Item.accountNumber = Item.hospital.AccountNumber;
			delete Item.hospitalID;
		}

		return Item;
	} else {
		return null;
	}
};

const getStaffData = async (doctorId, staffID, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #staffID = :staffID',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':staffID': staffID
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#staffID': 'staffID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		const Item = Items.map(
			({
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			}) => ({ ...rest, ...attributesRest })
		)[0];

		if (Item.hospitalID) {
			Item.hospital = await getHospital(Item.hospitalID);
			Item.hospital = Item.hospital.Name;
			Item.accountNumber = Item.hospital.AccountNumber;
			delete Item.hospitalID;
		}

		return Item;
	} else {
		return null;
	}
};

module.exports.getDoctorData = getDoctorData;
module.exports.getHospital = getHospital;
module.exports.getStaffData = getStaffData;
