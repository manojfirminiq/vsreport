exports.MESSAGES = {
	DOCTOR_PATIENT_UPDATE: 'Patient details updated successfully'
};
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = 'rpm';
exports.VALIDATION_MESSAGES = {
	USER_ID_REQ: 'User Id is required'
};
