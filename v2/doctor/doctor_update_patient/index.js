const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');

const getDoctorPatientDetails = async (doctorID, userID, docTableName) => {
	const params = {
		TableName: docTableName,
		IndexName: 'doctorID-userID-index',
		KeyConditionExpression: '#doctorID = :doctorID AND #userID = :userID',
		ExpressionAttributeValues: {
			':doctorID': doctorID,
			':userID': userID
		},
		ExpressionAttributeNames: {
			'#doctorID': 'doctorID',
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const updateDoctorPatient = async (
	doctorID,
	userID,
	attributesDoctorPatient,
	tableName
) => {
	const params = {
		TableName: tableName,
		Key: {
			doctorID: doctorID,
			userID: userID
		},
		UpdateExpression:
			'SET attributes = :attributes, modifiedDate = :modifiedDate',
		ExpressionAttributeValues: {
			':attributes': attributesDoctorPatient,
			':modifiedDate': new Date().getTime()
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const getDbIdenitifierFromHospital = async hospitalID => {
	const params = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospitalID = :hospitalID',
		ExpressionAttributeValues: {
			':hospitalID': hospitalID
		},
		ExpressionAttributeNames: {
			'#hospitalID': 'hospitalID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();

	return Count > 0 ? Items : false;
};

const updateDoctorPatientDetails = async event => {
	const hospitalDetail = await getDbIdenitifierFromHospital(event.hospitalID);
	const docTableName =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PATIENT;
	const doctorPatientDetails = await getDoctorPatientDetails(
		event.doctorID,
		event.body.userID,
		docTableName
	);
	if (doctorPatientDetails) {
		const docPatientattributes = doctorPatientDetails[0].attributes;

		const sourceEvent = Object.assign({}, event.body);
		delete sourceEvent.userID;
		Object.assign(docPatientattributes, sourceEvent);

		const tableName =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			hospitalDetail[0].dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PATIENT;
		await updateDoctorPatient(
			event.doctorID,
			event.body.userID,
			docPatientattributes,
			tableName
		);
		return Promise.resolve({
			success: true,
			message: CONSTANTS.MESSAGES.DOCTOR_PATIENT_UPDATE
		});
	}
};

const applyValidation = async event => {
	try {
		if (!event.body.userID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.USER_ID_REQ,
				errorCode: 'REQUIRED_PARAMETER'
			});
		}
		const obj = await updateDoctorPatientDetails(event);

		return obj;
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: error.message,
			errorCode: 'INTERNAL_SERVER_ERROR'
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
