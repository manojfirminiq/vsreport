const axios = require('axios');
const CONSTANTS = require('./CONSTANTS.js');
const INTERNAL_SERVER_ERROR = CONSTANTS.INTERNAL_SERVER_ERROR;
const API_CREDENTIAL = CONSTANTS.API_CREDENTIAL;
const UPS_API_ACCESS_LICENCE_NUMBER = CONSTANTS.UPS_API_ACCESS_LICENCE_NUMBER;
const API_URL = CONSTANTS.API_URL + CONSTANTS.REQUEST_OPTION;

// eslint-disable-next-line no-unused-vars
const isEmptyObject = obj => {
	return !Object.keys(obj).length;
};

// eslint-disable-next-line no-unused-vars
const isEmptyObjectAttributes = obj => {
	for (const atr in obj) {
		if (!obj[atr]) {
			return true;
		}
	}
	return false;
};

const getRequestHeader = () => {
	return {
		headers: {
			'Content-Type': 'application/json',
			AccessLicenseNumber: UPS_API_ACCESS_LICENCE_NUMBER,
			Username: API_CREDENTIAL.USERNAME,
			Password: API_CREDENTIAL.PASSWORD
		}
	};
};

const getAPIErrorCode = errorCode => {
	return CONSTANTS.API_ERROR_MESSAGES['ERR_' + errorCode]
		? CONSTANTS.API_ERROR_MESSAGES['ERR_' + errorCode].CODE
		: CONSTANTS.API_ERROR_MESSAGES.ERR_DEFAULT.CODE;
};

const processResponse = apiResponse => {
	const responseData = {};
	// if status code is 1 and NoCandidatesIndicator node present then error else success
	// if (apiResponse.data.XAVResponse.Response.ResponseStatus.Code === '1' && apiResponse.data.XAVResponse.hasOwnProperty('NoCandidatesIndicator')) {
	if (
		apiResponse.data.XAVResponse.Response.ResponseStatus.Code === '1' &&
		Object.prototype.hasOwnProperty.call(
			apiResponse.data.XAVResponse,
			'NoCandidatesIndicator'
		)
	) {
		// return error object
		responseData.success = false;
		responseData.message =
			CONSTANTS.API_ERROR_MESSAGES.ERR_INVALID_ADDRESS.MESSAGE;
		responseData.errorCode =
			CONSTANTS.API_ERROR_MESSAGES.ERR_INVALID_ADDRESS.CODE;
		return responseData;
	}

	// if candidate is present
	let candidate = [];
	if (
		apiResponse.data.XAVResponse.Candidate &&
		apiResponse.data.XAVResponse.Candidate.constructor.name === 'Array'
	) {
		candidate = apiResponse.data.XAVResponse.Candidate;
		responseData.candidate = candidate.map(candidateData => {
			candidateData.AddressKeyFormat.AddressLine =
				candidateData.AddressKeyFormat.AddressLine.constructor.name === 'String'
					? [candidateData.AddressKeyFormat.AddressLine]
					: candidateData.AddressKeyFormat.AddressLine;
			return candidateData;
		});
	} else if (
		apiResponse.data.XAVResponse.Candidate &&
		apiResponse.data.XAVResponse.Candidate.constructor.name === 'Object'
	) {
		apiResponse.data.XAVResponse.Candidate.AddressKeyFormat.AddressLine =
			apiResponse.data.XAVResponse.Candidate.AddressKeyFormat.AddressLine
				.constructor.name === 'String'
				? [apiResponse.data.XAVResponse.Candidate.AddressKeyFormat.AddressLine]
				: apiResponse.data.XAVResponse.Candidate.AddressKeyFormat.AddressLine;
		candidate.push(apiResponse.data.XAVResponse.Candidate);
		responseData.candidate = candidate;
	}

	return responseData;
};

const callValidateAPI = async ({
	addressLine1,
	addressLine2,
	city,
	state,
	zip,
	countryCode
}) => {
	const headers = getRequestHeader();

	addressLine1 = addressLine1 || '';
	addressLine2 = addressLine2 || '';
	city = city || '';
	state = state || '';
	zip = zip || '';
	countryCode = countryCode || '';

	const addressKeyFormat = {
		AddressLine: [addressLine1, addressLine2],
		PoliticalDivision2: city,
		PoliticalDivision1: state,
		PostcodePrimaryLow: zip,
		CountryCode: countryCode
	};

	const requestPayload = {
		XAVRequest: {
			AddressKeyFormat: addressKeyFormat
		}
	};

	try {
		const apiResponse = await axios.post(API_URL, requestPayload, headers);
		const responseData = processResponse(apiResponse);
		if (responseData.success === false) {
			return Promise.resolve({
				success: false,
				message: responseData.message,
				errorCode: responseData.errorCode
			});
		}
		return Promise.resolve({ success: true, data: responseData });
	} catch (error) {
		console.log('error', error);
		const errorMessage =
			error.response.data.response.errors &&
			error.response.data.response.errors[0]
				? error.response.data.response.errors[0].message
				: 'API Error.';
		const errorCode =
			error.response.data.response.errors &&
			error.response.data.response.errors[0]
				? error.response.data.response.errors[0].code
				: 'DEFAULT';
		return Promise.resolve({
			success: false,
			message: errorMessage,
			errorCode: getAPIErrorCode(errorCode)
		});
	}
};

const applyValidation = async ({ address }) => {
	try {
		if (!address) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.ADDRESS_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.ADDRESS_REQUIRED.CODE
			});
		}

		if (address && !address.zip) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.ZIP_CODE_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.ZIP_CODE_REQUIRED.CODE
			});
		}
		const isValidZip = new RegExp(/(^\d{5}$)|(^\d{5}-\d{4}$)/).test(address.zip);
		if (!isValidZip) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.MESSAGE,
				errorCode: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.CODE
			});
		}
		const apiResponse = await callValidateAPI(address);

		if (apiResponse.success) {
			return Promise.resolve({ success: true, data: apiResponse.data });
		} else {
			return Promise.resolve({
				success: false,
				message: apiResponse.message,
				errorCode: apiResponse.errorCode
			});
		}
	} catch (error) {
		return Promise.resolve({
			success: false,
			message: error,
			errorCode: INTERNAL_SERVER_ERROR
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	if (event.body) {
		event = event.body;
	}
	const response = await applyValidation(event);
	callback(null, response);
};
