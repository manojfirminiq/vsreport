exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_ID_REQ: {
		CODE: 'USER_ID_REQ',
		MESSAGE: 'User id required'
	},
	USER_NOT_FOUND: {
		CODE: 'USER_NOT_FOUND',
		MESSAGE: 'User not found'
	},
	RESOLVE_STATUS_REQ: {
		CODE: 'RESOLVE_STATUS_REQ',
		MESSAGE: 'Resolve status required'
	},
	MEASUREMENT_DATE_REQ: {
		CODE: 'MEASUREMENT_DATE_REQ',
		MESSAGE: 'Measurement date required'
	},
	TYPE_REQ: {
		CODE: 'TYPE_REQ',
		MESSAGE: 'Type required'
	},
	RESOLVE_STATUS_INVALID: {
		CODE: 'RESOLVE_STATUS_INVALID',
		MESSAGE: 'Invalid value in resolve status'
	},
	TYPE_NOT_FOUND: {
		CODE: 'TYPE_NOT_FOUND',
		MESSAGE: 'Type not found'
	},
	MEASUREMENT_DATE_INVALID: {
		CODE: 'MEASUREMENT_DATE_INVALID',
		MESSAGE: 'Invalid value in measurement date'
	},
	NO_RECORD_FOUND: {
		CODE: 'NO_RECORD_FOUND',
		MESSAGE: 'No record found'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};

exports.RESOLVE_STATUS = {
	ZERO: '0',
	ONE: '1'
};

exports.BP_ALERT_TABLE = process.env.BP_ALERT_TABLE;
exports.NON_BP_ALERT_TABLE = process.env.NON_BP_ALERT_TABLE;
exports.DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.WEIGHT = 'weight';
exports.BP = 'bp';
exports.HODES = 'hodes';
exports.RPM = '_rpm_';
