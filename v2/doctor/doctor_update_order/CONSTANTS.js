exports.MESSAGES = {
	DOCTOR_PATIENT_TABLE_UPDATE: 'Patient details updated successfully',
	ORDER_PROCESSING_TABLE_UPDATE:
		'Order processing details updated successfully',
	UPDATED_ORDER: 'Order updated successfully'
};
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.ORDER_PROCESSING = process.env.ORDER_PROCESSING;
exports.STAGE = process.env.STAGE;
exports.RPM = 'rpm';

exports.BP = 'BP';
exports.HUB = 'Hub';
exports.LARGE = 'large';
exports.OMRON = 'Omron';
exports.SCALE = 'Scale';
exports.BP_MODELS = ['BP7250', 'HEM-9210T'];
exports.WEIGHT_MODELS = ['BCM500', 'HN-290T'];

exports.ERRORS = {
	USER_NOT_FOUND: {
		CODE: 'USER_NOT_FOUND',
		MESSAGE: 'User not found'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	ORDER_NOT_FOUND: {
		CODE: 'ORDER_NOT_FOUND',
		MESSAGE: 'Order does not found'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR'
	},
	INVALID_PACEMAKER: {
		CODE: 'INVALID_PACEMAKER',
		MESSAGE: 'Pacemaker value is not valid'
	}
};

exports.VALIDATION_MESSAGES = {
	USER_ID_REQ: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'User ID is required'
	}
};
