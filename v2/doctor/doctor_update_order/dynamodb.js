const AWS = require('aws-sdk');
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');

const getDbIdenitifierFromHospital = async hospitalID => {
	const params = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospitalID = :hospitalID',
		ExpressionAttributeValues: {
			':hospitalID': hospitalID
		},
		ExpressionAttributeNames: {
			'#hospitalID': 'hospitalID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const getDoctorPatientDetails = async (doctorId, userId, docTableName) => {
	const params = {
		TableName: docTableName,
		IndexName: 'doctorID-userID-index',
		KeyConditionExpression: '#doctorID = :doctorID AND #userID = :userID',
		ExpressionAttributeValues: {
			':doctorID': doctorId,
			':userID': userId
		},
		ExpressionAttributeNames: {
			'#doctorID': 'doctorID',
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const updateDoctorPatient = async (
	doctorId,
	userId,
	shippingAddress,
	pacemaker,
	updateUserData,
	tableName
) => {
	const params = {
		TableName: tableName,
		Key: {
			doctorID: doctorId,
			userID: userId
		},
		UpdateExpression:
			'SET #attributes.#shippingAddress = :shippingAddress, #attributes.#pacemaker = :pacemaker, modifiedDate = :modifiedDate, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#shippingAddress': 'shippingAddress',
			'#pacemaker': 'pacemaker',
			'#updates': 'updates'
		},
		ExpressionAttributeValues: {
			':shippingAddress': shippingAddress,
			':pacemaker': Number(pacemaker),
			':modifiedDate': new Date().getTime(),
			':updateUserData': [updateUserData],
      		':empty_list': []
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const getOrderProcessingDetails = async (userId, orderProcessingTable) => {
	const params = {
		TableName: orderProcessingTable,
		KeyConditionExpression: '#userID = :userID',
		ExpressionAttributeValues: {
			':userID': userId
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const updateOrderProcessing = async (
	userId,
	orderIDCreatedDate,
	shippingAddress,
	clinicalInfo,
	pacemaker,
	updateUserData,
	TABLE_NAME
) => {
	const params = {
		TableName: TABLE_NAME,
		Key: {
			userID: userId,
			orderID_createdDate: orderIDCreatedDate
		},
		UpdateExpression:
			'SET #attributes.#shippingAddress = :shippingAddress, #attributes.#clinicalInfo = :clinicalInfo, #attributes.#pacemaker = :pacemaker, modifiedDate = :modifiedDate, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#shippingAddress': 'shippingAddress',
			'#clinicalInfo': 'clinicalInfo',
			'#pacemaker': 'pacemaker',
			'#updates': 'updates'
		},
		ExpressionAttributeValues: {
			':shippingAddress': shippingAddress,
			':clinicalInfo': clinicalInfo,
			':pacemaker': Number(pacemaker),
			':modifiedDate': new Date().getTime(),
			':updateUserData': [updateUserData],
      		':empty_list': []
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

module.exports.getDbIdenitifierFromHospital = getDbIdenitifierFromHospital;
module.exports.getDoctorPatientDetails = getDoctorPatientDetails;
module.exports.updateDoctorPatient = updateDoctorPatient;
module.exports.getOrderProcessingDetails = getOrderProcessingDetails;
module.exports.updateOrderProcessing = updateOrderProcessing;
