const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./dynamodb');
const trackPatient = require('track_patients_record');

const updateDoctorPatientDetails = async (event, hospitalDetail) => {
	const docTableName =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PATIENT;
	const doctorPatientDetails = await DB.getDoctorPatientDetails(
		event.doctorId,
		event.body.userId,
		docTableName
	);
	if (doctorPatientDetails) {
		await DB.updateDoctorPatient(
			event.doctorId,
			event.body.userId,
			(event.body.shippingAddress = event.body.shippingAddress
				? event.body.shippingAddress
				: doctorPatientDetails[0].attributes.shippingAddress),
			event.body.pacemaker = (event.body && event.body.pacemaker) || 0,
			event.updateUserData,
			docTableName
		);
		return Promise.resolve({
			success: true,
			message: CONSTANTS.MESSAGES.DOCTOR_PATIENT_TABLE_UPDATE
		});
	} else {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
			errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
		});
	}
};

const updateClinicalInfo = (devices) => {
	const clinicalInfo = [];
	// eslint-disable-next-line no-unused-vars
	let bpModelNo, weightModelNo;
	// event.body.device = event.body.device ? event.body.device : [];
	for (const device of devices) {
		if (device.type === CONSTANTS.BP) {
			clinicalInfo.push({
				code: 1,
				description: CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.BP
			});
			if (device.size.toLowerCase() === CONSTANTS.LARGE) {
				bpModelNo = CONSTANTS.BP_MODELS[1];
			} else {
				bpModelNo = CONSTANTS.BP_MODELS[0];
			}
		} else {
			clinicalInfo.push({
				code: 2,
				description:
					CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.SCALE
			});
			if (device.size.toLowerCase() === CONSTANTS.LARGE) {
				weightModelNo = CONSTANTS.WEIGHT_MODELS[1];
			} else {
				weightModelNo = CONSTANTS.WEIGHT_MODELS[0];
			}
		}
	}
	clinicalInfo.push({ code: 3, description: CONSTANTS.HUB });
	return clinicalInfo;
};

const updateOrderProcessingDetails = async (event, hospitalDetail) => {
	const orderProcessingTable =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.ORDER_PROCESSING;
	const orderProcessingDetails = await DB.getOrderProcessingDetails(
		event.body.userId,
		orderProcessingTable
	);
	if (orderProcessingDetails) {
		const clinicalInfo = event.body.device ? updateClinicalInfo(event.body.device) : orderProcessingDetails[0].attributes.clinicalInfo;

		await DB.updateOrderProcessing(
			event.body.userId,
			orderProcessingDetails[0].orderID_createdDate,
			(event.body.shippingAddress = event.body.shippingAddress
				? event.body.shippingAddress
				: orderProcessingDetails[0].attributes.shippingAddress),
			clinicalInfo,
			event.body.pacemaker = (event.body && event.body.pacemaker) || 0,
			event.updateUserData,
			orderProcessingTable
		);
		return Promise.resolve({
			success: true,
			message: CONSTANTS.MESSAGES.ORDER_PROCESSING_TABLE_UPDATE
		});
	} else {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.ORDER_NOT_FOUND.MESSAGE,
			errorCode: CONSTANTS.ERRORS.ORDER_NOT_FOUND.CODE
		});
	}
};

const applyValidation = async event => {
	try {
		if (!event.body.userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.USER_ID_REQ,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.USER_ID_REQ.CODE
			});
		}
		const hospitalDetail = await DB.getDbIdenitifierFromHospital(
			event.hospitalID
		);
		if (!hospitalDetail) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		if (!event.body.pacemaker) {
			event.body.pacemaker = 0;
		} else {
			const validatePacemaker = new RegExp(/^[0|1]{1}$/).test(event.body.pacemaker);
			if (!validatePacemaker) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.INVALID_PACEMAKER.MESSAGE,
					errorCode: CONSTANTS.ERRORS.INVALID_PACEMAKER.CODE
				});
			}
		}
		const obj = {
			updateDoctorPatientDetails: await updateDoctorPatientDetails(
				event,
				hospitalDetail
			),
			updateOrderProcessingDetails: await updateOrderProcessingDetails(
				event,
				hospitalDetail
			)
		};

		if (
			obj.updateDoctorPatientDetails.success &&
			obj.updateOrderProcessingDetails.success
		) {
			return Promise.resolve({
				success: true,
				message: CONSTANTS.MESSAGES.UPDATED_ORDER
			});
		} else {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.ORDER_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.ORDER_NOT_FOUND.CODE
			});
		}
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: error.message,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
