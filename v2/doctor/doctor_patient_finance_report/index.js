const AWS = require('aws-sdk');
const s3Client = new AWS.S3();
const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');
const moment = require('moment');
const xl = require('excel4node');

AWS.config.update({ region: process.env.REGION });

const getAge = (dateString) => {
	const today = new Date();
	const birthDate = new Date(dateString);
	let age = today.getFullYear() - birthDate.getFullYear();
	const m = today.getMonth() - birthDate.getMonth();
	if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		age--;
	}
	return age;
};

const parseDOB = (str) => {
	const age = getAge(new Date(str));
	return isNaN(age) ? '' : age.toString();
};

const fetchPatientProfile = async patientsDetailsArr => {
	const filteredPatientsDetailsObj = {};
	for (const item of patientsDetailsArr) {
		const { doctorID, userID, firstName, lastName, dob, patientCode, icdCode } = item;
		filteredPatientsDetailsObj[userID] = {
			doctorID,
			firstName,
			lastName,
			userID,
			patientCode,
			age: parseDOB(dob),
			icdCode: icdCode
		};
	}
	return filteredPatientsDetailsObj;
};

const createExcelFile = async (value) => {
	const wb = new xl.Workbook({
		defaultFont: {
			size: 10,
			name: CONSTANTS.EXCEL_FONT
		}
	});

	const ws = wb.addWorksheet(CONSTANTS.FINANCE_WORKSHEET, {
		sheetFormat: {
			baseColWidth: 20
		}
	});

	ws.cell(1, 1, 1, 4, true).string(CONSTANTS.PATIENT_DETAILS).style({ alignment: { wrapText: true, horizontal: CONSTANTS.ALIGNMENT } });
	ws.cell(1, 5, 1, 10, true).string(CONSTANTS.CODE_DETAILS).style({ alignment: { wrapText: true, horizontal: CONSTANTS.ALIGNMENT } });
	ws.cell(1, 11).string(CONSTANTS.ICDCODE);
	ws.cell(2, 1).string(CONSTANTS.FIRST_NAME);
	ws.cell(2, 2).string(CONSTANTS.LAST_NAME);
	ws.cell(2, 3).string(CONSTANTS.AGE);
	ws.cell(2, 4).string(CONSTANTS.PATIENT_MRN);
	ws.cell(2, 5).string(CONSTANTS.CODE);
	ws.cell(2, 6).string(CONSTANTS.DATE_OF_SERVICE);
	ws.cell(2, 7).string(CONSTANTS.DATE_TWENTY_MIN_REACHED);
	ws.cell(2, 8).string(CONSTANTS.TOTAL_CLOCKED_TIME);
	ws.cell(2, 9).string(CONSTANTS.DATE_ADDITIONAL_TWENTY_MIN_REACHED);
	ws.cell(2, 10).string(CONSTANTS.TOTAL_CLOCKED_TIME);

	ws.cell(1, 1, 2, 11).style({
		border: {
			left: {
				style: CONSTANTS.BORDER_STYLE
			},
			right: {
				style: CONSTANTS.BORDER_STYLE
			},
			top: {
				style: CONSTANTS.BORDER_STYLE
			},
			bottom: {
				style: CONSTANTS.BORDER_STYLE
			}
		}
	});

	const rowShift = 3;

	for (let i = 0; i < value.length; i++) {
		  let j = 1;
		  ws.cell(i + rowShift, j).string(value[i].firstName || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].lastName || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].age || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].patientCode || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].code || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].dateOfService || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].dateOfService99457 || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].totalTimeClocked99457 || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].dateOfService99458 || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].totalTimeClocked99458 || '');
		  j++;
		  ws.cell(i + rowShift, j).string(value[i].icdCode || '');
	}
	const buffer = await wb.writeToBuffer();
	return Promise.resolve(buffer);
};

const joinIcdCode = (arr, separator = ' ') => {
	let result = '';
	for (let i = 0; i < arr.length; i++) {
	  if (result) {
			result += separator;
	  }
	  result += arr[i].code;
	}
	return result;
};

const getReportUrl = async (key) => {
	const params = {
		Bucket: CONSTANTS.S3_BUCKET,
		Key: key,
		Expires: 604800
	};
	try {
		const url = await s3Client.getSignedUrl('getObject', params);
		return url;
	} catch (error) {
		console.log('Error while generating pre-signed url', error);
		return Promise.resolve({
			success: false,
			message: error
		});
	}
};

const generateFinanceReport = async (event, hospitalTableDetails, userID, startDate, endDate, type, period) => {
	const patientDetails = await DB.getDoctorPatients(event.doctorId, hospitalTableDetails.DOCTOR_PATIENT);
	let filteredUsers = [];
	if (patientDetails.length > 0 && userID.length > 0) {
		for (const id of userID) {
			const data = patientDetails.filter(patient => patient.userID === id);
			if (data.length > 0) filteredUsers.push(data[0]);
		}
	} else {
		filteredUsers = filteredUsers.concat(patientDetails);
	}

	for (let i = 0; i < filteredUsers.length; i++) {
		if (filteredUsers[i].icdCode && filteredUsers[i].icdCode.length > 0) {
			filteredUsers[i].icdCode = joinIcdCode(filteredUsers[i].icdCode, ',');
		}
	}

	const patientProfile = await fetchPatientProfile(filteredUsers);
	let records = [];
	const promiseResp = filteredUsers.map(async (patient) => {
		const patientsFinanceRecords = [];
		const financePromiseCalls = [];

		if (type.includes(CONSTANTS.TYPE_99453)) {
			const _99453 = DB.get99453Data(patient.userID, startDate, endDate, hospitalTableDetails.TABLE_99453);
			financePromiseCalls.push(_99453);
		} else {
			financePromiseCalls.push(Promise.resolve([]));
		}

		if (type.includes(CONSTANTS.TYPE_99454)) {
			const _99454 = DB.get99454Data(patient.userID, startDate, endDate, hospitalTableDetails.TABLE_99454);
			financePromiseCalls.push(_99454);
		} else {
			financePromiseCalls.push(Promise.resolve([]));
		}

		if (type.includes(CONSTANTS.TYPE_99457)) {
			const _99457 = DB.get99457Data(patient.userID, startDate, endDate, hospitalTableDetails.TABLE_99457);
			financePromiseCalls.push(_99457);
		} else {
			financePromiseCalls.push(Promise.resolve([]));
		}

		if (type.includes(CONSTANTS.TYPE_99458)) {
			const _99458 = DB.get99458Data(patient.userID, startDate, endDate, hospitalTableDetails.TABLE_99458);
			financePromiseCalls.push(_99458);
		} else {
			financePromiseCalls.push(Promise.resolve([]));
		}

		const [_99453Data, _99454Data, _99457Data, _99458Data] = await Promise.all(financePromiseCalls);

		if (_99453Data.length > 0) {
			for (let i = 0; i < _99453Data.length; i++) {
				const data = {
					userID: _99453Data[i].userID,
					code: _99453Data[i].attributes && _99453Data[i].attributes.transaction && _99453Data[i].attributes.transaction.procedureCode,
					dateOfService:
							_99453Data[i].attributes && _99453Data[i].attributes.transaction &&
							_99453Data[i].attributes.transaction.dateOfService ? moment(_99453Data[i].attributes.transaction.dateOfService).format('MM/DD/YYYY') : '',
					firstName: patientProfile[patient.userID].firstName,
					lastName: patientProfile[patient.userID].lastName,
					icdCode: patientProfile[patient.userID].icdCode
				};
				if (i === 0) {
					data.patientCode = patientProfile[patient.userID].patientCode;
					data.age = patientProfile[patient.userID].age;
				}
				patientsFinanceRecords.push(data);
			}
		}

		if (_99454Data.length > 0) {
			for (let i = 0; i < _99454Data.length; i++) {
				const data = {
					userID: _99454Data[i].userID,
					code: _99454Data[i].attributes && _99454Data[i].attributes.transaction && _99454Data[i].attributes.transaction.procedureCode,
					dateOfService:
							_99454Data[i].attributes && _99454Data[i].attributes.transaction &&
							_99454Data[i].attributes.transaction.dateOfService ? moment(_99454Data[i].attributes.transaction.dateOfService).format('MM/DD/YYYY') : '',
					firstName: patientProfile[patient.userID].firstName,
					lastName: patientProfile[patient.userID].lastName,
					icdCode: patientProfile[patient.userID].icdCode
				};
				if (_99453Data.length === 0 && i === 0) {
					data.patientCode = patientProfile[patient.userID].patientCode;
					data.age = patientProfile[patient.userID].age;
				}
				patientsFinanceRecords.push(data);
			}
		}

		if (_99457Data.length > 0) {
			for (let i = 0; i < _99457Data.length; i++) {
				const data = {
					userID: _99457Data[i].userID,
					code: _99457Data[i].attributes && _99457Data[i].attributes.transaction && _99457Data[i].attributes.transaction.procedureCode,
					dateOfService99457:
							_99457Data[i].attributes && _99457Data[i].attributes.transaction &&
							_99457Data[i].attributes.transaction.dateOfService ? moment(_99457Data[i].attributes.transaction.dateOfService).format('MM/DD/YYYY') : '',
					totalTimeClocked99457:
							_99457Data[i].attributes && _99457Data[i].attributes.transaction &&
							_99457Data[i].attributes.transaction.totalTimeClocked ? moment.utc(CONSTANTS.DEFAULT_CLOCKED_TIME * CONSTANTS.IN_SECONDS * 1000).format('HH:mm:ss') : '',
					firstName: patientProfile[patient.userID].firstName,
					lastName: patientProfile[patient.userID].lastName,
					icdCode: patientProfile[patient.userID].icdCode
				};
				if (_99453Data.length === 0 && _99454Data.length === 0 && i === 0) {
					data.patientCode = patientProfile[patient.userID].patientCode;
					data.age = patientProfile[patient.userID].age;
				}
				patientsFinanceRecords.push(data);
			}
		}

		if (_99458Data.length > 0) {
			for (let i = 0; i < _99458Data.length; i++) {
				const data = {
					userID: _99458Data[i].userID,
					code: _99458Data[i].attributes && _99458Data[i].attributes.transaction && _99458Data[i].attributes.transaction.procedureCode,
					dateOfService99458:
							_99458Data[i].attributes && _99458Data[i].attributes.transaction &&
							_99458Data[i].attributes.transaction.dateOfService ? moment(_99458Data[i].attributes.transaction.dateOfService).format('MM/DD/YYYY') : '',
					totalTimeClocked99458:
							_99458Data[i].attributes && _99458Data[i].attributes.transaction &&
							_99458Data[i].attributes.transaction.totalTimeClocked ? moment.utc(CONSTANTS.DEFAULT_CLOCKED_TIME * CONSTANTS.IN_SECONDS * 1000).format('HH:mm:ss') : '',
					firstName: patientProfile[patient.userID].firstName,
					lastName: patientProfile[patient.userID].lastName,
					icdCode: patientProfile[patient.userID].icdCode
				};
				if (_99453Data.length === 0 && _99454Data.length === 0 && _99457Data.length === 0 && i === 0) {
					data.patientCode = patientProfile[patient.userID].patientCode;
					data.age = patientProfile[patient.userID].age;
				}
				patientsFinanceRecords.push(data);
			}
		}
		records = patientsFinanceRecords.concat(records);
	});
	await Promise.all(promiseResp);
	const file = await createExcelFile(records);
	const s3Params = {
		Bucket: CONSTANTS.S3_BUCKET,
		Key: `${hospitalTableDetails.dbIdentifier}/${event.doctorId}/${period}-${CONSTANTS.BILLING_REPORT}.xlsx`,
		Body: file
	};
	await s3Client.putObject(s3Params).promise();
	const reportUrl = await getReportUrl(s3Params.Key);
	return reportUrl;
};

const applyValidation = async (event) => {
	try {
		if (!event.hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		if (!event.body.startDate) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.START_DATE.MESSAGE,
				errorCode: CONSTANTS.ERRORS.START_DATE.CODE
			});
		}
		if (!event.body.endDate) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.END_DATE.MESSAGE,
				errorCode: CONSTANTS.ERRORS.END_DATE.CODE
			});
		}

		if (!event.body.userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQ.CODE
			});
		}

		if (!event.body.period) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.PERIOD.MESSAGE,
				errorCode: CONSTANTS.ERRORS.PERIOD.CODE
			});
		}

		if (!event.body.type) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.TYPE.MESSAGE,
				errorCode: CONSTANTS.ERRORS.TYPE.CODE
			});
		}
		const startDate = event.body.startDate;
		const endDate = event.body.endDate;
		const userID = event.body.userId;
		let period = event.body.period;
		const type = event.body.type;
		if (period === CONSTANTS.YEARLY) {
			period = new Date(startDate).getFullYear();
		}
		const hospitalTableDetails = await DB.verifyHospital(event.hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const report = await generateFinanceReport(event, hospitalTableDetails, userID, startDate, endDate, type, period);

		return Promise.resolve({
			success: true,
			report: report
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
