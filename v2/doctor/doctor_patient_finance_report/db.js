const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS');

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			dbIdentifier: Items[0].dbIdentifier,
			DOCTOR_PROFILE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.DOCTOR_PROFILE_TABLE,
			DOCTOR_PATIENT:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.DOCTOR_PATIENT_TABLE,
			TABLE_99453:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.TABLE_99453,
			TABLE_99454:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.TABLE_99454,
			TABLE_99457:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.TABLE_99457,
			TABLE_99458:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.TABLE_99458
		};
		return obj;
	}
};

const getDoctorPatients = async (doctorId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID'
		},
		ScanIndexForward: false
	};

	let { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return [];
	}
	Items = Items.map(
		({
			doctorID,
			userID,
			createdDate,
			patientCode,
			attributes: { firstName, lastName, dob, icdCode, ...attributesRest },
			...rest
		}) => ({ doctorID, userID, firstName, lastName, dob, patientCode, icdCode })
	);
	return Items;
};

const get99453Data = async (userId, from, to, table) => {
	const params = {
		TableName: table,
		KeyConditionExpression: '#uid = :id',
		FilterExpression:
			'#attributes.#transaction.#dateOfService BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': Number(from),
			':maxDate': Number(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userID',
			'#attributes': 'attributes',
			'#transaction': 'transaction',
			'#dateOfService': 'dateOfService'
		}
	};

	const { Items, Count } = await dbClient.query(params).promise();
	return Count ? Items : [];
};

const get99454Data = async (userId, from, to, table) => {
	const params = {
		TableName: table,
		KeyConditionExpression: '#uid = :id',
		FilterExpression:
			'#attributes.#transaction.#dateOfService BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': Number(from),
			':maxDate': Number(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userID',
			'#attributes': 'attributes',
			'#transaction': 'transaction',
			'#dateOfService': 'dateOfService'
		}
	};

	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach((item) => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');
	return (queryResults.length > 0) ? queryResults : [];
};

const get99457Data = async (userId, from, to, table) => {
	const params = {
		TableName: table,
		KeyConditionExpression: '#uid = :id',
		FilterExpression:
			'#attributes.#transaction.#dateOfService BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': Number(from),
			':maxDate': Number(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userID',
			'#attributes': 'attributes',
			'#transaction': 'transaction',
			'#dateOfService': 'dateOfService'
		}
	};

	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach((item) => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');
	return (queryResults.length > 0) ? queryResults : [];
};

const get99458Data = async (userId, from, to, table) => {
	const params = {
		TableName: table,
		KeyConditionExpression: '#uid = :id',
		FilterExpression:
			'#attributes.#transaction.#dateOfService BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': Number(from),
			':maxDate': Number(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userID',
			'#attributes': 'attributes',
			'#transaction': 'transaction',
			'#dateOfService': 'dateOfService'
		}
	};

	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach((item) => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');
	return (queryResults.length > 0) ? queryResults : [];
};

module.exports.verifyHospital = verifyHospital;
module.exports.getDoctorPatients = getDoctorPatients;
module.exports.get99453Data = get99453Data;
module.exports.get99454Data = get99454Data;
module.exports.get99457Data = get99457Data;
module.exports.get99458Data = get99458Data;
