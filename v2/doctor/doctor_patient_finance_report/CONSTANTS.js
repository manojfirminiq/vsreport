exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = '_rpm_';
exports.S3_BUCKET = process.env.S3_BUCKET;
exports.DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE;
exports.TABLE_99453 = process.env.TABLE_99453;
exports.TABLE_99454 = process.env.TABLE_99454;
exports.TABLE_99457 = process.env.TABLE_99457;
exports.TABLE_99458 = process.env.TABLE_99458;
exports.CURRENT_MONTH = 'currentMonth';
exports.LAST_MONTH = 'lastMonth';
exports.LAST_THREE_MONTHS = 'lastThreeMonths';
exports.YEARLY = 'yearly';
exports.IN_SECONDS = 60;
exports.DEFAULT_CLOCKED_TIME = 20;
exports.TYPE_99453 = '99453';
exports.TYPE_99454 = '99454';
exports.TYPE_99457 = '99457';
exports.TYPE_99458 = '99458';
exports.FINANCE_WORKSHEET = 'Finance Report';
exports.EXCEL_FONT = 'Arial';
exports.BORDER_STYLE = 'thin';

exports.ALIGNMENT = 'center';
exports.PATIENT_DETAILS = 'Patient Details';
exports.CODE_DETAILS = 'Code Details';
exports.ICDCODE = 'ICD-10';
exports.FIRST_NAME = 'Patient First Name';
exports.LAST_NAME = 'Patient Last Name';
exports.AGE = 'Patient Age';
exports.PATIENT_MRN = 'Patient MRN';
exports.CODE = 'Code';
exports.DATE_OF_SERVICE = 'Date of Service';
exports.DATE_TWENTY_MIN_REACHED = 'Date 20 mins Reached';
exports.TOTAL_CLOCKED_TIME = 'Total Clocked Time';
exports.DATE_ADDITIONAL_TWENTY_MIN_REACHED = 'Date Additional 20 mins Reached';
exports.BILLING_REPORT = 'Billing-Report';

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_ID_REQ: {
		CODE: 'USER_ID_REQ',
		MESSAGE: 'User id required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	FROM_DATE: {
		CODE: 'INVALID_FROM_DATE',
		MESSAGE: 'Invalid from date'
	},
	TO_DATE: {
		CODE: 'INVALID_TO_DATE',
		MESSAGE: 'Invalid to date'
	},
	TYPE: {
		CODE: 'INVALID_FINANCE_TYPE',
		MESSAGE: 'Please provide finance type'
	}
};
