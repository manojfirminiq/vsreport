exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	FROM_DATE: {
		CODE: 'INVALID_FROM_DATE',
		MESSAGE: 'Invalid from date'
	},
	TO_DATE: {
		CODE: 'INVALID_TO_DATE',
		MESSAGE: 'Invalid to date'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	S3_BUCKET_ERROR: {
		CODE: 'S3_BUCKET_ERROR'
	}
};

exports.RPM = '_rpm_';
exports.DOCTOR_ID_MEASURMENT_DATE_INDEX = 'doctorID-measurementDate-index';
exports.COMMON_IMAGE_BUCKET_NAME = process.env.COMMON_IMAGE_BUCKET_NAME;
exports.TYPE_BP = 'bp';
exports.TYPE_WEIGHT = 'weight';
exports.S3_KEY_EXPIRES_IN = 60 * 60;
