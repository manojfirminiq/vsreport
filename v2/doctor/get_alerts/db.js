const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const STAGE = process.env.STAGE;
const HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
const DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
const ALERT_BP_TABLE = process.env.ALERT_BP_TABLE;
const ALERT_NON_BP_TABLE = process.env.ALERT_NON_BP_TABLE;

const verifyHospital = async hospitalId => {
	const params = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};

	const { Items } = await dbClient.query(params).promise();

	if (Items.length === 0) {
		return false;
	}
	const obj = {
		DOCTOR_PATIENT_TABLE: `${STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${DOCTOR_PATIENT_TABLE}`,
		ALERT_BP_TABLE: `${STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${ALERT_BP_TABLE}`,
		ALERT_NON_BP_TABLE: `${STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${ALERT_NON_BP_TABLE}`
	};

	return obj;
};

const getPatient = async (doctorId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':deleteFlag': true
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#deleteFlag': 'deleteFlag'
		},
		FilterExpression: '#deleteFlag <> :deleteFlag',
		ScanIndexForward: false
	};

	return dbClient.query(params).promise();
};

const getBPReadingAlerts = async (alertBpTable, doctorId, fromDate, toDate) => {
	const from = new Date(Number(fromDate));
	const to = new Date(Number(toDate));

	const params = {
		TableName: alertBpTable,
		IndexName: CONSTANTS.DOCTOR_ID_MEASURMENT_DATE_INDEX,
		KeyConditionExpression:
			'#doctor = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeNames: {
			'#doctor': 'doctorID'
		},
		ExpressionAttributeValues: {
			':id': doctorId,
			':minDate': from.getTime(),
			':maxDate': to.getTime()
		}
	};

	params.ScanIndexForward = false;

	return dbClient.query(params).promise();
};

const getNonBPReadingAlerts = async (
	alertNonBpTable,
	doctorId,
	fromDate,
	toDate
) => {
	const from = new Date(Number(fromDate));
	const to = new Date(Number(toDate));

	const params = {
		TableName: alertNonBpTable,
		IndexName: CONSTANTS.DOCTOR_ID_MEASURMENT_DATE_INDEX,
		KeyConditionExpression:
			'#doctor = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeNames: {
			'#doctor': 'doctorID'
		},
		ExpressionAttributeValues: {
			':id': doctorId,
			':minDate': from.getTime(),
			':maxDate': to.getTime()
		}
	};

	params.ScanIndexForward = false;

	return dbClient.query(params).promise();
};

module.exports.verifyHospital = verifyHospital;
module.exports.getPatient = getPatient;
module.exports.getBPReadingAlerts = getBPReadingAlerts;
module.exports.getNonBPReadingAlerts = getNonBPReadingAlerts;
