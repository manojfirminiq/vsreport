const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');

const getProfileUrl = async key => {
	const params = {
		Bucket: CONSTANTS.COMMON_IMAGE_BUCKET_NAME,
		Key: key,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	};

	try {
		const url = await s3.getSignedUrl('getObject', params);
		return url;
	} catch (error) {
		console.log('Error while generating pre-signed url', error);
		return Promise.resolve({
			success: false,
			message: error,
			errorCode: CONSTANTS.ERRORS.S3_BUCKET_ERROR.CODE
		});
	}
};

const fetchPatientProfile = async patientsDetailsArr => {
	const filteredPatientsDetailsObj = {};
	for (const item of patientsDetailsArr.Items) {
		const {
			attributes: { name, firstName, lastName, profilePicture, dob, gender } = {
				name: null,
				firstName: null,
				lastName: null,
				profilePicture: null,
				dob: null,
				gender: null
			},
			userID,
			patientCode
		} = item;
		const profilePic = profilePicture
			? await getProfileUrl(profilePicture)
			: null;
		filteredPatientsDetailsObj[userID] = {
			name,
			firstName,
			lastName,
			userID,
			patientCode,
			profilePicture: profilePic,
			dob,
			gender
		};
	}
	return filteredPatientsDetailsObj;
};

const filterBpAlertReadings = (bpReadingAlerts, patientProfile) => {
	const bpAlertItems = bpReadingAlerts.Items.map(
		({
			alerts: {
				thresholdBpUnit,
				bp: { Notes, ...bprest },
				threshold: { ...thresholdRest }
			},
			attributes: {
				averageType,
				alertType,
				...rest
			},
			measurementDate,
			actualUserID
		}) => ({
			...patientProfile[actualUserID],
			measurementDate,
			type: CONSTANTS.TYPE_BP,
			thresholdBpUnit,
			notes: Notes || '',
			...bprest,
			...thresholdRest,
			averageType: averageType || null,
			alertType: alertType || null
		})
	).filter(({ name }) => name !== undefined);

	return bpAlertItems;
};

const filterWeightAlertReadings = (nonBpReadingAlerts, patientProfile) => {
	const nonBpAlertItems = nonBpReadingAlerts.Items.map(
		({
			alerts: {
				thresholdWeightUnit,
				weight: { Notes, ...weightRest },
				threshold: { ...thresholdRest }
			},
			measurementDate,
			actualUserID
		}) => ({
			...patientProfile[actualUserID],
			measurementDate,
			type: CONSTANTS.TYPE_WEIGHT,
			thresholdWeightUnit,
			notes: Notes || '',
			...weightRest,
			...thresholdRest
		})
	).filter(({ name }) => name !== undefined);

	return nonBpAlertItems;
};

const applyValidation = async ({
	doctorId,
	hospitalID,
	fromDate,
	toDate,
	limit
}) => {
	if (!fromDate) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.FROM_DATE.MESSAGE,
			errorCode: CONSTANTS.ERRORS.FROM_DATE.CODE
		});
	}
	if (!toDate) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.TO_DATE.MESSAGE,
			errorCode: CONSTANTS.ERRORS.TO_DATE.CODE
		});
	}

	try {
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const response = {
			success: true,
			data: {
				alerts: []
			}
		};

		let bpAlertItems = [];
		let nonBpAlertItems = [];

		const patientsDetailsArr = await DB.getPatient(
			doctorId,
			hospitalTableDetails.DOCTOR_PATIENT_TABLE
		);

		const patientProfile = await fetchPatientProfile(patientsDetailsArr);

		const bpReadingAlerts = await DB.getBPReadingAlerts(
			hospitalTableDetails.ALERT_BP_TABLE,
			doctorId,
			fromDate,
			toDate
		);

		if (bpReadingAlerts.Count > 0) {
			bpAlertItems = filterBpAlertReadings(bpReadingAlerts, patientProfile);
		}

		const nonBpReadingAlerts = await DB.getNonBPReadingAlerts(
			hospitalTableDetails.ALERT_NON_BP_TABLE,
			doctorId,
			fromDate,
			toDate
		);

		if (nonBpReadingAlerts.Count > 0) {
			nonBpAlertItems = filterWeightAlertReadings(
				nonBpReadingAlerts,
				patientProfile
			);
		}
		response.data.alerts = bpAlertItems.concat(nonBpAlertItems);
		response.data.alerts = response.data.alerts.sort((o1, o2) => {
			const m1Date = new Date(o1.measurementDate).getTime();
			const m2Date = new Date(o2.measurementDate).getTime();
			return m2Date - m1Date;
		});

		if (limit) {
			response.data.alerts = response.data.alerts.slice(0, Number(limit));
		}

		response.data.bpAlertCount = bpAlertItems.length;
		response.data.nonBpAlertCount = nonBpAlertItems.length;
		response.data.totalCount =
			response.data.bpAlertCount + response.data.nonBpAlertCount;

		return Promise.resolve(response);
	} catch (error) {
		console.log('error', error);
		return Promise.resolve({
			success: false,
			message: error,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
