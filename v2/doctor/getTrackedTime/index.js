const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');
const AWS = require('aws-sdk');
const s3Client = new AWS.S3();

const getUserProfileUrl = profilePicture => {
	return s3Client.getSignedUrl('getObject', {
		Bucket: CONSTANTS.PROFILE_BUCKET,
		Key: profilePicture,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	});
};

const getTrackTime = async (doctorId, hospitalTable) => {
	let consultationData;
	consultationData = await DB.getConsultation(
		doctorId,
		hospitalTable.DOCTOR_CONSULTATION_TABLE
	);
	const date = getCurrentMonthFirstAndLastDate();
	consultationData = consultationData.filter(item => {
		return item.startTime >= date.firstDay && item.startTime <= date.lastDay;
	});
	const patientTotalConsultTime = getTotalConsultationTime(consultationData);
	const billingDetails = getBillingDetails(patientTotalConsultTime);
	let patients = await DB.getAllPatients(
		doctorId,
		hospitalTable.DOCTOR_PATIENT_TABLE
	);

	patients = patients.map(
		({
			userID,
			patientCode,
			attributes: { name, lastName, gender, dob, icdCode, profilePicture } = {}
		}) => ({
			userID,
			name: name || '',
			lastName: lastName || '',
			patientCode: patientCode || '',
			gender: gender || '',
			dob: dob || '',
			icdCode: icdCode || [],
			profilePicture: profilePicture ? getUserProfileUrl(profilePicture) : null
		})
	);

	const billingObj = billingDetails.reduce((acc, billObj) => {
		const { userID } = billObj;
		acc[userID] = billObj;
		return acc;
	}, {});

	const response = patients.reduce((acc, data) => {
		const { userID } = data;
		const billingData = billingObj[userID] || [];
		acc.push({
			...data,
			billingDetails: billingData.billingDetails || []
		});
		return acc;
	}, []);

	return response;
};

const getCurrentMonthFirstAndLastDate = () => {
	const date = new Date();
	const firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getTime();
	let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	lastDay.setHours(23);
	lastDay.setMinutes(59);
	lastDay.setSeconds(59);
	lastDay = lastDay.getTime();
	return { firstDay, lastDay };
};

const getTotalConsultationTime = consultationData => {
	const counts = consultationData.reduce((prev, curr) => {
		const count = prev.get(curr.userID) || 0;
		prev.set(curr.userID, curr.consultationTime + count);
		return prev;
	}, new Map());

	const reducedObjArr = [...counts].map(([userID, consultationTime]) => {
		return { userID, consultationTime };
	});

	return reducedObjArr;
};

const getColorCode = remainingTime => {
	let colorCode = null;
	if (remainingTime === 0) {
		colorCode = CONSTANTS.GREEN;
		return colorCode;
	} else if (remainingTime < 5) {
		colorCode = CONSTANTS.RED;
		return colorCode;
	} else if (remainingTime < 10) {
		colorCode = CONSTANTS.ORANGE;
		return colorCode;
	} else if (remainingTime < 15) {
		colorCode = CONSTANTS.YELLOW;
		return colorCode;
	} else {
		colorCode = null;
		return colorCode;
	}
};

const getBillingDetails = patientTotalConsultTime => {
	const outputArray = [];
	patientTotalConsultTime.forEach(data => {
		const output = { userID: data.userID, billingDetails: [] };
		const minutes = data.consultationTime / 60;
		for (let i = 0; i < minutes; i += 20) {
			const bD = {
				code: '',
				totalTimeClocked: '',
				remainingConsultingTime: '',
				colorCode: ''
			};
			if (i === 0) {
				bD.code = CONSTANTS.CODE_99457;
			} else {
				bD.code = CONSTANTS.CODE_99458;
			}
			bD.totalTimeClocked = minutes - i > 20 ? 20 : minutes - i;
			bD.remainingConsultingTime = 20 - bD.totalTimeClocked;
			bD.colorCode = getColorCode(bD.remainingConsultingTime);
			output.billingDetails.push(bD);
		}
		outputArray.push(output);
	});
	return outputArray;
};

const applyValidation = async ({ doctorId, hospitalID }) => {
	try {
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		const response = await getTrackTime(doctorId, hospitalTable);
		return Promise.resolve({ success: true, data: response });
	} catch (error) {
		console.log('Error while fetching tracked time', error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
