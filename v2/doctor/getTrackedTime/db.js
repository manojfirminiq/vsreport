const AWS = require('aws-sdk');
const CONSTANTS = require('./CONSTANTS.js');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: CONSTANTS.HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};

	const { Count, Items } = await dbClient.query(paramsTable).promise();
	if (Count <= 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_PATIENT_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${CONSTANTS.DOCTOR_TABLE}`,
			DOCTOR_CONSULTATION_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${CONSTANTS.DOCTOR_CONSULTATION}`
		};
		return obj;
	}
};

const getAllPatients = async (doctorId, doctorPatientTable) => {
	const params = {
		TableName: doctorPatientTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorId
		},
		ExpressionAttributeNames: {
			'#uid': 'doctorID'
		}
	};

	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach(item => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return queryResults;
};

const getConsultation = async (doctorId, consultationTable) => {
	const params = {
		TableName: consultationTable,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':isdeletedValue': true
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#isdeleted': 'isdeleted'
		},
		FilterExpression: '#isdeleted <> :isdeletedValue',
		ScanIndexForward: false
	};
	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach(item => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return queryResults;
};

module.exports.verifyHospital = verifyHospital;
module.exports.getAllPatients = getAllPatients;
module.exports.getConsultation = getConsultation;
