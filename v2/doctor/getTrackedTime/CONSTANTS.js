exports.ERRORS = {
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	NO_PATIENT_FOUND: {
		CODE: 'NO PATIENT',
		MESSAGE: 'No Patient F'
	}
};

exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.DOCTOR_TABLE = process.env.DOCTOR_TABLE;
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;
exports.PROFILE_BUCKET = process.env.PROFILE_BUCKET;
exports.S3_KEY_EXPIRES_IN = 60 * 60;
exports.RPM = '_rpm_';
exports.GREEN = 'GREEN';
exports.RED = 'RED';
exports.ORANGE = 'ORANGE';
exports.YELLOW = 'YELLOW';
exports.CODE_99457 = 99457;
exports.CODE_99458 = 99458;
