const CONSTANTS = require('./CONSTANTS.js');
const getStaticContent = async contentType => {
	try {
		const itemList = CONSTANTS.VALUES[contentType];
		return Promise.resolve({
			success: true,
			data: itemList || []
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

const applyValidation = async event => {
	try {
		if (!event.contentType) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.CONTENT_TYPE_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.CONTENT_TYPE_REQ.CODE
			});
		}
		if (CONSTANTS.SUPPORTED_CONTENT.indexOf(event.contentType) !== -1) {
			const response = await getStaticContent(event.contentType);
			return response;
		} else {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.CONTENT_TYPE_NOT_SUPPORTED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.CONTENT_TYPE_NOT_SUPPORTED.CODE
			});
		}
	} catch (error) {
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
