exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		MESSAGE: 'Internal Server Error'
	},
	CONTENT_TYPE_REQ: {
		CODE: 'CONTENT_TYPE_REQ',
		MESSAGE: 'Content Type required'
	},
	CONTENT_TYPE_NOT_SUPPORTED: {
		CODE: 'CONTENT_TYPE_NOT_SUPPORTED',
		MESSAGE: 'Content Type not supported'
	}
};

exports.VALUES = {
	TODO: [
		{
			key: 'appointmentReminder',
			value: 'Appointment Reminder',
			isbillable: false
		},
		{
			key: 'billingReminder',
			value: 'Billing Reminder',
			isbillable: false
		},
		{
			key: 'patientVitalsReview',
			value: 'Patient Vitals review',
			isbillable: true
		},
		{
			key: 'patientLabResultReview',
			value: 'Patient Lab Result Review',
			isbillable: true
		}
	]
};

exports.SUPPORTED_CONTENT = ['TODO'];
