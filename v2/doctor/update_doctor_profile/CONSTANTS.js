exports.MESSAGES = {
	DOCTOR_PROFILE_UPDATE: 'Doctor details updated successfully',
	STAFF_PROFILE_UPDATE: 'Staff details updated successfully'
};
exports.DOCTOR_PROFILE = process.env.DOCTOR_PROFILE;
exports.STAFF_TABLE_NAME = process.env.STAFF_TABLE_NAME;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = 'rpm';
exports.VALIDATION_MESSAGES = {
	USER_ID_REQ: {
		CODE: 'REQUIRED PARAMETER',
		MESSAGE: 'User Id is required'
	}
};
exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR'
	}
};
