const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');

const getDoctorProfileDetails = async (userID, docTableName) => {
	const params = {
		TableName: docTableName,
		KeyConditionExpression: '#userID = :userID',
		ExpressionAttributeValues: {
			':userID': userID
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const getStaffProfileDetails = async (doctorId, staffID, staffTableName) => {
	const params = {
		TableName: staffTableName,
		KeyConditionExpression: '#doctorId = :doctorId AND #staffID = :staffID',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':staffID': staffID
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#staffID': 'staffID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const updateDoctorProfile = async (doctorId, item, tableName) => {
	const params = {
		TableName: tableName,
		Key: {
			userID: doctorId
		},
		UpdateExpression:
			'SET attributes = :attributes, phoneNumber=:phoneNumber, subscriptionFlag=:subscriptionFlag, secondaryEmailSubscription=:secondaryEmailSubscription, modifiedDate = :modifiedDate',
		ExpressionAttributeValues: {
			':attributes': item.attributes,
			':phoneNumber': item.phoneNumber,
			':subscriptionFlag': item.subscriptionFlag,
			':secondaryEmailSubscription': item.secondaryEmailSubscription,
			':modifiedDate': new Date().getTime()
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const updateStaffProfile = async (doctorId, staffID, item, tableName) => {
	const params = {
		TableName: tableName,
		Key: {
			doctorID: doctorId,
			staffID: staffID
		},
		UpdateExpression:
			'SET attributes = :attributes, phoneNumber=:phoneNumber, subscriptionFlag=:subscriptionFlag, secondaryEmailSubscription=:secondaryEmailSubscription, modifiedDate = :modifiedDate',
		ExpressionAttributeValues: {
			':attributes': item.attributes,
			':phoneNumber': item.phoneNumber,
			':subscriptionFlag': item.subscriptionFlag,
			':secondaryEmailSubscription': item.secondaryEmailSubscription,
			':modifiedDate': new Date().getTime()
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const getDbIdentifierFromHospital = async hospitalID => {
	const params = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospitalID = :hospitalID',
		ExpressionAttributeValues: {
			':hospitalID': hospitalID
		},
		ExpressionAttributeNames: {
			'#hospitalID': 'hospitalID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();

	return Count > 0 ? Items : false;
};

const updateDoctorProfileDetails = async (
	doctorId,
	hospitalID,
	name,
	qualification,
	phoneNumber,
	secondaryEmailAddress,
	subscriptionFlag,
	secondaryEmailSubscription,
	inActivePeriod,
	timeZone
) => {
	const hospitalDetail = await getDbIdentifierFromHospital(hospitalID);
	const docTableName =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PROFILE;
	const doctorProfileDetails = await getDoctorProfileDetails(
		doctorId,
		docTableName
	);
	if (doctorProfileDetails) {
		const item = {
			phoneNumber: phoneNumber || doctorProfileDetails[0].phoneNumber,
			subscriptionFlag:
				subscriptionFlag === true || subscriptionFlag === false
					? subscriptionFlag
					: doctorProfileDetails[0].subscriptionFlag
						? doctorProfileDetails[0].subscriptionFlag
						: false,
			secondaryEmailSubscription:
				secondaryEmailSubscription === true ||
				secondaryEmailSubscription === false
					? secondaryEmailSubscription
					: doctorProfileDetails[0].secondaryEmailSubscription
						? doctorProfileDetails[0].secondaryEmailSubscription
						: false
		};
		const doctorAttributes = {
			name: name || doctorProfileDetails[0].attributes.name,
			qualification:
				qualification || doctorProfileDetails[0].attributes.qualification,
			secondaryEmailAddress:
				secondaryEmailAddress || doctorProfileDetails[0].secondaryEmailAddress,
			profilePicture: doctorProfileDetails[0].attributes.profilePicture || null,
			inActivePeriod: inActivePeriod || (doctorProfileDetails[0].attributes.inActivePeriod ? doctorProfileDetails[0].attributes.inActivePeriod : null),
			timeZone: timeZone || doctorProfileDetails[0].attributes.timeZone
		};
		item.attributes = doctorAttributes;
		await updateDoctorProfile(doctorId, item, docTableName);
		return Promise.resolve({
			success: true,
			message: CONSTANTS.MESSAGES.DOCTOR_PROFILE_UPDATE
		});
	}
};

const updateStaffProfileDetails = async (
	doctorId,
	staffID,
	hospitalID,
	name,
	qualification,
	phoneNumber,
	secondaryEmailAddress,
	subscriptionFlag,
	secondaryEmailSubscription
) => {
	const hospitalDetail = await getDbIdentifierFromHospital(hospitalID);
	const staffTableName =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.STAFF_TABLE_NAME;
	const staffProfileDetails = await getStaffProfileDetails(
		doctorId,
		staffID,
		staffTableName
	);
	if (staffProfileDetails) {
		const item = {
			phoneNumber: phoneNumber || staffProfileDetails[0].phoneNumber,
			subscriptionFlag:
				subscriptionFlag === true || subscriptionFlag === false
					? subscriptionFlag
					: staffProfileDetails[0].subscriptionFlag
						? staffProfileDetails[0].subscriptionFlag
						: false,
			secondaryEmailSubscription:
				secondaryEmailSubscription === true ||
				secondaryEmailSubscription === false
					? secondaryEmailSubscription
					: staffProfileDetails[0].secondaryEmailSubscription
						? staffProfileDetails[0].secondaryEmailSubscription
						: false
		};
		const staffAttributes = {
			name: name || staffProfileDetails[0].attributes.name,
			qualification:
				qualification || staffProfileDetails[0].attributes.qualification,
			secondaryEmailAddress:
				secondaryEmailAddress || staffProfileDetails[0].secondaryEmailAddress,
			profilePicture: staffProfileDetails[0].attributes.profilePicture || null
		};
		item.attributes = staffAttributes;
		await updateStaffProfile(doctorId, staffID, item, staffTableName);
		return Promise.resolve({
			success: true,
			message: CONSTANTS.MESSAGES.STAFF_PROFILE_UPDATE
		});
	}
};

const applyValidation = async ({
	doctorId,
	staffID,
	hospitalID,
	body: {
		name,
		qualification,
		phoneNumber,
		secondaryEmailAddress,
		subscriptionFlag,
		secondaryEmailSubscription,
		inActivePeriod,
		timeZone
	}
}) => {
	try {
		if (!doctorId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.VALIDATION_MESSAGES.USER_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.VALIDATION_MESSAGES.USER_ID_REQ.CODE
			});
		} else if (staffID) {
			const obj = await updateStaffProfileDetails(
				doctorId,
				staffID,
				hospitalID,
				name,
				qualification,
				phoneNumber,
				secondaryEmailAddress,
				subscriptionFlag,
				secondaryEmailSubscription
			);
			return obj;
		} else {
			const obj = await updateDoctorProfileDetails(
				doctorId,
				hospitalID,
				name,
				qualification,
				phoneNumber,
				secondaryEmailAddress,
				subscriptionFlag,
				secondaryEmailSubscription,
				inActivePeriod,
				timeZone
			);
			return obj;
		}
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: error.message,
			errorCode: CONSTANTS.ERRORS.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
