exports.DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.PROFILE_BUCKET = process.env.PROFILE_BUCKET;
exports.S3_KEY_EXPIRES_IN = 60 * 60;
exports.WEBSITE_IMAGE = 'website-image';

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_NOT_FOUND: {
		CODE: 'USER_NOT_FOUND',
		MESSAGE: 'User not found'
	},
	USER_ID_REQUIRED: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'User Id is required'
	},
	IMAGE_REQ: {
		CODE: 'IMAGE_REQ',
		MESSAGE: 'Image is required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	TYPE_REQ: {
		CODE: 'TYPE_REQ',
		MESSAGE: 'Type is required'
	},
	TYPE_INVALID: {
		CODE: 'TYPE_INVALID',
		MESSAGE: 'Type is invalid'
	},
	DOCTOR_IMG_UPLOAD_ERROR: {
		CODE: 'DOCTOR_IMG_UPLOAD_ERROR',
		MESSAGE: 'Error while uploading doctor image to s3'
	},
	PATIENT_IMG_UPLOAD_ERROR: {
		CODE: 'PATIENT_IMG_UPLOAD_ERROR',
		MESSAGE: 'Error while uploading patient image to s3'
	}
};

exports.TYPE = {
	DOCTOR: 'doctor',
	PATIENT: 'patient'
};

exports.RPM = '_rpm_';
