const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const s3Client = new AWS.S3();
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./dynamodb');
const s3Bucket = CONSTANTS.PROFILE_BUCKET;
const trackPatient = require('track_patients_record');

const uploadDoctorImageToS3 = async (
	user,
	hospitalIdentifier,
	image,
	type,
	tableName,
	updateUserData
) => {
	try {
		const buf = Buffer.from(
			image.replace(/^data:image\/\w+;base64,/, ''),
			'base64'
		);
		const key = `${CONSTANTS.WEBSITE_IMAGE}/${hospitalIdentifier}/${type}/${user}`;
		const params = {
			Bucket: s3Bucket,
			Key: key,
			Body: buf,
			ContentEncoding: 'base64',
			ContentType: 'image/jpeg'
		};
		await s3Client.putObject(params).promise();
		const url = s3Client.getSignedUrl('getObject', {
			Bucket: s3Bucket,
			Key: key,
			Expires: CONSTANTS.S3_KEY_EXPIRES_IN
		});
		await DB.updateDoctorUserProfile(user, key, tableName, updateUserData);
		return { success: true, data: { profilePicture: url } };
	} catch (error) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.DOCTOR_IMG_UPLOAD_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.DOCTOR_IMG_UPLOAD_ERROR.CODE
		});
	}
};

const uploadPatientImageToS3 = async (
	doctorId,
	user,
	hospitalIdentifier,
	image,
	type,
	tableName,
	updateUserData
) => {
	try {
		const buf = Buffer.from(
			image.replace(/^data:image\/\w+;base64,/, ''),
			'base64'
		);
		const key = `${CONSTANTS.WEBSITE_IMAGE}/${hospitalIdentifier}/${type}/${user}`;
		const params = {
			Bucket: s3Bucket,
			Key: key,
			Body: buf,
			ContentEncoding: 'base64',
			ContentType: 'image/jpeg'
		};
		await s3Client.putObject(params).promise();
		const url = s3Client.getSignedUrl('getObject', {
			Bucket: s3Bucket,
			Key: key,
			Expires: CONSTANTS.S3_KEY_EXPIRES_IN
		});
		await DB.updatePatientUserProfile(
			doctorId,
			user,
			key,
			tableName,
			updateUserData
		);
		return { success: true, data: { profilePicture: url } };
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.PATIENT_IMG_UPLOAD_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.PATIENT_IMG_UPLOAD_ERROR.CODE
		});
	}
};

const applyValidation = async ({
	doctorId,
	hospitalID,
	updateUserData,
	body: { userId, image, type }
}) => {
	try {
		if (!image) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.IMAGE_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.IMAGE_REQ.CODE
			});
		}
		if (!type) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.TYPE_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.TYPE_REQ.CODE
			});
		}
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		if (type.toLowerCase() === CONSTANTS.TYPE.DOCTOR) {
			return await uploadDoctorImageToS3(
				doctorId,
				hospitalTableDetails.Items[0].dbIdentifier,
				image,
				type.toLowerCase(),
				hospitalTableDetails.DOCTOR_PROFILE,
				updateUserData
			);
		} else if (type.toLowerCase() === CONSTANTS.TYPE.PATIENT) {
			if (!userId) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
					errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
				});
			}
			const { Count, Items } = await DB.getPatient(
				doctorId,
				userId,
				hospitalTableDetails.DOCTOR_PATIENT
			);
			if (Count === 0) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
					errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
				});
			}
			return await uploadPatientImageToS3(
				doctorId,
				Items[0].userID,
				hospitalTableDetails.Items[0].dbIdentifier,
				image,
				type.toLowerCase(),
				hospitalTableDetails.DOCTOR_PATIENT,
				updateUserData
			);
		} else {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.TYPE_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.TYPE_INVALID.CODE
			});
		}
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};
exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
