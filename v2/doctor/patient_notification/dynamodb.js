const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('CONSTANTS');
const HOSPITAL_TABLE_NAME = CONSTANTS.DOCTOR_HOSPITAL_TABLE;

const getHospital = async (hospitalId) => {
	const params = {
		TableName: HOSPITAL_TABLE_NAME,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': hospitalId
		},
		ExpressionAttributeNames: {
			'#uid': 'hospitalID'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
        	hospitalDetails: Items[0],
			DOCTOR_PROFILE_TABLE: process.env.STAGE + CONSTANTS.RPM + Items[0].dbIdentifier + '_' + CONSTANTS.DOCTOR_PROFILE_TABLE,
			DOCTOR_PATIENT_TABLE: process.env.STAGE + CONSTANTS.RPM + Items[0].dbIdentifier + '_' + CONSTANTS.DOCTOR_PATIENT_TABLE
		};
		return obj;
	}
};

const getDoctorData = async (doctorID, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorID
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};

	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		const Item = Items.map(
			({
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			}) => ({ createdDate, ...rest, ...attributesRest })
		)[0];

		if (Item.hospitalID) {
			const hospital = await getHospital(Item.hospitalID);
			Item.hospital = hospital.hospitalDetails.name;

			delete Item.hospitalID;
		}
		return Item;
	} else {
		return null;
	}
};

const getPatient = async (doctorId, userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #userId = :userId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userId': userId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userId': 'userID'
		}
	};

	return dbClient.query(params).promise();
};

module.exports.getPatient = getPatient;
module.exports.getHospital = getHospital;
module.exports.getDoctorData = getDoctorData;
