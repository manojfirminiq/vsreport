exports.ERRORS = {
	PATIENTS_ID_NOT_FOUND: {
		CODE: 'PATIENTS_ID_NOT_FOUND',
		MESSAGE: 'patientID not found in request'
	},
	MESSAGE_NOT_FOUND: {
		CODE: 'MESSAGE_NOT_FOUND',
		MESSAGE: 'message not found in request'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal server error'
	},
	CONSENT_ERROR: {
		CODE: 'CONSENT_ERROR',
		MESSAGE: 'Do not have consent'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};

exports.DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.DOCTOR_HOSPITAL_TABLE = process.env.DOCTOR_HOSPITAL_TABLE;
exports.RPM = '_rpm_';
exports.COUNTRY_CODE = '+1';
exports.PHONE_NO_LENGTH = 10;
