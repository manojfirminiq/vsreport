const AWS = require('aws-sdk');
const db = require('dynamodb');
const CONSTANTS = require('CONSTANTS');
AWS.config.update({ region: process.env.REGION });

const checkCountryCode = (phNo) => {
	let phoneNumber;
	if (phNo && phNo.length === CONSTANTS.PHONE_NO_LENGTH) {
		phoneNumber = CONSTANTS.COUNTRY_CODE + phNo;
	} else {
		phoneNumber = phNo;
	}
	return phoneNumber;
};

const sendMessageToPatient = async (doctorId, hospitalID, userId, message) => {
	const hospitalTable = await db.getHospital(hospitalID);
	if (!hospitalTable) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
			errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
		});
	}
	const getDoctorDetails = await db.getDoctorData(
		doctorId,
		hospitalTable.DOCTOR_PROFILE_TABLE
	);
	// eslint-disable-next-line no-unused-expressions
	getDoctorDetails.qualification ? getDoctorDetails.name = `${getDoctorDetails.name}, ${getDoctorDetails.qualification}` : '';

	const getPatientDetails = await db.getPatient(
		doctorId,
		userId,
		hospitalTable.DOCTOR_PATIENT_TABLE
	);
	if (
		getPatientDetails &&
		getPatientDetails.Items &&
		getPatientDetails.Items.length
	) {
		if (getPatientDetails.Items[0].attributes.consent) {
			const contactVai = getPatientDetails.Items[0].attributes.contactVai;
			if (contactVai.phone || contactVai.email) {
				if (contactVai.phone) {
					const params = {
						Message: `${message}
						
${getDoctorDetails.name}
${getDoctorDetails.hospital}`,
						PhoneNumber: checkCountryCode(getPatientDetails.Items[0].attributes.mobilePhone)
					};
					console.log('params=====>', params);
					// Create promise and SNS service object
					const publishTextPromise = new AWS.SNS({ region: process.env.REGION })
						.publish(params)
						.promise();

					// Handle promise's fulfilled/rejected states
					publishTextPromise
						.then(function (data) {
							console.log('MessageID is ' + JSON.stringify(data));
						})
						.catch(function (err) {
							console.error(err, err.stack);
						});
				}
				if (contactVai.email) {
					const params = {
						Destination: {
							ToAddresses: [getPatientDetails.Items[0].attributes.emailAddress]
						},
						Message: {
							Body: {
								Text: {
									Data: `${message}<br/> <br/> ${getDoctorDetails.name}<br/>${getDoctorDetails.hospital}`
								}
							},
							Subject: {
								Data: 'VitalSight Notification'
							}
						},
						Source: `${process.env.SENDER_EMAIL}`
					};

					// Create the promise and SES service object
					const sendPromise = new AWS.SES({ region: process.env.REGION })
						.sendEmail(params)
						.promise();

					// Handle promise's fulfilled/rejected states
					sendPromise
						.then(function (data) {
							console.log(data.MessageId);
						})
						.catch(function (err) {
							console.error(err, err.stack);
						});
				}
				return {
					message: 'Successfully sent notification',
					success: true
				};
			} else {
				return {
					message: CONSTANTS.ERRORS.CONSENT_ERROR.MESSAGE,
					errorCode: CONSTANTS.ERRORS.CONSENT_ERROR.CODE,
					success: false
				};
			}
		} else {
			return {
				message: CONSTANTS.ERRORS.CONSENT_ERROR.MESSAGE,
				errorCode: CONSTANTS.ERRORS.CONSENT_ERROR.CODE,
				success: false
			};
		}
	} else {
		return { error: true };
	}
};

const applyValidation = async ({
	doctorId,
	hospitalID,
	body: { userId, message }
}) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.PATIENTS_ID_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.PATIENTS_ID_NOT_FOUND.CODE
			});
		}

		if (!message) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.MESSAGE_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.MESSAGE_NOT_FOUND.CODE
			});
		}

		const result = await sendMessageToPatient(
			doctorId,
			hospitalID,
			userId,
			message
		);
		return result;
	} catch (e) {
		console.log('error', e);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
