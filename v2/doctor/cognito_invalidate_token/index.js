const crypto = require('crypto');
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const USER_POOL_ID = process.env.USERPOOLID;
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const EMAIL_REQ = 'Please provide a valid emailAddress';
const TOKEN_TRACK_TABLE = process.env.TOKEN_TRACK_TABLE;

const generateUserId = email => {
	let userID = crypto
		.createHash('sha256')
		.update(email)
		.digest('hex');
	userID = crypto
		.createHash('sha256')
		.update(userID)
		.digest('hex');
	console.log(userID);
	return userID;
};

const updateTokenTrack = async emailAddress => {
	const userId = generateUserId(emailAddress);
	const params = {
		TableName: TOKEN_TRACK_TABLE,
		Item: {
			userID: userId,
			updatedAt: new Date().getTime()
		}
	};
	const dbClient = new AWS.DynamoDB.DocumentClient();
	return dbClient.put(params).promise();
};

const cognitoInvalidateToken = async emailAddress => {
	const params = {
		UserPoolId: USER_POOL_ID /* required */,
		Username: emailAddress /* required */
	};
	return new Promise((resolve, reject) => {
		cognitoidentityserviceprovider.adminUserGlobalSignOut(
			params,
			async (signoutErr, data) => {
				console.log(signoutErr, 'signoutErr');
				if (signoutErr) {
					resolve({ success: false, error: signoutErr.message });
				} else {
					await updateTokenTrack(emailAddress);
					resolve({ success: true, message: 'Inavalidated all tokens' });
				}
			}
		);
	});
};

const applyValidation = async ({ emailAddress }) => {
	try {
		emailAddress = emailAddress.toLowerCase();
		if (!emailAddress) { return Promise.resolve({ success: false, error: EMAIL_REQ }); }
		return await cognitoInvalidateToken(emailAddress);
	} catch (err) {
		return Promise.resolve({ success: false, error: err.message });
	}
};

exports.handler = async (event, context, callback) => {
	const response = await applyValidation(event);
	callback(null, response);
};
