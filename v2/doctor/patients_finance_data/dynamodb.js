const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_PATIENT_TABLE = CONSTANTS.DOCTOR_PATIENT_TABLE;
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;
const FINANCE_99453_TABLE = CONSTANTS.FINANCE_99453_TABLE;
const FINANCE_99454_TABLE = CONSTANTS.FINANCE_99454_TABLE;
const CLAIM_TABLE = CONSTANTS.CLAIM_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_PATIENT:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT_TABLE,
			FINANCE_99453_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				FINANCE_99453_TABLE,
			FINANCE_99454_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				FINANCE_99454_TABLE,
			CLAIM_TABLE:
				process.env.STAGE + '_rpm_' + Items[0].dbIdentifier + '_' + CLAIM_TABLE
		};
		return obj;
	}
};

const get99453Data = async (userID, FINANCE_99453_TABLE) => {
	try {
		const queryParams = {
			TableName: FINANCE_99453_TABLE,
			KeyConditionExpression: '#userID = :userID',
			ExpressionAttributeValues: {
				':userID': userID
			},
			ExpressionAttributeNames: {
				'#userID': 'userID'
			}
		};
		const { Count, Items } = await dbClient.query(queryParams).promise();
		return Count > 0 ? Items : [];
	} catch (e) {
		console.log('error', e);
		throw e;
	}
};

const getPatient = async (doctorId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID'
		}
	};

	return dbClient.query(params).promise();
};

const getFinanceData = async (userId, FINANCE_99454_TABLE) => {
	const params = {
		TableName: FINANCE_99454_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': userId
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	return dbClient.query(params).promise();
};

const getClaimData = async (ehrID, CLAIM_TABLE, from, to) => {
	const params = {
		TableName: CLAIM_TABLE,
		KeyConditionExpression:
			'#uid = :id AND yearMonth BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': ehrID,
			':minDate': from,
			':maxDate': to
		},
		ExpressionAttributeNames: {
			'#uid': 'ehrID'
		}
	};
	return dbClient.query(params).promise();
};

module.exports.getPatient = getPatient;
module.exports.verifyHospital = verifyHospital;
module.exports.get99453Data = get99453Data;
module.exports.getFinanceData = getFinanceData;
module.exports.getClaimData = getClaimData;
