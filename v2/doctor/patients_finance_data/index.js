const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();

const getYearMonth = timestamp => {
	const date = new Date(timestamp);
	let month = date.getMonth() + 1;
	month = month < CONSTANTS.TENTH ? CONSTANTS.ZERO + month : month;
	const year = date.getFullYear().toString();
	return year + month;
};

const getClaimData = async (
	hospitalID,
	CLAIM_TABLE,
	financialStartDate,
	financialEndDate,
	doctorPatientsData
) => {
	const claimData = [];
	const startYearMonth = parseInt(getYearMonth(parseInt(financialStartDate)));
	const endYearMonth = parseInt(getYearMonth(parseInt(financialEndDate)));
	const claimRecords = await DB.getClaimData(
		hospitalID,
		CLAIM_TABLE,
		startYearMonth,
		endYearMonth
	);
	if (claimRecords.Count > 0 && claimRecords.Items) {
		claimRecords.Items.map(data => {
			const claimYearMonth = {
				count:
					data.attributes && data.attributes.countOfUsers
						? data.attributes.countOfUsers
						: null,
				yearMonth: data.yearMonth ? data.yearMonth : null,
				claimTimestamp: data.attributes.claimTimestamp
					? data.attributes.claimTimestamp
					: null,
				status: data.attributes.status
					? data.attributes.status
					: CONSTANTS.READY_FOR_SUBMISSION,
				users: []
			};
			if (
				data.attributes &&
				data.attributes.userIDBilled &&
				data.attributes.userIDBilled.length !== 0
			) {
				const billedUserData = data.attributes.userIDBilled.map(
					billedDUserId => {
						const patientsNames = doctorPatientsData.filter(
							PatientsData =>
								PatientsData.userID && PatientsData.userID === billedDUserId
						);
						if (patientsNames.length !== 0) {
							return {
								patientName: patientsNames[0].name
									? patientsNames[0].name
									: null,
								firstName: patientsNames[0].firstName
									? patientsNames[0].firstName
									: null,
								lastName: patientsNames[0].lastName
									? patientsNames[0].lastName
									: null,
								profilePicture: patientsNames[0].profilePicture
									? patientsNames[0].profilePicture
									: null,
								icdCode: patientsNames[0].icdCode
									? patientsNames[0].icdCode
									: null,
								userID: billedDUserId || null
							};
						}
					}
				);
				claimYearMonth.users = billedUserData;
			}
			claimData.push(claimYearMonth);
		});
	}
	return Promise.resolve({
		success: true,
		claimData: claimData
	});
};

const get99453Data = (_99453Details, financialStartDate, financialEndDate) => {
	return _99453Details
		.filter(
			patientData =>
				patientData.attributes &&
				patientData.attributes.transaction &&
				patientData.attributes.transaction.dateOfService &&
				parseInt(patientData.attributes.transaction.dateOfService) >=
					parseInt(financialStartDate) &&
				parseInt(patientData.attributes.transaction.dateOfService) <=
					parseInt(financialEndDate)
		)
		.map(data => ({
			patientName: data.name ? data.name : null,
			firstName: data.firstName ? data.firstName : null,
			lastName: data.lastName ? data.lastName : null,
			profilePicture: data.profilePicture ? data.profilePicture : null,
			icdCode: data.icdCode ? data.icdCode : null,
			status: data.attributes.status
				? data.attributes.status
				: CONSTANTS.READY_FOR_SUBMISSION,
			claimTimestamp: data.attributes.claimTimestamp
				? data.attributes.claimTimestamp
				: null,
			dateOfService: data.attributes.transaction.dateOfService
				? data.attributes.transaction.dateOfService
				: null,
			userID: data.userID ? data.userID : null
		}));
};

const getFinanceData = async (
	doctorPatientsData,
	financialStartDate,
	financialEndDate,
	FINANCE_99453_TABLE,
	FINANCE_99454_TABLE
) => {
	const _99453Details = [];
	let financeData = [];
	for (let i = 0; i < doctorPatientsData.length; i++) {
		const userID = doctorPatientsData[i].userID;
		const _99453UserData = await DB.get99453Data(userID, FINANCE_99453_TABLE);
		if (_99453UserData.length > 0) {
			_99453UserData[0].name = doctorPatientsData[i].name
				? doctorPatientsData[i].name
				: null;
			_99453UserData[0].firstName = doctorPatientsData[i].firstName
				? doctorPatientsData[i].firstName
				: null;
			_99453UserData[0].lastName = doctorPatientsData[i].lastName
				? doctorPatientsData[i].lastName
				: null;
			_99453UserData[0].profilePicture = doctorPatientsData[i].profilePicture
				? doctorPatientsData[i].profilePicture
				: null;
			_99453UserData[0].icdCode = doctorPatientsData[i].icdCode
				? doctorPatientsData[i].icdCode
				: null;
			_99453Details.push(_99453UserData[0]);
		}

		const financeRecords = await DB.getFinanceData(userID, FINANCE_99454_TABLE);

		if (financeRecords.Count > 0 && financeRecords.Items) {
			const financeUserData = financeRecords.Items.filter(
				data =>
					parseInt(data.billingEndDate) >= parseInt(financialStartDate) &&
					parseInt(data.billingEndDate) <= parseInt(financialEndDate)
			).map(data => {
				return {
					patientName: doctorPatientsData[i].name
						? doctorPatientsData[i].name
						: null,
					firstName: doctorPatientsData[i].firstName
						? doctorPatientsData[i].firstName
						: null,
					lastName: doctorPatientsData[i].lastName
						? doctorPatientsData[i].lastName
						: null,
					profilePicture: doctorPatientsData[i].profilePicture
						? doctorPatientsData[i].profilePicture
						: null,
					icdCode: doctorPatientsData[i].icdCode
						? doctorPatientsData[i].icdCode
						: null,
					orderID: data.attributes.orderID ? data.attributes.orderID : null,
					status: data.attributes.status
						? data.attributes.status
						: CONSTANTS.READY_FOR_SUBMISSION,
					billingStartDate: data.billingStartDate
						? data.billingStartDate
						: null,
					billingEndDate: data.billingEndDate ? data.billingEndDate : null,
					claimTimestamp: data.attributes.claimTimestamp
						? data.attributes.claimTimestamp
						: null,
					userID: data.userID ? data.userID : null
				};
			});
			financeData = [...financeData, ...financeUserData];
		}
	}
	return Promise.resolve({
		success: true,
		financeData: financeData,
		_99453Details: _99453Details
	});
};

const getPatientsData = async Items => {
	try {
		const patients = Items.map(
			async ({
				userID,
				createdDate,
				modifiedDate,
				patientCode,
				attributes: {
					name,
					firstName,
					lastName,
					gender,
					dob,
					profilePicture,
					icdCode,
					...attributesRest
				},
				...rest
			}) => {
				let profilePic = null;
				if (profilePicture) {
					profilePic = await getProfileUrl(profilePicture);
				}
				return {
					userID,
					name,
					firstName,
					lastName,
					gender,
					dob,
					patientCode,
					icdCode,
					profilePicture: profilePic
				};
			}
		);
		return Promise.all(patients);
	} catch (error) {
		console.log('Error while getting Patient Data', error);
	}
};

const getDoctorPatients = async (doctorId, hospitalTable) => {
	const { Items } = await DB.getPatient(doctorId, hospitalTable);
	if (Items.length < 0) {
		return Promise.resolve({
			success: false
		});
	} else {
		const patients = await getPatientsData(Items);
		return Promise.resolve({
			success: true,
			data: patients
		});
	}
};

const getProfileUrl = async key => {
	const params = {
		Bucket: CONSTANTS.COMMON_IMAGE_BUCKET_NAME,
		Key: key
	};

	try {
		const url = await s3.getSignedUrl('getObject', params);
		return url;
	} catch (error) {
		console.log('Error while generating pre-signed url', error);
		// eslint-disable-next-line prefer-promise-reject-errors
		return Promise.reject({
			success: false,
			message: error,
			errorCode: CONSTANTS.ERRORS.S3_BUCKET_ERROR.CODE
		});
	}
};

const applyValidation = async ({
	doctorId,
	hospitalID,
	financialStartDate,
	financialEndDate
}) => {
	try {
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}

		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		if (!financialStartDate && financialEndDate) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.START_DATE_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.START_DATE_REQ.CODE
			});
		}
		if (!financialEndDate && financialStartDate) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.END_DATE_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.END_DATE_REQ.CODE
			});
		}

		if (!financialStartDate && !financialEndDate) {
			const currentDate = new Date();
			financialEndDate = currentDate.getTime();
			currentDate.setFullYear(currentDate.getFullYear() - 1);
			financialStartDate = currentDate.getTime();
		}

		let _99453Details = [];
		let financeData = [];
		let claimData = [];
		let doctorPatientsData;
		let _99453Data = [];

		const doctorPatientsRecords = await getDoctorPatients(
			doctorId,
			hospitalTableDetails.DOCTOR_PATIENT
		);
		if (doctorPatientsRecords.success) {
			doctorPatientsData = doctorPatientsRecords.data;

			const financeRecords = await getFinanceData(
				doctorPatientsData,
				financialStartDate,
				financialEndDate,
				hospitalTableDetails.FINANCE_99453_TABLE,
				hospitalTableDetails.FINANCE_99454_TABLE
			);

			if (financeRecords.success) {
				_99453Details = financeRecords._99453Details;
				financeData = financeRecords.financeData;
			}

			const claimRecords = await getClaimData(
				hospitalID,
				hospitalTableDetails.CLAIM_TABLE,
				financialStartDate,
				financialEndDate,
				doctorPatientsData
			);

			if (claimRecords.success) {
				claimData = claimRecords.claimData;
			}

			_99453Data = get99453Data(
				_99453Details,
				financialStartDate,
				financialEndDate
			);

			return Promise.resolve({
				success: true,
				data: {
					_99453: _99453Data,
					_99454: financeData,
					monthly: claimData
				}
			});
		} else {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.PATIENTS_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.PATIENTS_NOT_FOUND.CODE
			});
		}
	} catch (error) {
		return Promise.resolve({
			success: false,
			message: error,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
