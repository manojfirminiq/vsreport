exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_ID_REQ: {
		CODE: 'USER_ID_REQ',
		MESSAGE: 'User id required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	PATIENTS_NOT_FOUND: {
		CODE: 'PATIENTS_NOT_FOUND',
		MESSAGE: 'Patients not found'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	START_DATE_REQ: {
		CODE: 'START_DATE_REQ',
		MESSAGE: 'Start date required'
	},
	END_DATE_REQ: {
		CODE: 'END_DATE_REQ',
		MESSAGE: 'End date required'
	},
	S3_BUCKET_ERROR: {
		CODE: 'S3_BUCKET_ERROR'
	}
};

exports.RPM = '_rpm_';
exports.TENTH = 10;
exports.ZERO = '0';
exports.READY_FOR_SUBMISSION = 'READY_TO_SUBMIT';
exports.FIRST_CYCLE_COUNT = '16';
exports.DOCTOR_PATIENT_TABLE = process.env.DOCTOR_PATIENT_TABLE;
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.STAGE = process.env.STAGE;
exports.FINANCE_99453_TABLE = process.env.FINANCE_99453_TABLE;
exports.CLAIM_TABLE = process.env.CLAIM_TABLE;
exports.FINANCE_99454_TABLE = process.env.FINANCE_99454_TABLE;
exports.COMMON_IMAGE_BUCKET_NAME = process.env.COMMON_IMAGE_BUCKET_NAME;
