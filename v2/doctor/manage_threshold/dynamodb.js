const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const DOCTOR_PATIENT = CONSTANTS.DOCTOR_PATIENT;
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;
const EHR_USER = CONSTANTS.EHR_USER;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_PATIENT_TABLE:
				process.env.STAGE +
				'_rpm_' +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT,
			EHR_USER_TABLE:
				process.env.STAGE + '_rpm_' + Items[0].dbIdentifier + '_' + EHR_USER
		};
		return obj;
	}
};

const getPatient = async (doctorId, patientId, doctorPatientTable) => {
	const params = {
		TableName: doctorPatientTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #userId = :userId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userId': patientId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userId': 'userID'
		}
	};

	return dbClient.query(params).promise();
};

const getEhrDetails = async (userId, ehrTable) => {
	const params = {
		TableName: ehrTable,
		KeyConditionExpression: '#userId = :userId',
		ExpressionAttributeValues: {
			':userId': userId
		},
		ExpressionAttributeNames: {
			'#userId': 'userID'
		}
	};
	return dbClient.query(params).promise();
};

const updateThreshold = async (threshold, table) => {
	const params = {
		TableName: table,
		Item: threshold
	};
	return dbClient.put(params).promise();
};

module.exports.verifyHospital = verifyHospital;
module.exports.getPatient = getPatient;
module.exports.getEhrDetails = getEhrDetails;
module.exports.updateThreshold = updateThreshold;
