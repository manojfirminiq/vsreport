exports.RANGE = {
	DIA_LOW: 40,
	DIA_HIGH: 180,
	SYS_LOW: 60,
	SYS_HIGH: 230,
	WEIGHT_LOWER_LIMIT: 22,
	WEIGHT_UPPER_LIMIT: 300
};
exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_NOT_FOUND: {
		CODE: 'USER_NOT_FOUND',
		MESSAGE: 'User not found'
	},
	USER_ID_REQUIRED: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'User Id is required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	WEIGHT_LOWER_REQUIRED: {
		CODE: 'WEIGHT_GAIN_24_HOURS_REQUIRED',
		MESSAGE: 'Weight gain 24 hr parameter is required'
	},
	WEIGHT_UPPER_REQUIRED: {
		CODE: 'WEIGHT_GAIN_72_HOURS_REQUIRED',
		MESSAGE: 'Weight gain 72 hr parameter is required'
	},
	DIA_INVALID_RANGE: {
		CODE: 'DIA_INVALID_RANGE',
		MESSAGE: `dia should be range between ${this.RANGE.DIA_LOW} and ${this.RANGE.DIA_HIGH}`
	},
	SYS_INVALID_RANGE: {
		CODE: 'SYS_INVALID_RANGE',
		MESSAGE: `sys should be range between ${this.RANGE.SYS_LOW} and ${this.RANGE.SYS_HIGH}`
	},
	WEIGHT_INVALID_RANGE: {
		CODE: 'WEIGHT_INVALID_RANGE',
		MESSAGE: `weight should be range between ${this.RANGE.WEIGHT_LOWER_LIMIT} and ${this.RANGE.WEIGHT_UPPR_LIMIT}`
	},
	INPUT_VALIDATION_ERROR: {
		CODE: 'INPUT_VALIDATION_ERROR',
		MESSAGE: 'Error in input provided'
	}
};

exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.EHR_USER = process.env.EHR_USER;
exports.WEIGHT_UPPER_LIMIT = 20;
exports.WEIGHT_LOWER_LIMIT = 0;
