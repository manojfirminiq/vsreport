const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');
const trackPatient = require('track_patients_record');

const applyValidation = async ({
	doctorId,
	hospitalID,
	updateUserData,
	body: {
		userId,
		diaLow,
		diaHigh,
		sysLow,
		sysHigh,
		weight24hr,
		weight72hr,
		diaLowAverage,
		diaHighAverage,
		sysLowAverage,
		sysHighAverage,
		bpAverage
	}
}) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
			});
		}

		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}

		if (weight24hr) {
			if (
				isNaN(weight24hr) ||
				Number(weight24hr) < CONSTANTS.WEIGHT_LOWER_LIMIT ||
				Number(weight24hr) > CONSTANTS.WEIGHT_UPPER_LIMIT
			) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.MESSAGE,
					errorCode: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.CODE
				});
			}
		}

		if (weight72hr) {
			if (
				isNaN(weight72hr) ||
				Number(weight72hr) < CONSTANTS.WEIGHT_LOWER_LIMIT ||
				Number(weight72hr) > CONSTANTS.WEIGHT_UPPER_LIMIT
			) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.MESSAGE,
					errorCode: CONSTANTS.ERRORS.INPUT_VALIDATION_ERROR.CODE
				});
			}
		}

		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		const { Count, Items } = await DB.getPatient(
			doctorId,
			userId,
			hospitalTable.DOCTOR_PATIENT_TABLE
		);
		if (Count === 0) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
			});
		}

		Items[0].attributes.threshold.diaLow = Number(diaLow) || 0;
		Items[0].attributes.threshold.diaHigh = Number(diaHigh) || 0;
		Items[0].attributes.threshold.sysLow = Number(sysLow) || 0;
		Items[0].attributes.threshold.sysHigh = Number(sysHigh) || 0;
		Items[0].attributes.threshold.diaLowAverage = Number(diaLowAverage) || 0;
		Items[0].attributes.threshold.diaHighAverage = Number(diaHighAverage) || 0;
		Items[0].attributes.threshold.sysLowAverage = Number(sysLowAverage) || 0;
		Items[0].attributes.threshold.sysHighAverage = Number(sysHighAverage) || 0;
		Items[0].attributes.threshold.bpAverage = {};
		Items[0].attributes.threshold.bpAverage['7day'] =
			bpAverage && bpAverage['7day'] ? bpAverage['7day'] : 0;
		Items[0].attributes.threshold.bpAverage['10readings'] =
			bpAverage && bpAverage['10readings'] ? bpAverage['10readings'] : 0;
		// Items[0].attributes.threshold.weightLowerLimit = weightLowerLimit;
		// Items[0].attributes.threshold.weightUpperLimit = weightUpperLimit;
		Items[0].attributes.threshold.weight24hr = Number(weight24hr) || 0;
		Items[0].attributes.threshold.weight72hr = Number(weight72hr) || 0;
		Items[0].updates = Items[0].updates || [];
		Items[0].updates.push(updateUserData);

		await DB.updateThreshold(Items[0], hospitalTable.DOCTOR_PATIENT_TABLE);

		const ehrUserDetails = await DB.getEhrDetails(
			userId,
			hospitalTable.EHR_USER_TABLE
		);
		if (ehrUserDetails.Count > 0 && ehrUserDetails.Items[0]) {
			ehrUserDetails.Items[0].threshold.diaLowerAverage =
				Number(diaLowAverage) || 0;
			ehrUserDetails.Items[0].threshold.diaLowerLimit = Number(diaLow) || 0;
			ehrUserDetails.Items[0].threshold.diaUpperAverage =
				Number(diaHighAverage) || 0;
			ehrUserDetails.Items[0].threshold.diaUpperLimit = Number(diaHigh) || 0;
			ehrUserDetails.Items[0].threshold.sysLowerAverage =
				Number(sysLowAverage) || 0;
			ehrUserDetails.Items[0].threshold.sysLowerLimit = Number(sysLow) || 0;
			ehrUserDetails.Items[0].threshold.sysUpperAverage =
				Number(sysHighAverage) || 0;
			ehrUserDetails.Items[0].threshold.sysUpperLimit = Number(sysHigh) || 0;
			ehrUserDetails.Items[0].threshold.weight24hr = Number(weight24hr) || 0;
			ehrUserDetails.Items[0].threshold.weight72hr = Number(weight72hr) || 0;
			ehrUserDetails.Items[0].threshold.bpAverage = {};
			ehrUserDetails.Items[0].threshold.bpAverage['7day'] =
				bpAverage && bpAverage['7day'] ? bpAverage['7day'] : 0;
			ehrUserDetails.Items[0].threshold.bpAverage['10readings'] =
				bpAverage && bpAverage['10readings'] ? bpAverage['10readings'] : 0;
			await DB.updateThreshold(
				ehrUserDetails.Items[0],
				hospitalTable.EHR_USER_TABLE
			);
		}

		return Promise.resolve({
			success: true
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
