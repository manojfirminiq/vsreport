exports.VALIDATION_MESSAGES = {
	REQUIRED_PARAMETER: 'Missing required parameter',
	INVALID_GRANT: 'Invalid session for the user, session is expired',
	OTP_NOT_MATCH: 'Verification OTP not match',
	UNEXPECTED_ERROR: 'unexpected error found',
	USER_NOT_FOUND: '"User does not exist'
};

exports.ERROR_CODES = {
	REQUIRED_PARAMETER: 'REQUIRED_PARAMETER',
	INVALID_GRANT: 'INVALID_GRANT',
	OTP_NOT_MATCH: 'OTP_NOT_MATCH',
	UNEXPECTED_ERROR: 'UNEXPECTED_ERROR',
	USER_NOT_FOUND: 'USER_NOT_FOUND'
};

exports.COGNITO_ERROR = {
	INVALID_GRANT: 'NotAuthorizedException',
	REQUIRED_PARAMETER: 'InvalidParameterException',
	OTP_NOT_MATCH: 'CodeMismatchException',
	USER_NOT_FOUND: 'UserNotFoundException'
};
