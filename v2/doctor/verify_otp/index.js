const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
const CONSTANTS = require('./CONSTANTS.js');

const loginChallenge = async ({ otp, userName, sessionToken }) => {
	try {
		const response = await cognitoidentityserviceprovider
			.respondToAuthChallenge({
				ChallengeName: 'SMS_MFA',
				ChallengeResponses: {
					SMS_MFA_CODE: otp,
					USERNAME: userName
				},
				ClientId: process.env.CLIENT_ID,
				Session: sessionToken
			})
			.promise();
		return {
			success: true,
			accessToken: response.AuthenticationResult.IdToken,
			refreshToken: response.AuthenticationResult.RefreshToken,
			expiresIn: response.AuthenticationResult.ExpiresIn,
			tokenType: response.AuthenticationResult.TokenType,
			updateToken: response.AuthenticationResult.AccessToken
		};
	} catch (error) {
		console.log(JSON.stringify(error));
		const response = {
			success: false,
			errorCode: '',
			message: ''
		};
		switch (error.code) {
		case CONSTANTS.COGNITO_ERROR.INVALID_GRANT:
			response.errorCode = CONSTANTS.ERROR_CODES.INVALID_GRANT;
			response.message = CONSTANTS.VALIDATION_MESSAGES.INVALID_GRANT;
			break;
		case CONSTANTS.COGNITO_ERROR.REQUIRED_PARAMETER:
			response.errorCode = CONSTANTS.ERROR_CODES.REQUIRED_PARAMETER;
			response.message = CONSTANTS.VALIDATION_MESSAGES.REQUIRED_PARAMETER;
			break;
		case CONSTANTS.COGNITO_ERROR.OTP_NOT_MATCH:
			response.errorCode = CONSTANTS.ERROR_CODES.OTP_NOT_MATCH;
			response.message = CONSTANTS.VALIDATION_MESSAGES.OTP_NOT_MATCH;
			break;
		case CONSTANTS.COGNITO_ERROR.USER_NOT_FOUND:
			response.errorCode = CONSTANTS.ERROR_CODES.USER_NOT_FOUND;
			response.message = CONSTANTS.VALIDATION_MESSAGES.USER_NOT_FOUND;
			break;
		default:
			response.errorCode = CONSTANTS.ERROR_CODES.UNEXPECTED_ERROR;
			response.message = CONSTANTS.VALIDATION_MESSAGES.UNEXPECTED_ERROR;
		}
		return response;
	}
};
exports.handler = async (event, context, callback) => {
	const response = await loginChallenge(event);
	callback(null, response);
};
