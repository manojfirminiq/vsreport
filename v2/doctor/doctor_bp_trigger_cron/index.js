const AWS = require('aws-sdk');
const CONSTANTS = require('CONSTANTS');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const moment = require('moment-timezone');

const fetchWeight = async (userID, dbIdentifier) => {
	const params = {
		TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.NON_BP_READINGS_TABLE}`,
		KeyConditionExpression: 'userId_type_deviceLocalName = :id',
		ExpressionAttributeValues: {
			':id': userID + '_weight_Manual'
		},
		Limit: 10,
		ScanIndexForward: false
	};
	const { Items, Count } = await dbClient.query(params).promise();
	return Count ? Items : false;
};

const getImeiDetails = async (imei, dbIdentifier) => {
	try {
		const queryParams = {
			TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.RPM_USER_TABLE}`,
			IndexName: 'imei-index',
			KeyConditionExpression: 'imei = :imeikey',
			ExpressionAttributeValues: {
				':imeikey': imei
			}
		};
		return dbClient.query(queryParams).promise();
	} catch (e) {
		console.log('error', e);
		throw e;
	}
};

const fetchAllPatients = async dbIdentifier => {
	const params = {
		TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.DOCTOR_BP_AVERAGE_TABLE}`,
		ScanIndexForward: false
	};

	let patients = [];
	let lastPageKey;

	while (1) {
		if (lastPageKey) {
			params.ExclusiveStartKey = lastPageKey;
		}
		const { Items, Count, LastEvaluatedKey } = await dbClient
			.scan(params)
			.promise();
		if (Count) {
			patients = patients.concat(Items);
		}
		if (LastEvaluatedKey) {
			lastPageKey = LastEvaluatedKey;
		} else {
			break;
		}
	}
	return patients.length ? patients : false;
};

const fetchDoctorPatient = async (userID, dbIdentifier) => {
	const params = {
		TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.DOCTOR_PATIENT_TABLE}`,
		IndexName: 'userID-doctorID-index',
		KeyConditionExpression: 'userID = :id',
		FilterExpression: '#deleteFlag <> :deleteFlag AND #hubID <> :hubID ',
		ExpressionAttributeValues: {
			':id': userID,
			':deleteFlag': true,
			':hubID': ''
		},
		ExpressionAttributeNames: {
			'#hubID': 'hubID',
			'#deleteFlag': 'deleteFlag'
		},
		ScanIndexForward: false
	};
	const { Items, Count } = await dbClient.query(params).promise();
	return Count ? Items[0] : false;
};

const fetchHighLowAvgReading = record => {
	if (record && record.numberOfReadings > 0) {
		record.systolic = Math.round(record.systolic / record.numberOfReadings);
		record.diastolic = Math.round(record.diastolic / record.numberOfReadings);
		record.pulse = Math.round(record.pulse / record.numberOfReadings);
		record.irregularHB = parseInt(record.irregularHB);

		record.readings.sort(function (a, b) {
			return a.systolic - b.systolic;
		});
		record.highest.systolic =
			record.readings[record.readings.length - 1].systolic;
		record.lowest.systolic = record.readings[0].systolic;

		record.readings.sort(function (a, b) {
			return a.diastolic - b.diastolic;
		});
		record.highest.diastolic =
			record.readings[record.readings.length - 1].diastolic;
		record.lowest.diastolic = record.readings[0].diastolic;
	}
	if (record && record.readings) {
		delete record.readings;
	}

	return record;
};

const fetchLastReadings = async (userID, dbIdentifier, limit) => {
	const params = {
		TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.BP_READINGS_TABLE}`,
		KeyConditionExpression: 'userID = :id',
		ExpressionAttributeValues: {
			':id': userID
		},
		Limit: limit,
		ScanIndexForward: false
	};

	const { Items, Count } = await dbClient.query(params).promise();
	if (Count) {
		Items.sort(function (a, b) {
			return a.measurementDate - b.measurementDate;
		});
	}
	return Count ? Items : false;
};

const readingsAvg = (records, weight) => {
	const avg = {
		systolic: 0,
		diastolic: 0,
		pulse: 0,
		irregularHeartBeat: 0,
		lastReportedDate:
			records && records.length
				? parseInt(records[records.length - 1].measurementDate)
				: null,
		weight: weight && weight.attributes ? weight.attributes.weight : 0,
		lastSystolic: records && records.length && records[records.length - 1].attributes &&
			records[records.length - 1].attributes.systolic
			? records[records.length - 1].attributes.systolic : null,
		lastDiastolic: records && records.length && records[records.length - 1].attributes &&
			records[records.length - 1].attributes.diastolic
			? records[records.length - 1].attributes.diastolic : null
	};
	if (records && records.length) {
		for (const record of records) {
			avg.systolic += parseInt(record.attributes.systolic);
			avg.diastolic += parseInt(record.attributes.diastolic);
			avg.pulse += parseInt(record.attributes.pulse);
			if (record.attributes.irregularHB > 0) {
				avg.irregularHeartBeat += parseInt(record.attributes.irregularHB);
			}
		}

		avg.systolic = Math.round(avg.systolic / records.length);
		avg.diastolic = Math.round(avg.diastolic / records.length);
		avg.pulse = Math.round(avg.pulse / records.length);
	}

	if (
		weight &&
		weight.measurementDate &&
		weight.measurementDate > avg.lastReportedDate
	) {
		avg.lastReportedDate = weight.measurementDate;
	}

	const highest = {};
	const lowest = {};
	if (records && records.length) {
		records.sort(function (a, b) {
			return parseInt(a.attributes.systolic) - parseInt(b.attributes.systolic);
		});
	}
	highest.systolic =
		records && records.length
			? records[records.length - 1].attributes.systolic
			: 0;
	lowest.systolic =
		records && records.length ? records[0].attributes.systolic : 0;

	if (records && records.length) {
		records.sort(function (a, b) {
			return a.attributes.diastolic - b.attributes.diastolic;
		});
	}
	highest.diastolic =
		records && records.length
			? records[records.length - 1].attributes.diastolic
			: 0;
	lowest.diastolic =
		records && records.length ? records[0].attributes.diastolic : 0;
	avg.highest = highest;
	avg.lowest = lowest;

	return avg;
};

const formatRecords = async (bpReadings, userID, dbIdentifier, timeZone) => {
	const attributes = JSON.parse(JSON.stringify(CONSTANTS.attributes));

	for (const reading of bpReadings) {
		let startOfTheDay = new Date(new Date().setHours(0, 0, 0, 0));
		if (timeZone !== null && timeZone !== 'undefined') {
			const year = Number(
				moment()
					.tz(timeZone)
					.format('YYYY')
			);
			const month =
				Number(
					moment()
						.tz(timeZone)
						.format('MM')
				) - 1;
			const day = Number(
				moment()
					.tz(timeZone)
					.format('DD')
			);
			startOfTheDay = new Date(new Date().setHours(0, 0, 0, 0));
			startOfTheDay.setDate(day);
			startOfTheDay.setMonth(month);
			startOfTheDay.setFullYear(year);
		}

		const readingDate =
			parseInt(reading.measurementDate) +
			parseInt(reading.attributes.timeZone) * 1000;

		const lastNinetyDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 89
		);
		const lastThirtyDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 29
		);
		const lastSevenDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 6
		);
		const lastFourteenDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 13
		);
		const lastTwentyOneDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 20
		);
		const prevSevenDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 13
		);
		const prevFourteenDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 27
		);
		const prevTwentyOneDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 41
		);
		const prevNinetyDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 179
		);
		const prevThirtyDays = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 59
		);

		const yToD = new Date(new Date().getFullYear(), 0, 1).getTime();
		const prevYToD = new Date(new Date().getFullYear() - 1, 0, 1).getTime();
		const lastYearThisDate = new Date(new Date().setHours(0, 0, 0, 0)).setDate(
			startOfTheDay.getDate() - 365
		);

		if (readingDate >= lastFourteenDays) {
			attributes.lastFourteenDays.numberOfReadings++;
			attributes.lastFourteenDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.lastFourteenDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.lastFourteenDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.lastFourteenDays.irregularHB += 1;
			}
			attributes.lastFourteenDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		} else if (readingDate >= prevFourteenDays) {
			attributes.prevFourteenDays.numberOfReadings++;
			attributes.prevFourteenDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.prevFourteenDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.prevFourteenDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.prevFourteenDays.irregularHB += 1;
			}
			attributes.prevFourteenDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		}

		if (readingDate >= lastTwentyOneDays) {
			attributes.lastTwentyOneDays.numberOfReadings++;
			attributes.lastTwentyOneDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.lastTwentyOneDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.lastTwentyOneDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.lastTwentyOneDays.irregularHB += 1;
			}
			attributes.lastTwentyOneDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		} else if (readingDate >= prevTwentyOneDays) {
			attributes.prevTwentyOneDays.numberOfReadings++;
			attributes.prevTwentyOneDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.prevTwentyOneDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.prevTwentyOneDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.prevTwentyOneDays.irregularHB += 1;
			}
			attributes.prevTwentyOneDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		}

		if (readingDate >= lastNinetyDays) {
			attributes.lastNinetyDays.numberOfReadings++;
			attributes.lastNinetyDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.lastNinetyDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.lastNinetyDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.lastNinetyDays.irregularHB += 1;
			}
			attributes.lastNinetyDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		} else if (readingDate >= prevNinetyDays) {
			attributes.prevNinetyDays.numberOfReadings++;
			attributes.prevNinetyDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.prevNinetyDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.prevNinetyDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.prevNinetyDays.irregularHB += 1;
			}
			attributes.prevNinetyDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		}

		if (readingDate >= lastSevenDays) {
			attributes.lastSevenDays.numberOfReadings++;
			attributes.lastSevenDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.lastSevenDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.lastSevenDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.lastSevenDays.irregularHB += 1;
			}
			attributes.lastSevenDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		} else if (readingDate >= prevSevenDays) {
			attributes.prevSevenDays.numberOfReadings++;
			attributes.prevSevenDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.prevSevenDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.prevSevenDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.prevSevenDays.irregularHB += 1;
			}
			attributes.prevSevenDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		}

		if (readingDate >= lastThirtyDays) {
			attributes.lastThirtyDays.numberOfReadings++;
			attributes.lastThirtyDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.lastThirtyDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.lastThirtyDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.lastThirtyDays.irregularHB += 1;
			}
			attributes.lastThirtyDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		} else if (readingDate >= prevThirtyDays) {
			attributes.prevThirtyDays.numberOfReadings++;
			attributes.prevThirtyDays.diastolic += parseInt(
				reading.attributes.diastolic
			);
			attributes.prevThirtyDays.systolic += parseInt(
				reading.attributes.systolic
			);
			attributes.prevThirtyDays.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.prevThirtyDays.irregularHB += 1;
			}
			attributes.prevThirtyDays.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		}

		if (readingDate >= yToD) {
			attributes.YtoD.numberOfReadings++;
			attributes.YtoD.diastolic += parseInt(reading.attributes.diastolic);
			attributes.YtoD.systolic += parseInt(reading.attributes.systolic);
			attributes.YtoD.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.YtoD.irregularHB += 1;
			}
			attributes.YtoD.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		} else if (readingDate >= prevYToD && readingDate < lastYearThisDate) {
			attributes.prevYtoD.numberOfReadings++;
			attributes.prevYtoD.diastolic += parseInt(reading.attributes.diastolic);
			attributes.prevYtoD.systolic += parseInt(reading.attributes.systolic);
			attributes.prevYtoD.pulse += parseInt(reading.attributes.pulse);
			if (reading.attributes.irregularHB > 0) {
				attributes.prevYtoD.irregularHB += 1;
			}
			attributes.prevYtoD.readings.push({
				systolic: parseInt(reading.attributes.systolic),
				diastolic: parseInt(reading.attributes.diastolic)
			});
		}
	}

	attributes.lastFourteenDays = fetchHighLowAvgReading(
		attributes.lastFourteenDays
	);
	attributes.prevFourteenDays = fetchHighLowAvgReading(
		attributes.prevFourteenDays
	);

	attributes.lastTwentyOneDays = fetchHighLowAvgReading(
		attributes.lastTwentyOneDays
	);
	attributes.prevTwentyOneDays = fetchHighLowAvgReading(
		attributes.prevTwentyOneDays
	);

	attributes.lastSevenDays = fetchHighLowAvgReading(attributes.lastSevenDays);
	attributes.prevSevenDays = fetchHighLowAvgReading(attributes.prevSevenDays);

	attributes.lastThirtyDays = fetchHighLowAvgReading(attributes.lastThirtyDays);
	attributes.prevThirtyDays = fetchHighLowAvgReading(attributes.prevThirtyDays);

	attributes.lastNinetyDays = fetchHighLowAvgReading(attributes.lastNinetyDays);
	attributes.prevNinetyDays = fetchHighLowAvgReading(attributes.prevNinetyDays);

	attributes.YtoD = fetchHighLowAvgReading(attributes.YtoD);
	attributes.prevYtoD = fetchHighLowAvgReading(attributes.prevYtoD);
	// eslint-disable-next-line no-unused-vars
	let lastTenAvgReadings, lastFifteeenAvgReadings, lastTwentyAvgReadings;

	if (bpReadings.length >= 10) {
		lastTenAvgReadings = bpReadings.slice(
			bpReadings.length - 10,
			bpReadings.length
		);
	} else {
		lastTenAvgReadings = await fetchLastReadings(userID, dbIdentifier, 10);
	}

	if (bpReadings.length >= 15) {
		lastFifteeenAvgReadings = bpReadings.slice(
			bpReadings.length - 15,
			bpReadings.length
		);
	} else {
		lastFifteeenAvgReadings = await fetchLastReadings(userID, dbIdentifier, 15);
	}

	if (bpReadings.length >= 20) {
		lastTwentyAvgReadings = bpReadings.slice(
			bpReadings.length - 20,
			bpReadings.length
		);
	} else {
		lastTwentyAvgReadings = await fetchLastReadings(userID, dbIdentifier, 20);
	}

	const weight = await fetchWeight(userID, dbIdentifier);
	let weightReading;
	if (weight) {
		for (let i = 0; i < weight.length; i++) {
			if (weight[i].attributes.deleteFlag === '0') {
				weightReading = weight[i];
				break;
			}
		}
	}

	const avg = {};
	avg.lastTenAvgReadings = readingsAvg(lastTenAvgReadings, weightReading);
	avg.lastFifteeenAvgReadings = readingsAvg(
		lastFifteeenAvgReadings,
		weightReading
	);
	avg.lastTwentyAvgReadings = readingsAvg(lastTwentyAvgReadings, weightReading);
	return { attributes: attributes, userID: userID, avgReadings: avg };
};

const fetchBpReadings = async (userID, dbIdentifier, timeZone) => {
	const from = new Date(new Date().getFullYear() - 2, 11, 31).getTime(); // 1 day less then last year start Date
	const params = {
		TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.BP_READINGS_TABLE}`,
		KeyConditionExpression: 'userID = :id AND measurementDate > :lastYear',
		ExpressionAttributeValues: {
			':id': userID,
			':lastYear': from
		},
		ScanIndexForward: true // fetch readings in ascending order
	};
	let bpReadings = [];
	let lastPageKey;
	while (1) {
		if (lastPageKey) {
			params.ExclusiveStartKey = lastPageKey;
		}
		const bpR = await dbClient.query(params).promise();

		if (bpR.Count) {
			bpReadings = bpReadings.concat(bpR.Items);
		}
		if (bpR.LastEvaluatedKey) {
			lastPageKey = bpR.LastEvaluatedKey;
		} else {
			break;
		}
	}

	return await formatRecords(bpReadings, userID, dbIdentifier, timeZone);
};

const saveDoctorBPAverage = async (record, dbIdentifier) => {
	const params = {
		TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.DOCTOR_BP_AVERAGE_TABLE}`,
		Item: record
	};
	return dbClient.put(params).promise();
};

const saveDoctorPatient = async (record, dbIdentifier) => {
	const params = {
		TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.DOCTOR_PATIENT_TABLE}`,
		Item: record
	};
	return dbClient.put(params).promise();
};

const getHospitals = async () => {
	const params = {
		TableName: CONSTANTS.MASTER_HOSPITAL_TABLE
	};

	const { Items } = await dbClient.scan(params).promise();
	const hospitals = [];
	for (const item of Items) {
		hospitals.push(item);
	}

	return hospitals;
};

const identifyTable = (hospitals, eventSource) => {
	let dbIdentifier = null;
	Object.keys(hospitals).forEach(function (key) {
		if (eventSource.includes(hospitals[key].dbIdentifier)) {
			dbIdentifier = hospitals[key].dbIdentifier;
		}
	});

	return dbIdentifier;
};

const getDoctorProfile = async (doctorID, dbIdentifier) => {
	try {
		const queryParams = {
			TableName: `${process.env.STAGE}_${CONSTANTS.RPM}_${dbIdentifier}_${CONSTANTS.DOCTOR_PROFILE_TABLE}`,
			KeyConditionExpression: 'userID  = :doctorID',
			ExpressionAttributeValues: {
				':doctorID': doctorID
			}
		};
		return dbClient.query(queryParams).promise();
	} catch (e) {
		console.log('error', e);
		throw e;
	}
};

exports.handler = async (event, context) => {
	const userIds = new Set();
	const hospitals = await getHospitals();
	const hospitalIdentifier = {};
	if (event.Records) {
		// trigger flow
		for (const record of event.Records) {
			const dbIdentifier = identifyTable(hospitals, record.eventSourceARN);
			const recordToCheck =
				record.eventName === CONSTANTS.constants.insert ||
				record.eventName === CONSTANTS.constants.modify
					? AWS.DynamoDB.Converter.unmarshall(record.dynamodb.NewImage)
					: AWS.DynamoDB.Converter.unmarshall(record.dynamodb.OldImage);
			if (recordToCheck.attributes.isDoctorLinked) {
				let userId = null;
				if (recordToCheck.userId_type_deviceLocalName) {
					userId = recordToCheck.userId_type_deviceLocalName.split('_')[0];
				} else {
					userId = recordToCheck.userID;
				}
				userIds.add(userId);
				hospitalIdentifier[userId] = dbIdentifier;
			}
		}
	} else {
		// cron flow
		for (const hospital of hospitals) {
			if (!hospital.isEHR && hospital.isActive) {
				const patients = await fetchAllPatients(hospital.dbIdentifier);
				if (patients) {
					for (const patient of patients) {
						userIds.add(patient.userID);
						hospitalIdentifier[patient.userID] = hospital.dbIdentifier;
					}
				}
			}
		}
	}

	if (userIds.size) {
		for (const userId of userIds.values()) {
			let patientID;
			if (hospitalIdentifier[userId]) {
				if (hospitalIdentifier[userId] === CONSTANTS.HODES) {
					const imeiDetails = await getImeiDetails(
						Number(userId),
						hospitalIdentifier[userId]
					);
					if (imeiDetails.Items.length === 0) {
						continue;
					}
					patientID = imeiDetails.Items[0].userID;
				} else {
					patientID = userId;
				}

				const docPatient = await fetchDoctorPatient(
					patientID,
					hospitalIdentifier[userId]
				);
				if (docPatient) {
					let fetchUserId;
					let timeZone = null;
					const doctorProfile = await getDoctorProfile(
						docPatient.doctorID,
						hospitalIdentifier[userId]
					);
					if (doctorProfile.Items.length > 0) {
						timeZone = doctorProfile.Items[0].attributes.timeZone || null;
					}
					if (hospitalIdentifier[userId] === CONSTANTS.HODES) {
						if (!docPatient.hubID || docPatient.hubID.length === 0) {
							continue;
						}
						fetchUserId = docPatient.hubID;
					} else {
						fetchUserId = docPatient.userID;
					}
					const Item = await fetchBpReadings(
						String(fetchUserId),
						hospitalIdentifier[userId],
						timeZone
					);

					const avg = Item.avgReadings;
					delete Item.avgReadings;

					await saveDoctorBPAverage(Item, hospitalIdentifier[userId]);

					const averageBp = {};

					averageBp.lastTen = {
						systolic: avg.lastTenAvgReadings.systolic,
						diastolic: avg.lastTenAvgReadings.diastolic,
						pulse: avg.lastTenAvgReadings.pulse,
						lowest: avg.lastTenAvgReadings.lowest,
						highest: avg.lastTenAvgReadings.highest,
						irregularHB: avg.lastTenAvgReadings.irregularHeartBeat
					};

					averageBp.lastFifteen = {
						systolic: avg.lastFifteeenAvgReadings.systolic,
						diastolic: avg.lastFifteeenAvgReadings.diastolic,
						pulse: avg.lastFifteeenAvgReadings.pulse,
						lowest: avg.lastFifteeenAvgReadings.lowest,
						highest: avg.lastFifteeenAvgReadings.highest,
						irregularHB: avg.lastFifteeenAvgReadings.irregularHeartBeat
					};

					averageBp.lastTwenty = {
						systolic: avg.lastTwentyAvgReadings.systolic,
						diastolic: avg.lastTwentyAvgReadings.diastolic,
						pulse: avg.lastTwentyAvgReadings.pulse,
						lowest: avg.lastTwentyAvgReadings.lowest,
						highest: avg.lastTwentyAvgReadings.highest,
						irregularHB: avg.lastTwentyAvgReadings.irregularHeartBeat
					};

					averageBp.lastSevenDays = {
						systolic: Item.attributes.lastSevenDays.systolic,
						diastolic: Item.attributes.lastSevenDays.diastolic,
						pulse: Item.attributes.lastSevenDays.pulse,
						lowest: Item.attributes.lastSevenDays.lowest,
						highest: Item.attributes.lastSevenDays.highest,
						irregularHB: Item.attributes.lastSevenDays.irregularHB
					};

					averageBp.lastFourteenDays = {
						systolic: Item.attributes.lastFourteenDays.systolic,
						diastolic: Item.attributes.lastFourteenDays.diastolic,
						pulse: Item.attributes.lastFourteenDays.pulse,
						lowest: Item.attributes.lastSevenDays.lowest,
						highest: Item.attributes.lastSevenDays.highest,
						irregularHB: Item.attributes.lastFourteenDays.irregularHB
					};

					averageBp.lastTwentyOneDays = {
						systolic: Item.attributes.lastTwentyOneDays.systolic,
						diastolic: Item.attributes.lastTwentyOneDays.diastolic,
						pulse: Item.attributes.lastTwentyOneDays.pulse,
						lowest: Item.attributes.lastSevenDays.lowest,
						highest: Item.attributes.lastSevenDays.highest,
						irregularHB: Item.attributes.lastTwentyOneDays.irregularHB
					};

					docPatient.attributes.averageBp = averageBp;
					docPatient.attributes.lastReportedDate =
						avg.lastTenAvgReadings.lastReportedDate;
					docPatient.attributes.lastDiastolic = avg.lastTenAvgReadings.lastDiastolic;
					docPatient.attributes.lastSystolic = avg.lastTenAvgReadings.lastSystolic;
					if (docPatient.attributes.lastReportedDate) {
						delete docPatient.processStatus;
					}
					if (avg.lastTenAvgReadings.weight) {
						docPatient.attributes.weight = avg.lastTenAvgReadings.weight;
					}
					await saveDoctorPatient(docPatient, hospitalIdentifier[userId]);
				}
			}
		}
	}
	return `Successfully processed ${userIds.size} records.`;
};
