const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const getConnectionDetails = async (connectionTable, connectionId) => {
	const params = {
		TableName: connectionTable,
		KeyConditionExpression: '#connectionID = :id',
		ExpressionAttributeNames: {
			'#connectionID': 'connectionID'
		},
		ExpressionAttributeValues: {
			':id': connectionId
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count === 0) return false;
	return Items[0];
};

const getUserConnections = async (connectionTable, doctorID) => {
	const params = {
		TableName: connectionTable,
		IndexName: 'doctorID-index',
		KeyConditionExpression: '#doctorID = :doctorID',
		ExpressionAttributeNames: {
			'#doctorID': 'doctorID'
		},
		ExpressionAttributeValues: {
			':doctorID': doctorID
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count === 0) return false;
	return Items;
};

const updateConnectionDetails = async (connectionTable, Item) => {
	const params = {
		TableName: connectionTable,
		Item: Item
	};
	return await dbClient.put(params).promise();
};

const updatePatientConsultation = async (consultation, consultationTable) => {
	consultation.modifiedDate = new Date().getTime();
	const params = {
		TableName: consultationTable,
		Item: consultation
	};
	return await dbClient.put(params).promise();
};

const getLastConsultation = async (doctorId, consultationTable, userId) => {
	const params = {
		TableName: consultationTable,
		KeyConditions: {
			userID_startTime: {
				ComparisonOperator: 'BEGINS_WITH',
				AttributeValueList: [
					userId
				]
			},
			doctorID: {
				ComparisonOperator: 'EQ',
				AttributeValueList: [
					doctorId
				]
			}
		},
		ScanIndexForward: false
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count ? Items[0] : false;
};

const deleteConnection = async (connectionId, connectionTable) => {
	const params = {
		TableName: connectionTable,
		Key: {
			connectionID: connectionId
		}
	};
	await dbClient.delete(params).promise();
};
module.exports.getConnectionDetails = getConnectionDetails;
module.exports.getUserConnections = getUserConnections;
module.exports.getLastConsultation = getLastConsultation;
module.exports.updateConnectionDetails = updateConnectionDetails;
module.exports.updatePatientConsultation = updatePatientConsultation;
module.exports.deleteConnection = deleteConnection;
