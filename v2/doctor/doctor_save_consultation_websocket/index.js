const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');
const AWS = require('aws-sdk');
const trackPatient = require('track_patients_record');
AWS.config.update({
	region: process.env.REGION
});
const connectionTable = `${CONSTANTS.STAGE}${CONSTANTS.RPM}${CONSTANTS.TIMER_CONNECTION_TABLE}`;

const generateParams = (
	doctorId,
	userId,
	start,
	updateUserData,
	connectionID
) => {
	const params = {
		doctorID: doctorId,
		userID_startTime: userId + '_' + start,
		startTime: start,
		userID: userId,
		contactPatient: '',
		notRecordedConfirmation: '',
		notes: '',
		createdDate: Date.now(),
		consultationUnit: CONSTANTS.SECONDS,
		isdeleted: CONSTANTS.isdeleted,
		isManual: false,
		reason: '',
		updates: [],
		tempEndTime: start,
		connectionID: connectionID
	};
	params.updates.push(updateUserData);
	console.log('generated params');
	return params;
};

const checkConsultation = async (
	doctorId,
	consultationTable,
	userId,
	start,
	connectionID,
	patientConsultation
) => {
	let updateUserData;
	const trackUser = trackPatient.verifyUserType({
		doctorId: doctorId
	});
	if (trackUser.success) {
		updateUserData = trackUser.data;
	}

	const apigwManagementApi = new AWS.ApiGatewayManagementApi({
		apiVersion: '2018-11-29',
		endpoint: `${CONSTANTS.WEBSOCKET_DOMAIN}/${CONSTANTS.WEBSOCKET_STAGE}`
	});

	console.log('api gw management api created');
	const connectionArray = await DB.getUserConnections(connectionTable, doctorId);
	console.log('doctor connections fetched');
	let previousConnection;
	for (let i = 0; i < connectionArray.length; i++) {
		if (connectionArray[i].userID === userId && connectionArray[i].connectionID !== connectionID) {
			previousConnection = connectionArray[i];
			console.log('previous connection found');
			break;
		}
	}

	if (patientConsultation && !patientConsultation.endTime && patientConsultation.tempEndTime) { // checks if active consultations
		console.log(' consultation has no end time');
		if (previousConnection) {
			console.log('has prev connection');
			const consultationTime = JSON.stringify({
				success: true,
				start: patientConsultation.startTime,
				end: patientConsultation.tempEndTime
			});

			const postToConnectionParams = {
				ConnectionId: previousConnection.connectionID,
				Data: consultationTime
			};
			const newConnectionParams = {
				ConnectionId: connectionID,
				Data: consultationTime
			};

			const alreadyPresentParams = {
				ConnectionId: previousConnection.connectionID,
				Data: CONSTANTS.CONSULTATION_ALREADY_PRESENT
			};

			const postToConnectionPromiseArr = [];
			postToConnectionPromiseArr.push(apigwManagementApi.postToConnection(postToConnectionParams).promise());
			postToConnectionPromiseArr.push(apigwManagementApi.postToConnection(newConnectionParams).promise());
			postToConnectionPromiseArr.push(apigwManagementApi.postToConnection(alreadyPresentParams).promise());
			await Promise.all(postToConnectionPromiseArr);
		}
		if (patientConsultation.startTime === start) return new Error(CONSTANTS.ERRORS.ACTIVE_CONSULTATION_PRESENT.MESSAGE);

		if (patientConsultation.startTime === patientConsultation.tempEndTime) {
			patientConsultation.isdeleted = true;
		}

		patientConsultation.endTime = patientConsultation.tempEndTime;
		patientConsultation.consultationTime = (patientConsultation.endTime - patientConsultation.startTime) / 1000;
		patientConsultation.consultationUnit = CONSTANTS.SECONDS;
		delete patientConsultation.tempEndTime;
		delete patientConsultation.connectionID;
		await DB.updatePatientConsultation(patientConsultation, consultationTable);
		console.log('made last active consultation inactive');
	} else if (patientConsultation && !patientConsultation.endTime && !patientConsultation.tempEndTime) {
		patientConsultation.endTime = patientConsultation.startTime;
		patientConsultation.isdeleted = true;
		await DB.updatePatientConsultation(patientConsultation, consultationTable);
	}
	if (start < patientConsultation.endTime) {
		console.log(' consultation overlapping');
		console.log('start: ', start, 'tempEnd: ', patientConsultation.endTime);
		return new Error(CONSTANTS.ERRORS.CONSULTATION_OVERLAP.MESSAGE);
	}
	if (previousConnection) { // delete previous user connection
		const deleteConnectionParams = {
			ConnectionId: previousConnection.connectionID /* required */
		};
		console.log('prev connection deleteConnectionParams', deleteConnectionParams);
		await apigwManagementApi.deleteConnection(deleteConnectionParams).promise().catch(async (rejected) => {
			console.log('previous connection gone');
			await DB.deleteConnection(previousConnection.connectionID, connectionTable); // delete from timer table
		});
		console.log('deleted previous connection', previousConnection.connectionID);
	}

	return generateParams(
		doctorId,
		userId,
		start,
		updateUserData,
		connectionID
	);
};

const applyValidation = async (event) => {
	const {
		userId
	} = event.body;
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
			});
		}
		const connectionDetails = await DB.getConnectionDetails(connectionTable, event.connectionId);
		if (connectionDetails) {
			console.log('connection details present');
			console.log('connectionID', event.connectionId);
			const {
				doctorID,
				hospitalID,
				offSetInMilliSecs,
				dbIdentifier
			} = connectionDetails;

			if (!hospitalID) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
					errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
				});
			}

			if (!dbIdentifier) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
					errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
				});
			}
			const consultationTable = `${CONSTANTS.STAGE}${CONSTANTS.RPM}${dbIdentifier}_${CONSTANTS.DOCTOR_CONSULTATION}`;
			const patientConsultation = await DB.getLastConsultation(doctorID, consultationTable, userId);
			let start = event.requestTimeEpoch ? Number(event.requestTimeEpoch) : new Date().getTime();

			start = start + offSetInMilliSecs;
			if (!connectionDetails.userID || (connectionDetails.userID && connectionDetails.userID !== userId)) {
				connectionDetails.userID = userId;
				await DB.updateConnectionDetails(connectionTable, connectionDetails);
			}

			const params = await checkConsultation(doctorID, consultationTable, userId, start, event.connectionId, patientConsultation);
			console.log('new consultation is created');
			if (params instanceof Error) {
				throw new Error(params.message);
			}
			await DB.updatePatientConsultation(params, consultationTable);
			console.log('updated consultation table');
			const response = {
				success: true,
				start: start
			};
			console.log('reached end of consultation_time lambda with success message');
			return Promise.resolve(response);
		} else {
			console.log('no connection details');
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.CONNECTION_ID_ERROR.CODE,
				message: CONSTANTS.ERRORS.CONNECTION_ID_ERROR.MESSAGE
			});
		}
	} catch (error) {
		console.log('save consultation websocket error', error);
		if (error.message === CONSTANTS.ERRORS.ACTIVE_CONSULTATION_PRESENT.MESSAGE) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.ACTIVE_CONSULTATION_PRESENT.CODE,
				message: CONSTANTS.ERRORS.ACTIVE_CONSULTATION_PRESENT.MESSAGE
			});
		} else if (error.message === CONSTANTS.ERRORS.CONSULTATION_OVERLAP.MESSAGE) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.CONSULTATION_OVERLAP.CODE,
				message: CONSTANTS.ERRORS.CONSULTATION_OVERLAP.MESSAGE
			});
		} else if (error.message === CONSTANTS.ERRORS.TEMP_END_ERROR.MESSAGE) {
			return Promise.resolve({
				success: false,
				errorCode: CONSTANTS.ERRORS.TEMP_END_ERROR.CODE,
				message: CONSTANTS.ERRORS.TEMP_END_ERROR.MESSAGE
			});
		}
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const response = await applyValidation(event);
	callback(null, response);
};
