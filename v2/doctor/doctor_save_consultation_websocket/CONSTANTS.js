exports.TIMER_CONNECTION_TABLE = process.env.TIMER_CONNECTION_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = '_rpm_';
exports.WEBSOCKET_DOMAIN = process.env.WEBSOCKET_DOMAIN;
exports.WEBSOCKET_STAGE = process.env.WEBSOCKET_STAGE;
exports.isdeleted = false;
exports.SECONDS = 'seconds';
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	USER_ID_REQUIRED: {
		CODE: 'REQUIRED_PARAMETER',
		MESSAGE: 'User Id is required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	ACTIVE_CONSULTATION_PRESENT: {
		CODE: 'ACTIVE_CONSULTATION_PRESENT',
		MESSAGE: 'Consultation already active'
	},
	CONSULTATION_OVERLAP: {
		CODE: 'CONSULTATION_OVERLAP',
		MESSAGE: 'Consultation time is overlapping'
	},
	CONNECTION_ID_ERROR: {
		CODE: 'CONNECTION_ID_ERROR',
		MESSAGE: 'A connection with given connection Id doesn\'t exist'
	},
	TEMP_END_ERROR: {
		CODE: 'DATE_TIME_ERROR',
		MESSAGE: 'given update time is less than previous update time'
	}
};

exports.CONSULTATION_ALREADY_PRESENT = JSON.stringify({
	success: false,
	message: 'New Timer has started'
});
