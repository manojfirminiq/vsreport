exports.ERRORS = {
	NP_ID_NOT_FOUND: {
		MESSAGE: 'npID not found in Doctor Profile Table'
	},
	DB_IDENTIFIER_NOT_FOUND: {
		MESSAGE: 'dbIdentifier for the hospital is not found'
	},
	ORDER_ALREADY_PROCESSED: {
		MESSAGE: 'Order already processed'
	},
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR'
	},
	INVALID_PACEMAKER: {
		MESSAGE: 'Pacemaker value is not valid'
	}
};

exports.VALIDATION_MESSAGES = {
	FIRST_NAME_KEY_REQ: {
		MESSAGE: 'Patient First Name is required'
	},
	LAST_NAME_KEY_REQ: {
		MESSAGE: 'Patient Last Name is required'
	},
	ADDRESS_KEY_REQ: {
		MESSAGE: 'Patient Address is required'
	},
	MRN_KEY_REQ: {
		MESSAGE: 'MRN Number is required'
	},
	DEVICE_KEY_REQ: {
		MESSAGE: 'Device Type is required'
	},
	HOSPITAL_ID_KEY_REQ: {
		MESSAGE: 'Hospital ID is required'
	},
	DOCTOR_ID_KEY_REQ: {
		MESSAGE: 'Doctor ID is required'
	},
	MOBILE_REQ: {
		MESSAGE: 'Mobile Number is required'
	},
	GENDER_REQ: {
		MESSAGE: 'Gender is required'
	},
	SHIPPING_ADDRESS_REQ: {
		MESSAGE: 'Shipping Address is required'
	},
	DATE_OF_BIRTH_REQ: {
		MESSAGE: 'DOB is required'
	},
	FIRST_NAME_KEY_INVALID: {
		MESSAGE: 'Patient First Name is invalid'
	},
	LAST_NAME_KEY_INVALID: {
		MESSAGE: 'Patient Last Name is invalid'
	},
	MRN_KEY_INVALID: {
		MESSAGE: 'MRN Number is invalid'
	},
	EMAIL_KEY_INVALID: {
		MESSAGE: 'Patient email is invalid'
	},
	GENDER_KEY_INVALID: {
		MESSAGE: 'Gender is invalid'
	},
	MOBILE_KEY_INVALID: {
		MESSAGE: 'Mobile number is invalid'
	},
	HOME_PHONE_KEY_INVALID: {
		MESSAGE: 'Home phone number is invalid'
	},
	PATIENT_DOB_KEY_INVALID: {
		MESSAGE:
			'Patient dob is invalid. Please provide the dob in DD-MM-YYYY format'
	}
};

exports.SUCCESS = {
	SUBMIT_DETAILS: 'Sucessfully submitted details',
	ORDER_PROCESSING_UPDATE: 'Order Processing Updated successfully',
	DOCTOR_PATIENT: 'Doctor Patient Updated successfully'
};

exports.ORDER_PROCESSING = process.env.ORDER_PROCESSING;
exports.DOCTOR_PROFILE_TABLE = process.env.DOCTOR_PROFILE_TABLE;
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.STAGE = process.env.STAGE;
exports.RPM = 'rpm';
exports.ORDER_PROCESSING = 'order_processing';
exports.WEIGHT_UNIT = 'lbs';
exports.BP = 'BP';
exports.HUB = 'Hub';
exports.OMRON = 'Omron';
exports.SCALE = 'Scale';
exports.MRN = 'MRN';
exports.NPI = 'NPI';
exports.BP_MODELS = ['BP7250', 'HEM-9210T'];
exports.WEIGHT_MODELS = ['BCM500', 'HN-290T'];
exports.PROCESS_STATUS = 1;
exports.ORDER_PROCESSED_STATUS = 4;
exports.GENDER = ['m', 'f'];
exports.WEIGHT = 'Weight';
exports.SMALL = 'small';
exports.LARGE = 'large';
exports.START_DATE = '01-01-1910';
