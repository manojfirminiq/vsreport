const crypto = require('crypto');
const AWS = require('aws-sdk');
const XLSX = require('xlsx');
const moment = require('moment');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');

const saveItem = (item, TABLE_NAME) => {
	const params = {
		TableName: TABLE_NAME,
		Item: item
	};
	return dbClient.put(params).promise();
};

const getPatientDetails = async (userID, docPatientTable) => {
	const params = {
		TableName: docPatientTable,
		IndexName: 'userID-doctorID-index',
		KeyConditionExpression: '#userID = :userID ',
		ExpressionAttributeValues: {
			':userID': userID
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const { Items } = await dbClient.query(params).promise();
	return Items;
};

const generateUserId = mrn => {
	let userID = crypto
		.createHash('sha256')
		.update(mrn)
		.digest('hex');
	userID = crypto
		.createHash('sha256')
		.update(userID)
		.digest('hex');
	return userID;
};

const getNpIDFromDoctorProfile = async (doctorID, table) => {
	const params = {
		TableName: table,
		KeyConditionExpression: '#userID = :userID',
		ExpressionAttributeValues: {
			':userID': doctorID
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	return Count > 0 ? Items : false;
};

const getDbIdenitifierFromHospital = async hospitalID => {
	const params = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospitalID = :hospitalID',
		ExpressionAttributeValues: {
			':hospitalID': hospitalID
		},
		ExpressionAttributeNames: {
			'#hospitalID': 'hospitalID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();

	return Count > 0 ? Items : false;
};

const saveOrderProcessingDetail = async (
	userID,
	hospitalDetail,
	firstName,
	lastName,
	patientAddress,
	patientMRN,
	pacemaker,
	deviceType,
	hospitalID,
	doctorID,
	email,
	dob,
	threshold,
	gender,
	language,
	icdCode,
	officePhone,
	homePhone,
	mobilePhone,
	shippingAddress
) => {
	let response = {};
	const currentDate = new Date().getTime();
	const docProfileTable =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PROFILE_TABLE;
	const npIDDetails = await getNpIDFromDoctorProfile(doctorID, docProfileTable);
	if (npIDDetails && hospitalDetail) {
		const clinicalInfo = [];
		let bpModelNo = '';
		let weightModelNo = '';
		for (const device of deviceType) {
			if (device.type === CONSTANTS.BP) {
				clinicalInfo.push({
					code: 1,
					description: CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.BP
				});
				if (device.size.toLowerCase() === CONSTANTS.LARGE) {
					bpModelNo = CONSTANTS.BP_MODELS[1];
				} else {
					bpModelNo = CONSTANTS.BP_MODELS[0];
				}
			} else {
				clinicalInfo.push({
					code: 2,
					description:
						CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.SCALE
				});
				if (device.size.toLowerCase() === CONSTANTS.LARGE) {
					weightModelNo = CONSTANTS.WEIGHT_MODELS[1];
				} else {
					weightModelNo = CONSTANTS.WEIGHT_MODELS[0];
				}
			}
		}
		clinicalInfo.push({ code: 3, description: CONSTANTS.HUB });
		const item = {
			userID: userID,
			orderID_createdDate:
				hospitalDetail[0].orderIdentifier + currentDate + '_' + currentDate,
			modifiedDate: currentDate,
			createdDate: currentDate,
			attributes: {},
			processStatus: CONSTANTS.PROCESS_STATUS,
			orderDate: currentDate,
			orderTZ: 0, // local time, hence keeping it 0
			updates: []
		};
		if (!threshold) {
			threshold = {};
		}
		const attributes = {
			patientIdentifier: '',
			bpModelNo: bpModelNo,
			scaleModelNo: weightModelNo,
			transactionDateTime: currentDate,
			applicationOrderID: item.orderID_createdDate,
			collectionDateTime: currentDate,
			eventDateTime: currentDate,
			status: '',
			clinicalInfo: clinicalInfo,
			mrNumberType: CONSTANTS.MRN,
			firstName: firstName,
			middleName: '',
			lastName: lastName,
			name: `${firstName} ${lastName}`,
			dateOfBirth: dob,
			gender: gender || '',
			homePhone: homePhone ? homePhone.toString() : '',
			officePhone: officePhone || '',
			mobilePhone: mobilePhone.toString(),
			mrNumber: patientMRN.toString(),
			pacemaker: Number(pacemaker),
			ehrID: hospitalID,
			address: patientAddress,
			npID: npIDDetails[0].npID,
			ehrEmailAddress: [],
			notes: [],
			threshold: threshold,
			weightPrefUnit: CONSTANTS.WEIGHT_UNIT,
			goals: {
				sys: 0,
				dia: 0,
				pulse: 0,
				weight: 0
			},
			language: language,
			race: '',
			maritalStatus: '',
			visit: {
				visitNumber: '',
				accountNumber: '',
				visitDateTime: ''
			},
			diagnosis: [],
			code: '',
			codeSet: '',
			procedure: {
				code: '',
				codeSet: ''
			},
			provider: {
				ID: npIDDetails[0].npID,
				type: CONSTANTS.NPI,
				firstName: npIDDetails[0].attributes.name.split(' ')[0],
				lastName: npIDDetails[0].attributes.name.split(' ')[1]
			},
			shippingAddress: shippingAddress,
			icdCode: icdCode,
			facilityCode: ''
		};
		attributes.ehrEmailAddress.push(email);

		item.attributes = attributes;
		item.attributes.isDoctorLinked = 1;
		const tableName =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			hospitalDetail[0].dbIdentifier +
			'_' +
			CONSTANTS.ORDER_PROCESSING;
		await saveItem(item, tableName);
		return Promise.resolve({
			success: true,
			message: CONSTANTS.SUCCESS.SUBMIT_DETAILS
		});
	} else {
		if (!npIDDetails) {
			response = {
				mrnNumber: patientMRN,
				name: `${firstName} ${lastName}`,
				reason: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.MESSAGE
			};
		}
		if (!hospitalDetail) {
			response = {
				mrnNumber: patientMRN,
				name: `${firstName} ${lastName}`,
				reason: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.MESSAGE
			};
		}
		return response;
	}
};

const saveDoctorPatientDetails = async (
	userID,
	hospitalDetail,
	firstName,
	lastName,
	patientAddress,
	patientMRN,
	pacemaker,
	deviceType,
	hospitalID,
	doctorID,
	email,
	dob,
	threshold,
	gender,
	language,
	icdCode,
	officePhone,
	homePhone,
	mobilePhone,
	shippingAddress,
	consent,
	contactVai
) => {
	let response = {};
	const currentDate = new Date().getTime();
	const docProfileTable =
		CONSTANTS.STAGE +
		'_' +
		CONSTANTS.RPM +
		'_' +
		hospitalDetail[0].dbIdentifier +
		'_' +
		CONSTANTS.DOCTOR_PROFILE_TABLE;
	const npIDDetails = await getNpIDFromDoctorProfile(doctorID, docProfileTable);
	if (npIDDetails && hospitalDetail) {
		const clinicalInfo = [];
		for (const device of deviceType) {
			if (device.type === CONSTANTS.BP) {
				clinicalInfo.push({
					code: 1,
					description: CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.BP
				});
			} else {
				clinicalInfo.push({
					code: 2,
					description:
						CONSTANTS.OMRON + ' ' + device.size + ' ' + CONSTANTS.SCALE
				});
			}
		}
		clinicalInfo.push({ code: 3, description: CONSTANTS.HUB });
		const item = {
			userID: userID,
			modifiedDate: currentDate,
			createdDate: currentDate,
			doctorID: doctorID,
			processStatus: CONSTANTS.PROCESS_STATUS,
			patientCode: '',
			hubID: '',
			attributes: {},
			updates: []
		};
		if (!threshold) {
			threshold = {};
		}
		const attributesDoctorPatient = {
			dob: dob,
			gender: gender,
			irregularHeartBeat: '',
			lastReportedDate: '',
			language: language,
			consent: consent,
			contactVai: contactVai,
			firstName: firstName,
			lastName: lastName,
			pacemaker: Number(pacemaker),
			name: `${firstName} ${lastName}`,
			threshold: {
				diaHigh: threshold.diaUpperLimit
					? Number(threshold.diaUpperLimit)
					: null,
				diaLow: threshold.diaLowerLimit
					? Number(threshold.diaLowerLimit)
					: null,
				sysHigh: threshold.sysUpperLimit
					? Number(threshold.sysUpperLimit)
					: null,
				sysLow: threshold.sysLowerLimit
					? Number(threshold.sysLowerLimit)
					: null,
				diaLowAverage: threshold.diaLowerAverage
					? Number(threshold.diaLowerAverage)
					: null,
				diaHighAverage: threshold.diaUpperAverage
					? Number(threshold.diaUpperAverage)
					: null,
				sysLowAverage: threshold.sysLowerAverage
					? Number(threshold.sysLowerAverage)
					: null,
				sysHighAverage: threshold.sysUpperAverage
					? Number(threshold.sysUpperAverage)
					: null,
				bpAverage: threshold.bpAverage ? threshold.bpAverage : null,
				weight24hr: threshold.weight24hr ? Number(threshold.weight24hr) : null,
				weight72hr: threshold.weight72hr ? Number(threshold.weight72hr) : null
			},
			thresholdBpUnit: '',
			thresholdWeightUnit: '',
			timeZone: 0,
			icdCode: icdCode,
			shippingAddress: shippingAddress,
			emailAddress: email || '',
			officePhone: officePhone || '',
			homePhone: homePhone ? homePhone.toString() : '',
			mobilePhone: mobilePhone.toString()
		};
		item.attributes = attributesDoctorPatient;
		item.attributes.isDoctorLinked = 1;
		const tableName =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			hospitalDetail[0].dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PATIENT;
		await saveItem(item, tableName);
		return Promise.resolve({
			success: true,
			userId: userID,
			message: CONSTANTS.SUCCESS.SUBMIT_DETAILS,
			createdDate: currentDate
		});
	} else {
		if (!npIDDetails) {
			response = {
				mrnNumber: patientMRN,
				name: `${firstName} ${lastName}`,
				reason: CONSTANTS.ERRORS.NP_ID_NOT_FOUND.MESSAGE
			};
		}
		if (!hospitalDetail) {
			response = {
				mrnNumber: patientMRN,
				name: `${firstName} ${lastName}`,
				reason: CONSTANTS.ERRORS.DB_IDENTIFIER_NOT_FOUND.MESSAGE
			};
		}
		return response;
	}
};

const applyValidation = async (
	{
		doctorID,
		hospitalID,
		updateUserData,
		firstName,
		lastName,
		pacemaker,
		patientAddress,
		patientMRN,
		dateOfBirth,
		patientEmail,
		device,
		patientThreshold,
		gender,
		language,
		icdCode,
		officePhone,
		homePhone,
		mobilePhone,
		shippingAddress,
		consent,
		contactVai
	},
	hospitalDetail
) => {
	try {
		let response = {};
		let patientName = '';

		if (firstName && lastName) {
			patientName = `${firstName} ${lastName}`;
		} else {
			if (firstName && !lastName) {
				patientName = firstName;
			} else {
				patientName = '';
			}
		}
		let validRecord = true;

		if (!dateOfBirth) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.DATE_OF_BIRTH_REQ.MESSAGE
			};
			validRecord = false;
		} else {
			const END_DATE = moment(new Date()).format('YYYY-MM-DD');
			if (
				moment(dateOfBirth, 'DD-MM-YYYY').isValid() &&
				moment(dateOfBirth).isBetween(CONSTANTS.START_DATE, END_DATE)
			) {
				dateOfBirth = moment(dateOfBirth).format('YYYY-MM-DD');
			} else {
				response = {
					mrnNumber: patientMRN,
					name: patientName,
					reason: CONSTANTS.VALIDATION_MESSAGES.PATIENT_DOB_KEY_INVALID.MESSAGE
				};
				validRecord = false;
			}
		}

		if (!firstName) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.FIRST_NAME_KEY_REQ.MESSAGE
			};
			validRecord = false;
		}
		const isValidFirstName = new RegExp(/^[a-z,.'-]{3,50}$/i).test(firstName);
		if (!isValidFirstName) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.FIRST_NAME_KEY_INVALID.MESSAGE
			};
			validRecord = false;
		}
		if (!lastName) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.LAST_NAME_KEY_REQ.MESSAGE
			};
			validRecord = false;
		}
		const isValidLastName = new RegExp(/^[a-z0-9 ,.'-]{1,50}$/i).test(lastName);
		if (!isValidLastName) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.LAST_NAME_KEY_INVALID.MESSAGE
			};
			validRecord = false;
		}
		if (!patientAddress) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.ADDRESS_KEY_REQ.MESSAGE
			};
			validRecord = false;
		}
		if (!patientMRN) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.MRN_KEY_REQ.MESSAGE
			};
			validRecord = false;
		}
		const isValidPatientMRN = new RegExp(/^[a-zA-Z0-9 ]{3,50}$/).test(
			patientMRN
		);
		if (!isValidPatientMRN) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.MRN_KEY_INVALID.MESSAGE
			};
			validRecord = false;
		}
		if (patientEmail) {
			const isValidEmail = new RegExp(
				/^[a-zA-Z0-9](?!.*?[^\na-zA-Z0-9]{2})[^\s@]+@[^\s@]+\.[^\s@]+[a-zA-Z0-9]$/
			).test(patientEmail);
			if (!isValidEmail) {
				response = {
					mrnNumber: patientMRN,
					name: patientName,
					reason: CONSTANTS.VALIDATION_MESSAGES.EMAIL_KEY_INVALID.MESSAGE
				};
				validRecord = false;
			}
		}
		if (!device[0] || device[0].type !== CONSTANTS.BP) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.DEVICE_KEY_REQ.MESSAGE
			};
			validRecord = false;
		}
		if (!gender) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.GENDER_REQ.MESSAGE
			};
			validRecord = false;
		}
		if (gender) {
			const isValidGender = CONSTANTS.GENDER.includes(gender.toLowerCase());
			if (!isValidGender) {
				response = {
					mrnNumber: patientMRN,
					name: patientName,
					reason: CONSTANTS.VALIDATION_MESSAGES.GENDER_KEY_INVALID.MESSAGE
				};
				validRecord = false;
			}
		}
		if (!doctorID) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.DOCTOR_ID_KEY_REQ.MESSAGE
			};
			validRecord = false;
		}
		if (!hospitalID) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.HOSPITAL_ID_KEY_REQ.MESSAGE
			};
			validRecord = false;
		}
		if (!mobilePhone) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.MOBILE_REQ.MESSAGE
			};
			validRecord = false;
		}
		if (mobilePhone) {
			const isValidMobile = new RegExp(/^[0-9]{10}$/).test(mobilePhone);
			if (!isValidMobile) {
				response = {
					mrnNumber: patientMRN,
					name: patientName,
					reason: CONSTANTS.VALIDATION_MESSAGES.MOBILE_KEY_INVALID.MESSAGE
				};
				validRecord = false;
			}
		}
		if (!shippingAddress) {
			response = {
				mrnNumber: patientMRN,
				name: patientName,
				reason: CONSTANTS.VALIDATION_MESSAGES.SHIPPING_ADDRESS_REQ.MESSAGE
			};
			validRecord = false;
		}

		if (homePhone) {
			const isValidHomePhone = new RegExp(/^[0-9]{10}$/).test(homePhone);
			if (!isValidHomePhone) {
				response = {
					mrnNumber: patientMRN,
					name: patientName,
					reason: CONSTANTS.VALIDATION_MESSAGES.HOME_PHONE_KEY_INVALID.MESSAGE
				};
				validRecord = false;
			}
		}

		if (!pacemaker) {
			pacemaker = 0;
		} else {
			const validatePacemaker = new RegExp(/^[0|1]{1}$/).test(pacemaker);
			if (!validatePacemaker) {
				response = {
					mrnNumber: patientMRN,
					name: patientName,
					reason: CONSTANTS.ERRORS.INVALID_PACEMAKER.MESSAGE
				};
				validRecord = false;
			}
		}

		const docPatientTable =
			CONSTANTS.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			hospitalDetail[0].dbIdentifier +
			'_' +
			CONSTANTS.DOCTOR_PATIENT;
		const userID = await generateUserId(patientMRN.toString().toLowerCase());
		const patientDetails = await getPatientDetails(userID, docPatientTable);
		if (patientDetails.length > 0) {
			response = {
				name: patientName,
				enrolledDate: patientDetails[0].createdDate,
				mrnNumber: patientMRN
			};
			validRecord = false;
		}
		if (validRecord) {
			const saveOrderProcessingDetailPromise = Promise.resolve(
				saveOrderProcessingDetail(
					userID,
					hospitalDetail,
					firstName,
					lastName,
					patientAddress,
					patientMRN,
					pacemaker,
					device,
					hospitalID,
					doctorID,
					patientEmail,
					dateOfBirth,
					patientThreshold,
					gender,
					language,
					icdCode,
					officePhone,
					homePhone,
					mobilePhone,
					shippingAddress
				)
			);
			const saveDoctorPatientDetailsPromise = Promise.resolve(
				saveDoctorPatientDetails(
					userID,
					hospitalDetail,
					firstName,
					lastName,
					patientAddress,
					patientMRN,
					pacemaker,
					device,
					hospitalID,
					doctorID,
					patientEmail,
					dateOfBirth,
					patientThreshold,
					gender,
					language,
					icdCode,
					officePhone,
					homePhone,
					mobilePhone,
					shippingAddress,
					consent,
					contactVai
				)
			);
			return Promise.all([
				saveOrderProcessingDetailPromise,
				saveDoctorPatientDetailsPromise
			])
				.then(data => {
					const obj = {
						saveOrderProcessingDetail: data[0],
						saveDoctorPatientDetails: data[1]
					};
					if (
						obj &&
						obj.saveOrderProcessingDetail.success &&
						obj.saveDoctorPatientDetails.success
					) {
						response = {
							mrnNumber: patientMRN,
							name: patientName,
							createdDate: obj.saveDoctorPatientDetails.createdDate
						};
					} else {
						console.log(
							'Update error occured in Order Processing and Doctor Patient',
							obj
						);
					}
					return response;
				})
				.catch(e => {
					console.log('error>>', e);
				});
		} else {
			return response;
		}
	} catch (error) {
		console.log('error>>>>', error);
		return Promise.resolve({
			success: false,
			message: error.message,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

const readExcel = async function (record) {
	try {
		if (record) {
			const workbook = XLSX.read(record, {
				type: 'buffer',
				cellDates: true
			});
			const sheetNames = workbook.SheetNames;
			let data = [];
			const sheetData = XLSX.utils.sheet_to_json(
				workbook.Sheets[sheetNames[0]],
				{
					dateNF: 'dd-mm-yyyy'
				}
			);
			let keys = [];
			if (sheetData.length !== 0) {
				keys = Object.keys(sheetData[0]);
			}
			if (keys[0] !== 'patientMRN' || keys.length === 0) {
				return Promise.resolve({
					success: false,
					message: 'Invalid excel file'
				});
			}
			data = [...data, ...sheetData];
			return data;
		}
	} catch (err) {
		console.log(err);
	}
};

exports.handler = async (event, context, callback) => {
	console.log('event----->', event);
	const hospitalDetail = await getDbIdenitifierFromHospital(event.hospitalID);
	let finalResponse = {};
	const dataBuffer = Buffer.from(
		JSON.stringify(event.body.base64.replace(/^data:\/\w+;base64,/, '')),
		'base64'
	);
	let data = await Promise.resolve(readExcel(dataBuffer));
	data = [data];
	if (data[0] && data[0].success === false) {
		finalResponse = data[0];
		callback(null, finalResponse);
	}
	data = data.flat();
	const excelDataArray = Array.isArray(data) ? data : [data];
	const newExcelArray = [];
	const previouslyEnrolledOrders = [];
	excelDataArray.map(record => {
		if (
			newExcelArray.filter(
				newRecord => newRecord.patientMRN === record.patientMRN
			).length < 1
		) {
			newExcelArray.push(record);
		} else {
			const response = {
				name: record.firstName + ' ' + record.lastName,
				enrolledDate: null,
				mrnNumber: record.patientMRN
			};
			previouslyEnrolledOrders.push(response);
		}
	});
	const failedOrders = [];
	const successOrders = [];

	const excelPromiseArray = newExcelArray.map(record => {
		return new Promise((resolve, reject) => {
			let modifyPatientRecord = Object.assign(record, {});

			if (
				!modifyPatientRecord.addressLine1 ||
				!modifyPatientRecord.city ||
				!modifyPatientRecord.state ||
				!modifyPatientRecord.zip
			) {
				resolve({
					mrnNumber: modifyPatientRecord.patientMRN,
					name: `${modifyPatientRecord.firstName} ${modifyPatientRecord.lastName}`,
					reason: CONSTANTS.VALIDATION_MESSAGES.ADDRESS_KEY_REQ.MESSAGE
				});
			} else {
				modifyPatientRecord = {
					...modifyPatientRecord,
					patientAddress: {
						city: record.city,
						state: record.state,
						streetAddress: record.addressLine2
							? record.addressLine1 + ' ' + record.addressLine2
							: record.addressLine1,
						zip: record.zip.toString()
					}
				};
			}

			if (
				record.shippingAddressLine1 &&
				record.shippingCity &&
				record.shippingZip &&
				record.shippingState
			) {
				modifyPatientRecord.shippingAddress = {
					city: record.shippingCity,
					state: record.shippingState,
					streetAddress: record.shippingAddressLine2
						? record.shippingAddressLine1 + ' ' + record.shippingAddressLine2
						: record.shippingAddressLine1,
					zip: record.shippingZip.toString()
				};
			} else {
				modifyPatientRecord.shippingAddress =
					modifyPatientRecord.patientAddress;
			}
			const device = [];
			if (
				record.bpDevice &&
				(record.bpDevice.toLowerCase() === CONSTANTS.SMALL ||
					record.bpDevice.toLowerCase() === CONSTANTS.LARGE)
			) {
				const devicearr = {
					type: CONSTANTS.BP,
					size: record.bpDevice
				};
				device.push(devicearr);
			}
			if (record.pacemaker === undefined || record.pacemaker === 0) {
				if (
					record.weightScale &&
					(record.weightScale.toLowerCase() === CONSTANTS.SMALL ||
						record.weightScale.toLowerCase() === CONSTANTS.LARGE)
				) {
					const devicearr = {
						type: CONSTANTS.WEIGHT,
						size: record.weightScale
					};
					device.push(devicearr);
				}
			}

			modifyPatientRecord.device = device;
			modifyPatientRecord.consent = false;
			modifyPatientRecord.contactVai = {
				email: false,
				phone: false
			};
			const icdcode = [];
			modifyPatientRecord.icdCode = icdcode;
			modifyPatientRecord.doctorID = event.doctorId;
			modifyPatientRecord.hospitalID = event.hospitalID;
			resolve(applyValidation(modifyPatientRecord, hospitalDetail));
		});
	});
	return Promise.all(excelPromiseArray)
		.then(resultArray => {
			return resultArray.map(result => {
				if (result.reason || result.success === false) {
					failedOrders.push(result);
				} else if (result.enrolledDate) {
					previouslyEnrolledOrders.push(result);
				} else {
					successOrders.push(result);
				}
			});
		})
		.then(() => {
			if (finalResponse.success !== false) {
				let newPreviouslyEnrolledOrders = [];
				newPreviouslyEnrolledOrders = previouslyEnrolledOrders
					.map(record => {
						if (!record.enrolledDate) {
							const tempRecord = successOrders.filter(
								successRecord => successRecord.patientMRN === record.patientMRN
							);
							if (tempRecord.length) {
								record.enrolledDate = tempRecord[0].createdDate;
							}
						}
						return record;
					})
					.map(record => {
						if (!record.enrolledDate) {
							record.enrolledDate = previouslyEnrolledOrders.filter(
								prvRecord =>
									prvRecord.enrolledDate &&
									prvRecord.patientMRN === record.patientMRN
							)[0].enrolledDate;
						}
						return record;
					});

				finalResponse = {
					success: true,
					failedOrders: failedOrders,
					successOrders: successOrders,
					previouslyEnrolledOrders: newPreviouslyEnrolledOrders
				};
			}
			callback(null, finalResponse);
		})
		.catch(e => {
			console.log('e>>>>>', e);
			callback(null, []);
		});
};
