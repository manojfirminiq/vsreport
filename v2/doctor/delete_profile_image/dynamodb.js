const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: CONSTANTS.HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			Items,
			DOCTOR_PROFILE:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.DOCTOR_PROFILE_TABLE,
			DOCTOR_PATIENT:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.DOCTOR_PATIENT_TABLE
		};
		return obj;
	}
};

const getPatient = async (doctorId, userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #userId = :userId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userId': userId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userId': 'userID'
		}
	};

	return dbClient.query(params).promise();
};

const updateDoctorUserProfile = async (
	user,
	profileKey,
	tableName,
	updateUserData
) => {
	const params = {
		TableName: tableName,
		Key: {
			userID: user
		},
		UpdateExpression:
			'set attributes.profilePicture = :val, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':val': null,
			':updateUserData': [updateUserData],
      		':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ReturnValues: 'UPDATED_NEW'
	};
	const docClient = new AWS.DynamoDB.DocumentClient();
	return docClient.update(params).promise();
};

const updatePatientUserProfile = async (
	doctorId,
	user,
	profileKey,
	tableName,
	updateUserData
) => {
	const params = {
		TableName: tableName,
		Key: {
			doctorID: doctorId,
			userID: user
		},
		UpdateExpression:
			'set attributes.profilePicture = :val, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':val': null,
			':updateUserData': [updateUserData],
      		':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ReturnValues: 'UPDATED_NEW'
	};
	const docClient = new AWS.DynamoDB.DocumentClient();
	return docClient.update(params).promise();
};

module.exports.getPatient = getPatient;
module.exports.verifyHospital = verifyHospital;
module.exports.updateDoctorUserProfile = updateDoctorUserProfile;
module.exports.updatePatientUserProfile = updatePatientUserProfile;
