const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const s3Client = new AWS.S3();
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./dynamodb');
const s3Bucket = CONSTANTS.PROFILE_BUCKET;
const trackPatient = require('track_patients_record');

const deleteDoctorImageToS3 = async (
	user,
	hospitalName,
	type,
	tableName,
	updateUserData
) => {
	const key = user;
	const params = {
		Bucket: s3Bucket,
		Key: key
	};

	await s3Client.deleteObject(params).promise();
	await DB.updateDoctorUserProfile(user, key, tableName, updateUserData);
	return { success: true, message: 'Profile image deleted' };
};

const deletePatientImageToS3 = async (
	doctorId,
	user,
	hospitalName,
	type,
	tableName,
	updateUserData
) => {
	const key = `${hospitalName}/${type}/${user}`;
	const params = {
		Bucket: s3Bucket,
		Key: key
	};
	await s3Client.deleteObject(params).promise();
	await DB.updatePatientUserProfile(
		doctorId,
		user,
		key,
		tableName,
		updateUserData
	);
	return { success: true, message: 'Profile image deleted' };
};

const applyValidation = async ({
	doctorId,
	hospitalID,
	updateUserData,
	body: { userId, type }
}) => {
	try {
		if (!type) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.TYPE_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.TYPE_REQ.CODE
			});
		}
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		if (type.toLowerCase() === CONSTANTS.TYPE.DOCTOR) {
			return await deleteDoctorImageToS3(
				doctorId,
				hospitalTableDetails.Items[0].name,
				type.toLowerCase(),
				hospitalTableDetails.DOCTOR_PROFILE,
				updateUserData
			);
		} else if (type.toLowerCase() === CONSTANTS.TYPE.PATIENT) {
			if (!userId) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
					errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
				});
			}
			const { Count, Items } = await DB.getPatient(
				doctorId,
				userId,
				hospitalTableDetails.DOCTOR_PATIENT
			);
			if (Count === 0) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.USER_NOT_FOUND,
					errorCode: 'USER_NOT_FOUND'
				});
			}
			return await deletePatientImageToS3(
				doctorId,
				Items[0].userID,
				hospitalTableDetails.Items[0].name,
				type.toLowerCase(),
				hospitalTableDetails.DOCTOR_PATIENT,
				updateUserData
			);
		} else {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.TYPE_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.TYPE_INVALID.CODE
			});
		}
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};
exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
