const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');
const trackPatient = require('track_patients_record');

const applyValidation = async ({
	doctorId,
	method,
	userId,
	hospitalID,
	updateUserData,
	start
}) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQUIRED.CODE
			});
		}
		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const params = {
			doctorID: doctorId,
			userID_startDate: userId + '_' + start,
			userID: userId,
			createdDate: Date.now()
		};

		if (!start) {
			const date = new Date();
			date.setMinutes(0);
			date.setSeconds(0);
			const startDate = date.setMilliseconds(0);
			params.userID_startDate = userId + '_' + startDate;
			params.startDate = startDate;
		} else {
			params.startDate = start;
			params.userID_startDate = userId + '_' + start;
		}

		if (method.toLowerCase() === CONSTANTS.DELETE) {
			const Items = await DB.getNotesStartDate(
				hospitalTable,
				doctorId,
				userId,
				start
			);
			if (Items) {
				Items[0].updates = Items[0].updates || [];
				Items[0].updates.push(updateUserData);
				Items[0].deleteFlag = 1;
				await DB.flagNotesForDelete(Items[0], hospitalTable);
			}
		}

		return Promise.resolve({
			success: true
		});
	} catch (error) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
