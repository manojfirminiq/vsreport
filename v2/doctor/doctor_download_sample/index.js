const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const s3Client = new AWS.S3();

const S3_KEY_EXPIRES_IN = 604800;
const S3_BUCKET = process.env.S3_BUCKET;
const FOLDER = process.env.FOLDER;

const getDoctorReportSignedUrl = () => {
	return s3Client.getSignedUrl('getObject', {
		Bucket: S3_BUCKET,
		Key: FOLDER,
		Expires: S3_KEY_EXPIRES_IN
	});
};

exports.handler = async (event, context, callback) => {
	console.log('event', event);
	const response = await getDoctorReportSignedUrl();
	if (response) {
		return Promise.resolve({
			success: true,
			s3Url: response
		});
	}
	callback(null, response);
};
