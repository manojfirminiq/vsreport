const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

const CONSTANTS = require('./CONSTANTS');

const forgotPassword = async emailAddress => {
	const params = { Username: emailAddress, ClientId: process.env.CLIENT_ID };
	return new Promise(resolve => {
		cognitoidentityserviceprovider.forgotPassword(params, (err, res) => {
			if (err) {
				console.log('Error while performing forgotPassword', err);
				// Removing other error messages to avoid valid/ invalid email guess.
				if (err.code === CONSTANTS.ERRORS.LIMIT_EXCEEDED_EXCEPTION) {
					return resolve({
						success: false,
						message: CONSTANTS.VALIDATION_MESSAGES.LIMIT_EXCEEDED_EXCEPTION,
						errorCode: 'LIMIT_EXCEEDED_EXCEPTION'
					});
				}
			}
			return resolve({
				success: true,
				message: CONSTANTS.RESET_PASSWORD_MESSAGES.SUCCESS
			});
		});
	});
};

module.exports.forgotPassword = forgotPassword;
