exports.VALIDATION_MESSAGES = {
	EMAIL_REQ: 'Please provide a valid emailAddress',
	PASSWORD_REQ: 'Please provide a valid password',
	DEVICENAME_REQ: 'Please provide a devicelocalName',
	USER_NOT_FOUND: 'User not found',
	USER_ALREADY_EXIST: 'User already exist',
	INVALID_INPUT: 'Please check the input parameters!',
	INVALID_PASSWORD: 'Password not long enough',
	INVALID_STUDY_CODE: 'Invalid study code',
	INVALID_REGION: 'Invalid Region.',
	INVALID_PARAMETER_EXCEPTION:
		'Cannot reset password for the user as there is no registered/verified email or phone number',
	USER_NOT_FOUND_EXCEPTION: 'Username/client id combination not found.',
	LIMIT_EXCEEDED_EXCEPTION:
		'Attempt limit exceeded, please try after some time.'
};

exports.ERRORS = {
	LIMIT_EXCEEDED_EXCEPTION: 'LimitExceededException',
	INVALID_PARAMETER_EXCEPTION: 'InvalidParameterException',
	USER_NOT_FOUND_EXCEPTION: 'UserNotFoundException'
};

exports.MESSAGES = {
	SUCCESS:
		'We have sent you a verification mail to the email address you provided. Please check your inbox and click on the verification link in the mail to proceed with the account setup'
};

exports.RESET_PASSWORD_MESSAGES = {
	SUCCESS: 'Check your inbox for the link to reset your password'
};
