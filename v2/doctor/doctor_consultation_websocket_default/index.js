exports.handler = async (event) => {
	const response = {
		statusCode: 200,
		body: JSON.stringify({ success: false, message: 'unrecognized action' })
	};
	return response;
};
