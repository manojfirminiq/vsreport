const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS.js');
const AWS = require('aws-sdk');
const s3Client = new AWS.S3();

const getUserProfileUrl = profilePicture => {
	return s3Client.getSignedUrl('getObject', {
		Bucket: CONSTANTS.PROFILE_BUCKET,
		Key: profilePicture,
		Expires: CONSTANTS.S3_KEY_EXPIRES_IN
	});
};

const getPatientFinanceData = async (patient, hospitalTableDetails, startDate, endDate, nextPaginationKey) => {
	let financeObject = {
		billingDetails: []
	};
	const userId = patient.userID;

	let finalStatus = '';

	const fromMonth = new Date(parseInt(startDate)).getMonth() + 1;
	const toMonth = new Date(parseInt(endDate)).getMonth() + 1;

	const financePromiseCalls = [];

	financePromiseCalls.push(DB.get99453Data(
		userId,
		hospitalTableDetails.TABLE_99453,
		startDate,
		endDate,
		nextPaginationKey
	));
	financePromiseCalls.push(DB.get99454Data(
		userId,
		hospitalTableDetails.TABLE_99454,
		startDate,
		endDate,
		nextPaginationKey
	));
	financePromiseCalls.push(DB.get99457Data(
		userId,
		hospitalTableDetails.TABLE_99457,
		startDate,
		endDate,
		nextPaginationKey
	));
	financePromiseCalls.push(DB.get99458Data(
		userId,
		hospitalTableDetails.TABLE_99458,
		startDate,
		endDate,
		nextPaginationKey
	));

	const [data99453, data99454, data99457, data99458] = await Promise.all(financePromiseCalls);
	if (fromMonth !== toMonth) {
		finalStatus = null;
	} else {
		if (
			(data99453.Items.length > 0 &&
                data99453.Items[0].attributes.status ===
                CONSTANTS.FINANCIAL_STATUS.READY_TO_SUBMIT.statusCode) ||
            (data99457.Items.length > 0 &&
                data99457.Items[0].attributes.status ===
                CONSTANTS.FINANCIAL_STATUS.READY_TO_SUBMIT.statusCode) ||
            (data99458.Items.length > 0 &&
                data99458.Items[0].attributes.status ===
                CONSTANTS.FINANCIAL_STATUS.READY_TO_SUBMIT.statusCode)
		) {
			finalStatus = CONSTANTS.FINANCIAL_STATUS.READY_TO_SUBMIT;
		} else if (
			(data99453.Items.length === 0 ||
                data99453.Items[0].attributes.status ===
                CONSTANTS.FINANCIAL_STATUS.SUBMITTED.statusCode) &&
            (data99457.Items.length === 0 ||
                data99457.Items[0].attributes.status ===
                CONSTANTS.FINANCIAL_STATUS.SUBMITTED.statusCode) &&
            (data99458.Items.length === 0 ||
                data99458.Items[0].attributes.status ===
                CONSTANTS.FINANCIAL_STATUS.SUBMITTED.statusCode)
		) {
			finalStatus = CONSTANTS.FINANCIAL_STATUS.SUBMITTED;
		}
	}

	let profilepic;
	if (patient.attributes.profilePicture) {
		profilepic = getUserProfileUrl(patient.attributes.profilePicture);
	} else {
		profilepic = null;
	}

	data99453.Items.forEach(function (Item) {
		const billingDetail = {
			code: CONSTANTS.FINANCIAL_CODE.CODE_99453,
			dateOfService: Item.attributes.transaction.dateOfService // epoch
		};
		financeObject.billingDetails.push(billingDetail);
	});

	data99454.Items.forEach(function (Item) {
		const billingDetail = {
			code: CONSTANTS.FINANCIAL_CODE.CODE_99454,
			dateOfService: Item.attributes.transaction.dateOfService, // epoch
			billingStartDate: Item.billingStartDate
		};
		financeObject.billingDetails.push(billingDetail);
	});

	data99457.Items.forEach(function (Item) {
		const billingDetail = {
			code: CONSTANTS.FINANCIAL_CODE.CODE_99457,
			dateOfService: Item.attributes.transaction.dateOfService,
			totalTimeClocked: Item.attributes.transaction.totalTimeClocked *
                CONSTANTS.IN_SECONDS, // in seconds
			endDate: Item.endDate
		};
		financeObject.billingDetails.push(billingDetail);
	});

	data99458.Items.forEach(function (Item) {
		const billingDetail = {
			code: CONSTANTS.FINANCIAL_CODE.CODE_99458,
			dateOfService: Item.attributes.transaction.dateOfService,
			totalTimeClocked: Item.attributes.transaction.totalTimeClocked *
                CONSTANTS.IN_SECONDS, // in seconds
			createdDate: Item.createdDate
		};
		financeObject.billingDetails.push(billingDetail);
	});

	if (financeObject.billingDetails.length > 0) {
		financeObject = {
			userID: patient.userID,
			name: patient.attributes.name,
			firstName: patient.attributes.firstName,
			lastname: patient.attributes.lastName,
			gender: patient.attributes.gender,
			patientCode: patient.patientCode,
			icdCode: patient.attributes.icdCode,
			dob: patient.attributes.dob,
			profilePicture: profilepic,
			status: finalStatus,
			billingDetails: financeObject.billingDetails
		};
		return financeObject;
	}
	return null;
};

const getSummary = async (
	doctorId,
	hospitalID,
	startDate,
	endDate,
	nextPaginationKey
) => {
	try {
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);
		if (!hospitalTableDetails) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const patients = (await DB.getPatient(
			doctorId,
			hospitalTableDetails.DOCTOR_PATIENT
		)).Items;
		if (patients.length <= 0) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
			});
		} else {
			const response = {
				success: true,
				data: []
			};

			const financePromiseArray = [];

			for (let i = 0; i < patients.length; i++) {
				financePromiseArray.push(getPatientFinanceData(patients[i], hospitalTableDetails, startDate, endDate, nextPaginationKey));
			}

			response.data = (await Promise.all(financePromiseArray)).filter((element) => {
				return element != null;
			});
			return Promise.resolve(response);
		}
	} catch (error) {
		console.log('error', error);
		return Promise.resolve({
			success: false,
			message: error,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

const applyValidation = async ({
	doctorId,
	hospitalID,
	startDate,
	endDate,
	nextPaginationKey
}) => {
	if (!doctorId) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
			errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
		});
	}
	if (!hospitalID) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
			errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
		});
	}
	if (!startDate) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.FROM_DATE.MESSAGE,
			errorCode: CONSTANTS.ERRORS.FROM_DATE.CODE
		});
	}
	if (!endDate) {
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.TO_DATE.MESSAGE,
			errorCode: CONSTANTS.ERRORS.TO_DATE.CODE
		});
	}
	const financeSummary = await getSummary(
		doctorId,
		hospitalID,
		startDate,
		endDate,
		nextPaginationKey
	);
	return financeSummary;
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await applyValidation(event);
	callback(null, response);
};
