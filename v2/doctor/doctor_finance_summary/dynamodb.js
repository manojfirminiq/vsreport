const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
const DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
const TABLE_99453 = process.env.TABLE_99453;
const TABLE_99454 = process.env.TABLE_99454;
const TABLE_99457 = process.env.TABLE_99457;
const TABLE_99458 = process.env.TABLE_99458;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			TABLE_99453:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				TABLE_99453,
			TABLE_99454:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				TABLE_99454,
			TABLE_99457:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				TABLE_99457,
			TABLE_99458:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				TABLE_99458,
			DOCTOR_PATIENT:
				process.env.STAGE +
				CONSTANTS.RPM +
				Items[0].dbIdentifier +
				'_' +
				DOCTOR_PATIENT
		};
		return obj;
	}
};

const getPatient = async (doctorId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID'
		}
	};
	return dbClient.query(params).promise();
};

const get99453Data = async (
	userId,
	TABLE_99453,
	from,
	to,
	nextPaginationKey
) => {
	try {
		const params = {
			TableName: TABLE_99453,
			KeyConditionExpression: '#uid = :id',
			FilterExpression:
				'#attributes.#transaction.#dateOfService BETWEEN :minDate AND :maxDate',
			ExpressionAttributeValues: {
				':id': userId,
				':minDate': parseInt(from),
				':maxDate': parseInt(to)
			},
			ExpressionAttributeNames: {
				'#uid': 'userID',
				'#attributes': 'attributes',
				'#transaction': 'transaction',
				'#dateOfService': 'dateOfService'
			}
		};

		if (nextPaginationKey) {
			params.ExclusiveStartKey = {
				userID: userId,
				measurementDate: Number(nextPaginationKey)
			};
		}
		params.ScanIndexForward = false;
		return dbClient.query(params).promise();
	} catch (error) {
		console.log(error);
	}
};

const get99454Data = async (
	userId,
	TABLE_99454,
	from,
	to,
	nextPaginationKey
) => {
	const params = {
		TableName: TABLE_99454,
		KeyConditionExpression: '#uid = :id',
		FilterExpression:
			'#attributes.#transaction.#dateOfService BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': parseInt(from),
			':maxDate': parseInt(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userID',
			'#attributes': 'attributes',
			'#transaction': 'transaction',
			'#dateOfService': 'dateOfService'
		}
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = {
			userID: userId,
			measurementDate: Number(nextPaginationKey)
		};
	}
	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

const get99457Data = async (
	userId,
	TABLE_99457,
	from,
	to,
	nextPaginationKey
) => {
	const params = {
		TableName: TABLE_99457,
		KeyConditionExpression: '#uid = :id',
		FilterExpression:
			'#attributes.#transaction.#dateOfService BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': parseInt(from),
			':maxDate': parseInt(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userID',
			'#attributes': 'attributes',
			'#transaction': 'transaction',
			'#dateOfService': 'dateOfService'
		}
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = {
			userID: userId,
			measurementDate: Number(nextPaginationKey)
		};
	}
	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

const get99458Data = async (
	userId,
	TABLE_99458,
	from,
	to,
	nextPaginationKey
) => {
	const params = {
		TableName: TABLE_99458,
		KeyConditionExpression: '#uid = :id',
		FilterExpression:
			'#attributes.#transaction.#dateOfService BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': parseInt(from),
			':maxDate': parseInt(to)
		},
		ExpressionAttributeNames: {
			'#uid': 'userID',
			'#attributes': 'attributes',
			'#transaction': 'transaction',
			'#dateOfService': 'dateOfService'
		}
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = {
			userID: userId,
			measurementDate: Number(nextPaginationKey)
		};
	}
	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

module.exports.getPatient = getPatient;
module.exports.get99453Data = get99453Data;
module.exports.get99454Data = get99454Data;
module.exports.verifyHospital = verifyHospital;
module.exports.get99457Data = get99457Data;
module.exports.get99458Data = get99458Data;
