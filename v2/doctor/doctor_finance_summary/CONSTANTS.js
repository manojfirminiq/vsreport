exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	USER_ID_REQ: {
		CODE: 'USER_ID_REQ',
		MESSAGE: 'User id required'
	},
	USER_NOT_FOUND: {
		CODE: 'USER_NOT_FOUND',
		MESSAGE: 'User not found'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	FROM_DATE: {
		CODE: 'INVALID_FROM_DATE',
		MESSAGE: 'Invalid from date'
	},
	TO_DATE: {
		CODE: 'INVALID_TO_DATE',
		MESSAGE: 'Invalid to date'
	}
};

exports.FINANCIAL_STATUS = {
	READY_TO_SUBMIT: {
		statusCode: 'READY_TO_SUBMIT',
		statusLabel: 'Ready for Submission'
	},
	SUBMITTED: {
		statusCode: 'SUBMITTED',
		statusLabel: 'Submitted'
	}
};
exports.FINANCIAL_CODE = {
	CODE_99453: '99453',
	CODE_99454: '99454',
	CODE_99457: '99457',
	CODE_99458: '99458'
};

exports.IN_SECONDS = 60;
exports.RPM = '_rpm_';
exports.HODES = 'hodes';
exports.S3_KEY_EXPIRES_IN = 60 * 60;
exports.PROFILE_BUCKET = process.env.PROFILE_BUCKET;
