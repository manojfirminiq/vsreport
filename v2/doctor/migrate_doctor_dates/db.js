
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');

const verifyHospital = async (hospitalId) => {
	const params = {
		TableName: CONSTANTS.HOSPITAL_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': hospitalId
		},
		ExpressionAttributeNames: {
			'#uid': 'hospitalID'
		}
	};

	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_PROFILE_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${CONSTANTS.DOCTOR_PROFILE}`,
			DOCTOR_PATIENT_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${CONSTANTS.DOCTOR_PATIENT}`,
			NOTES_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${CONSTANTS.DOCTOR_NOTES}`,
			CONSULTATION_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${CONSTANTS.DOCTOR_CONSULTATION}`,
			TODO_TABLE: `${process.env.STAGE}${CONSTANTS.RPM}${Items[0].dbIdentifier}_${CONSTANTS.DOCTOR_TODO}`
		};
		return obj;
	}
};

const getDoctorProfile = async (doctorID, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorID
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		return Items[0];
	}
	return false;
};

const updateDoctorProfile = async (doctorID, tZone, tableName) => {
	const params = {
		TableName: tableName,
		Key: {
			userID: doctorID
		},
		UpdateExpression:
			'set #attributes.#tZone = :tZone',
		ExpressionAttributeNames: {
			'#attributes': 'attributes',
			'#tZone': 'timeZone'
		},
		ExpressionAttributeValues: {
			':tZone': tZone
		}
	};
	return dbClient.update(params).promise();
};

const getAllDoctors = async (doctorTable) => {
	const params = {
		TableName: doctorTable
	};

	const scanResults = [];
	let items;
	do {
		items = await dbClient.scan(params).promise();
		items.Items.forEach((item) => scanResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return scanResults;
};

const getAllNotes = async (table) => {
	const params = {
		TableName: table,
		FilterExpression: '#deleteFlag <> :deleteFlagValue',
		ExpressionAttributeValues: {
			':deleteFlagValue': 1
		},
		ExpressionAttributeNames: {
			'#deleteFlag': 'deleteFlag'
		}
	};

	const scanResults = [];
	let items;
	do {
		items = await dbClient.scan(params).promise();
		items.Items.forEach((item) => scanResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return scanResults;
};

const getAllConsultation = async (table) => {
	const params = {
		TableName: table,
		FilterExpression: '#isdeleted <> :isdeletedValue',
		ExpressionAttributeValues: {
			':isdeletedValue': true
		},
		ExpressionAttributeNames: {
			'#isdeleted': 'isdeleted'
		}
	};

	const scanResults = [];
	let items;
	do {
		items = await dbClient.scan(params).promise();
		items.Items.forEach((item) => scanResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return scanResults;
};

const getAllTodo = async (table) => {
	const params = {
		TableName: table
	};

	const scanResults = [];
	let items;
	do {
		items = await dbClient.scan(params).promise();
		items.Items.forEach((item) => scanResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return scanResults;
};

const getNotes = async (doctorId, table) => {
	const params = {
		TableName: table,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':deleteFlagValue': 1
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#deleteFlag': 'deleteFlag'
		},
		FilterExpression: '#deleteFlag <> :deleteFlagValue',
		ScanIndexForward: false
	};

	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach((item) => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return queryResults;
};

const getTodo = async (doctorID, table) => {
	const params = {
		TableName: table,
		KeyConditionExpression: '#doctorID = :doctorID',
		ExpressionAttributeNames: {
			'#doctorID': 'doctorID'
		},
		ExpressionAttributeValues: {
			':doctorID': doctorID
		}
	};

	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach((item) => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return queryResults;
};

const getConsultation = async (doctorId, consultationTable) => {
	const params = {
		TableName: consultationTable,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':isdeletedValue': true
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#isdeleted': 'isdeleted'
		},
		FilterExpression: '#isdeleted <> :isdeletedValue',
		ScanIndexForward: false
	};

	const queryResults = [];
	let items;
	do {
		items = await dbClient.query(params).promise();
		items.Items.forEach((item) => queryResults.push(item));
		params.ExclusiveStartKey = items.LastEvaluatedKey;
	} while (typeof items.LastEvaluatedKey !== 'undefined');

	return queryResults;
};

const saveNotes = async (convertedDate, table, item) => {
	item.startDateUTC = item.startDate;
	item.startDate = convertedDate;
	item.userID_startDate = `${item.userID}_${convertedDate}`;
	item.modifiedDate = new Date().getTime();
	const params = {
		TableName: table,
		Item: item
	};
	return await dbClient.put(params).promise();
};

const saveTodo = async (convertedDate, table, item) => {
	item.dueDateUTC = item.dueDate;
	item.dueDate = convertedDate;
	item.modifiedDate = new Date().getTime();
	const params = {
		TableName: table,
		Item: item
	};
	return await dbClient.put(params).promise();
};

const saveConsultation = async (convertedStartTime, convertedEndTime, table, item) => {
	item.startTimeUTC = item.startTime;
	item.endTimeUTC = item.endTime;
	item.startTime = convertedStartTime;
	item.endTime = convertedEndTime;
	item.userID_startTime = `${item.userID}_${convertedStartTime}`;
	item.modifiedDate = new Date().getTime();
	const params = {
		TableName: table,
		Item: item
	};
	return await dbClient.put(params).promise();
};

const deleteNote = async (table, item) => {
	const params = {
		TableName: table,
		Key: {
			doctorID: item.doctorID,
			userID_startDate: `${item.userID}_${item.startDate}`
		}
	};
	return dbClient.delete(params).promise();
};

const deleteToDo = async (table, item) => {
	const params = {
		TableName: table,
		Key: {
			doctorID: item.doctorID,
			dueDate: item.dueDate
		}
	};
	return dbClient.delete(params).promise();
};

const deleteConsultation = async (table, item) => {
	const params = {
		TableName: table,
		Key: {
			doctorID: item.doctorID,
			userID_startTime: `${item.userID}_${item.startTime}`
		}
	};
	return dbClient.delete(params).promise();
};

module.exports.verifyHospital = verifyHospital;
module.exports.getDoctorProfile = getDoctorProfile;
module.exports.getAllDoctors = getAllDoctors;
module.exports.getNotes = getNotes;
module.exports.getTodo = getTodo;
module.exports.getConsultation = getConsultation;
module.exports.saveNotes = saveNotes;
module.exports.saveTodo = saveTodo;

module.exports.saveConsultation = saveConsultation;
module.exports.deleteNote = deleteNote;
module.exports.deleteToDo = deleteToDo;
module.exports.deleteConsultation = deleteConsultation;
module.exports.getAllConsultation = getAllConsultation;
module.exports.getAllTodo = getAllTodo;
module.exports.getAllNotes = getAllNotes;
module.exports.updateDoctorProfile = updateDoctorProfile;
