
require('dotenv').config();
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const credentials = new AWS.SharedIniFileCredentials({ profile: 'deepesh.kumar' });
AWS.config.credentials = credentials;
const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');

const momentTimezone = require('moment-timezone');

const fetchAllDoctors = (doctorProfileDetailsArr) => {
	const filteredDoctorDetailsObj = {};
	for (const item of doctorProfileDetailsArr) {
		const {
			attributes: { timeZone } = { timeZone: null },
			userID
		} = item;
		filteredDoctorDetailsObj[userID] = {
			userID,
			timeZone
		};
	}
	return filteredDoctorDetailsObj;
};

const calculateDate = (date, timeZone) => {
	const offSetInMilliSecs = momentTimezone.tz(date, timeZone).utcOffset() * 60000;
	return date + offSetInMilliSecs;
};

const migrateDoctorDate = async (doctor, doctorsArr, hospitalTable, timeZone) => {
	let allDoctors;
	let tZone;
	let notes;
	let todo;
	let consultation;

	try {
		if (doctor && doctor.userID) {
			console.log('for one doctor', doctor.userID);
			notes = await DB.getNotes(doctor.userID, hospitalTable.NOTES_TABLE);
			todo = await DB.getTodo(doctor.userID, hospitalTable.TODO_TABLE);
			consultation = await DB.getConsultation(doctor.userID, hospitalTable.CONSULTATION_TABLE);
		} else {
			console.log('all doctors in a hospital');
			if (doctorsArr && doctorsArr.length > 0) {
				allDoctors = fetchAllDoctors(doctorsArr);
			}
			notes = await DB.getAllNotes(hospitalTable.NOTES_TABLE);
			todo = await DB.getAllTodo(hospitalTable.TODO_TABLE);
			consultation = await DB.getAllConsultation(hospitalTable.CONSULTATION_TABLE);
		}

		// NOTES
		for (let i = 0; i < notes.length; i++) {
			if (doctor && doctor.attributes && doctor.attributes.timeZone) {
				tZone = doctor.attributes.timeZone;
			} else if (allDoctors && allDoctors[notes[i].doctorID] && allDoctors[notes[i].doctorID].timeZone) {
				tZone = allDoctors[notes[i].doctorID].timeZone;
			} else {
				tZone = timeZone;
			}
			const date = calculateDate(notes[i].startDate, tZone);
			const oldNote = { ...notes[i] };
			await DB.saveNotes(date, hospitalTable.NOTES_TABLE, oldNote);
			await DB.deleteNote(hospitalTable.NOTES_TABLE, notes[i]);
		}
		console.log('successfully migrated notes');

		// TODO
		for (let i = 0; i < todo.length; i++) {
			if (doctor && doctor.attributes && doctor.attributes.timeZone) {
				tZone = doctor.attributes.timeZone;
			} else if (allDoctors && allDoctors[todo[i].doctorID] && allDoctors[todo[i].doctorID].timeZone) {
				tZone = allDoctors[todo[i].doctorID].timeZone;
			} else {
				tZone = timeZone;
			}
			const date = calculateDate(todo[i].dueDate, tZone);
			const oldTodo = { ...todo[i] };
			await DB.saveTodo(date, hospitalTable.TODO_TABLE, oldTodo);
			await DB.deleteToDo(hospitalTable.TODO_TABLE, todo[i]);
		}

		console.log('successfully migrated todos');

		// CONSULTATION
		for (let i = 0; i < consultation.length; i++) {
			let startTime;
			let endTime;

			if (doctor && doctor.attributes && doctor.attributes.timeZone) {
				tZone = doctor.attributes.timeZone;
			} else if (allDoctors && allDoctors[consultation[i].doctorID] && allDoctors[consultation[i].doctorID].timeZone) {
				tZone = allDoctors[consultation[i].doctorID].timeZone;
			} else {
				tZone = timeZone;
			}
			if (consultation[i].startTime) {
				startTime = calculateDate(consultation[i].startTime, tZone);
			}
			if (consultation[i].endTime) {
				endTime = calculateDate(consultation[i].endTime, tZone);
			}
			const oldConsultation = { ...consultation[i] };
			await DB.saveConsultation(startTime, endTime, hospitalTable.CONSULTATION_TABLE, oldConsultation);
			await DB.deleteConsultation(hospitalTable.CONSULTATION_TABLE, consultation[i]);
		}

		console.log('successfully migrated consultations');

		// update timezone
		if (doctor && doctor.userID && doctor.attributes && !doctor.attributes.timeZone) {
			console.log('doctor doesnot have timeZone');
			await DB.updateDoctorProfile(doctor.userID, timeZone, hospitalTable.DOCTOR_PROFILE_TABLE);
		} else if (doctorsArr && doctorsArr.length > 0) {
			console.log('update timezone for all doctors');
			for (let i = 0; i < doctorsArr.length; i++) {
				if (doctorsArr[i].attributes && !doctorsArr[i].attributes.timeZone) {
					await DB.updateDoctorProfile(doctorsArr[i].userID, timeZone, hospitalTable.DOCTOR_PROFILE_TABLE);
				}
			}
		}
	} catch (error) {
		console.log('Error in migrating patients', error);
		return Promise.resolve({ success: false, errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE, message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE });
	}
};

const applyValidation = async (hospitalID, doctorID, timeZone) => {
	try {
		if (!hospitalID) {
			return Promise.resolve({ success: false, errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE, message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE });
		}

		const hospitalTable = await DB.verifyHospital(hospitalID);

		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		let doctorData;
		let doctorsArr;

		if (doctorID) {
			doctorData = await DB.getDoctorProfile(doctorID, hospitalTable.DOCTOR_PROFILE_TABLE);
			if (!doctorData) {
				return Promise.resolve({ success: false, errorCode: CONSTANTS.ERRORS.DOCTOR_NOT_FOUND.CODE, message: CONSTANTS.ERRORS.DOCTOR_NOT_FOUND.MESSAGE });
			}
			await migrateDoctorDate(doctorData, doctorsArr, hospitalTable, timeZone);
		} else {
			doctorsArr = await DB.getAllDoctors(hospitalTable.DOCTOR_PROFILE_TABLE);
			await migrateDoctorDate(doctorData, doctorsArr, hospitalTable, timeZone);
		}

		return Promise.resolve({ success: true, message: CONSTANTS.SUCCESS });
	} catch (e) {
		console.log('Error in migrating patients', e);
		return Promise.resolve({ success: false, errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE, message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE });
	}
};

(async () => {
	const hospitalID = CONSTANTS.hospitalID;
	const doctorID = CONSTANTS.doctorID;
	const timeZone = CONSTANTS.timeZone;
	const response = await applyValidation(hospitalID, doctorID, timeZone);
	if (response) {
		return Promise.resolve({ success: true, message: CONSTANTS.SUCCESS });
	}
})();
