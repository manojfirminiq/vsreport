exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	DOCTOR_ID_REQ: {
		CODE: 'DOCTOR_ID_REQ',
		MESSAGE: 'Doctor Id is required'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	DOCTOR_NOT_FOUND: {
		CODE: 'DOCTOR_NOT_FOUND',
		MESSAGE: 'Doctor Not Found'
	},
	INVALID_TIMEZONE: {
	    CODE: 'INVALID_TIMEZONE',
	    MESSAGE: 'Invalid Timezone'
	}
};

exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.DOCTOR_PROFILE = process.env.DOCTOR_PROFILE;
exports.DOCTOR_NOTES = process.env.DOCTOR_NOTES;
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;
exports.DOCTOR_TODO = process.env.DOCTOR_TODO;
exports.RPM = '_rpm_';
exports.DELETE_FLAG = 1;
exports.SUCCESS = 'Migration Completed Successfully';
exports.hospitalID = process.env.hospitalID;
exports.doctorID = process.env.doctorID;
exports.timeZone = process.env.timeZone;
