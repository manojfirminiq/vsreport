/* eslint-disable security/detect-non-literal-fs-filename */
const AWS = require('aws-sdk');
const xml2js = require('xml2js');
const fs = require('fs');
const path = require('path');
const util = require('util');
const {
	ELASTICSEARCH_DOMAIN, REGION, ES_INDEX, ICD_JSON_FILE, ICD_XML_FILE,
	AUTH_TYPE, USER_NAME, PASSWORD, ENCODE_TYPE,
	SUCCESS_MESSAGE, BULK_IMPORT_ERROR_MESSAGE
} = require('./CONSTANTS');

const write = util.promisify(fs.writeFile);
const read = util.promisify(fs.readFile);

const xmlFilePath = path.join(__dirname, ICD_XML_FILE);
const jsonFilepath = path.join(__dirname, ICD_JSON_FILE);

const auth = AUTH_TYPE + Buffer.from(USER_NAME + ':' + PASSWORD).toString(ENCODE_TYPE);
const endPoint = new AWS.Endpoint(ELASTICSEARCH_DOMAIN);
const httpClient = new AWS.HttpClient();

const getICDCodes = (chapters) => {
	const output = {};

	for (const chapter of chapters) {
		const description = chapter.desc[0];
		const code = description.substring(
		    description.lastIndexOf('(') + 1,
		    description.lastIndexOf(')')
		);

		if (code.startsWith('I') || code.startsWith('J')) {
			for (const section of chapter.section) {
				if (section.diag && Array.isArray(section.diag)) {
					for (const diag of section.diag) {
						const icdCode = {
							id: diag.name[0],
							description: diag.desc[0]
						};
						if (!output[code]) {
							output[code] = {
								group: code,
								codes: []
							};
						}
						output[code].codes.push(icdCode);
						if (diag.diag && Array.isArray(diag.diag)) {
							for (const innerDiag of diag.diag) {
								const subCcode = {
									id: innerDiag.name[0],
									description: innerDiag.desc[0]
								};
								if (!output[code]) {
									output[code] = {
										group: code,
										cods: []
									};
								}
								output[code].codes.push(subCcode);
							}
						}
					}
				}
			}
		}
	}

	const finalJson = [];

	for (const key in output) {
		finalJson.push(output[key]);
	}

	return finalJson;
};

/**
 * Sends a request to elastic search
 */
const sendElasticSearchRequest = async ({ httpMethod, requestPath, payload }) => {
	const request = new AWS.HttpRequest(endPoint, REGION);

	request.method = httpMethod;
	request.path += requestPath;
	request.body = payload;
	request.headers['Content-Type'] = 'application/json';
	request.headers.host = ELASTICSEARCH_DOMAIN;
	request.headers['Content-Length'] = Buffer.byteLength(request.body);
	request.headers.Authorization = auth;

	return new Promise((resolve, reject) => {
		httpClient.handleRequest(request, null, (response) => {
			let responseBody = '';

			response.on('data', chunk => {
				responseBody += chunk;
			});

			response.on('end', (chunk) => {
				resolve(responseBody);
			});
		}, err => {
			console.log('Error while sending request to es', err);
			reject(err);
		});
	});
};

/**
 * Create an index in es
 */

const createIndex = async (index) => {
	try {
		const params = {
			httpMethod: 'PUT',
			requestPath: index,
			payload: JSON.stringify({
				settings: {
					number_of_shards: 1,
					analysis: {
						filter: {
							ngram_filter: {
								type: 'edge_ngram',
								min_gram: 1,
								max_gram: 20
							},
							group_filter: {
								type: 'edge_ngram',
								min_gram: 1,
								max_gram: 7
							}
						},
						analyzer: {
							ngram_analyzer: {
								type: 'custom',
								tokenizer: 'standard',
								filter: [
									'lowercase', 'ngram_filter'
								]
							},
							group_analyzer: {
								type: 'custom',
								tokenizer: 'standard',
								filter: [
									'lowercase', 'group_filter'
								]
							}
						}
					}
				},
				mappings: {
					properties: {
						group: {
							type: 'text',
							analyzer: 'group_analyzer',
							search_analyzer: 'standard'
						},
						codes: {
							type: 'nested',
							properties: {
								id: {
									type: 'text',
									analyzer: 'ngram_analyzer',
									search_analyzer: 'standard'
								},
								description: {
									type: 'text',
									analyzer: 'ngram_analyzer',
									search_analyzer: 'standard'
								}
							}
						}
					}
				}
			})
		};
		await sendElasticSearchRequest(params);
	} catch (error) {
		console.log('Error in creating index', error);
		throw error;
	}
};

/**
 * import bulk icd code
 */
const importBulkData = async (index, icdCode) => {
	try {
		const bulkBody = [];
		icdCode.map(doc => {
			bulkBody.push({
				index: { _index: index, _id: doc.group }
			});
			bulkBody.push(doc);
		});

		// convert to nd-json format
		const bulkPayload = bulkBody.map(JSON.stringify).join('\n') + '\n';

		const params = {
			httpMethod: 'POST',
			requestPath: index + '/_bulk',
			payload: bulkPayload
		};

		await sendElasticSearchRequest(params);
	} catch (error) {
		console.log('Error in importBulkData', error);
		throw error;
	}
};

(async () => {
	try {
		const xmlData = await read(xmlFilePath);
		const parsedJSONData = await xml2js.parseStringPromise(xmlData);
		const icdCodes = getICDCodes(parsedJSONData['ICD10CM.tabular'].chapter);
		await write(jsonFilepath, JSON.stringify(icdCodes));
		const icdData = await read(jsonFilepath, 'utf8');
		await createIndex(ES_INDEX);
		console.log('index created successfully');
		await importBulkData(ES_INDEX, JSON.parse(icdData));
		console.log('Bulk data uploaded successfully');
		return Promise.resolve({ success: true, message: SUCCESS_MESSAGE });
	} catch (error) {
		console.log('Error in importing icd code to elastic search', error);
		return Promise.resolve({ success: false, error: BULK_IMPORT_ERROR_MESSAGE });
	}
})();
