const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');
const trackPatient = require('track_patients_record');

const applyValidation = async ({
	doctorId,
	hospitalID,
	updateUserData,
	body: { userId, notes, dueDate, resolveStatus }
}) => {
	try {
		if (!dueDate) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.DUE_DATE_REQUIRED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.DUE_DATE_REQUIRED.CODE
			});
		}
		if (resolveStatus !== CONSTANTS.RESOLVE_STATUS.ONE &&
				resolveStatus !== CONSTANTS.RESOLVE_STATUS.ZERO
		) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.RESOLVED_STATUS_NOT_MATCHED.MESSAGE,
				errorCode: CONSTANTS.ERRORS.RESOLVED_STATUS_NOT_MATCHED.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		if (!userId || userId === null) {
			const { Items } = await DB.getDoctorTodo(
				doctorId,
				dueDate,
				hospitalTable
			);
			const attributes = {
				itemType: Items[0] && Items[0].attributes && Items[0].attributes.itemType,
				description: Items[0] && Items[0].attributes && Items[0].attributes.description,
				notes: notes || null,
				resolveStatus: Number(resolveStatus)
			};
			await DB.updateTodo(doctorId, dueDate, attributes, updateUserData, hospitalTable);
		} else {
			const { Count, Items } = await DB.getTodo(userId, dueDate, hospitalTable);
			if (Count === 0) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.USER_NOT_FOUND.MESSAGE,
					errorCode: CONSTANTS.ERRORS.USER_NOT_FOUND.CODE
				});
			}
			const attributes = {
				itemType: Items[0] && Items[0].attributes && Items[0].attributes.itemType,
				description: Items[0] && Items[0].attributes && Items[0].attributes.description,
				notes: notes || null,
				resolveStatus: Number(resolveStatus)
			};
			await DB.updateTodo(doctorId, dueDate, attributes, updateUserData, hospitalTable);
		}

		return Promise.resolve({
			success: true
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
