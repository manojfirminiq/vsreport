exports.TODO_TABLE = process.env.TODO_TABLE;
exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	DUE_DATE_REQUIRED: {
		CODE: 'DUE_DATE_REQUIRED',
		MESSAGE: 'Due date required'
	},
	USER_NOT_FOUND: {
		CODE: 'USER_NOT_FOUND',
		MESSAGE: 'User not found'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	RESOLVED_STATUS_NOT_MATCHED: {
		CODE: 'RESOLVED_STATUS_NOT_MATCHED',
		MESSAGE: 'Resolved status not matched'
	}
};

exports.RPM = 'rpm';
exports.RESOLVE_STATUS = {
	ONE: 1,
	ZERO: 0
};
