const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Count, Items } = await dbClient.query(paramsTable).promise();
	if (Count <= 0) {
		return false;
	} else {
		return (
			process.env.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			Items[0].dbIdentifier +
			'_' +
			CONSTANTS.TODO_TABLE
		);
	}
};

const getTodo = async (userId, dueDate, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		IndexName: 'userID-dueDate-index',
		KeyConditionExpression: '#userId = :userId AND #dueDate = :dueDate',
		ExpressionAttributeValues: {
			':userId': userId,
			':dueDate': dueDate
		},
		ExpressionAttributeNames: {
			'#userId': 'userID',
			'#dueDate': 'dueDate'
		}
	};
	console.log('params', params);
	return dbClient.query(params).promise();
};

const getDoctorTodo = async (doctorId, dueDate, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #dueDate = :dueDate',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':dueDate': dueDate
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#dueDate': 'dueDate'
		}
	};
	console.log('params', params);
	return dbClient.query(params).promise();
};

const updateTodo = async (doctorId, dueDate, attributes, updateUserData, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		Key: {
			doctorID: doctorId,
			dueDate: dueDate
		},
		UpdateExpression:
			'SET attributes = :attributes, modifiedDate = :modifiedDate, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ExpressionAttributeValues: {
			':attributes': attributes,
			':modifiedDate': new Date().getTime(),
			':updateUserData': [updateUserData],
      		':empty_list': []
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

module.exports.verifyHospital = verifyHospital;
module.exports.getTodo = getTodo;
module.exports.updateTodo = updateTodo;
module.exports.getDoctorTodo = getDoctorTodo;
