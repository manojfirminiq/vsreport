const AWS = require('aws-sdk');
const CONSTANTS = require('./CONSTANTS');

const { DEFAULT_TABLES, STAGE } = CONSTANTS;
AWS.config.update(CONSTANTS.AWS_CONFIG);
// const credentials = new AWS.SharedIniFileCredentials({ profile: 'mfa' });
// AWS.config.credentials = credentials;
const dbClient = new AWS.DynamoDB.DocumentClient();

const DOCTOR_PROFILE_TABLE =
	process.env.DOCTOR_PROFILE_TABLE || DEFAULT_TABLES.DOCTOR_PROFILE_TABLE;
const ORDER_DETAILS_TABLE =
	process.env.ORDER_DETAILS_TABLE || DEFAULT_TABLES.ORDER_DETAILS_TABLE;
const DOCTOR_PATIENT_TABLE =
	process.env.DOCTOR_PATIENT_TABLE || DEFAULT_TABLES.DOCTOR_PATIENT_TABLE;
const BP_TABLE = process.env.BP_TABLE || DEFAULT_TABLES.BP_TABLE;
const NON_BP_TABLE = process.env.NON_BP_TABLE || DEFAULT_TABLES.NON_BP_TABLE;
const DOCTOR_TABLE = process.env.DOCTOR_TABLE || DEFAULT_TABLES.DOCTOR_TABLE;
const ALERT_NON_BP_TABLE =
	process.env.ALERT_NON_BP_TABLE || DEFAULT_TABLES.ALERT_NON_BP_TABLE;
const ALERT_BP_TABLE =
	process.env.ALERT_BP_TABLE || DEFAULT_TABLES.ALERT_BP_TABLE;
const EHR_USER_TABLE =
	process.env.EHR_USER_TABLE || DEFAULT_TABLES.EHR_USER_TABLE;
const NOTES_TABLE = process.env.NOTES_TABLE || DEFAULT_TABLES.NOTES_TABLE;
const FINANCE_99453_TABLE =
	process.env.FINANCE_99453_TABLE || DEFAULT_TABLES.FINANCE_99453_TABLE;
const FINANCE_99454_TABLE =
	process.env.FINANCE_99454_TABLE || DEFAULT_TABLES.FINANCE_99454_TABLE;
const FINANCE_99457_TABLE =
	process.env.FINANCE_99457_TABLE || DEFAULT_TABLES.FINANCE_99457_TABLE;
const FINANCE_99458_TABLE =
	process.env.FINANCE_99458_TABLE || DEFAULT_TABLES.FINANCE_99458_TABLE;
const STAFF_PROFILE_TABLE =
	process.env.STAFF_PROFILE_TABLE || DEFAULT_TABLES.STAFF_PROFILE_TABLE;
const DOCTOR_CONSULTATION_TABLE =
	process.env.DOCTOR_CONSULTATION_TABLE ||
	DEFAULT_TABLES.DOCTOR_CONSULTATION_TABLE;
const DOCTOR_REPORTS =
	process.env.DOCTOR_REPORTS || DEFAULT_TABLES.DOCTOR_REPORTS;

const saveItem = async (Item, table) => {
	console.log('item>>>', Item);
	console.log('table>>>', table);
	const params = {
		TableName: table,
		Item: Item
	};
	return dbClient.put(params).promise();
};

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: DOCTOR_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const tableInitials = `${process.env.STAGE || STAGE}${CONSTANTS.RPM}${
			Items[0].dbIdentifier
		}`;
		const obj = {
			Name: Items[0].name || null,
			DOCTOR_PROFILE: `${tableInitials}_${DOCTOR_PROFILE_TABLE}`,
			ORDER_DETAILS: `${tableInitials}_${ORDER_DETAILS_TABLE}`,
			DOCTOR_PATIENT: `${tableInitials}_${DOCTOR_PATIENT_TABLE}`,
			EHR_USER_TABLE: `${tableInitials}_${EHR_USER_TABLE}`,
			BP_TABLE: `${tableInitials}_${BP_TABLE}`,
			NON_BP_TABLE: `${tableInitials}_${NON_BP_TABLE}`,
			ALERT_BP_TABLE: `${tableInitials}_${ALERT_BP_TABLE}`,
			ALERT_NON_BP_TABLE: `${tableInitials}_${ALERT_NON_BP_TABLE}`,
			NOTES_TABLE: `${tableInitials}_${NOTES_TABLE}`,
			FINANCE_99453_TABLE: `${tableInitials}_${FINANCE_99453_TABLE}`,
			FINANCE_99454_TABLE: `${tableInitials}_${FINANCE_99454_TABLE}`,
			FINANCE_99457_TABLE: `${tableInitials}_${FINANCE_99457_TABLE}`,
			FINANCE_99458_TABLE: `${tableInitials}_${FINANCE_99458_TABLE}`,
			STAFF_PROFILE: `${tableInitials}_${STAFF_PROFILE_TABLE}`,
			CONSULTATION_TABLE: `${tableInitials}_${DOCTOR_CONSULTATION_TABLE}`,
			DOCTOR_REPORTS: `${tableInitials}_${DOCTOR_REPORTS}`,
			Items
		};
		return obj;
	}
};

const getDoctorProfile = async (doctorID, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': doctorID
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	const { Count, Items } = await dbClient.query(params).promise();
	if (Count > 0) {
		const Item = Items.map(
			({
				createdDate,
				modifiedDate,
				attributes: { ...attributesRest },
				...rest
			}) => ({ ...rest, ...attributesRest })
		)[0];

		if (Item.hospitalID) {
			Item.hospital = await verifyHospital(Item.hospitalID);
			Item.hospital = Item.hospital.Name;
			delete Item.hospitalID;
		}
		return Item;
	} else {
		return null;
	}
};

const getStaffProfile = async (doctorId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID'
		}
	};
	const { Items } = await dbClient.query(params).promise();
	return Items;
};

const getPatientDetails = async (doctorId, userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId AND #userId = :userId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':userId': userId
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#userId': 'userID'
		}
	};

	return dbClient.query(params).promise();
};

const getEhrDetails = async (userId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#userId = :userId',
		ExpressionAttributeValues: {
			':userId': userId
		},
		ExpressionAttributeNames: {
			'#userId': 'userID'
		}
	};
	return dbClient.query(params).promise();
};

const getOrderDetails = async (orderID, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#orderID = :orderID',
		ExpressionAttributeValues: {
			':orderID': orderID
		},
		ExpressionAttributeNames: {
			'#orderID': 'orderID'
		}
	};
	return dbClient.query(params).promise();
};

const getNotes = async (doctorId, userID, start, end, hospitalTable) => {
	const paramsTable = {
		TableName: hospitalTable,
		IndexName: 'userID-startDate-index',
		KeyConditionExpression: '#userId = :userId',
		ExpressionAttributeNames: {
			'#userId': 'userID',
			'#deleteFlag': 'deleteFlag'
		},
		ExpressionAttributeValues: {
			':userId': userID,
			':deleteFlagValue': 1
		},
		FilterExpression: '#deleteFlag <> :deleteFlagValue',
		ScanIndexForward: false
	};
	if (start || end) {
		if (start && end) {
			paramsTable.KeyConditionExpression +=
				' AND #sortKey BETWEEN :minDate AND :maxDate';
			paramsTable.ExpressionAttributeValues[':minDate'] = parseInt(start);
			paramsTable.ExpressionAttributeValues[':maxDate'] = parseInt(end);
		} else if (!start) {
			paramsTable.KeyConditionExpression += ' AND #sortKey <= :maxDate';
			paramsTable.ExpressionAttributeValues[':maxDate'] = parseInt(end);
		} else {
			paramsTable.KeyConditionExpression += ' AND #sortKey >= :minDate';
			paramsTable.ExpressionAttributeValues[':minDate'] = parseInt(start);
		}

		paramsTable.ExpressionAttributeNames['#sortKey'] = 'startDate';
	}

	const { Count, Items } = await dbClient.query(paramsTable).promise();

	if (Count <= 0) {
		console.log('Count: ', Count);
		return [];
	} else {
		const records = Items.filter(function (item) {
			return item.doctorID === doctorId;
		});
		return records;
	}
};

const getBpReadings = async (userId, from, to, BP_TABLE, nextPaginationKey) => {
	const fromDate = new Date(Number(from));

	const toDate = new Date(Number(to));
	console.log(BP_TABLE);
	const params = {
		TableName: BP_TABLE,
		KeyConditionExpression:
			'#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': fromDate.getTime(),
			':maxDate': toDate.getTime()
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = {
			userID: userId,
			measurementDate: Number(nextPaginationKey)
		};
	}
	params.ScanIndexForward = false;

	return dbClient.query(params).promise();
};

const getNonBpReadings = async (
	userId,
	from,
	to,
	NON_BP_TABLE,
	nextPaginationKey
) => {
	const fromDate = new Date(Number(from));
	const toDate = new Date(Number(to));

	const params = {
		TableName: NON_BP_TABLE,
		KeyConditionExpression:
			'#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId + CONSTANTS.WEIGHT_MANUAL,
			':minDate': fromDate.getTime(),
			':maxDate': toDate.getTime()
		},
		ExpressionAttributeNames: {
			'#uid': 'userId_type_deviceLocalName'
		}
	};

	if (nextPaginationKey) {
		params.ExclusiveStartKey = {
			userID: userId,
			measurementDate: Number(nextPaginationKey)
		};
	}
	params.ScanIndexForward = false;
	console.log(params);
	return dbClient.query(params).promise();
};

const getBpAlerts = async (userId, from, to, ALERT_BP_TABLE) => {
	const fromDate = new Date(Number(from));
	const toDate = new Date(Number(to));
	const params = {
		TableName: ALERT_BP_TABLE,
		KeyConditionExpression:
			'#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId,
			':minDate': fromDate.getTime(),
			':maxDate': toDate.getTime()
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};

	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

const getNonBpAlerts = async (userId, from, to, ALERT_NON_BP_TABLE) => {
	const fromDate = new Date(Number(from));
	const toDate = new Date(Number(to));
	const params = {
		TableName: ALERT_NON_BP_TABLE,
		KeyConditionExpression:
			'#uid = :id AND measurementDate BETWEEN :minDate AND :maxDate',
		ExpressionAttributeValues: {
			':id': userId + CONSTANTS.WEIGHT_MANUAL,
			':minDate': fromDate.getTime(),
			':maxDate': toDate.getTime()
		},
		ExpressionAttributeNames: {
			'#uid': 'userId_type_deviceLocalName'
		}
	};
	console.log(params);
	params.ScanIndexForward = false;
	return dbClient.query(params).promise();
};

const get99453Data = async (userID, FINANCE_99453_TABLE) => {
	try {
		const queryParams = {
			TableName: FINANCE_99453_TABLE,
			KeyConditionExpression: '#userID = :userID',
			ExpressionAttributeValues: {
				':userID': userID
			},
			ExpressionAttributeNames: {
				'#userID': 'userID'
			}
		};
		const { Count, Items } = await dbClient.query(queryParams).promise();
		return Count > 0 ? Items : [];
	} catch (e) {
		console.log('error', e);
		throw e;
	}
};

const get99457Data = async (userID, FINANCE_99457_TABLE) => {
	try {
		const queryParams = {
			TableName: FINANCE_99457_TABLE,
			KeyConditionExpression: '#userID = :userID',
			ExpressionAttributeValues: {
				':userID': userID
			},
			ExpressionAttributeNames: {
				'#userID': 'userID'
			}
		};
		const { Count, Items } = await dbClient.query(queryParams).promise();
		return Count > 0 ? Items : [];
	} catch (e) {
		console.log('error', e);
		throw e;
	}
};

const get99458Data = async (userID, FINANCE_99458_TABLE) => {
	const queryParams = {
		TableName: FINANCE_99458_TABLE,
		KeyConditionExpression: '#userID = :userID',
		ExpressionAttributeValues: {
			':userID': userID
		},
		ExpressionAttributeNames: {
			'#userID': 'userID'
		}
	};
	return await dbClient.query(queryParams).promise();
};

const getFinanceData = async (userId, FINANCE_99454_TABLE) => {
	const params = {
		TableName: FINANCE_99454_TABLE,
		KeyConditionExpression: '#uid = :id',
		ExpressionAttributeValues: {
			':id': userId
		},
		ExpressionAttributeNames: {
			'#uid': 'userID'
		}
	};
	return dbClient.query(params).promise();
};

const getConsultation = async (doctorId, hospitalTable) => {
	const params = {
		TableName: hospitalTable,
		KeyConditionExpression: '#doctorId = :doctorId',
		ExpressionAttributeValues: {
			':doctorId': doctorId,
			':isdeletedValue': true
		},
		ExpressionAttributeNames: {
			'#doctorId': 'doctorID',
			'#isdeleted': 'isdeleted'
		},
		FilterExpression: '#isdeleted <> :isdeletedValue',
		ScanForwardIndex: false
	};
	return dbClient.query(params).promise();
};

module.exports.getDoctorProfile = getDoctorProfile;
module.exports.getPatientDetails = getPatientDetails;
module.exports.getEhrDetails = getEhrDetails;
module.exports.getOrderDetails = getOrderDetails;
module.exports.getNonBpReadings = getNonBpReadings;
module.exports.getBpReadings = getBpReadings;
module.exports.verifyHospital = verifyHospital;
module.exports.getBpAlerts = getBpAlerts;
module.exports.getNonBpAlerts = getNonBpAlerts;
module.exports.getNotes = getNotes;
module.exports.get99453Data = get99453Data;
module.exports.get99457Data = get99457Data;
module.exports.get99458Data = get99458Data;
module.exports.getFinanceData = getFinanceData;
module.exports.getStaffProfile = getStaffProfile;
module.exports.getConsultation = getConsultation;
module.exports.saveItem = saveItem;
