/* eslint-disable handle-callback-err */
/* eslint-disable security/detect-non-literal-fs-filename */
/* eslint-disable no-return-assign */
/* eslint-disable camelcase */
const moment = require('moment');
const exporter = require('highcharts-export-server');

exporter.initPool();

const generateGraph = async exportSettings => {
	return new Promise(function (resolve, reject) {
		exporter.export(exportSettings, function (err, res) {
			if (err) {
				console.log('err', err);
			}
			if (res && res.data) {
				const imageb64 = res.data;
				resolve(imageb64);
			}
		});
	});
};

exports.chartObject = async dbData => {
	const promiseArray = [];
	for (const readingKey in dbData.patientsReadings) {
		const dataBp = JSON.parse(
			JSON.stringify(dbData.patientsReadings[readingKey].bp)
		);
		const dataWeight = JSON.parse(
			JSON.stringify(dbData.patientsReadings[readingKey].weight)
		);
		const labels = [];
		const diaData = [];
		const sysData = [];
		let sysAvg = 0;
		let diaAvg = 0;
		const diaAlerts = [];
		const sysAlerts = [];
		const labelsW = [];
		const weightData = [];

		if (dataBp.length) {
			dataBp.reverse();
			dataBp.forEach(d => {
				if (d.measurementDate) {
					labels.push(moment(d.measurementDate).format('MMM Do hh:mm a'));
					diaData.push(Number(d.attributes.diastolic));
					sysData.push(Number(d.attributes.systolic));
					diaAlerts.push(
						(d.attributes.haveAlert &&
							d.attributes.alerts &&
							d.attributes.alerts.diaDiff &&
							true) ||
							false
					);
					sysAlerts.push(
						(d.attributes.haveAlert &&
							d.attributes.alerts &&
							d.attributes.alerts.sysDiff &&
							true) ||
							false
					);
				}
			});
			sysAvg = parseInt(sysData.reduce((a, b) => a + b, 0) / sysData.length);
			diaAvg = parseInt(diaData.reduce((a, b) => a + b, 0) / diaData.length);
			const exportSettingsBp = {
				type: 'png',
				scale: 2,
				options: {
					chart: {
						width: 800
					},
					title: {
						text: ''
					},
					height: 100,
					credits: {
						enabled: false
					},
					legend: {
						itemStyle: {
							fontSize: 10,
							fontWeight: 'normal',
							color: '#88868D'
						},
						align: 'left',
						verticalAlign: 'top',
						floating: false,
						x: 0,
						y: 30
					},
					yAxis: {
						min: 55,
						title: {
							text: ''
						},
						gridLineColor: '#768da4',
						gridZIndex: 4,
						gridLineWidth: 0.2,
						labels: {
							overflow: 'justify',
							style: {
								fontSize: 8,
								color: '#768da4'
							}
						},
						plotLines: [
							{
								color: '#88868D',
								value: sysAvg,
								width: '1',
								zIndex: 4,
								dashStyle: 'shortdash',
								label: {
									useHTML: true,
									text: `<span style = "border-radius: 3px; font-size:6px; padding:1px; background-color: gray; color: #FFFFFF"> Sys avg ${sysAvg}</span>`,
									style: {
										fontWeight: 'bold'
									},
									align: 'left',
									x: 0
								}
							},
							{
								color: '#88868D',
								value: diaAvg,
								width: '1',
								zIndex: 4,
								dashStyle: 'shortdash',
								label: {
									useHTML: true,
									text: `<span style = "border-radius: 3px; font-size:6px; padding:1px; background-color: gray; color: #FFFFFF"> Dia avg ${diaAvg}</span>`,
									style: {
										fontWeight: 'bold'
									},
									align: 'left',
									x: 0
								}
							}
						]
					},
					xAxis: {
						categories: labels,
						gridLineColor: '#768da4',
						gridLineWidth: 0.2,
						gridZIndex: 4,
						labels: {
							style: {
								fontSize: 9,
								color: '#768da4'
							}
						}
					},
					series: [
						{
							type: 'area',
							name: 'Systolic',
							data: sysData,
							marker: {
								symbol: 'circle',
								fillColor: 'white',
								color: '#3794FC',
								lineWidth: 1,
								lineColor: '#3794FC',
								radius: 3
							},
							color: '#3794FC',
							fillColor: 'rgba(55, 148, 252, 0.1)',
							plotOptions: {
								series: {
									fillOpacity: 0.1
								}
							},
							lineWidth: 1
						},
						{
							type: 'area',
							name: 'Diastolic',
							data: diaData,
							marker: {
								symbol: 'circle',
								fillColor: 'white',
								color: '#9D60FB',
								lineWidth: 1,
								lineColor: '#9D60FB',
								radius: 3
							},
							color: '#9D60FB',
							fillColor: 'rgba(157, 96, 251 ,0.1)',
							plotOptions: {
								series: {
									fillOpacity: 0.1
								}
							},
							lineWidth: 1
						}
					]
				}
			};

			promiseArray.push(
				Promise.resolve(generateGraph(exportSettingsBp)).then(data => {
					return (dbData.patientsReadings[readingKey].bpChart = data.toString(
						'base64'
					));
				})
			);
		}

		if (dataWeight.length) {
			dataWeight.reverse();
			dataWeight.forEach(d => {
				if (d.measurementDate) {
					labelsW.push(moment(d.measurementDate).format('MMM Do hh:mm a'));
					weightData.push(
						Number(parseFloat(d.attributes.weightLBS).toFixed(2))
					);
				}
			});

			const exportSettingsWeight = {
				type: 'png',
				scale: 2,
				options: {
					chart: {
						width: 800
					},
					title: {
						text: ''
					},
					credits: {
						enabled: false
					},
					legend: {
						symbolPadding: 0,
						symbolWidth: 0,
						symbolHeight: 0,
						squareSymbol: false
					},
					yAxis: {
						title: {
							text: ''
						},
						gridLineColor: '#768da4',
						gridZIndex: 4,
						gridLineWidth: 0.2,
						labels: {
							overflow: 'justify',
							style: {
								fontSize: 8,
								color: '#768da4'
							}
						}
					},
					xAxis: {
						categories: labels,
						gridLineColor: '#768da4',
						gridLineWidth: 0.2,
						gridZIndex: 4,
						labels: {
							style: {
								fontSize: 9,
								color: '#768da4'
							}
						}
					},
					series: [
						{
							type: 'area',
							data: weightData,
							marker: {
								symbol: 'circle',
								fillColor: 'white',
								color: '#9D60FB',
								lineWidth: 1,
								lineColor: '#9D60FB',
								radius: 3
							},
							color: '#9D60FB',
							fillColor: 'rgba(157, 96, 251 ,0.1)',
							plotOptions: {
								series: {
									fillOpacity: 0.1
								}
							},
							lineWidth: 1,
							name: ''
						}
					]
				}
			};

			promiseArray.push(
				Promise.resolve(generateGraph(exportSettingsWeight)).then(data => {
					return (dbData.patientsReadings[
						readingKey
					].weightChart = data.toString('base64'));
				})
			);
		}
	}
	return Promise.all(promiseArray).then(data => {
		exporter.killPool();
		return dbData;
	});
};
