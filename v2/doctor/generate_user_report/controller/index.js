/* eslint-disable no-dupe-keys */
/* eslint-disable security/detect-non-literal-fs-filename */
const AWS = require('aws-sdk');
const pdf = require('html-pdf');
const fs = require('fs');
const ejs = require('ejs');
const jwt = require('jsonwebtoken');
const mergePdf = require('easy-pdf-merge');
const moment = require('moment');
const DB = require('./dynamodb');
const chart = require('./chart');
const CONSTANTS = require('./CONSTANTS');
require('moment-duration-format');
const momentTimezone = require('moment-timezone');

AWS.config.update(CONSTANTS.AWS_CONFIG);
const s3Client = new AWS.S3();

const weightToLbs = weight => {
	return (Number(weight) * 2.20462).toFixed(1).toString();
};

const formatPhoneNumber = phoneNumberString => {
	const cleaned = ('' + phoneNumberString).replace(/\D/g, '');
	const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
	if (match) {
		const intlCode = match[1] ? '+1 ' : '';
		return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('');
	}
	return phoneNumberString;
};

const generatePdfFromHtml = async (dbData, reportGeneratedAt) => {
	dbData.patientDetails.Items[0].createdDate = moment(
		dbData.patientDetails.Items[0].createdDate
	).format('MM/DD/YYYY');
	if (dbData.patientDetails.Items[0].attributes.dob) {
		dbData.patientDetails.Items[0].attributes.dob = moment(
			dbData.patientDetails.Items[0].attributes.dob
		).format('MM/DD/YYYY');
	}
	if (dbData.patientDetails.Items[0].attributes.gender) {
		if (
			dbData.patientDetails.Items[0].attributes.gender.toLowerCase() === 'm'
		) {
			dbData.patientDetails.Items[0].attributes.gender = 'Male';
		} else if (
			dbData.patientDetails.Items[0].attributes.gender.toLowerCase() === 'f'
		) {
			dbData.patientDetails.Items[0].attributes.gender = 'Female';
		}
	}
	const newDbData = await chart.chartObject(dbData);

	const reportPeriod = dbData.reportPeriod;

	const ejsPromise = new Promise(resolve => {
		ejs.renderFile(
			`${__dirname}/omronpdf/index.ejs`,
			{
				dbData: { ...newDbData, reportGeneratedAt },
				logoUrl: CONSTANTS.LOGO_URL,
				footerLogoUrl: CONSTANTS.FOOTER_LOGO_URL,
				reportPeriod,
				CONSTANTS
			},
			async (err, data) => {
				if (err) {
					console.log('EJSerrrr>>>', err);
					return err;
				} else {
					const options = {
						format: 'A2',
						header: {
							height: '6mm'
						}
					};
					const promise = new Promise((resolve, reject) => {
						const filePathName = `report${moment()
							.utc()
							.valueOf()}.pdf`;
						pdf
							.create(data, options)
							.toFile(`${__dirname}/${filePathName}`, function (err, res) {
								if (err) return reject(err);
								console.log(res); // { filename: '/app/businesscard.pdf' }
								res.filePathName = filePathName;
								resolve(res);
							});
					});
					resolve(
						Promise.resolve(promise)
							.then(data => {
								return { pdfData: data, error: false };
							})
							.catch(e => {
								return { error: true };
							})
					);
				}
			}
		);
	});

	const ejsPromise2 = new Promise(resolve => {
		ejs.renderFile(
			`${__dirname}/omronpdf/logoPage.ejs`,
			{
				dbData: newDbData,
				logoUrl: CONSTANTS.LOGO_URL,
				footerLogoUrl: CONSTANTS.FOOTER_LOGO_URL,
				reportPeriod
			},
			async (err, data) => {
				if (err) {
					console.log('EJSerrrr>>>', err);
					return err;
				} else {
					const options = {
						format: 'A2',
						header: {
							height: '0mm'
						},
						footer: {
							height: '0mm'
						}
					};
					const promise = new Promise((resolve, reject) => {
						const filePathName = 'logoPage.pdf';
						pdf
							.create(data, options)
							.toFile(`${__dirname}/${filePathName}`, function (err, res) {
								if (err) return reject(err);
								console.log(res); // { filename: '/app/businesscard.pdf' }
								res.filePathName = filePathName;
								resolve(res);
							});
					});
					resolve(
						Promise.resolve(promise)
							.then(data => {
								return { pdfData: data, error: false };
							})
							.catch(e => {
								return { error: true };
							})
					);
				}
			}
		);
	});
	return Promise.all([ejsPromise2, ejsPromise]).then(pdfArray => {
		const logoFile = `${__dirname}/${pdfArray[0].pdfData.filePathName}`;
		const mainFile = `${__dirname}/${pdfArray[1].pdfData.filePathName}`;
		let pathString = reportPeriod.replace(', ', ' ');
		pathString = pathString.replace(', ', ' ');
		const newMergedFilePath = `Patient Report-${pathString}.pdf`;
		console.log('newMergedFilePath>>>>', newMergedFilePath);
		const mergePdfPromise = new Promise(resolve => {
			mergePdf(
				[logoFile, mainFile],
				`${__dirname}/${newMergedFilePath}`,
				function (err) {
					fs.unlink(logoFile, () => {
						console.log('File deleted', logoFile);
					});
					fs.unlink(mainFile, () => {
						console.log('File deleted', mainFile);
					});
					if (err) {
						console.log('mergeError', err);
						resolve({ error: true });
					}
					resolve({ error: false });
				}
			);
		});
		return Promise.resolve(mergePdfPromise).then(result => {
			if (result.error) {
				return { error: false };
			}
			return {
				pdfData: {
					filePathName: newMergedFilePath
				},
				error: false
			};
		});
	});
};

const getDataForUser = async ({
	doctorId,
	hospitalID,
	body: { userId, startDate, endDate }
}) => {
	try {
		const hospitalTableDetails = await DB.verifyHospital(hospitalID);

		if (!hospitalTableDetails) {
			return Promise.resolve({
				error: true,
				errorDetails: {
					success: false,
					message: CONSTANTS.ERROR.HOSPITAL_ID_INVALID.message,
					errorCode: CONSTANTS.ERROR.HOSPITAL_ID_INVALID.code
				},
				data: {}
			});
		}
		const doctorProfile = Promise.resolve(
			DB.getDoctorProfile(doctorId, hospitalTableDetails.DOCTOR_PROFILE)
		);
		const patientDetails = await DB.getPatientDetails(
			doctorId,
			userId,
			hospitalTableDetails.DOCTOR_PATIENT
		);
		if ([1, 4, 6].indexOf(patientDetails.Items[0].processStatus) > -1) {
			return Promise.resolve({
				error: true,
				errorDetails: {
					success: false,
					message: CONSTANTS.ERROR.ORDER_IN_PROGRESS.message,
					errorCode: CONSTANTS.ERROR.ORDER_IN_PROGRESS.code
				},
				data: {}
			});
		}
		let bpReadings, nonBpReadings, bpAlertReadings, nonBpAlertReadings;
		if (hospitalTableDetails.Items[0].dbIdentifier === CONSTANTS.HODES) {
			bpReadings = Promise.resolve(
				DB.getBpReadings(
					patientDetails.Items[0].hubID || userId,
					startDate,
					endDate,
					hospitalTableDetails.BP_TABLE
				)
			);
			nonBpReadings = Promise.resolve(
				DB.getNonBpReadings(
					patientDetails.Items[0].hubID || userId,
					startDate,
					endDate,
					hospitalTableDetails.NON_BP_TABLE
				)
			);
			bpAlertReadings = Promise.resolve(
				DB.getBpAlerts(
					patientDetails.Items[0].hubID || userId,
					startDate,
					endDate,
					hospitalTableDetails.ALERT_BP_TABLE
				)
			);
			nonBpAlertReadings = Promise.resolve(
				DB.getNonBpAlerts(
					patientDetails.Items[0].hubID || userId,
					startDate,
					endDate,
					hospitalTableDetails.ALERT_NON_BP_TABLE
				)
			);
		} else {
			bpReadings = Promise.resolve(
				DB.getBpReadings(
					userId,
					startDate,
					endDate,
					hospitalTableDetails.BP_TABLE
				)
			);
			nonBpReadings = Promise.resolve(
				DB.getNonBpReadings(
					userId,
					startDate,
					endDate,
					hospitalTableDetails.NON_BP_TABLE
				)
			);
			bpAlertReadings = Promise.resolve(
				DB.getBpAlerts(
					userId,
					startDate,
					endDate,
					hospitalTableDetails.ALERT_BP_TABLE
				)
			);
			nonBpAlertReadings = Promise.resolve(
				DB.getNonBpAlerts(
					userId,
					startDate,
					endDate,
					hospitalTableDetails.ALERT_NON_BP_TABLE
				)
			);
		}
		const ehrUserDetails = Promise.resolve(
			DB.getEhrDetails(userId, hospitalTableDetails.EHR_USER_TABLE)
		);
		const getNotes = Promise.resolve(
			DB.getNotes(
				doctorId,
				userId,
				startDate,
				endDate,
				hospitalTableDetails.NOTES_TABLE
			)
		);
		const get99453Data = Promise.resolve(
			DB.get99453Data(userId, hospitalTableDetails.FINANCE_99453_TABLE)
		);
		const getFinanceData = Promise.resolve(
			DB.getFinanceData(userId, hospitalTableDetails.FINANCE_99454_TABLE)
		);
		const get99457Data = Promise.resolve(
			DB.get99457Data(userId, hospitalTableDetails.FINANCE_99457_TABLE)
		);
		const get99458Data = Promise.resolve(
			DB.get99458Data(userId, hospitalTableDetails.FINANCE_99458_TABLE)
		);
		const staffProfile = Promise.resolve(
			DB.getStaffProfile(doctorId, hospitalTableDetails.STAFF_PROFILE)
		);
		const doctorConsultation = Promise.resolve(
			DB.getConsultation(doctorId, hospitalTableDetails.CONSULTATION_TABLE)
		);

		return Promise.all([
			doctorProfile,
			ehrUserDetails,
			bpReadings,
			nonBpReadings,
			bpAlertReadings,
			nonBpAlertReadings,
			getNotes,
			get99453Data,
			getFinanceData,
			get99457Data,
			get99458Data,
			staffProfile,
			doctorConsultation
		])
			.then(async outResult => {
				const orderDetails = await DB.getOrderDetails(
					outResult[1].Items[0].orderID,
					hospitalTableDetails.ORDER_DETAILS
				);
				const returnData = {
					doctorProfile: outResult[0],
					patientDetails: patientDetails,
					ehrUserDetails: outResult[1],
					bpReadings: outResult[2],
					nonBpReadings: outResult[3],
					bpAlertReadings: outResult[4],
					NonbpAlertReadings: outResult[5],
					getNotes: outResult[6],
					get99453Data: outResult[7],
					getFinanceData: outResult[8],
					get99457Data: outResult[9],
					get99458Data: outResult[10],
					staffProfile: outResult[11],
					doctorConsultation: outResult[12],
					orderDetails
				};
				return {
					error: false,
					errorDetails: {},
					data: returnData
				};
			})
			.catch(e => {
				console.log('getDataForUser error>>>>', e);
			});
	} catch (err) {
		return Promise.reject(err);
	}
};

const createReport = async ({
	doctorId,
	hospitalID,
	body: { userId, startDate, endDate }
}) => {
	try {
		let getUserData = await getDataForUser({
			doctorId,
			hospitalID,
			body: { userId, startDate, endDate }
		});
		if (getUserData.error) {
			return {
				success: false,
				message: getUserData.errorDetails.message,
				errorCode: getUserData.errorDetails.errorCode
			};
		}
		getUserData = getUserData.data;
		if (getUserData && Object.keys(getUserData).length > 0) {
			const startDateMonth = moment(startDate).format('MM');
			const startDateYear = moment(startDate).format('Y');
			const endDateMonth = moment(endDate).format('MM');
			const endDateYear = moment(endDate).format('Y');
			if (startDateMonth === endDateMonth && startDateYear === endDateYear) {
				getUserData.reportPeriod = `${moment(startDate).format(
					'MMMM'
				)}, ${startDateYear}`;
			} else {
				getUserData.reportPeriod = `${moment(startDate).format(
					'MMMM'
				)}, ${startDateYear} - ${moment(endDate).format(
					'MMMM'
				)}, ${endDateYear}`;
			}
			const differenceInMonth = moment(endDate).diff(
				moment(startDate),
				'M',
				true
			);
			const data = {};
			data[`${startDateYear}${startDateMonth}`] = {
				bp: [],
				weight: [],
				bpAlert: [],
				weightAlert: [],
				notes: [],
				group99453Data: [],
				groupFinanceData: [],
				group99457Data: [],
				group99458Data: [],
				groupconsultationData: [],
				key: `${moment(startDate).format('MMMM')}, ${startDateYear}`
			};
			if (differenceInMonth >= 1) {
				for (let i = 1; i <= differenceInMonth; i++) {
					const newMonthDate = moment(
						`${startDateYear}-${startDateMonth}-01`
					).add(i, 'M');
					const newMonth = moment(newMonthDate).format('MM');
					const newYear = moment(newMonthDate).format('Y');
					data[`${newYear}${newMonth}`] = {
						bp: [],
						weight: [],
						bpAlert: [],
						weightAlert: [],
						notes: [],
						group99453Data: [],
						groupFinanceData: [],
						group99457Data: [],
						group99458Data: [],
						groupconsultationData: [],
						key: `${moment(newMonthDate).format('MMMM')}, ${newYear}`
					};
				}
			}
			const {
				bpReadings,
				nonBpReadings,
				bpAlertReadings,
				NonbpAlertReadings,
				getNotes,
				get99453Data,
				getFinanceData,
				get99457Data,
				get99458Data,
				staffProfile,
				doctorProfile,
				doctorConsultation,
				patientDetails
			} = getUserData;
			for (const reading of bpReadings.Items) {
				const month = moment(reading.measurementDate).format('MM');
				const year = moment(reading.measurementDate).format('Y');
				const key = `${year}${month}`;
				reading.measurementDateFormat = moment(reading.measurementDate).format(
					'MM/DD/YYYY hh:mm a'
				);
				if (data[key]) {
					data[key].bp.push(reading);
				}
			}

			for (const reading of nonBpReadings.Items) {
				const month = moment(reading.measurementDate).format('MM');
				const year = moment(reading.measurementDate).format('Y');
				const key = `${year}${month}`;
				reading.attributes.weightLBS = weightToLbs(reading.attributes.weight);
				NonbpAlertReadings.Items.forEach(weightAlert => {
					if (weightAlert.measurementDate === reading.measurementDate) {
						reading.alerts = weightAlert.alerts;
						reading.attributes.haveAlert = true;
					}
				});
				reading.measurementDateFormat = moment(reading.measurementDate).format(
					'MM/DD/YYYY hh:mm a'
				);
				if (data[key]) {
					data[key].weight.push(reading);
				}
			}

			for (const reading of bpAlertReadings.Items) {
				const month = moment(reading.measurementDate).format('MM');
				const year = moment(reading.measurementDate).format('Y');
				const key = `${year}${month}`;
				reading.measurementDateFormat = moment(reading.measurementDate).format(
					'MM/DD/YYYY'
				);
				if (reading.alerts.bp.resolvedDate) {
					reading.resolvedDateFormat = moment(
						reading.alerts.bp.resolvedDate
					).format('MM/DD/YYYY');
				}
				if (reading.updates && reading.updates.length) {
					const updates = reading.updates.map(update => {
						let outObject = {};
						if (
							update.updatedUserType === 'STAFF_MEMBER' &&
							staffProfile &&
							staffProfile.length
						) {
							staffProfile.forEach(staff => {
								if (update.updatedBy === staff.staffID) {
									outObject = {
										...update,
										updatedBy: staff.attributes.name,
										updatedAt: moment(update.updatedAt).format('MM/DD/YYYY')
									};
								}
							});
						} else {
							if (update.updatedBy === doctorProfile.userID) {
								outObject = {
									...update,
									updatedBy: doctorProfile.name,
									updatedAt: moment(update.updatedAt).format('MM/DD/YYYY')
								};
							}
						}

						return outObject;
					});

					reading.updates = updates;
				} else {
					reading.updates = [
						{
							updatedBy: doctorProfile.name,
							updatedAt: moment(reading.modifiedDate).format('MM/DD/YYYY')
						}
					];
				}
				if (data[key]) {
					data[key].bpAlert.push(reading);
				}
			}

			for (const reading of NonbpAlertReadings.Items) {
				const month = moment(reading.measurementDate).format('MM');
				const year = moment(reading.measurementDate).format('Y');
				const key = `${year}${month}`;
				reading.measurementDateFormat = moment(reading.measurementDate).format(
					'MM/DD/YYYY'
				);
				reading.attributes.weightLBS = weightToLbs(reading.attributes.weight);
				if (reading.alerts.weight.resolvedDate) {
					reading.resolvedDateFormat = moment(
						reading.alerts.weight.resolvedDate
					).format('MM/DD/YYYY');
				}
				if (reading.updates && reading.updates.length) {
					const updates = reading.updates.map(update => {
						let outObject = {};
						if (
							update.updatedUserType === 'STAFF_MEMBER' &&
							staffProfile &&
							staffProfile.length
						) {
							staffProfile.forEach(staff => {
								if (update.updatedBy === staff.staffID) {
									outObject = {
										...update,
										updatedBy: staff.attributes.name,
										updatedAt: moment(update.updatedAt).format('MM/DD/YYYY')
									};
								}
							});
						} else {
							if (update.updatedBy === doctorProfile.userID) {
								outObject = {
									...update,
									updatedBy: doctorProfile.name,
									updatedAt: moment(update.updatedAt).format('MM/DD/YYYY')
								};
							}
						}

						return outObject;
					});

					reading.updates = updates;
				} else {
					reading.updates = [
						{
							updatedBy: doctorProfile.name,
							updatedAt: moment(reading.modifiedDate).format('MM/DD/YYYY')
						}
					];
				}
				if (data[key]) {
					data[key].weightAlert.push(reading);
				}
			}

			for (const reading of getNotes) {
				const month = moment(reading.startDate).format('MM');
				const year = moment(reading.startDate).format('Y');
				const key = `${year}${month}`;
				if (reading.updates && reading.updates.length) {
					const updates = reading.updates.map(update => {
						let outObject = {};
						if (
							update.updatedUserType === 'STAFF_MEMBER' &&
							staffProfile &&
							staffProfile.length
						) {
							staffProfile.forEach(staff => {
								if (update.updatedBy === staff.staffID) {
									outObject = {
										...update,
										updatedBy: staff.attributes.name,
										updatedAt: moment(update.updatedAt).format('MM/DD/YYYY')
									};
								}
							});
						} else {
							if (update.updatedBy === doctorProfile.userID) {
								outObject = {
									...update,
									updatedBy: doctorProfile.name,
									updatedAt: moment(update.updatedAt).format('MM/DD/YYYY')
								};
							}
						}

						return outObject;
					});
					reading.updates = updates;
				} else {
					reading.updates = [
						{
							updatedBy: doctorProfile.name,
							updatedAt: moment(reading.modifiedDate).format('MM/DD/YYYY')
						}
					];
				}
				if (data[key]) {
					reading.startDate = moment(reading.startDate).format('MM/DD/YYYY');
					data[key].notes.push(reading);
				}
			}

			for (const reading of get99453Data) {
				const month = moment(
					reading.attributes.transaction.dateOfService
				).format('MM');
				const year = moment(
					reading.attributes.transaction.dateOfService
				).format('Y');
				const key = `${year}${month}`;
				reading.dateOfServiceFormat = moment(
					reading.attributes.transaction.dateOfService
				).format('MM/DD/YYYY');
				if (data[key]) {
					data[key].group99453Data.push(reading);
				}
			}

			for (const reading of getFinanceData.Items) {
				const month = moment(
					reading.attributes.transaction.dateOfService
				).format('MM');
				const year = moment(
					reading.attributes.transaction.dateOfService
				).format('Y');
				const key = `${year}${month}`;
				reading.dateOfServiceFormat = moment(
					reading.attributes.transaction.dateOfService
				).format('MM/DD/YYYY');
				if (data[key]) {
					data[key].groupFinanceData.push(reading);
				}
			}

			for (const reading of get99457Data) {
				const month = moment(
					reading.attributes.transaction.dateOfService
				).format('MM');
				const year = moment(
					reading.attributes.transaction.dateOfService
				).format('Y');
				const key = `${year}${month}`;
				if (data[key]) {
					data[key].group99457Data.push(reading);
				}
			}

			for (const reading of get99458Data.Items) {
				const month = moment(
					reading.attributes.transaction.dateOfService
				).format('MM');
				const year = moment(
					reading.attributes.transaction.dateOfService
				).format('Y');
				const key = `${year}${month}`;
				if (data[key]) {
					data[key].group99458Data.push(reading);
				}
			}

			const userConsultation = doctorConsultation.Items.filter(
				record =>
					record.userID === patientDetails.Items[0].userID &&
					record.startTime >= startDate &&
					record.startTime <= endDate
			);
			doctorConsultation.Items = JSON.parse(JSON.stringify(userConsultation));
			let totalTime = 0;
			let lastKey;
			for (const reading of userConsultation) {
				const month = moment(reading.startTime).format('MM');
				const year = moment(reading.startTime).format('Y');
				const key = `${year}${month}`;
				if (lastKey && lastKey !== key) {
					totalTime = 0;
				}
				lastKey = key;
				if (reading.consultationTime) {
					totalTime = totalTime + parseInt(reading.consultationTime, 10);
				}
				const duration = moment.duration(
					parseInt(reading.consultationTime, 10),
					'seconds'
				);
				const formatted = duration.format('mm:ss', {
					trim: false
				});
				reading.consultationTime = formatted;
				if (reading.updates && reading.updates.length) {
					const updates = reading.updates.map(update => {
						let outObject = {};
						if (
							update.updatedUserType === 'STAFF_MEMBER' &&
							staffProfile &&
							staffProfile.length
						) {
							staffProfile.forEach(staff => {
								if (update.updatedBy === staff.staffID) {
									outObject = {
										...update,
										updatedBy: staff.attributes.name,
										updatedAt: moment(update.updatedAt).format('MM/DD/YYYY')
									};
								}
							});
						} else {
							if (update.updatedBy === doctorProfile.userID) {
								outObject = {
									...update,
									updatedBy: doctorProfile.name,
									updatedAt: moment(update.updatedAt).format('MM/DD/YYYY')
								};
							}
						}

						return outObject;
					});
					reading.updates = updates;
				} else {
					reading.updates = [
						{
							updatedBy: doctorProfile.name,
							updatedAt: moment(reading.modifiedDate).format('MM/DD/YYYY')
						}
					];
				}
				if (data[key]) {
					reading.startTime = moment(reading.startTime).format('MM/DD/YYYY');
					data[key].groupconsultationData.push(reading);
					const duration = moment.duration(totalTime, 'seconds');
					const formatted = duration.format('hh:mm:ss');
					data[key].totalTime = formatted;
				}
			}

			const timeZone =
				getUserData.doctorProfile && getUserData.doctorProfile.timeZone;
			let reportGeneratedAt;
			if (timeZone) {
				reportGeneratedAt = momentTimezone.tz(timeZone).format('MM/DD/YYYY');
			} else {
				reportGeneratedAt = `${new Date().getMonth() +
					1}/${new Date().getUTCDate()}/${new Date().getFullYear()}`;
			}
			getUserData.patientsReadings = JSON.parse(JSON.stringify(data));
			if (getUserData.doctorProfile.phoneNumber) {
				getUserData.doctorProfile.phoneNumber = formatPhoneNumber(
					getUserData.doctorProfile.phoneNumber
				);
			}
			const getPdf = await generatePdfFromHtml(getUserData, reportGeneratedAt);
			console.log(getPdf);
			if (getPdf.error === false) {
				const dataResult = await uploadReportToS3(
					`${__dirname}/${getPdf.pdfData.filePathName}`,
					getPdf.pdfData.filePathName
				);
				console.log('dataS#>>>', JSON.stringify(dataResult));
				const result = await getS3Url(dataResult.Key);
				const hospitalTableDetails = await DB.verifyHospital(hospitalID);
				const reportData = {
					createdDate: moment.utc().valueOf(),
					doctorID: doctorId,
					reportUrl: dataResult.Key,
					status: 'COMPLETED',
					type: 'PatientReport',
					user: [getUserData.patientDetails.Items[0].attributes.name]
				};
				await Promise.resolve(
					DB.saveItem(reportData, hospitalTableDetails.DOCTOR_REPORTS)
				);
				return { success: true, report: result };
			}
			return { success: false };
		}
		return { success: false, message: 'Missing Data' };
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: error.message,
			errorCode: 'INTERNAL_SERVER_ERROR'
		});
	}
};

const uploadReportToS3 = async (file, key) => {
	try {
		const data = fs.readFileSync(file);
		const timestamp = moment.utc().valueOf();
		const params = {
			Bucket: CONSTANTS.S3_BUCKET,
			Body: data,
			Key: `${CONSTANTS.PDF_BUCKET_FOLDER}/${timestamp}/${key}`,
			ContentType: 'application/pdf',
			ContentDisposition: 'attachment'
		};
		const result = await s3Client.upload(params).promise();
		fs.unlinkSync(file);
		return result;
	} catch (error) {
		return Promise.reject(error);
	}
};

const getS3Url = async Key => {
	const params = {
		Bucket: CONSTANTS.S3_BUCKET,
		Expires: CONSTANTS.EXPIRE_IN,
		Key: Key
	};
	console.log(params);
	return new Promise(function (resolve, reject) {
		s3Client.getSignedUrl('getObject', params, function (err, data) {
			console.log(err);
			resolve(data);
		});
	});
};

const generateReport = async ({
	doctorId,
	hospitalID,
	body: { userId, startDate, endDate }
}) => {
	console.log('userid', userId);
	let response = { success: true, data: {} };
	try {
		const report = await createReport({
			doctorId,
			hospitalID,
			body: { userId, startDate, endDate }
		});
		return report;
	} catch (error) {
		console.log(error);

		response = {
			success: false,
			message: CONSTANTS.ERROR.GENERATE_REPORT.message,
			errorCode: CONSTANTS.ERROR.GENERATE_REPORT.code
		};
	}

	return response;
};

const applyValidation = async (payload, hederData) => {
	let response = {};
	try {
		const { doctorid: doctorId, hospitalid: hospitalID } = hederData;
		let { userId, startDate, endDate } = payload;
		if (!userId || userId.length < 1) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERROR.REQUIRED_USER_ID.message,
				errorCode: CONSTANTS.ERROR.REQUIRED_USER_ID.code
			});
		}
		if (!startDate) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERROR.REQUIRED_START_DATE.message,
				errorCode: CONSTANTS.ERROR.REQUIRED_START_DATE.code
			});
		}
		if (!endDate) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERROR.REQUIRED_END_DATE.message,
				errorCode: CONSTANTS.ERROR.REQUIRED_END_DATE.code
			});
		}
		startDate = Number(startDate);
		endDate = Number(endDate);
		if (Array.isArray(userId)) {
			userId = userId[0].userID;
		}
		if (isNaN(startDate) || isNaN(endDate)) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERROR.INVALID_DATE.message,
				errorCode: CONSTANTS.ERROR.INVALID_DATE.code
			});
		}
		if (startDate > endDate) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERROR.INVALID_DATE_RANGE.message,
				errorCode: CONSTANTS.ERROR.INVALID_DATE_RANGE.code
			});
		}
		response = await generateReport({
			doctorId,
			hospitalID,
			body: { userId, startDate, endDate }
		});
		return response;
	} catch (error) {
		console.log(error);
		response = { success: false, message: CONSTANTS.userId, errorCode: 400 };
	}
	return response;
};

const validateToken = token => {
	if (!token) {
		return false;
	}
	const decodedJwt = jwt.decode(token, { complete: true });
	const expiryTime = new Date(decodedJwt.payload.exp * 1000).getTime();
	const currentTime = new Date().getTime();
	const userId = decodedJwt.payload['custom:userId'] || null;
	const hospitalID = decodedJwt.payload['custom:hospitalID'] || null;
	if (expiryTime < currentTime && 0) {
		return false;
	} else {
		return {
			doctorid: userId,
			hospitalid: hospitalID
		};
	}
};

const pdfView = async (req, res, next) => {
	console.log(req.body); // Log Request
	const header = req.headers ? req.headers : {};
	const token = header.Authorization || header.authorization || null;
	const isValid = validateToken(token);
	if (!isValid) {
		res.status(400).json({
			succes: false,
			errorCode: CONSTANTS.ERROR.INVALID_TOKEN.code,
			message: CONSTANTS.ERROR.INVALID_TOKEN.message
		});
	} else {
		const response = await applyValidation(req.body, isValid);
		res.status(200).json(response);
	}
};

module.exports = {
	pdfView
};
