exports.MAIL_CONSTANTS = {
	FROM: process.env.FROM_EMAIL,
	SUBJECT: 'Your Requested OMRON Report from',
	FILENAME: 'Report'
};

exports.FILE_PATH = '/tmp/report';

exports.ERROR = {
	INTERNAL_SERVER_ERROR: {
		code: 'INTERNAL_SERVER_ERROR',
		message: 'Internal server error'
	},
	REQUIRED_USER_ID: {
		code: 'REQUIRED_PARAMETER',
		message: 'userId is required'
	},
	REQUIRED_START_DATE: {
		code: 'REQUIRED_PARAMETER',
		message: 'startDate is required'
	},
	REQUIRED_END_DATE: {
		code: 'REQUIRED_PARAMETER',
		message: 'endDate is required'
	},
	INVALID_DATE: { code: 'INVALID_DATE', message: 'Invalid date format' },
	INVALID_DATE_RANGE: { code: 'INVALID_DATE_RANGE', message: 'Invalid date' },
	GENERATE_REPORT: {
		code: 'GENERATE_REPORT',
		message: 'Error in generating report'
	},
	NO_READINGS: {
		code: 'NO_READINGS',
		message: 'No readings available for report'
	},
	HOSPITAL_ID_INVALID: {
		code: 'HOSPITAL_ID_INVALID',
		message: 'Hospital Id is invalid'
	},
	INVALID_TOKEN: { code: 'INVALID_TOKEN', message: 'Invalid Token' },
	ORDER_IN_PROGRESS: {
		code: 'ORDER_IN_PROGRESS',
		message: 'Order is in Progress'
	}
};

exports.RPM = '_rpm_';
exports.HODES = 'hodes';
exports.WEIGHT_MANUAL = '_weight_Manual';
exports.REPORT_TABLE = process.env.REPORT_TABLE;
exports.S3_BUCKET = process.env.S3_BUCKET;
exports.STAGE = process.env.STAGE;
exports.REGION = process.env.REGION;
exports.SENDGRID_API_KEY = process.env.SENDGRID_KEY;
exports.AWS_CONFIG = {
	region: process.env.REGION || 'us-west-2'
};

exports.DEFAULT_TABLES = {
	DOCTOR_PROFILE_TABLE: 'doctor_profile',
	ORDER_DETAILS_TABLE: 'order_details',
	ALERT_BP_TABLE: 'bp_readings_alerts',
	FINANCE_99457_TABLE: 'ehr_fin_99457_transaction',
	FINANCE_99458_TABLE: 'ehr_fin_99458_transaction',
	EHR_USER_TABLE: 'ehr_user',
	DOCTOR_TABLE: 'prd_global_rpm_hospital_master',
	NON_BP_TABLE: 'non_bp_readings',
	NOTES_TABLE: 'doctor_notes',
	ALERT_NON_BP_TABLE: 'non_bp_readings_alerts',
	DOCTOR_PATIENT_TABLE: 'doctor_patient',
	FINANCE_99453_TABLE: 'ehr_fin_99453_transaction',
	BP_TABLE: 'bp_readings',
	FINANCE_99454_TABLE: 'ehr_fin_transaction',
	STAFF_PROFILE_TABLE: 'staff',
	DOCTOR_CONSULTATION_TABLE: 'doctor_consultation',
	DOCTOR_REPORTS: 'doctor_reports'
};
exports.STAGE = process.env.stage || 'prd_global';
exports.S3_BUCKET = process.env.S3_BUCKET || 'prd-vitalsight-website';
exports.EXPIRE_IN = 60 * 60;
exports.PDF_BUCKET_FOLDER = 'pdf-doctor-report';
exports.LOGO_URL =
	'https://dev-vitalsight-common-website.s3-us-west-2.amazonaws.com/share-report-images/bg.jpg';
exports.FOOTER_LOGO_URL =
	'https://dev-vitalsight-common-website.s3-us-west-2.amazonaws.com/share-report-images/logo.jpg';

exports.USER_TABLE = process.env.USER_TABLE;
exports.NO_RECORD_FOUND = 'No records to display';
exports.REVIEW_PATIENT_DATA = 'Reviewed Patient Data';
