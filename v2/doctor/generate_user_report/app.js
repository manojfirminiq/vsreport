const express = require('express');
const { pdfView } = require('./controller');
const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header(
		'Access-Control-Allow-Headers',
		'Origin, X-Requested-With, Content-Type, Accept'
	);
	res.header('X-Frame-Options', 'DENY');
	res.header(
		'Strict-Transport-Security',
		'max-age=63072000; includeSubdomains; preload'
	);
	res.header('X-XSS-Protection', '1');
	res.header('X-Content-Type-Options', 'nosniff');
	next();
});

app.set('view engine', 'ejs');

app.post('/api/v1/pdf', pdfView);

app.listen(3000, () => console.log('Listening on: 3000'));
