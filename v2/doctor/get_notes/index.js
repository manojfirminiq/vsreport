const DB = require('./dynamodb');
const CONSTANTS = require('./CONSTANTS');

const getNotes = async ({ doctorId, userId, hospitalID, start, end }) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQ.CODE
			});
		}

		if (!hospitalID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		let Items;
		if (!start) {
			Items = await DB.getNotes(hospitalTable, doctorId, userId);
			if (!Items) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.NOTES_NOT_FOUND.MESSAGE,
					errorCode: CONSTANTS.ERRORS.NOTES_NOT_FOUND.CODE
				});
			}
			Items = Items.sort((a, b) => b.startDate - a.startDate);
		} else {
			Items = await DB.getNotesStartDate(
				hospitalTable,
				doctorId,
				userId,
				start,
				end
			);
			if (!Items) {
				return Promise.resolve({
					success: false,
					message: CONSTANTS.ERRORS.NOTES_NOT_FOUND.MESSAGE,
					errorCode: CONSTANTS.ERRORS.NOTES_NOT_FOUND.CODE
				});
			}
		}

		const response = {
			success: true,
			data: Items
		};
		return Promise.resolve(response);
	} catch (error) {
		console.log('error', error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const payload = JSON.parse(JSON.stringify(event));
	delete payload.headers;
	const log = {
		USERID: event.staffID ? event.staffID : event.doctorId,
		ACTION: event.body ? event.body.action : '',
		PAYLOAD: payload
	};
	console.log('API-LOG--->', JSON.stringify(log));
	const response = await getNotes(event);
	callback(null, response);
};
