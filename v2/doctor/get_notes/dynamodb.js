const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};

	const { Count, Items } = await dbClient.query(paramsTable).promise();
	if (Count <= 0) {
		return false;
	} else {
		return (
			process.env.STAGE +
			'_' +
			CONSTANTS.RPM +
			'_' +
			Items[0].dbIdentifier +
			'_' +
			CONSTANTS.NOTES_TABLE
		);
	}
};

const getNotes = async (hospitalTable, doctorId, userID) => {
	const paramsTable = {
		TableName: hospitalTable,
		IndexName: 'doctorID-userID-index',
		KeyConditionExpression: '#doctor = :id AND #userId = :userId',
		ExpressionAttributeNames: {
			'#doctor': 'doctorID',
			'#userId': 'userID',
			'#deleteFlag': 'deleteFlag'
		},
		ExpressionAttributeValues: {
			':id': doctorId,
			':userId': userID,
			':deleteFlagValue': 1
		},
		FilterExpression: '#deleteFlag <> :deleteFlagValue',
		ScanIndexForward: false
	};

	const { Count, Items } = await dbClient.query(paramsTable).promise();
	if (Count <= 0) {
		return false;
	} else {
		return Items;
	}
};

const getNotesStartDate = async (
	hospitalTable,
	doctorId,
	userID,
	start,
	end
) => {
	const paramsTable = {
		TableName: hospitalTable,
		IndexName: 'userID-startDate-index',
		KeyConditionExpression: '#userId = :userId',
		ExpressionAttributeNames: {
			'#userId': 'userID',
			'#deleteFlag': 'deleteFlag'
		},
		ExpressionAttributeValues: {
			':userId': userID,
			':deleteFlagValue': 1
		},
		FilterExpression: '#deleteFlag <> :deleteFlagValue',
		ScanIndexForward: false
	};
	if (start || end) {
		if (start && end) {
			paramsTable.KeyConditionExpression +=
				' AND #sortKey BETWEEN :minDate AND :maxDate';
			paramsTable.ExpressionAttributeValues[':minDate'] = parseInt(start);
			paramsTable.ExpressionAttributeValues[':maxDate'] = parseInt(end);
		} else if (!start) {
			paramsTable.KeyConditionExpression += ' AND #sortKey <= :maxDate';
			paramsTable.ExpressionAttributeValues[':maxDate'] = parseInt(end);
		} else {
			paramsTable.KeyConditionExpression += ' AND #sortKey >= :minDate';
			paramsTable.ExpressionAttributeValues[':minDate'] = parseInt(start);
		}

		paramsTable.ExpressionAttributeNames['#sortKey'] = 'startDate';
	}

	const { Count, Items } = await dbClient.query(paramsTable).promise();

	if (Count <= 0) {
		console.log('Count: ', Count);
		return false;
	} else {
		const records = Items.filter(function (item) {
			return item.doctorID === doctorId;
		});
		return records;
	}
};

module.exports.verifyHospital = verifyHospital;
module.exports.getNotes = getNotes;
module.exports.getNotesStartDate = getNotesStartDate;
