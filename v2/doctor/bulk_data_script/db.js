const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');

const verifyHospital = async (hospitalID) => {
	const paramsTable = {
		TableName: CONSTANTS.HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalID
		}
	};

	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		return {
			HOSPITAL_IDENTIFIER: Items[0].dbIdentifier,
			DOCTOR_PATIENT: process.env.STAGE + '_' + CONSTANTS.RPM + '_' + Items[0].dbIdentifier + '_' + CONSTANTS.DOCTOR_PATIENT,
			BP_READINGS: process.env.STAGE + '_' + CONSTANTS.RPM + '_' + Items[0].dbIdentifier + '_' + CONSTANTS.BP_READINGS,
			NON_BP_READINGS: process.env.STAGE + '_' + CONSTANTS.RPM + '_' + Items[0].dbIdentifier + '_' + CONSTANTS.NON_BP_READINGS,
			DOCTOR_CONSULTATION: process.env.STAGE + '_' + CONSTANTS.RPM + '_' + Items[0].dbIdentifier + '_' + CONSTANTS.DOCTOR_CONSULTATION
		};
	}
};

const populatePatients = async (patientDetails, doctorPatientTable) => {
	const params = {
		TableName: doctorPatientTable,
		Item: patientDetails
	};
	return await dbClient.put(params).promise();
};

const populateBpReadings = async (bpDetails, bpTable) => {
	const params = {
		TableName: bpTable,
		Item: bpDetails
	};
	return await dbClient.put(params).promise();
};

const populateNonBpReadings = async (nonBpDetails, nonBpTable) => {
	const params = {
		TableName: nonBpTable,
		Item: nonBpDetails
	};
	return await dbClient.put(params).promise();
};

const populateDoctorConsultation = async (consultationDetails, consultationTable) => {
	const params = {
		TableName: consultationTable,
		Item: consultationDetails
	};
	return dbClient.put(params).promise();
};

module.exports.verifyHospital = verifyHospital;
module.exports.populatePatients = populatePatients;
module.exports.populateBpReadings = populateBpReadings;
module.exports.populateNonBpReadings = populateNonBpReadings;
module.exports.populateDoctorConsultation = populateDoctorConsultation;
