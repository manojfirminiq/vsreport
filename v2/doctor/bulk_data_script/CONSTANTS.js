exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	HOSPITAL_ID_REQ: {
		CODE: 'HOSPITAL_ID_REQ',
		MESSAGE: 'Hospital id required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	},
	DOCTOR_ID_REQ: {
		CODE: 'DOCTOR_ID_REQ',
		MESSAGE: 'Hospital Id is required'
	},
	USER_COUNT: {
		CODE: 'USER_COUNT',
		MESSAGE: 'User count is required'
	},
	USER_BP_COUNT: {
		CODE: 'USER_BP_COUNT',
		MESSAGE: 'User Bp count is required'
	},
	USER_NON_BP_COUNT: {
		CODE: 'USER_NON_BP_COUNT',
		MESSAGE: 'User Non Bp count is required'
	}
};

exports.SUCCESS = {
	MESSAGE: 'Mock data created successfully'
};

exports.HOSPITAL_TABLE = process.env.HOSPITAL_TABLE;
exports.DOCTOR_PATIENT = process.env.DOCTOR_PATIENT;
exports.BP_READINGS = process.env.BP_READINGS;
exports.NON_BP_READINGS = process.env.NON_BP_READINGS;
exports.DOCTOR_CONSULTATION = process.env.DOCTOR_CONSULTATION;
exports.RPM = 'rpm';
exports.HODES = 'hodes';
exports.DOCTOR_CONSULTATION_COUNT = 5; // hardcoded now
exports.BPUNIT = 'mmHg';
exports.WEIGHT_UNIT = 'lbs';
exports.WEIGHT = 'weight';
exports.WEIGHT_DEVICE_MODEL = ['BCM-500', 'BCM-501', 'BCM-510', 'BCM-552'];
exports.BP_DEVICE_MODEL = ['BP7250rt', 'BP8250rt', 'BP6251rt', 'BP9550rt'];
exports.WEIGHT_THRESHOLD_MOCK = ['1', '2', '3', '4', '4.5', '2.6'];
exports.PULSE_UNIT = 'bpm';
exports.SECONDS = 'seconds';
exports.GENDER = ['Female', 'Male'];
exports.THRESHOLD_TYPE_24HR = '24hr';
exports.THRESHOLD_TYPE_72HR = '72hr';
