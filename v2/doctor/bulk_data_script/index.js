const AWS = require('aws-sdk');
const faker = require('faker');
AWS.config.update({ region: process.env.REGION });
const CONSTANTS = require('./CONSTANTS');
const DB = require('./db');

const applyValidation = async ({ doctorID, hospitalID, userCount, userBpCount, userNonBpCount }) => {
	try {
		if (!doctorID) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.DOCTOR_ID_REQ.MESSAGE, errorCode: CONSTANTS.ERRORS.DOCTOR_ID_REQ.CODE });
		if (!hospitalID) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.MESSAGE, errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_REQ.CODE });
		if (!userCount) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.USER_COUNT.MESSAGE, errorCode: CONSTANTS.ERRORS.USER_COUNT.CODE });
		if (!userBpCount) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.USER_BP_COUNT.MESSAGE, errorCode: CONSTANTS.ERRORS.USER_BP_COUNT.CODE });
		if (!userNonBpCount) return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.USER_NON_BP_COUNT.MESSAGE, errorCode: CONSTANTS.ERRORS.USER_NON_BP_COUNT.CODE });

		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({ success: false, message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE, errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE });
		}

		await createMock(doctorID, userCount, userBpCount, userNonBpCount, hospitalTable);
		return Promise.resolve({ success: true, message: CONSTANTS.SUCCESS.MESSAGE });
	} catch (err) {
		console.log('error', err);
		return Promise.resolve({ success: false, errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE, message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE });
	}
};

const createMock = async (doctorID, userCount, userBpCount, userNonBpCount, hospitalTable) => {
	const consultCount = CONSTANTS.DOCTOR_CONSULTATION_COUNT;
	try {
		for (let i = 0; i < userCount; i++) {
			const patientMockData = createPatientMock(doctorID, hospitalTable.HOSPITAL_IDENTIFIER);
			await DB.populatePatients(patientMockData, hospitalTable.DOCTOR_PATIENT);
			await createBpMockData(patientMockData, userBpCount, hospitalTable.BP_READINGS);
			await createNonBpMockData(patientMockData, userNonBpCount, hospitalTable.NON_BP_READINGS);
			await createDoctorConsultationMockData(patientMockData, consultCount, hospitalTable.DOCTOR_CONSULTATION);
		}
	}	catch (error) {
		console.log('Error in creating mock data', error);
		throw error;
	}
};

const createPatientMock = (doctorID, hospitalIdentifier) => {
	const randomDOB = faker.date.past();
	const dob = randomDOB.getFullYear() + '' + (randomDOB.getMonth() + 1) + '' + randomDOB.getDate();
	const userId = faker.random.uuid().toString();
	const item = {
		attributes: {
			dob: dob,
			gender: faker.random.arrayElement(CONSTANTS.GENDER),
			irregularHeartBeat: faker.random.number(400),
			lastReportedDate: faker.date.past().getTime(),
			name: faker.name.findName(),
			profilePicture: `website-image/${hospitalIdentifier}/patient/${userId}`,
			icdCode: faker.random.arrayElement([[{
				code: '12345',
				description: 'test desc',
				name: 'ABC DEQ'
			}]]),
			threshold: {
				bpAverage: {
					'10readings': faker.random.arrayElement([0, 1]),
					'7day': faker.random.arrayElement([0, 1])
				},
				diaHigh: faker.random.number({ min: 10, max: 250 }),
				diaLow: faker.random.number({ min: 10, max: 250 }),
				sysHigh: faker.random.number({ min: 10, max: 250 }),
				sysLow: faker.random.number({ min: 10, max: 250 }),
				weight24hr: faker.random.number({ min: 0, max: 15 }),
				weight72hr: faker.random.number({ min: 0, max: 15 })
			},
			thresholdBpUnit: CONSTANTS.BPUNIT,
			thresholdWeightUnit: CONSTANTS.WEIGHT_UNIT,
			timeZone: faker.address.timeZone(),
			weight: faker.random.number({ min: 40, max: 110 })
		},
		createdDate: faker.date.recent().getTime(),
		doctorID: doctorID.toString(),
		hubID: faker.random.number({ min: 1495306111833, max: 1595306111833 }).toString(),
		modifiedDate: faker.date.recent().getTime(),
		patientCode: faker.random.number({ min: 10000, max: 50000 }),
		userID: userId
	};
	return item;
};

const createBpMockData = async (patient, bpCount, bpReadingsTable) => {
	try {
		for (let j = 0; j < bpCount; j++) {
			const item =
			{
				app: CONSTANTS.RPM.toUpperCase(),
				astuteRawData: {}, // ignoring now
				attributes: {
					atrialFibrillationDetection: faker.random.arrayElement([0, 1]),
					consecutiveMeasurement: faker.random.arrayElement([0, 1]),
					countArtifactDetection: faker.random.arrayElement([0, 1]),
					cuffFlag: faker.random.arrayElement([0, 1]),
					cuffWrapDetect: faker.random.arrayElement([0, 1]),
					dateEnabled: null,
					deleteFlag: faker.random.arrayElement([0, 1]),
					deviceModel: faker.random.arrayElement(CONSTANTS.BP_DEVICE_MODEL),
					deviceSerialID: null,
					diastolic: faker.random.number({ min: 10, max: 250 }),
					diastolicUnit: CONSTANTS.BPUNIT,
					errNumber: faker.random.arrayElement([0, 1]),
					errorDetails: 0,
					internalDeviceTemp: null,
					irregularHB: faker.random.arrayElement([0, 1]),
					irregularPulseDetection: faker.random.arrayElement([0, 1]),
					isDoctorLinked: faker.random.arrayElement([0, 1]),
					isManualEntry: faker.random.arrayElement([0, 1]),
					meanArterialPressure: faker.random.number({ min: 10, max: 250 }),
					measurementLocalDate: faker.date.recent().getTime(),
					measurementStartingMethod: null,
					mets: null,
					movementDetect: faker.random.arrayElement([0, 1]),
					movementError: faker.random.arrayElement([0, 1]),
					positionDetect: faker.random.arrayElement([0, 1]),
					positioningIndicator: faker.random.arrayElement([0, 1]),
					pulse: faker.random.number({ min: 10, max: 250 }),
					pulseUnit: CONSTANTS.PULSE_UNIT,
					roomTemperature: faker.random.arrayElement([0, 1]),
					systolic: faker.random.number({ min: 30, max: 220 }),
					systolicUnit: CONSTANTS.BPUNIT,
					timeZone: 0,
					timeZoneDevice: null,
					transferDate: faker.date.past().getTime(),
					userNumberInDevice: faker.random.arrayElement([0, 1])
				},
				createdDate: faker.date.recent().getTime(),
				measurementDate: faker.date.recent().getTime(),
				modifiedDate: faker.date.past().getTime(),
				userID: bpReadingsTable.includes(CONSTANTS.HODES) ? patient.hubID : patient.userID // here check for hodes or stats
			};

			await DB.populateBpReadings(item, bpReadingsTable);
		}
	} catch (error) {
		console.log('Error in populating Bp Readings Mock', error);
		throw error;
	}
};

const createNonBpMockData = async (patient, nonBpCount, nonBpReadingsTable) => {
	try {
		for (let k = 0; k < nonBpCount; k++) {
			const item = {
				astuteRawData: {}, // ignoring now
				attributes: {
					app: CONSTANTS.RPM.toUpperCase(),
					bmiValue: null,
					bodyFatPercentage: null,
					deleteFlag: faker.random.arrayElement([0, 1]),
					deviceModel: faker.random.arrayElement(CONSTANTS.WEIGHT_DEVICE_MODEL),
					deviceSerialID: faker.random.number({ min: 190801706, max: 200801706 }),
					deviceType: null,
					isDoctorLinked: faker.random.arrayElement([0, 1]),
					isManualEntry: faker.random.arrayElement([0, 1]),
					latitude: null,
					longitude: null,
					measurementLocalDate: faker.date.recent().getTime(),
					restingMetabolism: null,
					skeletalMusclePercentage: null,
					timeZone: 0,
					type: CONSTANTS.WEIGHT,
					visceralFatLevel: null,
					weight: faker.random.number({ min: 40, max: 250, precision: 0.01 }),
					weightLBS: faker.random.number({ min: 80, max: 550, precision: 0.01 }),
					zipCode: faker.address.zipCode()
				},
				createdDate: faker.date.recent().getTime(),
				measurementDate: faker.date.recent().getTime(),
				modifiedDate: faker.date.past().getTime(),
				userId_type_deviceLocalName: nonBpReadingsTable.includes(CONSTANTS.HODES) ? `${patient.hubID}_weight_Manual` : `${patient.userID}_weight_Manual`
			};

			await DB.populateNonBpReadings(item, nonBpReadingsTable);
		}
	} catch (error) {
		console.log('Error in populating NonBp Readings Mock', error);
		throw error;
	}
};

const createDoctorConsultationMockData = async (patient, consultCount, doctorConsultationTable) => {
	try {
		for (let p = 0; p < consultCount; p++) {
			const startTime = faker.date.past().getTime();
			const item = {
				consultationTime: faker.random.number({ min: -4000, max: 5000 }),
				consultationUnit: CONSTANTS.SECONDS,
				createdDate: new Date().getTime(),
				doctorID: patient.doctorID,
				isdeleted: faker.random.boolean(),
				isManual: faker.random.boolean(),
				modifiedDate: faker.date.recent().getTime(),
				notes: faker.random.word(),
				startTime: startTime,
				userID: patient.userID,
				userID_startTime: `${patient.userID}_${startTime}`
			};

			await DB.populateDoctorConsultation(item, doctorConsultationTable);
		}
	} catch (error) {
		console.log('Error in populating DoctorConsultationMockData', error);
		throw error;
	}
};
exports.handler = async (event, context, callback) => {
	console.log('event', event);
	const response = await applyValidation(event);
	callback(null, response);
};
