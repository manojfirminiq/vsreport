const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});
const CONSTANTS = require('./CONSTANTS.js');
const DB = require('./dynamodb');
const trackPatient = require('track_patients_record');
const AmazonCognitoIdentity = new AWS.CognitoIdentityServiceProvider();

const applyValidation = async ({
	doctorId,
	hospitalID,
	updateUserData,
	memberID
}) => {
	try {
		if (!memberID) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.STAFF_MEMBER_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.STAFF_MEMBER_ID_REQ.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}

		const Items = await DB.getStaffMember(doctorId, memberID, hospitalTable);
		if (Items) {
			Items[0].deleteFlag = CONSTANTS.DELETEFLAG;
			Items[0].updates = Items[0].updates || [];
			Items[0].updates.push(updateUserData);
			await DB.deleteStaffMember(Items[0], hospitalTable);
			const params = {
				UserPoolId: CONSTANTS.USER_POOL_ID,
				Username: Items[0].attributes.emailAddress
			};
			AmazonCognitoIdentity.adminDeleteUser(params, function (err, data) {
				if (err) console.log(err, err.stack);
				else console.log(data);
			});
			return {
				success: true,
				message: CONSTANTS.SUCCESS_MSG
			};
		} else {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.STAFF_ID_NOT_FOUND.MESSAGE,
				errorCode: CONSTANTS.ERRORS.STAFF_ID_NOT_FOUND.CODE
			});
		}
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			message: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.MESSAGE,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
