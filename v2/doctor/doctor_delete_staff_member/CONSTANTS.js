exports.DOCTOR_STAFF_TABLE = process.env.DOCTOR_STAFF_TABLE;
exports.HOSPITAL_MASTER_TABLE = process.env.HOSPITAL_MASTER_TABLE;
exports.RPM = '_rpm_';
exports.DELETEFLAG = 1;
exports.SUCCESS_MSG = 'Staff member deleted successfully';

exports.ERRORS = {
	INTERNAL_SERVER_ERROR: {
		CODE: 'INTERNAL_SERVER_ERROR',
		MESSAGE: 'Internal Server Error'
	},
	STAFF_ID_NOT_FOUND: {
		CODE: 'STAFF_ID_NOT_FOUND',
		MESSAGE: 'staffID not found'
	},
	STAFF_MEMBER_ID_REQ: {
		CODE: 'STAFF_MEMBER_ID_REQ',
		MESSAGE: 'Staff memeber ID is required'
	},
	HOSPITAL_ID_INVALID: {
		CODE: 'HOSPITAL_ID_INVALID',
		MESSAGE: 'Hospital Id is invalid'
	}
};
exports.USER_POOL_ID = process.env.USER_POOL_ID;
