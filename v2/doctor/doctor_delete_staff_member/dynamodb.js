const AWS = require('aws-sdk');
AWS.config.update({
	region: process.env.REGION
});
const dbClient = new AWS.DynamoDB.DocumentClient();
const CONSTANTS = require('./CONSTANTS.js');
const STAFF_TABLE = CONSTANTS.DOCTOR_STAFF_TABLE;
const HOSPITAL_MASTER_TABLE = CONSTANTS.HOSPITAL_MASTER_TABLE;
const RPM = CONSTANTS.RPM;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_MASTER_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Items } = await dbClient.query(paramsTable).promise();
	if (Items.length === 0) {
		return false;
	} else {
		const obj = {
			Items,
			STAFF_TABLE:
				process.env.STAGE + RPM + Items[0].dbIdentifier + '_' + STAFF_TABLE
		};
		return obj;
	}
};

const getStaffMember = async (doctorID, memberID, hospitalTable) => {
	const params = {
		TableName: hospitalTable.STAFF_TABLE,
		KeyConditionExpression:
			'#staffID = :staffIDVal AND #doctorID = :doctorIDVal',
		ExpressionAttributeValues: {
			':staffIDVal': memberID,
			':doctorIDVal': doctorID
		},
		ExpressionAttributeNames: {
			'#staffID': 'staffID',
			'#doctorID': 'doctorID'
		}
	};
	const { Items } = await dbClient.query(params).promise();
	if (Items.length === 0) {
		return false;
	} else {
		return Items;
	}
};

const deleteStaffMember = async (items, hospitalTable) => {
	items.modifiedDate = new Date().getTime();
	const params = {
		TableName: hospitalTable.STAFF_TABLE,
		Item: items
	};
	return await dbClient.put(params).promise();
};

exports.getStaffMember = getStaffMember;
exports.deleteStaffMember = deleteStaffMember;
exports.verifyHospital = verifyHospital;
