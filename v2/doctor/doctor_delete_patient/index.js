const CONSTANTS = require('./CONSTANTS');
const DB = require('./dynamodb');
const trackPatient = require('track_patients_record');

const applyValidation = async ({
	doctorId,
	userId,
	hospitalID,
	updateUserData
}) => {
	try {
		if (!userId) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.USER_ID_REQ.MESSAGE,
				errorCode: CONSTANTS.ERRORS.USER_ID_REQ.CODE
			});
		}
		const hospitalTable = await DB.verifyHospital(hospitalID);
		if (!hospitalTable) {
			return Promise.resolve({
				success: false,
				message: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.MESSAGE,
				errorCode: CONSTANTS.ERRORS.HOSPITAL_ID_INVALID.CODE
			});
		}
		const doctorPatient = await DB.getDoctorPatient(
			doctorId,
			userId,
			hospitalTable.DOCTOR_PATIENT
		);
		await DB.deleteDoctorPatient(
			doctorId,
			userId,
			hospitalTable.DOCTOR_PATIENT,
			updateUserData
		);
		if (doctorPatient.Items[0].hubID) {
			await DB.deleteHospitalDeviceMaster(
				doctorPatient.Items[0].hubID,
				hospitalTable.HOSPITAL_DEVICE_MASTER
			);
		}
		return Promise.resolve({
			success: true
		});
	} catch (error) {
		console.log(error);
		return Promise.resolve({
			success: false,
			errorCode: CONSTANTS.ERRORS.INTERNAL_SERVER_ERROR.CODE,
			message: error
		});
	}
};

exports.handler = async (event, context, callback) => {
	const trackUser = trackPatient.verifyUserType(event);
	if (trackUser.success) {
		event.updateUserData = trackUser.data;
	} else {
		callback(null, trackUser);
	}
	const response = await applyValidation(event);
	callback(null, response);
};
