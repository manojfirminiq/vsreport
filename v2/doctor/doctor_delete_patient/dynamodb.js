const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const dbClient = new AWS.DynamoDB.DocumentClient();

const CONSTANTS = require('./CONSTANTS.js');
const HOSPITAL_TABLE = CONSTANTS.HOSPITAL_TABLE;

const verifyHospital = async hospitalId => {
	const paramsTable = {
		TableName: HOSPITAL_TABLE,
		KeyConditionExpression: '#hospital = :id',
		ExpressionAttributeNames: {
			'#hospital': 'hospitalID'
		},
		ExpressionAttributeValues: {
			':id': hospitalId
		}
	};
	const { Count, Items } = await dbClient.query(paramsTable).promise();
	if (Count <= 0) {
		return false;
	} else {
		const obj = {
			DOCTOR_PATIENT:
				process.env.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				Items[0].dbIdentifier +
				'_' +
				CONSTANTS.DOCTOR_PATIENT,
			HOSPITAL_DEVICE_MASTER:
				process.env.STAGE +
				'_' +
				CONSTANTS.RPM +
				'_' +
				CONSTANTS.HOSPITAL_DEVICE_MASTER
		};
		return obj;
	}
};

const deleteDoctorPatient = async (
	doctorId,
	userId,
	doctorPatientTable,
	updateUserData
) => {
	const params = {
		TableName: doctorPatientTable,
		Key: {
			doctorID: doctorId,
			userID: userId
		},
		UpdateExpression:
			'SET deleteFlag = :deleteFlag, deletedAt = :deletedAt, deletedBy = :deletedBy, #updates = list_append(if_not_exists(#updates, :empty_list), :updateUserData)',
		ExpressionAttributeValues: {
			':deleteFlag': CONSTANTS.DELETE_FLAG,
			':deletedAt': new Date().getTime(),
			':deletedBy': doctorId,
			':updateUserData': [updateUserData],
      		':empty_list': []
		},
		ExpressionAttributeNames: {
			'#updates': 'updates'
		},
		ReturnValues: 'UPDATED_NEW'
	};
	return dbClient.update(params).promise();
};

const deleteHospitalDeviceMaster = async (hubID, deviceMasterTable) => {
	const params = {
		TableName: deviceMasterTable,
		Key: {
			hubID: hubID
		}
	};

	return dbClient.delete(params).promise();
};

const getDoctorPatient = async (doctorId, userID, doctorPatientTable) => {
	const params = {
		TableName: doctorPatientTable,
		KeyConditionExpression: '#uid = :id AND #userID = :userID',
		ExpressionAttributeValues: {
			':id': doctorId,
			':userID': userID
		},
		ExpressionAttributeNames: {
			'#uid': 'doctorID',
			'#userID': 'userID'
		}
	};

	return dbClient.query(params).promise();
};

module.exports.verifyHospital = verifyHospital;
module.exports.deleteDoctorPatient = deleteDoctorPatient;
module.exports.deleteHospitalDeviceMaster = deleteHospitalDeviceMaster;
module.exports.getDoctorPatient = getDoctorPatient;
