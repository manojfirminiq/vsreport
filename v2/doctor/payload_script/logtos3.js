const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.REGION });
const s3Client = new AWS.S3();

exports.uploadDataToS3 = async responseData => {
	const keyName = Date.now();
	const params = {
		Bucket: process.env.S3_ASTUTE,
		Key: process.env.S3_ASTUTE_FAIL_LOG + keyName + '.txt',
		Body: JSON.stringify(responseData),
		ContentType: 'text/plain'
	};

	const response = null;
	// eslint-disable-next-line no-useless-catch
	try {
		await s3Client.upload(params).promise();
	} catch (error) {
		throw error;
	}

	return Promise.resolve(response);
};
