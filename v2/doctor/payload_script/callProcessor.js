const AWS = require('aws-sdk');
const lambda = new AWS.Lambda({ region: process.env.REGION });
const { RPM_EHR_RESULT_PROCESSOR } = require('./CONSTANTS');

module.exports = params => {
	return new Promise((resolve, reject) => {
		lambda.invoke(
			{
				FunctionName: RPM_EHR_RESULT_PROCESSOR,
				Payload: JSON.stringify(params)
			},
			(error, result) => {
				if (error) {
					resolve({ succes: false });
				}
				result = JSON.parse(result.Payload);
				resolve({ result });
			}
		);
	});
};
