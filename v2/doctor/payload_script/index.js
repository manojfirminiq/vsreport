const SAVE_DB = require('./save_db');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const XLSX = require('xlsx');
const CONSTANTS = require('./CONSTANTS.js');

const formatData = data => {
	const items = [];
	const bpData = CONSTANTS.BPPayload;
	const weightData = CONSTANTS.weightPayload;
	for (let i = 1; i < data.length; i++) {
		if (
			data[i].Systolic >= CONSTANTS.RANGE.SYS_LOW &&
			data[i].Systolic <= CONSTANTS.RANGE.SYS_HIGH &&
			data[i].Diastolic >= CONSTANTS.RANGE.DIA_LOW &&
			data[i].Diastolic <= CONSTANTS.RANGE.DIA_HIGH
		) {
			const bp = JSON.parse(JSON.stringify(bpData));
			bp.HubId = data[i].HubId.toString();
			bp.Reading.HeartRate = data[i].HeartRate;
			bp.Reading.Systolic = data[i].Systolic;
			bp.Reading.Diastolic = data[i].Diastolic;
			bp.Reading.ArterialPressure = data[i].ArterialPressure;
			bp.Reading.IrregularHB = data[i].IrregularHB || 0;
			bp.Reading.MovementError = data[i].MovementError || 0;
			bp.Reading.ReadingTimestamp = data[i].MeasurementDate;
			bp.Reading.DeviceReportedReadingTimestamp = data[i].MeasurementDate;
			bp.Reading.ReadingTimestampUtc = data[i].MeasurementDate;
			bp.GatewayTimestamp = data[i].MeasurementDate;
			bp.ServerTimestamp = data[i].MeasurementDate;
			items.push(bp);
		} else {
			if (
				!(
					data[i].Systolic >= CONSTANTS.RANGE.SYS_LOW &&
					data[i].Systolic <= CONSTANTS.RANGE.SYS_HIGH
				)
			) {
				console.log('message', CONSTANTS.ERRORS.SYS_INVALID_RANGE.MESSAGE);
			} else if (
				!(
					data[i].Diastolic >= CONSTANTS.RANGE.DIA_LOW &&
					data[i].Diastolic <= CONSTANTS.RANGE.DIA_HIGH
				)
			) {
				console.log('message', CONSTANTS.ERRORS.DIA_INVALID_RANGE.MESSAGE);
			}
		}
		if (
			data[i].BodyWeightLb >= CONSTANTS.RANGE.WEIGHT_LOWER_LIMIT &&
			data[i].BodyWeightLb <= CONSTANTS.RANGE.WEIGHT_UPPER_LIMIT
		) {
			const weight = JSON.parse(JSON.stringify(weightData));
			weight.HubId = data[i].HubId;
			weight.GatewayTimestamp = data[i].MeasurementDate;
			weight.ServerTimestamp = data[i].MeasurementDate;
			weight.Reading.BodyWeightLb = Number(data[i].BodyWeightLb).toFixed(1);
			weight.Reading.BodyWeightKg = data[i].BodyWeightKg;
			weight.Reading.ReadingTimestamp = data[i].MeasurementDate;
			weight.Reading.DeviceReportedReadingTimestamp = data[i].MeasurementDate;
			weight.Reading.ReadingTimestampUtc = data[i].MeasurementDate;
			items.push(weight);
		} else {
			console.log('message', CONSTANTS.ERRORS.WEIGHT_INVALID_RANGE.MESSAGE);
		}
	}
	return items;
};

const readExcel = async function (record) {
	try {
		if (
			record &&
			record.s3 &&
			record.s3.object &&
			record.s3.bucket.name &&
			record.s3.object.key
		) {
			const { Body: fileData } = await s3
				.getObject({ Bucket: record.s3.bucket.name, Key: record.s3.object.key })
				.promise();
			const workbook = XLSX.read(fileData, { type: 'buffer' });
			const sheetNames = workbook.SheetNames;
			let data = [];
			sheetNames.forEach(sheetName => {
				const sheetData = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {
					header: [
						'HubId',
						'HeartRate',
						'Systolic',
						'Diastolic',
						'ArterialPressure',
						'MeasurementDate',
						'BodyWeightLb',
						'BodyWeightKg',
						'IrregularHB',
						'MovementError'
					]
				});
				data = [...data, ...sheetData];
			});
			return data;
		}
	} catch (err) {
		console.log(err);
	}
};

exports.handler = async (event, context, callback) => {
	try {
		const promiseArr = [];
		if (event && event.Records) {
			event.Records.forEach(record => {
				promiseArr.push(readExcel(record));
			});
		}

		let data = await Promise.all(promiseArr);
		data = data.flat();
		data = formatData(data);
		let response = {
			success: true,
			message: 'No new readings available'
		};
		response = await SAVE_DB.saveReading(data);
		callback(null, response);
	} catch (error) {
		console.log(error);
	}
};
