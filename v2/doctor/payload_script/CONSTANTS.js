exports.HOSPITAL_DEVICE_MASTER_TABLE = process.env.HOSPITAL_DEVICE_MASTER_TABLE;
exports.RPM_HOSPITAL_MASTER = process.env.RPM_HOSPITAL_MASTER;
exports.APP_TYPE = 'RPM';
exports.DEVICE_TYPE = 'HUB';
exports.BPPayload = {
	GatewayTimestamp: '2020-04-05T19:51:23Z',
	id: 'b24d572d-e48f-4493-b63d-169df617598a',
	_rid: 'YshqALDNj7RYAAAAAAAAAA==',
	_self: 'dbs/YshqAA==/colls/YshqALDNj7Q=/docs/YshqALDNj7RYAAAAAAAAAA==/',
	_ts: '1589399484',
	_etag: '"0200b924-0000-0100-0000-5ebc4fbc0000"',
	GatewayType: 'CTR-01',
	HubId: '1592213048406',
	DeviceType: 'bp',
	MacAddress: 'ec:21:e5:c1:1b:cc',
	ServerTimestamp: '2020-05-13T19:51:24.2827902Z',
	Reading: {
		HeartRate: 115,
		Systolic: 84,
		Diastolic: 65,
		ArterialPressure: 71,
		IrregularHB: 0,
		MovementError: 0,
		UserId: 1,
		HeartRateAvailable: true,
		TimeStampAvailable: true,
		UserIdAvailable: true,
		UnitskPa: false,
		MeasurementStatusFlagsAvailable: true,
		BodyMovementDetected: false,
		CuffTooLoose: false,
		IrregularPulseDetected: false,
		ImproperMeasurementDetected: false,
		PulseRateInRange: true,
		PulseRateExceedsUpperLimit: null,
		PulseRateBelowLowerLimit: null,
		BPUOM: 'mmHg',
		ReadingTimestamp: '2020-04-05T15:52:09',
		DeviceReportedReadingTimestamp: '2020-04-05T15:52:09',
		ReadingTimestampUtc: '2020-04-05T15:52:09',
		ReadingTimestampUtcSource: 'DeviceReportedWithNoAdjustment',
		DeviceType: 'BP'
	},
	ParsingErrors: [
		'Dangerous conversion for ReadingTimestampUtc since no UtcOffset present'
	],
	RawPayload:
		'Astute,v2.0.28,2020-04-05T19:51:23Z,CTR-01,352753091939197,ec21e5c11bcc,bp,OMRONHEALTHCARE,BP7250rt,,0000000000000100,D.00.7FB-12,1e530040004600e407050d0f34097500010000',
	PatientToken: null,
	DeviceManufacturer: 'OMRONHEALTHCARE',
	DeviceModel: 'BP7250rt',
	DeviceSerialNumber: '',
	DeviceHardwareVersion: '0000000000000100',
	DeviceSoftwareVersion: '',
	DeviceFirmwareVersion: 'D.00.7FB-12'
};

exports.weightPayload = {
	GatewayTimestamp: '2018-01-26T15:42:50Z',
	id: 'e5c510ac-5cd8-4663-a991-3f2157d9f16f',
	_rid: 'YshqALDNj7SUAAAAAAAAAA==',
	_self: 'dbs/YshqAA==/colls/YshqALDNj7Q=/docs/YshqALDNj7SUAAAAAAAAAA==/',
	_ts: '1590507772',
	_etag: '"4300b296-0000-0100-0000-5ecd38fc0000"',
	GatewayType: 'CTR-01',
	HubId: '1592323955348',
	DeviceType: 'weightscale',
	MacAddress: 'ec:21:e5:52:bd:d2',
	ServerTimestamp: '2018-01-26T15:42:51.9257593Z',
	Reading: {
		BodyWeightLb: 107,
		BodyWeightKg: 48.53,
		BodyFat: null,
		BMI: null,
		WeightUOM: 'lbs',
		ReadingTimestamp: '2018-01-26T10:42:11',
		DeviceReportedReadingTimestamp: '2018-01-26T10:42:11',
		ReadingTimestampUtc: '2018-01-26T10:42:11',
		ReadingTimestampUtcSource: 'DeviceReportedWithNoAdjustment',
		DeviceType: 'WEIGHTSCALE'
	},
	ParsingErrors: [
		'Dangerous conversion for ReadingTimestampUtc since no UtcOffset present'
	],
	RawPayload:
		'Astute,v2.0.30,2018-01-26T15:42:50Z,CTR-01,352753091939197,ec21e552bdd2,weightscale,OMRONHEALTHCARE,BCM-500,190801706F_Z,0000000000000100,D.00.7FD-09,0fcc29e407051a0a2a0bffa7009e02',
	PatientToken: null,
	DeviceManufacturer: 'OMRONHEALTHCARE',
	DeviceModel: 'BCM-500',
	DeviceSerialNumber: '190801706F_Z',
	DeviceHardwareVersion: '0000000000000100',
	DeviceSoftwareVersion: '',
	DeviceFirmwareVersion: 'D.00.7FD-09'
};

exports.RANGE = {
	DIA_LOW: 40,
	DIA_HIGH: 160,
	SYS_LOW: 60,
	SYS_HIGH: 230,
	WEIGHT_LOWER_LIMIT: 22,
	WEIGHT_UPPER_LIMIT: 300
};

exports.ERRORS = {
	DIA_INVALID_RANGE: {
		MESSAGE: `dia should be range between ${this.RANGE.DIA_LOW} and ${this.RANGE.DIA_HIGH}`
	},
	SYS_INVALID_RANGE: {
		MESSAGE: `sys should be range between ${this.RANGE.SYS_LOW} and ${this.RANGE.SYS_HIGH}`
	},
	WEIGHT_INVALID_RANGE: {
		MESSAGE: `weight should be range between ${this.RANGE.WEIGHT_LOWER_LIMIT} and ${this.RANGE.WEIGHT_UPPR_LIMIT}`
	}
};

exports.HODES = 'hodes';
exports.RPM_EHR_RESULT_PROCESSOR = process.env.RPM_EHR_RESULT_PROCESSOR;
exports.WEIGHT = 'weight';
exports.BPM = 'bpm';
